$(document).on('click', '.ver-email', function() {
    if ($(".show_mail_inp").text() != "Cancel") {
        $.ajax({
            type: 'post',
            url: site_url + "user/verify_mail",
            dataType: 'json',
            data: {
                'type': 'send'
            },
            beforeSend: function() {
                // $('.ver-email').attr('disabled', 'disabled');
                $('.ver-email').bind('click', false);
                $(".ver-email").html('<i class="fa fa-spinner fa-spin"></i> Sending');
            },
            success: function(resp) {
                message(resp);
                $('.success_msgBox').delay(2000).fadeOut();
                $('#verif-mail').modal('hide');
                $('.close').trigger('click');
                $('.ver-email').unbind('click', false);
                $(".ver-email").text('Resend Email');
            }
        });
    } else {
        $('.mail-form').parsley().validate();
        // console.log($('.mail-form').parsley().validate());

        if ($('.mail-form').parsley().isValid() != false) {
            email = $("#mail_inp").val();
            $.ajax({
                url: site_url + "user/change_mail",
                method: "POST",
                dataType: "json",
                data: {
                    'new_mail': email,
                },
                beforeSend: function() {
                    $('.ver-email').bind('click', false);
                    $(".ver-email").html('<i class="fa fa-spinner fa-spin"></i> Sending');
                },
                complete: function() {
                    $('.ver-email').unbind('click', false);
                    // $(".ver-email").text('Resend Email');

                },
                success: function(data) {
                    if (data.status == "success") {
                        $('#verif-mail').modal('hide');
                        $("#ver_mail_span").removeClass("d-none");
                        $("#mail_inp").addClass("d-none");
                        $(".ver-email").text('Resend Email');
                        $(".show_mail_inp").text('Change email');
                        $("#ver_mail_span").text("(" + email + ") to activate all the features.");
                        message(data);
                    } else {
                        $(".ver-email").text('update and Resend');
                        $(".show_mail_inp").text('Cancel');
                        $("#par_error").text(data.msg);
                        $("#par_error").css("color", "red");
                    }
                    // message(data);
                }
            });
        }
    }
});
$(document).on('click', '.close_modal', function() {
    alert('asgsagsa');
    // $(".modal-activatem").modal('hide');
});
$(".show_mail_msg").click(function() {
    messageBox1();
});

function messageBox1() {
    $('.msgBox_fail').show();
    $('.msgBox_fail').css('display', 'block');
    $(".fail_msg").html("Please verify your email first");
    $('.msgBox_fail').delay(2000).fadeOut('slow');
}

function message(data) {
    if (data.status == "success") {
        $('.success_msgBox').show();
        $('.success_msgBox').css('display', 'block');
        $(".success_msg").html(data.msg);
        $('.success_msgBox').delay(2000).fadeOut();
        setTimeout(
            function() {
                $('.success_msgBox').css('display', 'none');
            }, 2000);

    } else {
        $('.msgBox_fail').show();
        $('.msgBox_fail').css('display', 'block');
        $('.fail_msg').html(data.msg);
        $('.msgBox_fail').delay(2000).fadeOut();
        setTimeout(
            function() {
                $('.msgBox_fail').css('display', 'none');
            }, 2000);

    }

}
$(".show_mail_inp").click(function() {
    console.log($(".show_mail_inp").text());

    if ($(".show_mail_inp").text() != "Cancel") {
        $("#ver_mail_span").addClass("d-none");
        $("#mail_inp").removeClass("d-none");
        $("#mail_inp").focus();
        $(".ver-email").text('update and Resend');
        $(".show_mail_inp").text('Cancel');
    } else {
        $("#ver_mail_span").removeClass("d-none");
        $("#mail_inp").addClass("d-none");
        $(".ver-email").text('Resend Email');
        $(".show_mail_inp").text('Change email');
        $("#par_error").remove();
    }

})