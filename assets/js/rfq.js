$(document).ready(function() {
    cate = $("#sell1").val();
    if (cate) {
        site_url = $("#site_url").val();
        if (cate == '') {
            $(".sub_cate").html('');
            exit;
        }
        $.ajax({
            url: site_url + "category/getSubCategories/" + cate,
            method: "GET",
            dataType: "json",
            success: function(e) {
                $(".sub_cate").html(e.data)
            }
        })
    }









    (e = new Date).setDate(e.getDate() + 7);
    var e, t = ("0" + e.getDate()).slice(-2),
        a = ("0" + (e.getMonth() + 1)).slice(-2),
        n = e.getFullYear() + "-" + a + "-" + t;
    $("#date").val(n), (e = new Date).setDate(e.getDate() + 90);
    t = ("0" + e.getDate()).slice(-2), a = ("0" + (e.getMonth() + 1)).slice(-2), n = e.getFullYear() + "-" + a + "-" + t;
    $("#date").attr("max", n), window.scrollTo(0, 0);
    // $(document).on('keydown.commonAutoComplete', '.commonAutoComplete', function () {
    //      aa = $("#sel2").val() ? $("#sel2").val() : $("#sel1").val();
    //      $(this).autocomplete({
    //           dataType: 'json',
    //           serviceUrl: $(this).attr('data-url') + '/' + aa,
    //           onSelect: function (suggestion) {
    //                $("#prd_id").val(suggestion.data);
    //                // $("#moq_span").text("*MOQ for this product is " + suggestion.moq);
    //                // $('.inp_qty').attr("min", suggestion.moq);
    //                $("#unit_sel").val(suggestion.unit);

    //           }
    //      });
    // });
}), $(document).on("change", "#validity", function() {
    var e = new Date,
        t = parseInt($(this).val());
    e.setDate(e.getDate() + t);
    var a = ("0" + e.getDate()).slice(-2),
        n = ("0" + (e.getMonth() + 1)).slice(-2),
        l = e.getFullYear() + "-" + n + "-" + a;
    $("#date").val(l)
}), $("#attach_file").change(function() {
    var _this = $(this);
    if (_this[0].files[0].type.split('/')[0] === 'image') { // If image upload
        if (_this[0].files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.imgPreview').attr('src', e.target.result);
            };
            reader.readAsDataURL(_this[0].files[0]);
        }
    } else {
        $('.imgPreview').attr('src', 'images/document.jpg');
    }
    $('.btnRemoveImage').show();
    for (var e = $("#attach_file")[0].files[0].name, t = $("#attach_file")[0].files[0].size, a = new Array("Bytes", "KB", "MB", "GB"), n = 0; t > 900;)
        t /= 1024, n++;
    var l = Math.round(100 * t) / 100 + " " + a[n];
    $("#file_name").text(e + " (" + l + ")")
}), $(document).on('click', '.btnRemoveImage', function() {
    var replaceHtml = '<img src="" alt="" class="imgPreview" style="width: 190px;"/>' +
        '<div type="button" class="close btnRemoveImage" data-dismiss="modal" style="display: none;">X</div>';
    $(this).parent('div').html(replaceHtml);
}), $(document).on("change", "#sell1", function() {
    site_url = $("#site_url").val();
    cate = $(this).val();
    if (cate == '') {
        $(".sub_cate").html('');
        exit;
    }
    $.ajax({
        url: site_url + "category/getSubCategories/" + cate,
        method: "GET",
        dataType: "json",
        success: function(e) {
            $(".sub_cate").html(e.data)
        }
    })
});
$(document).on('click', '.refresh_captcha', function() {
    site_url = $("#site_url").val();
    $.ajax({
        url: site_url + "user/refreshCaptcha",
        method: "GET",
        dataType: "json",
        beforeSend: function() {
            $(".ajax_loader").removeClass("d-none");
        },
        complete: function() {
            $(".ajax_loader").addClass("d-none");
        },
        success: function(data) {
            $('#captcha_code').val('');
            $('#captcha_img').html(data.data.image);
            $('#captcha_code').val(data.data.word);
            $('#captcha').val('');
        }
    });
});