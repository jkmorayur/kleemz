pannellum.viewer('panorama', ﻿ {
    "default": {
        "author": "Fujishka",
        "firstScene": "building",
        "sceneFadeDuration": 2000,
		"autoLoad": true,
		"hfov": 120

    },


    "scenes": {
        "samsung-inner": {
            "title": "Samsung Inner",
            "type": "cubemap",
            "cubeMap": [
                "assets/images/panorama/4.jpg",
                "assets/images/panorama/2.jpg",
                "assets/images/panorama/3.jpg",
                "assets/images/panorama/1.jpg",
                "assets/images/panorama/5.jpg",
                "assets/images/panorama/0.jpg"
            ],
			"autoRotate": -3,
            "hotSpots": [
                {
                    "pitch": -5,
                    "yaw": 173,
                    "type": "info",
                    "text": "This is a new product",
					"URL": "http://google.com"
                },
                {
                    "pitch": 29,
                    "yaw": -10,
                    "type": "info",
                    "text": "This is a link to fujishka.",
                    "URL": "http://fujishka.com"
                },
				{
                    "pitch": 27,
                    "yaw": -25,
                    "type": "info",
                    "text": "This is a link to fujishka2.",
                    "URL": "http://fujishka.com"
                },
                {
                    "pitch": 0,
                    "yaw": -100,
                    "type": "scene",
                    "text": "Back to Front View",
                    "sceneId": "frontview"
                }
            ]
        },

		"shop2": {
            "title": "Shop 2",
            "type": "cubemap",
            "cubeMap": [
                "images/s1.jpg",
                "images/s2.jpg",
                "images/s3.jpg",
                "images/s4.jpg",
                "images/s5.jpg",
                "images/s0.jpg"
            ],
            "hotSpots": [

                {
                    "pitch": 27,
                    "yaw": -92,
                    "type": "info",
                    "text": "This is a link to fujishka.",
                    "URL": "http://fujishka.com"
                },
				{
                    "pitch": 37,
                    "yaw": -92,
                    "type": "info",
                    "text": "This is a link to fujishka2.",
                    "URL": "http://fujishka.com"
                },
                {
                    "pitch": 0,
                    "yaw": -60,
                    "type": "scene",
                    "text": "Back to First View",
                    "sceneId": "building"
                }
            ]
        },

		"frontview": {
            "title": "Samsung Front View",
            "panorama": "assets/images/panorama/testing-pic.jpg",
            "hotSpots": [
                {
                    "pitch": -15,
                    "yaw": 0,
                    "type": "scene",
                    "text": "Enter Samsung outlet",
                    "sceneId": "samsung-inner"
                },

				{
                    "pitch": -15,
                    "yaw": 100,
                    "type": "scene",
                    "text": "Back to building",
                    "sceneId": "building"
                }
            ]
        },

        "building": {
            "title": "Testing Pane 1",
            "panorama": "assets/images/panorama/test-pic.jpg",
			"autoRotate": -5,
            "hotSpots": [

				{
                    "pitch": 0,
                    "yaw": 10,
                    "type": "scene",
                    "text": "Samsung Front View",
                    "sceneId": "frontview"
                }
            ]
        }
    }
});
