$(window).load(function() {
    setTimeout(function() {
        $(".alert").delay(50000).hide();
    }, 5000);
});
$(document).ready(function() {
 

 
    $('#closemodal').click(function() {
        $('#prodModal').modal('hide');
    });
    /*
     * ----------------------------------------------------------------------------------------
     Product search
     * ----------------------------------------------------------------------------------------
     */
    //     $(document).on('keyup', '.txtQuickSearch', function(e) {
    //         search();
    //     });

    $(document).on('click', '.btnProductSearch', function() {

   

        var keyword = $('#productkey').val();
        var key = keyword.trim();
        var url =  site_url+'home/productsearch';
     

        if (key !== '') {
            $.ajax({
                type: 'post',
                url: url,
                dataType: 'json',
                data: {
                    keyword: key,
                    page:1
                },
                success: function(resp) {
                 
                    $('#datadiv').html(resp.data);
                
                 
                }
            });
        }
        $('.datadiv').html('');
    });

    $("#prodModal").keypress(function (e) {
        var keyword = $('#productkey').val();
        var key = keyword.trim();
        var url =  site_url+'home/productsearch';
        if ((e.keyCode == 13)&&key !== '') {
    
                $.ajax({
                    type: 'post',
                    url: url,
                    dataType: 'json',
                    data: {
                        keyword: key,
                        page:1
                    },
                    success: function(resp) {
                     
                        $('#datadiv').html(resp.data);
                    
                     
                    }
                });
            
            $('.datadiv').html('');
        }
    });

    $(document).on("click", ".pagination li a", function(event) {
       
        event.preventDefault();
        ref = $(this).attr('href');
        page = ref.substring(ref.lastIndexOf("/") + 1, ref.length);

        if (page == '')
            page = 1;
            var keyword = $('#productkey').val();
            var key = keyword.trim();
            var url =  site_url+'home/productsearch';
        
    
            if (key !== '') {
                $.ajax({
                    type: 'post',
                    url: url,
                    dataType: 'json',
                    data: {
                        keyword: key,
                        page:page
                    },
                    success: function(resp) {
                     
                        $('#datadiv').html(resp.data);
                        $('#pagination').html(resp.pagination_link);
                     
                    }
                });
            }
        window.scrollTo(0, 0);
    });
    var products = [];
    var prdidS =[];
    $(document).on("click", ".selectThisprd", function(event) {
      
    
    document.getElementById("demo").innerHTML = products;

    var link =  $(this).attr('link');
    var prdid =  $(this).attr('prdId');

    var idx = $.inArray(prdid, prdidS);
if (idx == -1) {
    products.push(link);
    prdidS.push(prdid);
} else {
    prdidS.splice(idx, 1);
    products.splice(idx, 1);
}
   

  
      document.getElementById("demo").innerHTML = products;
      $("#product_link").val(prdidS);

    
});

$(document).on("click", ".add_btn", function() {
    if($(this).html()=='Add'){
        $(this).html('Remove');
        $(this).css('color', 'red');
    }
    else{
        $(this).html('Add');
        $(this).css('color', 'green');
    }
    
});




    $(document).on('keydown.commonAutoComplete', '.commonAutoComplete', function() {
        if ($(this).hasClass('txtQuickSearch')) {
            var sec = $('.btnSearchMaster').html().toLowerCase();
            var url = $('.btnSearchMaster').attr('data-url');
            $(this).autocomplete({
                dataType: 'json',
                serviceUrl: url + '?section=' + sec,
                onSelect: function(suggestion) {
                    if (sec == 'product') {
                        location.href = site_url + 'product/product-listing?SearchText=' + suggestion.value;
                    }
                }
            });
        } else {
            aa = $("#sell2").val() ? $("#sell2").val() : $("#sell1").val();
            $(this).autocomplete({
                dataType: 'json',
                serviceUrl: $(this).attr('data-url') + '/' + aa,
                onSelect: function(suggestion) {
                    $("#prd_id").val(suggestion.data);
                    // $("#moq_span").text("*MOQ for this product is " + suggestion.moq);
                    // $('.inp_qty').attr("min", suggestion.moq);
                    $(".unit_sel").val(suggestion.unit);

                }
            });
        }
    });






    $(document).on('click', 'body', function() {
        $('.divSearchResult').html('');
    });

    /**
     * Subscribe Newsletter
     */
    $(document).on('submit', '.frmNewsletterSub', function(e) {
        e.preventDefault();
        var submitButtom = $("button", this);
        var email = $('.txtNewsSubEmail').val();
        var url = $('.frmNewsletterSub').attr('data-url');

        if (email !== '') {
            $.ajax({
                type: 'post',
                url: url,
                data: {
                    email: email
                },
                dataType: 'json',
                beforeSend: function(xhr) {
                    $('.newsLtrMsg').html('');
                    submitButtom.html('Please wait...');
                    submitButtom.prop('disabled', true);
                },
                success: function(resp) {
                    submitButtom.html('Subscribe');
                    submitButtom.prop('disabled', false);
                    $('.newsLtrMsg').html(resp.msg);
                    $('.txtNewsSubEmail').val('');
                }
            });
        }
    });
});