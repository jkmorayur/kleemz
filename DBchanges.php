ALTER TABLE `cpnl_products_order_master` ADD `ordm_delivery_boy` INT(11) NOT NULL DEFAULT '0' AFTER `ordm_pg_response`;
ALTER TABLE `cpnl_supplier_master` ADD `supm_countr_code` VARCHAR(11) NOT NULL AFTER `supm_viewed_by`;
INSERT INTO `cpnl_groups` (`id`, `name`, `grp_slug`, `description`, `grp_access`) VALUES (NULL, 'Virtual Shop Partner', 'DB', 'Virtual Shop Partner', NULL);
ALTER TABLE `cpnl_supplier_contacts` ADD `phone_code` INT(11) NOT NULL AFTER `spc_master_id`;
ALTER TABLE `cpnl_category` ADD `virtual_store_stat` VARCHAR(25) NOT NULL DEFAULT 'no' AFTER `cat_added_by`;

CREATE TABLE `cpnl_virtshop_catog_town_assoc` (
  `vsc_id` int(11) NOT NULL,
  `vsc_cat_id` int(11) NOT NULL,
  `vsc_town_id` int(11) NOT NULL,
  `vsc_added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
