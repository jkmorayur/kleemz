<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Inquiry summary</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                   <li role="presentation" class="active">
                                        <a href="javascript:;" goto="#rfq" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                             <i class="fa fa-list-alt"></i> Summary</a>
                                   </li>
                                   <li role="presentation">
                                        <a class="btnComments" href="javascript:;" goto="#messages" role="tab" id="profile-tab" 
                                           data-toggle="tab" aria-expanded="false"><i class="fa fa-list-alt"></i> Invoice</a>
                                   </li>
                              </ul>

                              <div id="myTabContent" class="tab-content">
                                   <div role="tabpanel" class="tab-pane fade active in" id="rfq" aria-labelledby="home-tab">
                                        <section class="content invoice">
                                             <!-- title row -->
                                             <div class="row">
                                                  <div class="col-xs-12 invoice-header">
                                                       <h1>
                                                            #<?php echo $order['ordm_number'];?>
                                                            <small class="pull-right">Date: <?php echo date('j M Y h:m A', strtotime($order['ordm_added_on']));?></small>
                                                       </h1>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- info row -->
                                             <div class="row invoice-info">
                                                  <div class="col-sm-4 invoice-col">
                                                       <?php if (check_permission($controller, 'canviewbuyerdetails')) {?>
                                                              <strong>Buyer</strong>
                                                              <address>
                                                                   <br>
                                                                   <?php
                                                                   echo isset($order['buy_usr_first_name']) ? $order['buy_usr_first_name'] . ', ' : '';
                                                                   echo isset($order['buy_usr_last_name']) ? $order['buy_usr_last_name'] : '';
                                                                   ?>
                                                                   <br>
                                                                   <br>Phone: <?php echo isset($order['buy_usr_phone']) ? $order['buy_usr_phone'] : '';?>
                                                                   <br>Email: <?php echo isset($order['buy_usr_email']) ? $order['buy_usr_email'] : '';?>
                                                              </address>
                                                         <?php }?>
                                                  </div>
                                                  <!-- /.col -->
                                                  <div class="col-sm-4 invoice-col">
                                                       <br>
                                                       <b>Status:</b>
                                                       <div class="divOrderStatuses">
                                                            <?php if (check_permission($controller, 'changeorderstatuse') && $order['pos_id'] != 3 && $order['pos_id'] != 4) {?>
                                                                   <select class="chkChangeOrderStatuse" data-url="<?php echo site_url('order/changeOrderStatuse/' . $order['ordm_id']);?>">
                                                                        <option value="">Select order status</option>
                                                                        <?php foreach ((array) $statuses as $key => $value) {?>
                                                                             <option <?php echo ($value['pos_id'] == $order['pos_id']) ? 'selected="selected"' : '';?>
                                                                                  value="<?php echo $value['pos_id'];?>"><?php echo $value['pos_status'];?></option>
                                                                             <?php }?>
                                                                   </select>
                                                              <?php } else {?>
                                                                   <strong><?php echo $order['pos_status'];?></strong>
                                                              <?php }?>
                                                       </div>
                                                  </div>
                                                  <div class="col-sm-4 invoice-col">
                                                       <br>
                                                       <?php if ($order['ordm_delivery_boy']) {?>
                                                              <div>
                                                                   <b>Delivery boy:</b> 
                                                                   <?php echo $order['dboy_usr_first_name'];?>
                                                              </div>
                                                         <?php } else {?>
                                                              <div>
                                                                   <b>Delivery boy:</b> 
                                                                   <?php if (check_permission($controller, 'canassigndeliveryboy')) {?>
                                                                        <select class="cmbAssignOrderToDboy" data-url="<?php echo site_url('order/setdeliveryboy/' . $order['ordm_id']);?>">
                                                                             <option value="0">Select delivery boy</option>                                                                          
                                                                             <?php foreach ((array) $deliveryBoys as $key => $value) {?>
                                                                                  <option value="<?php echo $value['usr_id'];?>"><?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?></option>
                                                                             <?php }?>
                                                                        </select>
                                                                   <?php }?>
                                                              </div>
                                                         <?php }?>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- /.row -->
                                             <div class="row invoice-info">
                                                  <div class="col-sm-4 invoice-col">
                                                       <?php if (check_permission($controller, 'canviewbuyerdetails')) {?>
                                                              <strong>Delivery Address</strong>
                                                              <address>
                                                                   Name : <?php echo $order['adbk_first_name'];?><br/>
                                                                   Email : <?php echo $order['adbk_email'];?><br/>
                                                                   Phone : <?php echo $order['adbk_phone'];?><br/>
                                                                   Address : <?php echo $order['adbk_address'];?><br/>
                                                                   Building name : <?php echo $order['adbk_bulding_name'];?><br/>
                                                                   Block name : <?php echo $order['adbk_block_name'];?><br/>
                                                                   Street : <?php echo $order['adbk_street'];?><br/>
                                                                   PIN : <?php echo $order['adbk_pin_code'];?><br/>
                                                                   Locality : <?php echo $order['adbk_locality'];?><br/>
                                                                   City : <?php echo $order['adbk_city'];?><br/>
                                                              </address>
                                                         <?php }?>
                                                  </div>

                                                  <div class="col-sm-4 invoice-col">
                                                       <strong>Summery</strong>
                                                       <?php $shopChrg = ($order['ordm_total_shop_charge'] > 0) ? $order['ordm_total_shop_charge'] : $order['shopCharg'];?>
                                                       <address>
                                                            TYPE : <?php echo $order['ordm_payment_type'] == 1 ? 'COD' : 'Online pay';?><br/>
                                                            TOTAL DELIVERY CHARGE : <?php echo $order['ordm_delivery_chrg'];?><br/>
                                                            TOTAL SHOP CHARGE : <?php echo $shopChrg;?><br/>
                                                            DISCOUNT : <?php echo $order['ordm_total_discount'];?><br/>
                                                            ORDER TOTAL : <?php echo $order['orderTotalRate']; ?>
                                                            <br/>
                                                            GRAND TOTAL : <?php echo $order['orderTotalRate'] + ($order['ordm_delivery_chrg'] + $order['ordm_total_shop_charge']); ?><br/>
                                                       </address>
                                                  </div>
                                             </div>
                                             <!-- Table row -->
                                             <div class="row">
                                                  <div class="col-xs-12 table">
                                                       <table class="table table-striped">
                                                            <thead>
                                                                 <tr>
                                                                      <th>SL NO</th>
                                                                      <th>Product</th>
                                                                      <th>Supplier</th>
                                                                      <th>Market</th>
                                                                      <th>Qty</th>
                                                                      <th>Actual WT</th>
                                                                      <th>Actual Rate</th>
                                                                      <th>Item count</th>
                                                                      <th>Unit amount</th>
                                                                      <th>Total</th>
                                                                 </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <?php
                                                                   $gtotalQty = 0;
                                                                   $gtotalUnt = 0;
                                                                   $gtotalFin = 0;
                                                                   if ($order['productDetails']) {
                                                                        foreach ($order['productDetails'] as $key => $value) {
                                                                             if ($value['ordd_actual_rate'] > 0) {
                                                                                  $price = $value['ordd_actual_rate'];
                                                                             } else {
                                                                                  $price = ($value['ordd_unit_discount'] > 0) ? $value['ordd_unit_discount'] / $value['ordd_noOf_items'] : 
                                                                                       $value['ordd_unit_total'] / $value['ordd_noOf_items'];
                                                                                  if ($price <= 0) {
                                                                                       $price = $value['ordd_unit_amount'];
                                                                                  }
                                                                             }
                                                                             $localName = !empty($value['psm_product_local_name']) ? ' (' . $value['psm_product_local_name'] . ') ' : '';
                                                                             $qty = ($value['pdsm_qty'] > 0) ? $value['pdsm_qty'] : $value['ordd_noOf_items'];
                                                                             $grandAmont = number_format($price * $value['ordd_noOf_items'], 2);
                                                                             $gtotalQty += $value['ordd_noOf_items'];
                                                                             $gtotalUnt += $price;
                                                                             $gtotalFin += $grandAmont;
                                                                             ?>
                                                                             <tr>
                                                                                  <td><?php echo $key + 1;?></td>
                                                                                  <td><?php echo $value['prd_name_en'] . $localName;?></td>
                                                                                  <td><?php echo $value['supm_name'];?></td>
                                                                                  <td><?php echo $value['mar_name'];?></td>
                                                                                  <td><?php echo number_format($qty, 2) . ' ' . $value['unt_unit_en'];?></td>
                                                                                  <td>
                                                                                       <?php
                                                                                       echo ((int) $value['ordd_actual_weight'] > 0 ) ?
                                                                                               $value['ordd_actual_weight'] : number_format($qty, 2);
                                                                                       echo ' ' . $value['unt_unit_en'];
                                                                                       ?>
                                                                                  </td>
                                                                                  <td>
                                                                                       <?php
                                                                                       echo ((int) $value['ordd_actual_rate'] > 0 ) ?
                                                                                               $value['ordd_actual_rate'] : $price;
                                                                                       ?>
                                                                                  </td>
                                                                                  <td><?php echo $value['ordd_noOf_items'];?></td>
                                                                                  <td><?php echo $price;?></td>
                                                                                  <td><?php echo $grandAmont?></td>
                                                                             </tr>
                                                                             <?php
                                                                        }
                                                                   }
                                                                 ?>
                                                                 <tr>
                                                                      <td class="center" colspan="7">Total</td>
                                                                      <td style="font-weight: bolder;"><?php echo $gtotalQty;?></td> <!-- QTY -->
                                                                      <td style="font-weight: bolder;"><?php echo number_format($gtotalUnt, 2);?></td> <!-- UNIT AMT -->
                                                                      <td style="font-weight: bolder;"><?php echo number_format($gtotalFin, 2);?></td> <!-- GND AMT -->
                                                                 </tr>
                                                            </tbody>
                                                       </table>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- /.row -->

                                             <!-- this row will not appear when printing -->
                                             <div class="row no-print">
                                                  <div class="col-xs-12">
                                                       <!--                                                       <button class="btn btn-success pull-right"  style="margin-right: 5px;">
                                                                                                                   <i class="fa fa-comments-o"></i> Message</button>-->

                                                       <button class="btn btn-primary pull-right" onclick="window.print();" style="margin-right: 5px;">
                                                            <i class="fa fa-print"></i> Print</button>
                                                  </div>
                                             </div>
                                        </section>
                                   </div>
                                   <div role="tabpanel" class="tab-pane fade" id="messages" aria-labelledby="profile-tab">
                                        <div class="row">
                                             <div class="col-xs-12 table">
                                                  <table class="table table-striped">
                                                       <thead>
                                                            <tr>
                                                                 <th>SL NO</th>
                                                                 <th>Supplier</th>
                                                                 <th>DB Code</th>
                                                                 <th>DB Name</th>
                                                                 <th>Invoice</th>
                                                            </tr>
                                                       </thead>
                                                       <tbody>
                                                            <?php
                                                              if ($orderInvoice) {
                                                                   foreach ($orderInvoice as $key => $value) {
                                                                        ?>
                                                                        <tr>
                                                                             <td><?php echo $key + 1;?></td>
                                                                             <td><?php echo $value['supm_name'];?></td>
                                                                             <td><?php echo $value['usr_code'];?></td>
                                                                             <td><?php echo $value['usr_username'];?></td>
                                                                             <td>
                                                                                  <i style="cursor: zoom-in;" data-toggle="modal" data-target="#big<?php echo $value['ppi_id']?>" class="fa fa-list"></i>
                                                                                  <!-- -->
                                                                                  <div class="modal fade invModel" id="big<?php echo $value['ppi_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                                       <div class="modal-dialog" role="document">
                                                                                            <div class="modal-content">
                                                                                                 <div class="modal-header">
                                                                                                      <h5 class="modal-title" id="exampleModalLabel">Invoice</h5>
                                                                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                           <span aria-hidden="true">&times;</span>
                                                                                                      </button>
                                                                                                 </div>
                                                                                                 <div class="modal-body">
                                                                                                      <!-- -->
                                                                                                      <div id="myCarousel<?php echo $value['ppi_id']?>" class="carousel slide" data-ride="carousel">
                                                                                                           <!-- Indicators -->
                                                                                                           <ol class="carousel-indicators">
                                                                                                                <li data-target="#myCarousel<?php echo $value['ppi_id']?>" data-slide-to="0" class="active"></li>
                                                                                                                <li data-target="#myCarousel<?php echo $value['ppi_id']?>" data-slide-to="1"></li>
                                                                                                                <li data-target="#myCarousel<?php echo $value['ppi_id']?>" data-slide-to="2"></li>
                                                                                                           </ol>

                                                                                                           <!-- Wrapper for slides -->
                                                                                                           <div class="carousel-inner">
                                                                                                                <?php
                                                                                                                if (!empty($value['images'])) {
                                                                                                                     foreach ($value['images'] as $imgkey => $img) {
                                                                                                                          ?>
                                                                                                                          <div class="item <?php echo $imgkey == 0 ? 'active' : '';?>">
                                                                                                                               <img src="uploads/product_order_invoice/<?php echo $img['ppii_image'];?>" 
                                                                                                                                    alt="Invoice bill <?php echo $imgkey;?>" style="width:100%;">
                                                                                                                          </div>
                                                                                                                          <?php
                                                                                                                     }
                                                                                                                }
                                                                                                                ?>
                                                                                                           </div>

                                                                                                           <!-- Left and right controls -->
                                                                                                           <a class="left carousel-control" href="#myCarousel<?php echo $value['ppi_id']?>" data-slide="prev">
                                                                                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                                                                                <span class="sr-only">Previous</span>
                                                                                                           </a>
                                                                                                           <a class="right carousel-control" href="#myCarousel<?php echo $value['ppi_id']?>" data-slide="next">
                                                                                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                                                                                <span class="sr-only">Next</span>
                                                                                                           </a>
                                                                                                      </div>
                                                                                                      <!-- -->
                                                                                                 </div>
                                                                                                 <div class="modal-footer">
                                                                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                                 </div>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  <!-- -->
                                                                             </td>
                                                                        </tr>
                                                                        <?php
                                                                   }
                                                              } else {
                                                                   ?>
                                                                   <tr>    
                                                                        <td colspan="8" style="text-align: center;">No invoice uploaded</td>
                                                                   </tr>
                                                              <?php }
                                                            ?>
                                                       </tbody>
                                                  </table>
                                             </div>
                                             <!-- /.col -->
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
     $(document).ready(function () {
          if ($(".chat_list").eq(0).length > 0) {
               $(".chat_list").eq(0).trigger("click");
          }

          var anchorHash = window.location.href.toString();
          if (anchorHash.lastIndexOf('#') != -1) {
               anchorHash = anchorHash.substr(anchorHash.lastIndexOf('#'));
               if ($('a[goto="' + anchorHash + '"]').length > 0) {
                    $('a[goto="' + anchorHash + '"]').trigger('click');
               }
          }
     });
</script>

<style>
     .modal-dialog { /* Width */
          max-width: 100%;
          height: auto;
          width: auto !important;
          display: inline-block;
     }
     .invModel {
          z-index: -1;
          display: flex !important;
          justify-content: center;
          align-items: center;
          /*padding-top: 260px;*/
     }

     .modal-open .invModel {
          z-index: 1050;
     }
</style>

