<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Product order list</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="dtOrderList" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Order No</th>
                                        <?php if (check_permission($controller, 'canviewbuyerdetails')) {?>
                                               <th>Buyer</th>
                                          <?php }?>
                                        <th>Location</th>
                                        <th>Added on</th>
                                        <th>Order Total</th>
                                        <th>Delivery Charge</th>
                                        <th>Grand Total</th>
                                        <th>Delivery boy</th>
                                        <th>Status</th>
                                        <th>Type</th>
                                        <?php echo ($this->uid == 1) ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $order as $key => $value) {
                                          $locationDetails = $this->order->getOrderTownAdmin($value['ordm_id']);
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/order_summery/' . encryptor($value['ordm_id']));?>">
                                               <td>
                                                    <?php if (check_permission($controller, 'track_dboy')) {?>
                                                         <a href="<?php echo site_url($controller . '/track_dboy/' . encryptor($value['ordm_id']));?>">
                                                              <i class="fa fa-map-marker"></i> 
                                                         </a>&nbsp;&nbsp;
                                                    <?php }?>
                                                    <?php echo $value['ordm_number'];?>
                                               </td>
                                               <?php if (check_permission($controller, 'canviewbuyerdetails')) {?>
                                                    <td class="trVOE"><?php echo $value['buy_usr_username'];?></td>
                                               <?php }?>
                                               <td class="trVOE"><?php echo isset($locationDetails['mar_name']) ? $locationDetails['mar_name'] : '';?></td>
                                               <td class="trVOE"><?php echo date('j M Y', strtotime($value['ordm_added_on']));?></td>
                                               <td class="trVOE"><?php echo $value['ordm_total_amount'];?></td>
                                               <td class="trVOE"><?php echo $value['ordm_delivery_chrg'];?></td>
                                               <td class="trVOE"><?php echo $value['ordm_grand_total'];?></td>
                                               <td class="trVOE"><?php echo $value['dboy_usr_first_name'] . ' ' . $value['dboy_usr_last_name'];?></td>
                                               <td class="trVOE"><?php echo $value['pos_status'];?></td>
                                               <td class="trVOE"><?php echo $value['ordm_payment_type'] == 1 ? 'COD' : 'Online pay';?></td>
                                               <?php if ($this->uid == 1) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . $value['ordm_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<script>
     $(function () {
          $('input[name="start_date"]').daterangepicker({
               singleDatePicker: true,
               showDropdowns: true,
               minYear: 1901,
               maxYear: parseInt(moment().format('YYYY'), 10),
               locale: {
                    format: 'YYYY-MM-DD'
               }
          });
     });
</script>

<script>
     $(function () {
          $('input[name="end_date"]').daterangepicker({
               singleDatePicker: true,
               showDropdowns: true,
               minYear: 1901,
               maxYear: parseInt(moment().format('YYYY'), 10),
               locale: {
                    format: 'YYYY-MM-DD'
               }
          });
     });
</script>
