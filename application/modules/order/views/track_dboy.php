<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_content">
                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                   <li role="presentation" class="active">
                                        <a class="btnComments" href="javascript:;" goto="#messages" role="tab" id="profile-tab" 
                                           data-toggle="tab" aria-expanded="false"><i class="fa fa-comments-o"></i> Track order</a>
                                   </li>
                                   <li role="presentation">
                                        <a href="javascript:;" goto="#rfq" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                             <i class="fa fa-list-alt"></i> Summary</a>
                                   </li>

                              </ul>

                              <div id="myTabContent" class="tab-content">
                                   <div role="tabpanel" class="tab-pane fade " id="rfq" aria-labelledby="home-tab">
                                        <section class="content invoice">
                                             <!-- title row -->
                                             <div class="row">
                                                  <div class="col-xs-12 invoice-header">
                                                       <h1>
                                                            #<?php echo $order['ordm_number'];?>
                                                            <small class="pull-right">Date: <?php echo date('j M Y', strtotime($order['ordm_added_on']));?></small>
                                                       </h1>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- info row -->
                                             <div class="row invoice-info">
                                                  <div class="col-sm-4 invoice-col">
                                                       <strong>Buyer</strong>
                                                       <address>
                                                            <br>
                                                            <?php
                                                              echo isset($order['buy_usr_first_name']) ? $order['buy_usr_first_name'] . ', ' : '';
                                                              echo isset($order['buy_usr_last_name']) ? $order['buy_usr_last_name'] : '';
                                                            ?>
                                                            <br>
                                                            <br>Phone: <?php echo isset($order['buy_usr_phone']) ? $order['buy_usr_phone'] : '';?>
                                                            <br>Email: <?php echo isset($order['buy_usr_email']) ? $order['buy_usr_email'] : '';?>
                                                       </address>
                                                  </div>
                                                  <!-- /.col -->
                                                  <div class="col-sm-4 invoice-col">
                                                       <br>
                                                       <b>Status:</b> 
                                                       <?php if (check_permission($controller, 'changeorderstatuse')) {?>
                                                              <select class="chkChangeOrderStatuse" data-url="<?php echo site_url('order/changeOrderStatuse/' . $order['ordm_id']);?>">
                                                                   <?php foreach ((array) $statuses as $key => $value) {?>
                                                                        <option <?php echo ($value['pos_status'] == $order['pos_status']) ? 'selected="selected"' : '';?>
                                                                             value="<?php echo $value['pos_id'];?>"><?php echo $value['pos_status'];?></option>
                                                                        <?php }?>
                                                              </select>
                                                         <?php } else {?>
                                                              <strong><?php echo $order['pos_status'];?></strong>
                                                         <?php }?>
                                                  </div>
                                                  <div class="col-sm-4 invoice-col">
                                                       <br>
                                                       <?php if ($order['ordm_delivery_boy']) {?>
                                                              <div>
                                                                   <b>Delivery boy:</b> 
                                                                   <?php echo $order['dboy_usr_first_name'];?>
                                                              </div>
                                                         <?php } else {?>
                                                              <div>
                                                                   <b>Delivery boy:</b> 
                                                                   <?php if (check_permission($controller, 'canassigndeliveryboy')) {?>
                                                                        <select class="cmbAssignOrderToDboy" data-url="<?php echo site_url('order/setdeliveryboy/' . $order['ordm_id']);?>">
                                                                             <option value="0">Select delivery boy</option>                                                                          
                                                                             <?php foreach ((array) $deliveryBoys as $key => $value) {?>
                                                                                  <option value="<?php echo $value['usr_id'];?>">
                                                                                       <?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?>
                                                                                  </option>
                                                                             <?php }?>
                                                                        </select>
                                                                   <?php }?>
                                                              </div>
                                                         <?php }?>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>

                                             <!-- /.row -->

                                             <!-- Table row -->
                                             <div class="row">
                                                  <div class="col-xs-12 table">
                                                       <table class="table table-striped">
                                                            <thead>
                                                                 <tr>
                                                                      <th>Product</th>
                                                                      <th>Supplier</th>
                                                                      <th>Market</th>
                                                                      <th>Qty</th>
                                                                      <th>Amount</th>
                                                                 </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <?php
                                                                   if ($order['productDetails']) {
                                                                        foreach ($order['productDetails'] as $key => $value) {
                                                                             $price = ($value['pdsm_offer_price'] > 0) ? $value['pdsm_offer_price'] : $value['pdsm_price'];
                                                                             $amount = $price * $value['ordd_noOf_items'];
                                                                             ?>
                                                                             <tr>
                                                                                  <td><?php echo $value['prd_name_en'];?></td>
                                                                                  <td><?php echo $value['supm_name'];?></td>
                                                                                  <td><?php echo $value['mar_name'];?></td>
                                                                                  <td><?php echo $value['ordd_noOf_items'] . ' ' . $value['unt_unit_en'];?></td>
                                                                                  <td><?php echo $amount;?></td>
                                                                             </tr>
                                                                             <?php
                                                                        }
                                                                   }
                                                                 ?>
                                                            </tbody>
                                                       </table>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- /.row -->

                                             <!-- this row will not appear when printing -->
                                             <div class="row no-print">
                                                  <div class="col-xs-12">
                                                       <!--                                                       <button class="btn btn-success pull-right"  style="margin-right: 5px;">
                                                                                                                   <i class="fa fa-comments-o"></i> Message</button>-->

                                                       <button class="btn btn-primary pull-right" onclick="window.print();" style="margin-right: 5px;">
                                                            <i class="fa fa-print"></i> Print</button>
                                                  </div>
                                             </div>
                                        </section>
                                   </div>
                                   <div role="tabpanel" class="tab-pane fade active in" id="messages" aria-labelledby="profile-tab">
                                        <!--                                        <button class="btnLoadMap" data-lat="-29.86519774" data-lon="30.98538962">Reload</button>
                                                                                <div id="map"></div>
                                                                                <div id="infowindow-content">
                                                                                     <img src="" width="16" height="16" id="place-icon">
                                                                                     <span id="place-name"  class="title"></span><br>
                                                                                     <span id="place-address"></span>
                                                                                </div>-->

                                        <div id="map"></div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>



<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }

     #map {
          height: 500px !important;
          width: 100%  !important;
          float: left !important;
     }
</style>

<script>
     $(document).ready(function () {
          if ($(".chat_list").eq(0).length > 0) {
               $(".chat_list").eq(0).trigger("click");
          }

          var anchorHash = window.location.href.toString();
          if (anchorHash.lastIndexOf('#') != -1) {
               anchorHash = anchorHash.substr(anchorHash.lastIndexOf('#'));
               if ($('a[goto="' + anchorHash + '"]').length > 0) {
                    $('a[goto="' + anchorHash + '"]').trigger('click');
               }
          }
          $('.btnLoadMap').on('click', function () {
               var lat = $(this).data('lat');
               var lon = $(this).data('lon');

          });
     });
</script>
<?php //debug($order); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR07gIkjnjfMhUqqj5WPZ3oUAjoo49wKQ&libraries=places"></script>

<script>
     var locations = {}; //A repository for markers (and the data from which they were contructed).

     //initial dataset for markers
     var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
//          maxZoom: 8,
//          minZoom: 1,
          streetViewControl: false,
          center: new google.maps.LatLng(30, 30),
          mapTypeId: google.maps.MapTypeId.ROADMAP
     });
     var infowindow = new google.maps.InfoWindow();
     function setMarkers(locObj) {
          var markers = [];
          $.ajax({
               url: "<?php echo site_url($controller . '/getLiveTrack/' . encryptor($order['ordm_id']));?>",
               method: "POST",
               dataType: "json",
               success: function (resp) {
                    var len = resp.length;
                    var dBoyPath = [];
                    $.each(resp, function (key, loc) {
                         var index = key + 1;
                         dBoyPath.push(
                                 {lat: parseFloat(loc.lat), lng: parseFloat(loc.lng)}
                         );

                         var marker;
                         marker = new google.maps.Marker({
                              position: new google.maps.LatLng(loc.lat, loc.lng),
                              map: map,
                              label: {
                                   text: index.toString(),
                                   color: "white"
                              },
                              anchorPoint: new google.maps.Point(0, -29)
                         });
                         map.setCenter(marker.getPosition());
//                              //Attach click listener to marker     
//                              google.maps.event.addListener(loc.marker, 'click', (function (key) {
//                                   return function () {
//                                        infowindow.setContent(locations[key].info);
//                                        infowindow.open(map, locations[key].marker);
//                                   }
//                              })(key));

                         //Remember loc in the `locations` so its info can be displayed and so its marker can be deleted.
                    });
                    var flightPath = new google.maps.Polyline({
                         path: dBoyPath,
                         geodesic: true,
                         strokeColor: '#FF0000',
                         strokeOpacity: 1.0,
                         strokeWeight: 2
                    });
                    flightPath.setMap(map);
               }
          });
     }

     var testLocs = {
          1: {
               info: '1. New Random info and new position',
               lat: -37,
               lng: 124.9634
          }
     }
     ;

     $(document).ready(function () {
          setInterval(checkRfq, 1000);
          function checkRfq() {
               setMarkers(testLocs);
          }
     });
</script>