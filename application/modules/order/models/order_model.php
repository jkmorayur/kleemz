<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class order_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getAllStatuses() {
            return $this->db->order_by('pos_id')->get(tbl_products_order_statuses)->result_array();
       }

       function getOrders($id = '', $date1 = '', $date2 = '') {

            if (!empty($id)) {
                 $selectOrderMaster = array(
                     tbl_products_order_master . '.*',
                     tbl_users . '.usr_username AS buy_usr_username',
                     tbl_users . '.usr_first_name AS buy_usr_first_name',
                     tbl_users . '.usr_last_name AS buy_usr_last_name',
                     tbl_users . '.usr_phone AS buy_usr_phone',
                     tbl_users . '.usr_email AS buy_usr_email',
                     tbl_users . '.usr_address AS buy_usr_address',
                     tbl_users . '.usr_city AS buy_usr_city',
                     tbl_users . '.usr_state AS buy_usr_state',
                     tbl_users . '.usr_district AS buy_usr_district',
                     tbl_users . '.usr_id AS buy_usr_id',
                     tbl_products_order_statuses . '.*',
                     'dboy.usr_first_name AS dboy_usr_first_name',
                     'dboy.usr_last_name AS dboy_usr_last_name',
                     tbl_address_book . '.*'
                 );

                 $orderMaster = $this->db->select(implode(',', $selectOrderMaster))
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                 ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                 ->join(tbl_address_book, tbl_address_book . '.adbk_id = ' . tbl_products_order_master . '.ordm_delivery_address', 'LEFT')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->get_where(tbl_products_order_master, array('ordm_id' => $id))->row_array();

                 $selectOrderDetails = array(
                     tbl_products_order_details . '.*',
                     tbl_products_stock_details . '.*',
                     tbl_products_stock_master . '.*',
                     tbl_products_master . '.*',
                     tbl_products_units . '.*',
                     tbl_supplier_master . '.supm_id',
                     tbl_supplier_master . '.supm_name',
                     tbl_supplier_master . '.supm_number',
                     tbl_supplier_master . '.supm_email',
                     tbl_supplier_master . '.supm_address',
                     tbl_supplier_master . '.supm_market',
                     tbl_supplier_master . '.supm_shop_charges',
                     tbl_market_places . '.mar_name',
                     tbl_market_places . '.mar_lat',
                     tbl_market_places . '.mar_long'
                 );
                 if (!empty($orderMaster)) {
                      $orderMaster['productDetails'] = $this->db->select(implode(',', $selectOrderDetails))
                                      ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                      ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_stock_details . '.pdsm_stock_master', 'LEFT')
                                      ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_order_details . '.ordd_unit_id', 'LEFT')
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $id))->result_array();
                 }
                 $orderMaster['tracking'] = $this->db->order_by('podt_id', 'ASC')->get_where(tbl_products_order_dboy_tracking, array('podt_order_id' => $id))->result_array();
                 $shopChrg = array();
                 $orderTotal = array();
                 $orderTotalDiscount = array();
                 if (!empty($orderMaster['productDetails'])) {
                      foreach ($orderMaster['productDetails'] as $key => $value) {
                           $shopChrg[$value['supm_id']] = $value['supm_shop_charges'];
                           $orderTotal[$key] = ($value['ordd_actual_rate'] > 0) ? $value['ordd_actual_rate'] : $value['ordd_unit_total'];
                           $orderTotalDiscount[$key] = $value['ordd_unit_discount'];
                      }
                 }
                 $orderMaster['shopCharg'] = array_sum($shopChrg);
                 $orderMaster['orderTotalRate'] = array_sum($orderTotal);
                 $orderMaster['orderTotalDiscount'] = array_sum($orderTotalDiscount);
                 $orderMaster['orderTotalDiscount'] = ($orderMaster['orderTotalDiscount'] > 0) ?
                         ($orderMaster['orderTotalRate'] - $orderMaster['orderTotalDiscount']) : 0;
//                 debug($orderMaster);
                 return $orderMaster;
            } else {
                 if ($this->usr_grp == 'TA') {

                      $myTows = array_column($this->db->select('tta_town_id')->get_where(tbl_townadmin_town_assoc, array('tta_user_id' => $this->uid))
                                      ->result_array(), 'tta_town_id');

                      $myorderMaster = $this->db->select('ordd_order_master_id')->where_in('ordd_town', $myTows)
                                      ->get(tbl_products_order_details)->result_array();

                      $this->db->where_in(tbl_products_order_master . '.ordm_id', array_column($myorderMaster, 'ordd_order_master_id'));
                 } else if ($this->usr_grp == 'SP') {

                      $myorderMaster = $this->db->select('ordd_order_master_id')->where_in('ordd_supplier', $this->suplr)
                                      ->get(tbl_products_order_details)->result_array();
                      $this->db->where_in(tbl_products_order_master . '.ordm_id', array_column($myorderMaster, 'ordd_order_master_id'));
                 }

                 $selectOrderMaster = array(
                     tbl_products_order_master . '.*',
                     tbl_supplier_master . '.supm_id',
                     tbl_supplier_master . '.supm_market',
                     tbl_supplier_master . '.supm_name',
                     tbl_supplier_master . '.supm_name',
                     tbl_users . '.usr_username AS buy_usr_username',
                     tbl_users . '.usr_phone AS buy_usr_phone',
                     tbl_users . '.usr_id AS buy_usr_id',
                     tbl_products_order_statuses . '.*',
                     tbl_market_places . '.mar_name',
                     'dboy.usr_first_name AS dboy_usr_first_name',
                     'dboy.usr_last_name AS dboy_usr_last_name',
                 );
                 return $this->db->select(implode(',', $selectOrderMaster))
                                 ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_master . '.ordm_supplier', 'LEFT')
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                 ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                 ->order_by(tbl_products_order_master . '.ordm_id', 'DESC')->get(tbl_products_order_master)->result_array();
            }
       }

       /**
        * Function for update order status
        * @param int $orderId
        * @param int $statusId
        * @return boolean
        * Author : JK
        */
       function changeOrderStatuse($orderId, $statusId) {

            $update['ordm_current_status'] = $statusId; //New status id
            $update['ordm_verified_by'] = $this->uid; // who verify the order
            $update['ordm_verified_on'] = date('Y-m-d h:i:s'); // verify date
            // Get old status
            $oldStatus = $this->db->get_where(tbl_products_order_master, array('ordm_id' => $orderId))->row_array();

            $this->db->where('ordm_id', $orderId);
            if ($this->db->update(tbl_products_order_master, $update)) {

                 for ($i = 0; $i <= $statusId; $i++) {
                      $isalready = $this->db->get_where(tbl_products_order_status_assoc, array(
                                  'pssa_order_master_id' => $orderId,
                                  'pssa_status_id' => $statusId))->row_array();
                      if (empty($isalready)) {
                           $this->db->insert(tbl_products_order_status_assoc, array(
                               'pssa_order_master_id' => $orderId,
                               'pssa_status_id' => $statusId,
                               'pssa_added_on' => date('Y-m-d h:i:s'),
                               'pssa_added_by' => $this->uid)
                           );
                      }
                 }

                 $oldStst = isset($oldStatus['ordm_current_status']) ? $oldStatus['ordm_current_status'] : 0; // old status
                 generate_log(array(
                     'log_title' => 'Order status changed',
                     'log_desc' => 'Order status changed to *' . $oldStst . '-' . $statusId,
                     'log_controller' => 'order-status-changed',
                     'log_action' => 'U',
                     'log_ref_id' => $orderId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            }
            return false;
       }

       function setDeliveryBoy($orderId, $dboy) {
            $this->db->where('ordm_id', $orderId);
            if ($this->db->update(tbl_products_order_master, array('ordm_delivery_boy' => $dboy))) {

                 //Change status
                 $isalready = $this->db->get_where(tbl_products_order_status_assoc, array(
                             'pssa_order_master_id' => $orderId,
                             'pssa_status_id' => ASSIGN_TO_DB))->row_array();
                 if (empty($isalready)) {
                      $this->db->insert(tbl_products_order_status_assoc, array(
                          'pssa_order_master_id' => $orderId,
                          'pssa_status_id' => ASSIGN_TO_DB,
                          'pssa_added_by' => $this->uid));
                      $this->db->where('ordm_id', $orderId)->update(tbl_products_order_master, array('ordm_current_status' => ASSIGN_TO_DB));
                 }
                 //Change status

                 generate_log(array(
                     'log_title' => 'Order assigned to delivery boy',
                     'log_desc' => 'Order assigned to delivery boy-' . $dboy,
                     'log_controller' => 'assign-dboy-order',
                     'log_action' => 'U',
                     'log_ref_id' => $orderId,
                     'log_added_by' => $this->uid
                 ));
                 return true;
            }
            return false;
       }

       function newInquiryMessage($datas) {
            if (!empty($datas)) {

                 //Send push notification
                 $toUser = $this->ion_auth->user($datas['poc_to'])->row_array();
                 if (isset($toUser['usr_pushy_token']) && !empty($toUser['usr_pushy_token'])) {
                      $push['id'] = encryptor($datas['poc_order_id']);
                      $push['msg_type'] = 'prod_inqry_reply_notfication';
                      $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_username');
                      $push['title'] = $datas['poc_title'];
                      $push['time'] = date("Y-m-d H:i:s");
                      $this->common_model->sendPushNotification($push, $toUser['usr_pushy_token'], '');
                 }

                 return $this->db->insert(tbl_products_order_comments, $datas);
            }
            return false;
       }

       function loadInquiryComments($excludeIds, $ordId, $from = '', $to = '') {
            if ($excludeIds) {
                 if (is_root_user()) {
                      return $this->db->select(tbl_products_order_comments .
                                              '.*, c_from.usr_first_name AS frm_usr_first_name, c_from.usr_last_name AS frm_usr_last_name, c_from.usr_avatar AS frm_usr_avatar, '
                                              . ' c_to.usr_first_name AS to_usr_first_name, c_to.usr_last_name AS to_usr_last_name, c_to.usr_avatar AS to_usr_avatar')
                                      ->join(tbl_users . ' c_from', 'c_from.usr_id = ' . tbl_products_order_comments . '.poc_from', 'LEFT')
                                      ->join(tbl_users . ' c_to', 'c_to.usr_id = ' . tbl_products_order_comments . '.poc_to', 'LEFT')
                                      ->where(tbl_products_order_comments . '.poc_order_id', $ordId)
                                      ->where('((' . tbl_products_order_comments . '.poc_from = ' . $from . ' AND ' . tbl_products_order_comments . '.poc_to = ' . $to . ') OR '
                                              . '(' . tbl_products_order_comments . '.poc_from = ' . $to . ' AND ' . tbl_products_order_comments . '.poc_to = ' . $from . '))')
                                      ->where_not_in(tbl_products_order_comments . '.poc_id', $excludeIds)
                                      ->order_by(tbl_products_order_comments . '.poc_added_on', 'ASC')
                                      ->get(tbl_products_order_comments)->result_array();
                 } else {
                      return $this->db->select(tbl_products_order_comments .
                                              '.*, c_from.usr_first_name AS frm_usr_first_name, c_from.usr_last_name AS frm_usr_last_name, c_from.usr_avatar AS frm_usr_avatar, '
                                              . ' c_to.usr_first_name AS to_usr_first_name, c_to.usr_last_name AS to_usr_last_name, c_to.usr_avatar AS to_usr_avatar')
                                      ->join(tbl_users . ' c_from', 'c_from.usr_id = ' . tbl_products_order_comments . '.poc_from', 'LEFT')
                                      ->join(tbl_users . ' c_to', 'c_to.usr_id = ' . tbl_products_order_comments . '.poc_to', 'LEFT')
                                      ->where(tbl_products_order_comments . '.poc_order_id', $ordId)
                                      ->where('((' . tbl_products_order_comments . '.poc_from = ' . $from . ' AND ' . tbl_products_order_comments . '.poc_to = ' . $to . ') OR '
                                              . '(' . tbl_products_order_comments . '.poc_from = ' . $to . ' AND ' . tbl_products_order_comments . '.poc_to = ' . $from . '))')
                                      ->where_not_in(tbl_products_order_comments . '.poc_id', $excludeIds)
                                      ->order_by(tbl_products_order_comments . '.poc_added_on', 'ASC')
                                      ->get(tbl_products_order_comments)->result_array();
                 }
            }
       }

       function deliveryBoys() {

            $this->db->where(tbl_groups . '.id', 10);
            $this->db->where(tbl_users . '.usr_active', 1);
            if ($this->uid != 1) {
                 $this->db->where(tbl_users . '.usr_town_admin', $this->uid);
            }
            return $this->db->select(tbl_users . '.*, ' .
                                    tbl_users_groups . '.group_id as group_id, ' .
                                    tbl_groups . '.name as group_name, ' .
                                    tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                    'supp.usr_first_name AS sup_first_name, supp.usr_last_name AS sup_last_name,' .
                                    tbl_supplier_master . '.*')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                            ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_users . ' supp', 'supp.usr_id = ' . tbl_users . '.usr_supplier', 'LEFT')
                            ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_users . '.usr_supplier', 'LEFT')
                            ->get(tbl_users)->result_array();
       }

       function getLiveTracking($order) {
            return $this->db->select('podt_lat AS lat, podt_lon AS lng')->order_by('podt_id', 'ASC')
                            ->get_where(tbl_products_order_dboy_tracking, array('podt_order_id' => $order))->result_array();
       }

       function delete($orderId) {
            if (!empty($orderId)) {
                 $this->db->delete(tbl_products_order_master, array('ordm_id' => $orderId));
                 $this->db->delete(tbl_products_order_status_assoc, array('pssa_order_master_id' => $orderId));
                 $this->db->delete(tbl_products_order_details, array('ordd_order_master_id' => $orderId));
                 $this->db->delete(tbl_products_order_dboy_tracking, array('podt_order_id' => $orderId));
                 return true;
            }
            return false;
       }

       function getOrderTownAdmin($orderMasterId) {
            $this->db->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT');
            $this->db->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT');
            return $this->db->get_where(tbl_products_order_details, array('ordd_order_master_id' => $orderMasterId))->row_array();
       }

       function getOrderInvoice($orderId) {
            if (!empty($orderId)) {
                 $invoiceMaster = $this->db->select(tbl_products_purchase_invoice . '.*,' . tbl_users . '.usr_code,' . tbl_users . '.usr_username,' . tbl_supplier_master . '.supm_name')
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_purchase_invoice . '.ppi_added_by', 'LEFT')
                                 ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_purchase_invoice . '.ppi_supplier_id', 'LEFT')
                                 ->where(tbl_products_purchase_invoice . '.ppi_order_id', $orderId)->get(tbl_products_purchase_invoice)->result_array();
                 if (!empty($invoiceMaster)) {
                      foreach ($invoiceMaster as $key => $value) {
                           $invoiceMaster[$key]['images'] = $this->db->get_where(tbl_products_purchase_invoice_images, array('ppii_invoice_id' => $value['ppi_id']))->result_array();
                      }
                 }
                 return $invoiceMaster;
            }
            return false;
       }

  }
  