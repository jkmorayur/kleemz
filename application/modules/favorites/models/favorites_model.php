<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class favorites_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_products = TABLE_PREFIX . 'products';
            $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
            $this->tbl_products_images = TABLE_PREFIX . 'products_images';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
            $this->tbl_supplier_shop_images = TABLE_PREFIX . 'supplier_shop_images';
       }

       function addProductToFavoriteList($prdId) {
            if (!empty($prdId)) {
                 $alreadyAdded = $this->db->get_where($this->tbl_favorite_list, array('fav_consign_id' => $prdId,
                             'fav_added_by' => $this->uid, 'fav_consign' => 'PRD'))->row_array();
                 if (empty($alreadyAdded)) {
                      $insData['fav_consign'] = 'PRD';
                      $insData['fav_consign_id'] = $prdId;
                      $insData['fav_added_by'] = $this->uid;
                      $this->db->insert($this->tbl_favorite_list, $insData);
                      return array('msg' => 'Added to your favorite list', 'status' => 'success', 'action' => 'c');
                 } else {
                      $this->db->where('fav_id', $alreadyAdded['fav_id']);
                      $this->db->delete($this->tbl_favorite_list);
                      return array('msg' => 'Removed from your favorite list', 'status' => 'success', 'action' => 'd');
                 }
                 return false;
            } else {
                 return false;
            }
       }

       function addSupplierToFavoriteList($supId) {
            if (!empty($supId)) {
                 $alreadyAdded = $this->db->get_where($this->tbl_favorite_list, array('fav_consign_id' => $supId,
                             'fav_added_by' => $this->uid, 'fav_consign' => 'SUP'))->row_array();
                 if (empty($alreadyAdded)) {
                      $insData['fav_consign'] = 'SUP';
                      $insData['fav_consign_id'] = $supId;
                      $insData['fav_added_by'] = $this->uid;
                      $this->db->insert($this->tbl_favorite_list, $insData);
                      return array('msg' => 'Added to your favorite list', 'status' => 'success', 'action' => 'c');
                 } else {
                      $this->db->where('fav_id', $alreadyAdded['fav_id']);
                      $this->db->delete($this->tbl_favorite_list);
                      return array('msg' => 'Removed from your favorite list', 'status' => 'success', 'action' => 'd');
                 }
                 return false;
            } else {
                 return false;
            }
       }

       function myFavoritesProducts() {
            if (!is_root_user()) { // supplier
                 $this->db->where($this->tbl_favorite_list . '.fav_added_by', $this->uid);
            }

            $products = $this->db->select('prd_default_image,prd_id,prd_name_en,fav_id')
                         //    ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_favorite_list . '.fav_added_by', 'LEFT')
                            ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_favorite_list . '.fav_consign_id', 'RIGHT')
                            ->where($this->tbl_favorite_list . '.fav_consign', 'PRD')->get($this->tbl_favorite_list)->result_array();

            if (!empty($products)) {
                 foreach ($products as $key => $value) {
                      if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                           $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id'], 'pimg_id' => $value['prd_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                      }
                      $products[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                    //   $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                 }
            }
            return $products;
       }

       function delete($id) {
            $this->db->where('fav_id', $id);
            $this->db->delete($this->tbl_favorite_list);
            return true;
       }

       function myFavoritesSuppliers() {
            if (!is_root_user()) { // supplier
                 $this->db->where($this->tbl_favorite_list . '.fav_added_by', $this->uid);
            }

            return $this->db->select($this->tbl_favorite_list . '.*,' . $this->tbl_supplier_master . '.supm_name_en,supm_domain_prefix,supm_id')
                         //    ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_favorite_list . '.fav_added_by', 'LEFT')
                            ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_favorite_list . '.fav_consign_id', 'LEFT')
                            ->where($this->tbl_favorite_list . '.fav_consign', 'SUP')->get($this->tbl_favorite_list)->result_array();
       }

       function addToFollow($supId) {
            if (!empty($supId)) {
                 $alreadyAdded = $this->db->get_where($this->tbl_supplier_followers, array('sfol_supplier' => $supId,
                             'sfol_followed_by' => $this->uid))->row_array();
                 if (empty($alreadyAdded)) {
                      $insData['sfol_supplier'] = $supId;
                      $insData['sfol_followed_by'] = $this->uid;
                      $this->db->insert($this->tbl_supplier_followers, $insData);
                      return array('msg' => 'Added to your followers list', 'status' => 'success', 'action' => 'c');
                 } else {
                      $this->db->where('sfol_id', $alreadyAdded['sfol_id']);
                      $this->db->delete($this->tbl_supplier_followers);
                      return array('msg' => 'Removed from your followers list', 'status' => 'success', 'action' => 'd');
                 }
                 return false;
            } else {
                 return false;
            }
       }

       function followingSuppliers() {
            return $this->db->select($this->tbl_supplier_followers . '.*,supm_id,supm_domain_prefix,supm_name_en')
                            ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_supplier_followers . '.sfol_supplier')
                            ->where($this->tbl_supplier_followers . '.sfol_followed_by', $this->uid)->get($this->tbl_supplier_followers)->result_array();
       }

       function removeFollow($folId) {
            if (!empty($folId)) {
                 $this->db->where('sfol_id', $folId);
                 $this->db->delete($this->tbl_supplier_followers);
                 return true;
            }
            return false;
       }

       function followers() {
            return $this->db->select($this->tbl_supplier_followers . '.*,' . $this->tbl_users . '.*')
                            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_supplier_followers . '.sfol_followed_by')
                            ->where($this->tbl_supplier_followers . '.sfol_supplier', $this->suplr)->get($this->tbl_supplier_followers)->result_array();
       }

       function getSuppliersImages($supId) {
            if (!empty($supId)) {
                 $suppliers = $this->db->get_where($this->tbl_supplier_master, array('supm_id' => $supId))->row_array();
                 if (!empty($suppliers)) {
                      if (isset($suppliers['supm_default_image']) && !empty($suppliers['supm_default_image'])) {
                           $sup_images = $this->db->get_where($this->tbl_supplier_shop_images, array('ssi_supplier' => $supId, 'ssi_id' => $suppliers['supm_default_image']))->row_array();
                      } else {
                           $sup_images = $this->db->limit(1)->get_where($this->tbl_supplier_shop_images, array('ssi_supplier' => $suppliers['supm_id']))->row_array();
                      }
                      $defaultImg = isset($sup_images['ssi_image']) ? $sup_images['ssi_image'] : '';
                      $suppliers['sup_default_image'] = 'assets/uploads/shops/' . $defaultImg;
                 }
                 return $suppliers;
            }
            return false;
       }

  }
  