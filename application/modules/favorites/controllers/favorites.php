<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class favorites extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Favorites | ' . STATIC_TITLE;
            $this->load->model('favorites_model', 'favorites');
            $this->load->model('product/product_model', 'product');
            $this->lock_in();
       }

       function addProductToFavoriteList($prodId) {
            $prodId = encryptor($prodId, 'D');
            $responce = $this->favorites->addProductToFavoriteList($prodId);
            die(json_encode($responce));
       }

       function addSupplierToFavoriteList($supId) {
            $supId = encryptor($supId, 'D');
            $responce = $this->favorites->addSupplierToFavoriteList($supId);
            die(json_encode($responce));
       }

       function supplier_list() {

            $data['list'] = $this->favorites->myFavoritesSuppliers();
            $this->render_page(__CLASS__ . '/supplier-list', $data);
       }

       function product_list() {
            $data['list'] = $this->favorites->myFavoritesProducts();
            $this->render_page(__CLASS__ . '/product-list', $data);
       }

       function removefaveprod($fid) {
            $fid = encryptor($fid, 'D');
            if ($this->favorites->delete($fid)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'false', 'msg' => "Row can't  delete")));
            }
       }

       function removefavesupp($fid) {
            $fid = encryptor($fid, 'D');
            if ($this->favorites->delete($fid)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'false', 'msg' => "Row can't  delete")));
            }
       }

       function addToFollow($supId) {
            $supId = encryptor($supId, 'D');
            $responce = $this->favorites->addToFollow($supId);
            die(json_encode($responce));
       }

       function removeFollow($follId) {
            $follId = encryptor($follId, 'D');
            if ($this->favorites->removeFollow($follId)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'You are unfollow the supplier')));
            } else {
                 die(json_encode(array('status' => 'false', 'msg' => "Row can't  delete")));
            }
       }

       function following() {
            $data['list'] = $this->favorites->followingSuppliers();
            $this->render_page(__CLASS__ . '/following', $data);
       }

       function followers() {
            $data['list'] = $this->favorites->followers();
            $this->render_page(__CLASS__ . '/followers', $data);
       }

  }
  