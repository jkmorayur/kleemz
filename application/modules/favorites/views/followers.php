<div class="right_col" role="main">
     <section class="feed-listing">
          <div class="container">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Followers list</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="row">
                         <?php foreach ($list as $key => $value) {?>
                                <div class="col-md-6">
                                     <div class="list-cont">
                                          <div class="list-img">
                                               <?php
                                               echo img(array("src" => './assets/uploads/avatar/' . $value['usr_avatar'], 'class' => 'img-responsive'));
                                               ?>
                                          </div>
                                          <div class="list-content">
                                               <?php echo $value['usr_first_name'];?>
                                          </div>
                                     </div>
                                </div>
                           <?php }?>
                    </div>
               </div>
          </div>
     </section>
</div>