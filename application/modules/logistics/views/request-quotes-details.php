<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Logistics request quote details</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/add');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="0" type="hidden" name="" id="usr_id"/>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Logistics</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['usr_first_name'];?></label>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">CBM</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_cbm'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Weight</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_weight'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product type</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_prod_type'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_email'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Mobile</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_mobile'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Location from</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_loc_from'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Location to</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12"><?php echo $requests['logq_loc_to'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Description</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12" style="height: auto;"><?php echo $requests['logq_desc'];?></label>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Selected Service</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="form-control col-md-7 col-xs-12" style="height: auto;"><?php echo $requests['logs_service_title'];?></label>
                                   </div>
                              </div>

                              <?php
                                if (isset($requests['types']) && !empty($requests['types'])) {
                                     foreach ($requests['types'] as $key => $value) {
                                          ?>
                                          <div class="form-group">
                                               <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"><?php echo $key == 0 ? 'Logistics types' : '';?></label>
                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <label class="form-control col-md-7 col-xs-12" style="height: auto;"><?php echo $value['logt_title'];?></label>
                                               </div>
                                          </div>
                                          <?php
                                     }
                                }
                              ?>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>