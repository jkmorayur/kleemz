<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Logistics Services</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <?php if (check_permission($controller, 'add')) {?>
                                <td>
                                     <a href="<?php echo site_url($controller . '/add');?>" class="btn btn-round btn-primary">
                                          <i class="fa fa-pencil-square-o"></i> New Logistics</a>
                                </td>
                           <?php } if (check_permission($controller, 'new_service')) {?>
                                <td>
                                     <a href="<?php echo site_url($controller . '/new_service');?>" class="btn btn-round btn-primary">
                                          <i class="fa fa-pencil-square-o"></i> New service</a>
                                </td>
                           <?php }?>
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Image</th>
                                        <th>Logistics</th>
                                        <th>Service name</th>
                                        <?php echo check_permission($controller, 'delete_service') ? '<th style="width: 10%";>Delet</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $services as $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/edit_service/' . encryptor($value['logs_id']));?>">
                                               <td class="trVOE">
                                                    <?php
                                                    $img = $this->logistics->getServiceDefaultImage($value['logs_id']);
                                                    echo img(array('src' => $img, 'width' => '80'));
                                                    ?>
                                               </td>
                                               <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                               <td class="trVOE"><?php echo $value['logs_service_title'];?></td>
                                               <?php if (check_permission($controller, 'delete_service')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete_service/' . $value['logs_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>