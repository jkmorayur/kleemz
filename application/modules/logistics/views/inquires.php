<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Enquiries</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Comments</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $inquires as $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/verify/' . encryptor($value['usr_id']));?>">
                                               <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                               <td class="trVOE"><?php echo $value['usr_email'];?></td>
                                               <td class="trVOE"><?php echo $value['usr_phone'];?></td>
                                               <td class="trVOE"><?php echo $value['usr_desc'];?></td>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>