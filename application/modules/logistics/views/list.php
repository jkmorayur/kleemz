<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Logistics</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table style="margin: 10px 10px;">
                              <tr>
                                   <?php if (check_permission($controller, 'add')) {?>
                                          <td>
                                               <a href="<?php echo site_url($controller . '/add');?>" class="btn btn-round btn-primary">
                                                    <i class="fa fa-pencil-square-o"></i> New Logistics</a>
                                          </td>
                                     <?php } if (check_permission($controller, 'new_service')) {?>
                                          <td>
                                               <a href="<?php echo site_url($controller . '/new_service');?>" class="btn btn-round btn-primary">
                                                    <i class="fa fa-pencil-square-o"></i> New service</a>
                                          </td>
                                     <?php }?>
                              </tr>
                         </table>

                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Comments</th>
                                        <?php echo check_permission($controller, 'changestatus') ? '<th style="width: 10%";>Status</th>' : '';?>
                                        <?php echo check_permission($controller, 'delete') ? '<th style="width: 10%";>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $inquires as $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/edit/' . encryptor($value['usr_id']));?>">
                                               <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                               <td class="trVOE"><?php echo $value['usr_email'];?></td>
                                               <td class="trVOE"><?php echo $value['usr_phone'];?></td>
                                               <td class="trVOE"><?php echo $value['usr_desc'];?></td>
                                               <?php if (check_permission($controller, 'changestatus')) {?>
                                                    <td>
                                                         <input type="checkbox" name="ischecked" 
                                                         <?php echo $value['usr_active'] == 1 ? 'checked' : '';?>
                                                                data-url="<?php echo site_url($controller . '/changestatus/' . encryptor($value['usr_id']));?>"
                                                                class="js-switch chkOnchange" value="1" data-switchery="true">
                                                    </td>
                                               <?php } if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . $value['usr_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>