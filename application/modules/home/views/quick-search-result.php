<?php if (!empty($result)) {?>
       <div class="sl-cont">
            <ul class="sidenav">
                 <?php
                 $searchKeyword = isset($searchParam['keyword']) ? $searchParam['keyword'] : '';
                 $segment = isset($searchParam['section']) ? $searchParam['section'] : '';
                 foreach ($result as $keyword => $products) {
                      if (isset($searchParam['section']) && isset($searchParam['keyword'])) {
                           $url = $searchParam['section'] == 'product' ?
                                   site_url('product/product-listing?SearchText=' . slugify($keyword)) :
                                   site_url('stuffs/search/' . slugify($keyword) . '?seg=' . $segment);
                      }
                      ?>
                      <li><a href="<?php echo $url;?>" class="dropdown-btn">
                                <?php
                                echo str_replace($searchKeyword, "<strong>$searchKeyword</strong>", clean_text($keyword));
                                ?>
                           </a>
                           <?php if (!empty($products)) {?>
                                <ul class="dropdown-container">
                                     <?php
                                     foreach ($products as $pkey => $product) {
                                          ?>
                                          <li><a href="<?php echo site_url('product/product_details/' . encryptor($product['prd_id']));?>">
                                                    <?php echo $product['prd_name_en'];?></a></li>
                                     <?php }
                                     ?>
                                </ul>
                           <?php }
                           ?>
                      </li>
                      <?php
                 }
                 ?>
            </ul>
            <div class="clearfix"></div>
       </div>
       <?php
  }?>