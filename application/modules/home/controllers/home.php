<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class home extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Home | ' . STATIC_TITLE;
            $this->load->model('add/add_model', 'add');
            $this->load->model('home_model', 'home');
            $this->load->model('manage_banner/manage_banner_model', 'manage_banner');
            $this->template->set_layout('home');
       }

       function index() {
            echo 'Here';
       }
  } 