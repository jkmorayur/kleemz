<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class home_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->tbl_users = TABLE_PREFIX . 'users';
        $this->tbl_groups = TABLE_PREFIX . 'groups';
        $this->tbl_products = TABLE_PREFIX . 'products';
        $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
        $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
        $this->tbl_products_images = TABLE_PREFIX . 'products_images';
        $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
        $this->tbl_products_keyword = TABLE_PREFIX . 'products_keyword';
        $this->tbl_supplier_images = TABLE_PREFIX . 'supplier_shop_images';
    }

    public function getLatestProducts()
    {
        $this->db->select('prd_id,prd_name_en,prd_is_stock_prod,prd_default_image')
            ->join($this->tbl_supplier_master, 'supm_id=prd_supplier')
            ->where('supm_status', 1)
            ->where('prd_id in(select max(prd_id) from ' . $this->tbl_products . ' group by prd_supplier)');
        $this->db->where($this->tbl_products . '.prd_status', 1);
        $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
        $this->db->limit(8);
        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
            $products[$key]['lazy_image'] = 'assets/uploads/product/thumb_' . $defaultImg;
            $products[$key]['pimg_alttag'] = isset($images['pimg_alttag']) ? $images['pimg_alttag'] : 'product image';
        }

        return $products;
    }

    public function getSuppliers()
    {
        $limit = get_settings_by_key('home_wholesale_shops_limit');

        $this->db->select('supm_id,supm_name_en,supm_panoramic_image,supm_default_image,supm_domain_prefix');
        $this->db->join($this->tbl_users, 'usr_supplier=supm_id', 'RIGHT');
        $this->db->join($this->tbl_users_groups, 'user_id = usr_id', 'RIGHT');
        $this->db->where('group_id', 2);
        $this->db->where('usr_active', 1);
        $this->db->order_by('supm_added_on', 'asc');
        $this->db->limit($limit);
        $suppliers = $this->db->where('supm_status', 1)->get($this->tbl_supplier_master)->result_array();
        foreach ($suppliers as $key => $val) {
            if (isset($suppliers[$key]['supm_default_image']) && !empty($suppliers[$key]['supm_default_image'])) {
                $images = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
            $suppliers[$key]['default_image'] = 'uploads/shops/' . $defaultImg;
            $suppliers[$key]['ssi_alttag'] = isset($images['ssi_alttag']) ? $images['ssi_alttag'] : 'supplier image';
        }
        return $suppliers;
    }

    public function getRecentProducts()
    {
        if (get_cookie('recent')) {
            $recent_product_id = array_unique(json_decode(get_cookie('recent'), true));
            $this->db->where($this->tbl_products . '.prd_status', 1);
            $products = $this->db->select('prd_id,prd_default_image')
                ->join($this->tbl_supplier_master, 'supm_id=prd_supplier')
                ->where('supm_status', 1)
                ->where_in('prd_id', $recent_product_id)
                ->limit(15)
                ->get($this->tbl_products)->result_array();
            foreach ($products as $key => $val) {
                if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                    $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                } else {
                    $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                }
                $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
                $products[$key]['lazy_image'] = 'assets/uploads/product/thumb_' . $defaultImg;
                $products[$key]['pimg_alttag'] = isset($images['pimg_alttag']) ? $images['pimg_alttag'] : 'product image';
            }

            return $products;
        } else {
            return '';
        }

    }

    public function getHotSellingProducts($limit = '')
    {
        $this->db->select('prd_id,prd_is_stock_prod,prd_name_en,prd_price_min,prd_offer_price_min,prd_default_image')
            ->join($this->tbl_supplier_master, 'supm_id=prd_supplier')
            ->where('supm_status', 1);
        $this->db->where('prd_hot_selling', 1);
        $this->db->where('prd_status', 1);
        $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
        if ($limit) {
            $this->db->limit($limit);
        }
        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
            $products[$key]['lazy_image'] = 'assets/uploads/product/thumb_' . $defaultImg;
            $products[$key]['pimg_alttag'] = isset($images['pimg_alttag']) ? $images['pimg_alttag'] : 'product image';
        }
        return $products;
    }

    public function getOfferProducts($limit)
    {
        $this->db->select('prd_id,prd_moq,prd_is_stock_prod,prd_name_en,prd_price_min,prd_offer_price_min,unt_unit_name_en,prd_default_image')
            ->join($this->tbl_supplier_master, 'supm_id=prd_supplier')
            ->where('supm_status', 1);
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        $this->db->where('prd_offer_price_min >', 0);
        $this->db->where('prd_offer_price_max >', 0);
        $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
        if ($limit) {
            $this->db->limit($limit);
        }
        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
            $products[$key]['lazy_image'] = 'assets/uploads/product/thumb_' . $defaultImg;
            $products[$key]['pimg_alttag'] = isset($images['pimg_alttag']) ? $images['pimg_alttag'] : 'product image';
        }
        return $products;
    }

    public function getStockProducts($limit, $type = '')
    {
        $this->db->select('prd_id,prd_moq,prd_stock_qty,prd_updated_on,prd_default_image,prd_is_stock_prod,prd_name_en,prd_price_min,prd_offer_price_min,unt_unit_name_en')
            ->join($this->tbl_supplier_master, 'supm_id=prd_supplier')
            ->where('supm_status', 1);
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        if ($type == 'stock') {
            $this->db->where('prd_is_stock_prod', 1);
            $this->db->where('prd_is_flash_sale', 0);
        } else if ($type == 'flash_stock') {
            $this->db->where('prd_is_stock_prod', 1);
            $this->db->where('prd_is_flash_sale', 1);
        }
        $this->db->order_by('prd_added_on', 'DESC');
        if ($limit) {
            $this->db->limit($limit);
        }
        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
            $products[$key]['lazy_image'] = 'assets/uploads/product/thumb_' . $defaultImg;
            $products[$key]['pimg_alttag'] = isset($images['pimg_alttag']) ? $images['pimg_alttag'] : 'product image';
        }
        return $products;
    }

    public function search($searchData)
    {
        $section = isset($searchData['section']) ? trim(slugify($searchData['section'])) : '';
        $keyWord = isset($searchData['query']) ? trim(strtolower($searchData['query'])) : '';
        if ($section == 'logistics') {

            return $this->db->select($this->tbl_users . '.usr_first_name AS value')
                ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                ->where(array($this->tbl_groups . '.id' => 6))
                ->like($this->tbl_users . '.usr_first_name', $keyWord, 'both')->get($this->tbl_users)->result_array();
        } else if ($section == 'quality-control') {
            return $this->db->select($this->tbl_users . '.usr_first_name AS value')
                ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                ->where(array($this->tbl_groups . '.id' => 7))
                ->like($this->tbl_users . '.usr_first_name', $keyWord, 'both')->get($this->tbl_users)->result_array();
        } else {
            if (!empty($searchData)) {
                $searchResult = array();

                $this->db->select('distinct(pkwd_val_en) as value')
                    ->join($this->tbl_products, 'prd_id=pkwd_product', 'LEFT')
                    ->join($this->tbl_supplier_master, 'supm_id=prd_supplier')
                    ->where('prd_status', 1)
                    ->where('supm_status', 1);
                if ($section == 'stock') {
                    $this->db->where('prd_is_stock_prod', 1);
                }
                $products = $this->db->like($this->tbl_products_keyword . '.pkwd_val_en', $keyWord, 'both')
                    ->order_by($this->tbl_products_keyword . '.pkwd_val_en')
                    ->get($this->tbl_products_keyword)->result_array();
                return $products;
            } else {
                return null;
            }
        }
    }

    public function productByKeyword($data, $limit = 10, $start = 0, $filter = array())
    {
        if (!empty($data)) {
            $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
                ->where("REPLACE(LOWER(pkwd_val_en), ' ', '-') = ", $data['SearchText'])
                ->get($this->tbl_products_keyword)->row()->pkwd_product;

            $output = '';

            $this->db->select($this->tbl_products . '.*')
                ->join($this->tbl_supplier_master, 'supm_id=prd_supplier');
            if (isset($filter['category'])) {
                $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
                $this->db->where($this->tbl_products_categories . '.pcat_category', $filter['category']);
            }
            if (isset($filter['suplier'])) {
                $this->db->where($this->tbl_products . '.prd_supplier', $filter['suplier']);
            }

            $this->db->limit($limit, $start);
            if (isset($filter['order_by'])) {
                if ($filter['order_by'] == "price_desc") {
                    $this->db->order_by("prd_price_min", "desc");
                }

                if ($filter['order_by'] == "price_asc") {
                    $this->db->order_by("prd_price_min", "asc");
                }

            }
            $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
            $this->db->where_in($this->tbl_products . '.prd_id', explode(',', $prodIds));
            $this->db->where($this->tbl_products . '.prd_status', 1)->where('supm_status', 1);

            $products = $this->db->get($this->tbl_products)->result_array();

            foreach ($products as $key => $val) {
                if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                    $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                } else {
                    $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                }
                $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                //     $products[$key]['default_image'] = 'assets/uploads/product/173x150_' . $defaultImg;
                $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
                $products[$key]['lazy_image'] = 'assets/uploads/product/thumb_' . $defaultImg;
            }
            foreach ($products as $prod) {
                $isStock = $prod['prd_is_stock_prod'] == 1 ? '<div class="stock-notif">Stock</div>' : '';
                $deadStock = $prod['prd_is_stock_prod'] == 1 ? '<p> Stock : ' . floatval($prod['prd_stock_qty']) . '</p>' : '';

                $output .= "<div class='single-prod'>
                <div class='prod-box'>
                <div class='prod-img'>";
                $output .= "<a href=";
                $output .= site_url('product/product_details/' . encryptor($prod['prd_id']));
                $output .= ">";
                $output .= img(array('src' => $prod['default_image'], 'id' => '', 'alt' => '', 'class' => 'img-responsive img-fluid'));
                $output .= "</a></div>" . $isStock;
                $output .= "<div class='prod-detail txt-height'>
                            <div class='details'>
                            <h4 class='prod-title-s'>
                            <a href='" . site_url('product/product_details/' . encryptor($prod['prd_id'])) . "'>";
                $output .= get_snippet($prod['prd_name_en'], 3);
                $output .= "</a></h4>";
                $output .= "<p>From <span class='price-text'>US $";
                $output .= $prod['prd_price_min'];
                $output .= "</span> / Piece</p>";
                $output .= "<p class='moq'>";
                $output .= $prod['prd_moq'];
                $output .= " Piece  (MOQ)</p></div>";
                $output .= '
                </div>
                </div>
                </div>';
            }
            if (!$output) {
                $output = "<h3 class='text-center'>No Products Found!!</h3>";
            }

            return $output;
        } else {

        }
    }

    public function getBuildings($mar_id)
    {
        return $this->db->select('bld_title,bld_id')->limit(7)->where('bld_market', $mar_id)->get(TABLE_PREFIX . 'buildins')->result_array();
    }

    public function getMarket()
    {
        $m_id = get_settings_by_key('home_page_market');
        $markets = $this->db->select('mar_id,mar_name')->where('mar_id', $m_id)->get(TABLE_PREFIX . 'market_places')->row_array();
        if (!$markets) {
            return $this->db->select('mar_id,mar_name')->limit(1)->order_by('mar_id', 'asc')->get(TABLE_PREFIX . 'market_places')->row_array();
        }
        return $markets;
    }

    public function productsearch($limit, $start, $filter)
    {

        $return = "<div class='stock-list'>
          <div class='stock-page'>
              <div class='cat-head2'>
                <div class='prd-name'>PRODUCT</div>
                <div class='prd-prce'>Company</div>
                <div class='prd-update'>Action</div>
              </div>
              <div class='stocks-container'>

                <div class='list-wrpr'>
                   <ul class='stk-list'>";

        $keyword = isset($filter['keyword']) ? $filter['keyword'] : 0;
        if (!$keyword) {
            return "<h3 class='text-center'>No Products Found!!</h3>";
        }

        //  $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
        // ->where("REPLACE(LOWER(pkwd_val_en), ' ', '-') = ", $keyword)
        // ->get($this->tbl_products_keyword)->row()->pkwd_product;

        $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
            ->where("pkwd_val_en LIKE '%" . $keyword . "%'")
            ->get($this->tbl_products_keyword)->row()->pkwd_product;

        $output = '';

        $this->db->select($this->tbl_supplier_master . '.supm_name_en,' . $this->tbl_products . '.prd_id,' . $this->tbl_products . '.prd_name_en,' . $this->tbl_products . '.prd_default_image');
        $this->db->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier');
        $this->db->limit($limit, $start);

        $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
        $this->db->where_in($this->tbl_products . '.prd_id', explode(',', $prodIds));
        $this->db->where($this->tbl_products . '.prd_status', 1);

        $products = $this->db->get($this->tbl_products)->result_array();

        if (!$products) {
            return "<h3 class='text-center'>No Products Found!!</h3>";
        }

        foreach ($products as $val) {

            $l = '"' . site_url() . "product/product-details/" . encryptor($val['prd_id']) . '"';
            $link = "<a target=" . '"_blank"' . "href=" . $l . ">" . $val['prd_name_en'] . "</a>";
            if ($val['prd_default_image']) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }

            $img = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $productImg = site_url() . 'assets/uploads/product/' . $img;

            $return .= "<li>
                    <div class='prd-name'>
                      <div class='img'>
                        <img src=" . $productImg . " alt=''>
                      </div>
                      <div class='nme'>
                        <h5>" . $val['prd_name_en'] . "</h5>
                      </div>
                    </div>
                    <div class='prd-prce'>" . $val['supm_name_en'] . "</div>
                    <div  class='prd-update selectThisprd' prdId ='" . $val['prd_id'] . "' link ='" . $link . "'><a class='add_btn' href='javascript:void(0)'>Add</a></div>
                  </li>";

        }
        $return .= "</ul> </div> </div> </div> <div id='pagination_link'>" . $filter['pages'] . "</div> </div>";
        //echo json_encode($return);
        return $return;

    }

    public function getProductsCount($keyword)
    {
//"REPLACE(LOWER(pkwd_val_en), ' ', '-') = ", $keyword
        $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
            ->where("pkwd_val_en LIKE '%" . $keyword . "%'")
            ->get($this->tbl_products_keyword)->row()->pkwd_product;

        return $this->db->select($this->tbl_products . '.prd_id,')
            ->where_in($this->tbl_products . '.prd_id', explode(',', $prodIds))
            ->where($this->tbl_products . '.prd_status', 1)->get($this->tbl_products)->num_rows();
    }
}
