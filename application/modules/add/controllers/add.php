<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class add extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Advertisement';
            $this->load->model('add_model', 'add');
            $this->lock_in();
       }

       public function index() {

            $data['count'] = $this->add->getCount();
            $data['projects'] = $this->add->getAdd();
            $this->current_section = 'Product';
            $this->render_page(__CLASS__ . '/list', $data);
       }

       public function newAdd() {
            if (!empty($_POST)) {
                 $this->load->library('upload');
                 ini_set('memory_limit', '-1');

                 if (isset($_FILES['prd_image']['name'][0]) && !empty($_FILES['prd_image']['name'][0])) {

                      $x1 = $this->input->post('x1');
                      $fileCount = count($x1);
                      $up = array();
                      for ($j = 0; $j < $fileCount; $j++) {
                           /**/
                           $data = array();
                           $angle = array();
                           $newFileName = rand(9999999, 0) . $_FILES['prd_image']['name'][$j];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'advt/';
                           $config['allowed_types'] = 'gif|jpg|png';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);

                           $angle['x1']['0'] = $_POST['x1'][$j];
                           $angle['x2']['0'] = $_POST['x2'][$j];
                           $angle['y1']['0'] = $_POST['y1'][$j];
                           $angle['y2']['0'] = $_POST['y2'][$j];
                           $angle['w']['0'] = $_POST['w'][$j];
                           $angle['h']['0'] = $_POST['h'][$j];

                           $_FILES['prd_image_tmp']['name'] = $_FILES['prd_image']['name'][$j];
                           $_FILES['prd_image_tmp']['type'] = $_FILES['prd_image']['type'][$j];
                           $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['prd_image']['tmp_name'][$j];
                           $_FILES['prd_image_tmp']['error'] = $_FILES['prd_image']['error'][$j];
                           $_FILES['prd_image_tmp']['size'] = $_FILES['prd_image']['size'][$j];
                           if (!$this->upload->do_upload('prd_image_tmp')) {
                                $up = array('error' => $this->upload->display_errors());
                           } else {
                                $udata = $this->upload->data();
                                crop($this->upload->data(), $angle);
                                $_POST['project']['add_image'] = $udata['file_name'];
                                $_POST['project']['add_imgalt'] = $_POST['add_imgalt'];
                           }
                      }
                 }
                 if ($id = $this->add->newAdd($this->input->post('project'))) {
                      $this->session->set_flashdata('app_success', 'Gallery successfully added!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't add project!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $this->render_page(__CLASS__ . '/add');
            }
       }

       public function view($id) {
            $data['projects'] = $this->add->getAdd($id);
            $data['maxOrder'] = $this->add->getNextOrder(true);
            $this->render_page(__CLASS__ . '/view', $data);
       }

       public function update() {
            ini_set('memory_limit', '-1');
            $id = $_POST['project']['glry_id'];
            $this->load->library('upload');
            /**/
            $x1 = $this->input->post('x1');
            $fileCount = count($x1);
            $up = array();
            if (isset($_FILES['prd_image']['name'][0]) && !empty($_FILES['prd_image']['name'][0])) {
                 for ($j = 0; $j < $fileCount; $j++) {
                      /**/
                      $data = array();
                      $angle = array();
                      $newFileName = rand(9999999, 0) . $_FILES['prd_image']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'advt/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $angle['x1']['0'] = $_POST['x1'][$j];
                      $angle['x2']['0'] = $_POST['x2'][$j];
                      $angle['y1']['0'] = $_POST['y1'][$j];
                      $angle['y2']['0'] = $_POST['y2'][$j];
                      $angle['w']['0'] = $_POST['w'][$j];
                      $angle['h']['0'] = $_POST['h'][$j];

                      $_FILES['prd_image_tmp']['name'] = $_FILES['prd_image']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['prd_image']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['prd_image']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['prd_image']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['prd_image']['size'][$j];
                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $up = array('error' => $this->upload->display_errors());
                      } else {
                       
                           $udata = $this->upload->data();
                           crop($this->upload->data(), $angle);
                           $_POST['project']['add_image'] = $udata['file_name'];
                           $_POST['project']['add_imgalt'] = $_POST['bimg_alt'];
                      }
                 }
            }
            if ($this->add->updateGallery($this->input->post('project'))) {
                 $this->session->set_flashdata('app_success', 'Gallery successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update project!");
            }
            redirect(strtolower(__CLASS__));
       }

       public function removeImage($id) {
            if ($this->add->removeImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Gallery image successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete project image"));
            }
       }

       public function delete($id) {
            if ($this->add->deleteGallery($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Product successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete product"));
            }
       }

       function setDefaultImage($imgId, $projectId) {
            if (!empty($imgId) && !empty($projectId)) {
                 if ($this->add->setDefaultImage($imgId, $projectId)) {
                      echo json_encode(array('status' => 'success', 'msg' => 'This image set as default'));
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => "Can't set this image as default"));
                 }
            }
       }

       function changePriority($projId, $newOrder) {
            if (!empty($projId) && !empty($newOrder)) {
                 if ($this->add->setPriority($projId, $newOrder)) {
                      echo json_encode(array('status' => 'success', 'msg' => 'This image set as default'));
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => "Can't set this image as default"));
                 }
            }
       }

  }
  