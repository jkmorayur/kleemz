<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Ads</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Order</th>
                                        <th>Category</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>URL</th>
                                        <th>Delete</th> 
                                        <?php echo (is_root_user()) ? "<th>Status</th>" : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     if (!empty($projects)) {
                                          foreach ($projects as $key => $value) {
                                               ?>
                                               <tr data-url="<?php echo site_url($controller . '/view/' . $value['add_id']);?>">
                                                    <td style="width: 50px;" class="trVOE"><?php echo $value['add_order'];?></td>
                                                    <td class="trVOE"><?php echo $value['add_category'] == 1 ? 'Website' : 'App';?></td>
                                                    <td class="trVOE" style="width: 100px;">
                                                         <?php
                                                         echo img(array('src' => FILE_UPLOAD_PATH . 'advt/' . $value['add_image'], 'height' => '80', 'width' => '130'));
                                                         ?>
                                                    </td>
                                                    <td class="trVOE"><?php echo $value['add_title'];?></td>
                                                    <td class="trVOE"><?php echo $value['add_url'];?></td>
                                                    <?php if (check_permission($controller, 'delete')) {?>
                                                         <td style="width: 100px;">
                                                              <a class="pencile deleteRow" href="javascript:void(0);" 
                                                                 data-url="<?php echo site_url($controller . '/delete/' . $value['add_id']);?>">
                                                                   <i class="fa fa-remove"></i>
                                                              </a>
                                                         </td>
                                                    <?php } if (is_root_user()) {?>
                                                         <td>
                                                              <label class="switch">
                                                                   <input type="checkbox" value="1" <?php echo ($value['add_status'] == 1) ? "checked" : '';?>
                                                                          class="chkOnchange" data-url="<?php echo site_url('general/cmnChangeStatus/' . encryptor($value['add_id']) . '/advertisement/add_status/add_id');?>"/>
                                                                   <span class='slider round'></span>
                                                              </label>
                                                         </td>
                                                    <?php }?>
                                               </tr>
                                               <?php
                                          }
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>