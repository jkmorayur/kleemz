<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Ad</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/update", array('id' => "frmProject", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" name="project[add_id]" value="<?php echo $projects['add_id'];?>" />
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter title" value="<?php echo $projects['add_title']?>" type="text" class="form-control" name="project[add_title]" id="glry_title" placeholder="Title"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Url</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" value="<?php echo $projects['add_url']?>" class="form-control" name="project[add_url]" id="add_url" placeholder="Url"/>
                              </div>
                         </div>
                         
                         <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control cmbAddCategory" name="project[add_category]">
                                             <option <?php echo ($projects['add_category'] == 1) ? "selected='selected'" : '';?> value="1">Website</option>
                                             <option <?php echo ($projects['add_category'] == 2) ? "selected='selected'" : '';?> value="2">App</option>
                                        </select>
                                   </div>
                              </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Is show on home page</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input  value="1" <?php echo $projects['add_show_on_home_page'] == 1 ? "checked='true'" : "";?> type="checkbox" 
                                           class="" name="project[add_show_on_home_page]" id="glry_contract"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Priority</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($maxOrder)) {?>
                                          <select name="project[add_order]" id="add_order" class="form-control">
                                               <option value="">Select Priority</option>
                                               <?php for ($i = 1; $i <= $maxOrder; $i++) {?>

                                                    <option <?php echo ($i == $projects['add_order']) ? "selected='selected'" : '';?> 
                                                         value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>

                         <div class="form-group">
                              <?php if (isset($projects['add_image']) && !empty($projects['add_image'])) {?>
                                     <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                     <div class="col-md-6 col-sm-6 col-xs-12" style="padding-left: 10px;">
                                          <div class="input-group">
                                               <?php echo img(array('src' => FILE_UPLOAD_PATH . 'advt/' . $projects['add_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                          </div>
                                          <?php if ($projects['add_image']) {?>
                                               <span class="help-block">
                                                    <a id="<?php echo $projects['add_id'];?>" data-url="<?php echo site_url($controller . '/removeImage/' . $projects['add_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                               </span>
                                          <?php }?>

                                          <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="project[add_imgalt]" 
                                                            id="add_imgalt" data-parsley-required ="true" value="<?php echo $projects['add_imgalt']; ?>"data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                                     </div>
                                     <?php
                                }
                              ?>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                              <div class="row-fluid">
                                   <div class="span4">
                                        <div class="form-group no-margin-bot">
                                             <div class=" col-md-6 col-sm-6 col-xs-12-row">
                                                  <div id="newupload">
                                                       <input type="hidden" id="x10" name="x1[]" />
                                                       <input type="hidden" id="y10" name="y1[]" />
                                                       <input type="hidden" id="x20" name="x2[]" />
                                                       <input type="hidden" id="y20" name="y2[]" />
                                                       <input type="hidden" id="w0" name="w[]" />
                                                       <input type="hidden" id="h0" name="h[]" />
                                                       <input <?php if (!$projects['add_image']) { ?> required data-parsley-required-message="Select add image" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                              type="file" class="form-control" name="prd_image[]" id="image_file0" onchange="fileSelectHandler(0, 800, 395, true)" />
                                                       <img id="preview0" class="preview"/>
                                                  </div>
                                                  <span class="help-inline">Approximate image dimension 800(Height) X 395(Width)</span>

                                              <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="bimg_alt" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                                                  
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>
<script type="text/template" id="temSpecification">
     <div class="form-group grp-specification">
     <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
     <div class="row-fluid">
     <div class="span4">
     <div class="form-group no-margin-bot">
     <div class=" col-md-6 col-sm-6 col-xs-12-row">
     <input type="text" class="input-block-level" placeholder="Technical Details Key" name="specification[spe_specification][]">
     </div>
     </div>
     </div>
     <div class="span4">
     <div class="form-group no-margin-bot">
     <div class=" col-md-6 col-sm-6 col-xs-12-row">
     <input type="text" class="input-block-level" placeholder="Technical Details Value" name="specification[spe_specification_detail][]">
     </div>
     </div>
     </div>
     <div class="span1">
     <div class="form-group no-margin-bot">
     <div class=" col-md-6 col-sm-6 col-xs-12-row">
     <button id="btnRemoveMoreSpecification" class="btn btnRemoveMoreSpecification"><i class="icon-minus"></i></button>
     </div>
     </div>
     </div>
     </div>
     </div>
</script>
<script>
  $('input[type=file]').change(function(e){
   var image =  $('input[type=file]')[0].files[0].name;
   if(image)
   {
    $('#bImgAlt').attr('data-parsley-required',true);
   }
   else
   {
          $('#bImgAlt').attr('data-parsley-required',false);
   }
});
  </script>