<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
require './vendor/autoload.php';

class supplier_api extends CI_Controller
{
    public $func_without_token = array(
        'login',
        'home',
        'istokenValid',
        'forgot_password',
        'reset_password'
    );
    public $mail = '';
    public $sup_id = '';
    public $uid = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_auth_model', 'api_auth');
        $this->load->model('supplier_api_model', 'api_model');
        $this->load->model('product/product_model', 'product_model');
        $this->load->model('user/user_model', 'user_model');
        $this->load->model('feed/feed_model', 'feed');
        $this->checkToken();
        $this->mail = new PHPMailer;
    }

    public function checkToken()
    {

        $method = $this->uri->segment(2);

        $log_id = $this->api_model->saveApiRequest('supplier_api',$method,$_SERVER,$_REQUEST,$_FILES);

        if ((!$this->input->get_request_header('Token', true) || !$this->input->get_request_header('Token')) && !in_array($method, $this->func_without_token)) {
            $return = array('error' => 'Access Denied');
            echo json_encode($return);
            die();
        }if (!in_array($method, $this->func_without_token)) {
            if (!$this->api_auth->checkTokenExist($this->input->get_request_header('Token'), $method)) {
                $return = array('error' => 'Invalid Access Token');
                echo json_encode($return);
                die();
            } else {
                $token = $this->input->get_request_header('Token');
                $det = $this->api_model->getUserDetailsFromToken($token);
               
                $this->sup_id =  $this->suplr =$det['usr_supplier'];
                $this->uid = $det['usr_id'];
                $this->u_name = $det['usr_first_name'] .''.$det['usr_last_name'] ;
                $this->usr_avatar = $det['usr_avatar'];
                $this->usr_grp = $this->api_model->getUsergroupFromId($det['usr_id']);
                $this->usr_email = $det['usr_email'];

                $this->api_model->updateApiRequest($log_id,$this->uid);

               
            }
        }

    }

    public function login()
    {
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $pushy_token = $this->input->post('pushy_token', true);
        if ($email != null && $email != '' && $password != '' && $password != null) {
            $det = $this->api_auth->login($email, $password);
            if ($det == "timeout") {
                $return = array('error' => 'Max Login Attempts Exceeded');
            } else if ($det == "inactive") {
                $return = array('error' => 'Account Inactive');
            } else if ($det == "invalid") {
                $return = array('error' => 'Invalid Credentials');
            } else {
                $this->api_model->updatePushyToken($pushy_token, 'usr_token', $det->usr_token);
                $return = array('token' => $det->usr_token, 'full_name' => $det->usr_first_name . ' ' . $det->usr_last_name, 'username' => $email, 'image' => site_url() . 'assets/uploads/avatar/' . $det->usr_avatar, 'home' => $this->home('login', $det->usr_token));
            }
            echo json_encode($return);
            die();
        } else {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
    }
    public function home($from = '', $log_token = '')
    {
        $token = $this->input->get_request_header('Token');
        if ($log_token) {
            $token = $log_token;
        }
        if (!$token) {
            $return = array('error' => 'Access Denied');
            echo json_encode($return);
            die();
        }
        if (!$this->api_auth->checkTokenExist($this->input->get_request_header('Token'))) {
            $return = array('error' => 'Invalid Access Token');
            echo json_encode($return);
            die();
        }
        $pushy_token = $this->input->post('pushy_token', true);
        $det = $this->api_model->getUserDetailsFromToken($token);
        if ($det) {
            if (!$from) {
                $this->api_model->updatePushyToken($pushy_token, 'usr_token', $token);
            }
            $data = $this->api_model->home($det['usr_supplier'], $det['usr_id']);
        }
        if ($from) {
            return $data;
        }
        echo json_encode($data);
        die();
    }
    public function products()
    {
        $post = $this->input->post();

          $type = isset($post['type'])?trim($post['type']):0;
          $offset = isset($post['offset'])?$post['offset']:0;
          $limit = isset($post['limit'])?$post['limit']:30;
          $keyword = isset($post['keyword'])?$post['keyword']:null;

        $data['count'] = $this->api_model->getProductCounts($this->sup_id);
        $data['products'] = $this->api_model->getSupProducts($this->sup_id, $type, $offset, $limit,$keyword);
        echo json_encode($data);
        die();
    }
    public function product_search()
    {
        $key = $this->input->post('key', true);
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        if ($key == null || $key == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data = $this->api_model->getSearchProducts($this->sup_id, $key, $offset, $limit);
        echo json_encode($data);
        die();
    }
    public function add_feed()
    {
        $prd_id = $this->input->post('prd_id', true);
        $text = $this->input->post('text', true);
        if ($prd_id == null || $prd_id == '' || $text == null || $text == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }

        if ($feed_id = $this->api_model->addFeed($this->sup_id, $prd_id, $text)) {
             $this->sendPushy($feed_id, $this->input->post('text'), $this->input->post('prd_id'));
            $return = array('msg' => 'Feed added.');
        } else {
            $return = array('error' => 'Something went wrong!!');
        }
        echo json_encode($return);
        die();
    }
    public function get_feeds()
    {
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        $data = $this->api_model->getFeeds($this->sup_id, $offset, $limit);
        echo json_encode($data);
        die();
    }
    function update_feed(){
        $feed_id = $this->input->post('feed_id', true);
        $prd_id = $this->input->post('prd_id', true);
        $text = $this->input->post('text', true);
        if ($prd_id == null || $prd_id == '' || $text == null || $text == '' || $feed_id == null || $feed_id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $res = $this->api_model->updateFeed($feed_id, $prd_id, $text);
        if ($res) {
            $return = array('msg' => 'Feed updated.');
        } else {
            $return = array('error' => 'Something went wrong!!');
        }
        echo json_encode($return);
        die();
    }

    //up to this will use..

    public function sendMail($tomail, $type, $mdata = '')
    {
        /* Sent activation email */
        $this->mail->isSMTP();
        $this->mail->Host = MAIL_HOST;
        $this->mail->SMTPAuth = true;
        $this->mail->Username = MAIL_USERNAME;
        $this->mail->Password = MAIL_PASSWORD;
        $this->mail->SMTPSecure = 'ssl';
        $this->mail->Port = 465;
        $this->mail->setFrom(FROM_MAIL, FROM_NAME);
        $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
        $this->mail->addAddress($tomail);
        //remove this from server
        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true,
            ),
        );
        //remove this from server
        if ($type == "reg") {
            $this->mail->Subject = 'Hi ' . $tomail . ', please verify your fujeeka account';
            $mailData['mail'] = $tomail;
            $mailData['otp'] = $mdata['otp'];
            $mailContent = $this->load->view('user/register-mail-otp-template', $mailData, true);
        }
        if ($type == "reset_pwd") {
            $this->mail->Subject = 'Reset your fujeeka password';
            $mailData['name'] = $mdata['name'];
            $mailData['otp'] = $mdata['otp'];
            $mailContent = $this->load->view('user/forget-mail-template', $mailData, true);
        }
        $this->mail->isHTML(true);

        $this->mail->Body = $mailContent;
        return $this->mail->send();
        /* Sent activation email */
    }

    public function inquiry()
    {
        $post = $this->input->post();
        $ord_id= isset($post['ord_id'])?$post['ord_id']:0;
        $ord_data= isset($post['ord_data'])?$post['ord_data']:0;
        if($ord_id)
        {
                if($ord_data)
                {

                  $data['details'] = $this->api_model->getOrders($this->sup_id,$ord_id,$post);
                  $data['comments']= $this->api_model->getInquiryComments($ord_id,$post);
                  echo json_encode($data); die();
                }
        else       
         echo json_encode($this->api_model->getInquiryComments($ord_id,$post));  die(); 
                    
             
        }
        else 
         echo json_encode($this->api_model->getOrders($this->sup_id,$ord_id,$post)); die();
        
       

       
    }


    public function send_message()
    {
        $post = $this->input->post();
        //$det = $this->product_model->getUserById($post_det['rfqn_supplier']);
        $data['poc_order_id'] = isset($post['ord_id'])?$post['ord_id']:null;
        $data['poc_to'] = isset($post['to_user'])?$post['to_user']:null;
        $data['poc_title'] = isset($post['subject'])?$post['subject']:null;
        $data['poc_desc'] = isset($post['message'])?$post['message']:null;
        $data['poc_from']     = $this->uid;
        if($data['poc_order_id'] && $data['poc_to'] && $data['poc_from'] && $data['poc_desc'] && $data['poc_title'])
        {
            if ($res=$this->api_model->sendMessage($data)) {

          $chat['title']= $data['poc_title'];
          $chat['desc'] = $data['poc_desc'];
          $chat['type'] = 'inq';
          $chat['time'] = date('h:i A') . ' | ' . date('M d');
          $chat['from'] = $this->u_name;
          $chat['order_id'] = $data['poc_order_id'];
          $chat['from_img'] ='assets/uploads/avatar/' . $this->usr_avatar;
          $chat['inq_link'] = site_url('order/order_summery/'. encryptor($data['poc_order_id']));

          $res = $this->sendPusherMessage(encryptor($data['poc_to']), 'inquiry-event', $chat);


                $return = array('msg' => 'Message sent');
            } else {
                $return = array('error' => 'Something went wrong!!');
            }
            echo json_encode($return);
            die();
        }
        else 
        $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
       
    }

    public function rfq_list()
    {
        $data = $this->api_model->getRfq($this->sup_id,$this->input->post());
        echo json_encode($data);
        die();
    }

    public function rfq_comments()
    {
        $request = $this->input->post();
  
        if($rfq_id = isset($request['rfq_id']) ? $request['rfq_id'] : 0 )
        {
          
           if($rfq_data = isset($request['rfq_data']) ? $request['rfq_data'] : 0)
           {
                
                 $data['comments'] = $this->api_model->getRfqComments($this->uid,$rfq_id,$request);
                 $data['list'] = $this->api_model->getRfq($this->sup_id,$this->input->post());
           }
           else
             $data = $this->api_model->getRfqComments($this->uid,$rfq_id,$request);

             echo json_encode($data);die();
        }
        else 
            echo json_encode(array('error' => 'Partial Content'));die();
       
        
            
    }

    public function dashboard_notification()
    {
        echo json_encode($this->api_model->getUnread($this->uid,$this->sup_id));
        die();
    }


    function compose_mail() {

        $post = $this->input->post();

        $data['mms_to'] = isset($post['mail_to'])?$post['mail_to']:null;
        $data['mms_subject'] = isset($post['subject'])?$post['subject']:null;
        $data['mms_message'] = isset($post['message'])?$post['message']:null;
        $data['mms_from']     = $this->uid;
        if($data['mms_to'] && $data['mms_subject'] && $data['mms_message'])
        {
            if ($mailId=$this->api_model->sendmailMessage($data)) {

 if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'mail_attachments/', 'file', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
                     $file_attchment = site_url() .'assets/uploads/mail_attachments/' .$file;

                    $this->api_model->addAttachments(array('mat_master_id' => $mailId, 'mat_attachment' => $file));

                }



                $return = array('status'=>true,'msg' => 'Message sent','code'=>$mailId);
            } else {
                $return = array('error' => 'Something went wrong!!');
            }
            echo json_encode($return);
            die();
        }
        else 
        $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
       
    }
    
    function search_recipient() {
        $recipient = $this->api_model->searchRecipient($this->input->post('keyword'),$this->uid);
        $recipient = array_values($recipient);
        echo json_encode($recipient);
        die();
   }

   function mailbox() {

    $filter = array('sent' => 2,'trash' => 3,'inbox' => 1); 
    $post = $this->input->post();
    $type = isset($post['type'])?$post['type']:null;
    if($type)
    {
        if(in_array($type,['sent','inbox','trash']))
        $data = $this->api_model->filterMails($this->uid,$filter[$type],$post);
        else
        {
            $return = array('error' => 'Invalid type');
            echo json_encode($return);
            die();
        }
       
    }
    
    else 
    {
        $data['sent'] = $this->api_model->filterMails($this->uid,2,$post);
        $data['inbox'] = $this->api_model->filterMails($this->uid,1,$post);
        $data['trash'] = $this->api_model->filterMails($this->uid,3,$post);
       
    }
     
    echo json_encode($data);
    die();
    
   
}



     function delete_mail() {

         $post = $this->input->post();
         $id = isset($post['mms_id'])?$post['mms_id']:null;

        if($id)
        {
            $this->api_model->deleteMail($id);

              echo json_encode(array('status'=>true,'msg' => 'Message successfully deleted',
                'code'=> $id));
              die();
        }
        else
        {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }

        }




    function sent() {
        if ($mailMaster = $this->mailbox->send('', $_POST['mms_to'], $_POST['mms_subject'], $_POST['mms_message'])) {
             $fileCount = isset($_FILES['attachments']['name']) ? count($_FILES['attachments']['name']) : 0;
             if ($fileCount > 0) {
                  for ($i = 0; $i < $fileCount; $i++) {
                       $newFileName = rand(9999999, 0) . $_FILES['attachments']['name'][$i];
                       $config['upload_path'] = FILE_UPLOAD_PATH . 'mail_attachments/';
                       $config['allowed_types'] = 'gif|jpg|png';
                       $config['file_name'] = $newFileName;
                       $this->upload->initialize($config);

                       $_FILES['tmp_attach']['name'] = $_FILES['attachments']['name'][$i];
                       $_FILES['tmp_attach']['type'] = $_FILES['attachments']['type'][$i];
                       $_FILES['tmp_attach']['tmp_name'] = $_FILES['attachments']['tmp_name'][$i];
                       $_FILES['tmp_attach']['error'] = $_FILES['attachments']['error'][$i];
                       $_FILES['tmp_attach']['size'] = $_FILES['attachments']['size'][$i];

                       if (!$this->upload->do_upload('tmp_attach')) {
                            $uploadDataEr = $this->upload->display_errors();
                       } else {
                            $uploadData = $this->upload->data();
                            $file = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                            $this->mailbox->addAttachments(array('mat_master_id' => $mailMaster, 'mat_attachment' => $file));
                       }
                  }
             }
             die(json_encode(array('status' => 'success', 'msg' => 'Message successfully sent')));
        } else {
             die(json_encode(array('status' => 'fail', 'msg' => "Can't sent message")));
        }
   }


   function product_units() {
    echo json_encode($this->api_model->getUnits());
    die();
    }


    function supplier_categrory() {
        echo json_encode($this->api_model->getSupplierCategories($this->sup_id));
        die();
        }

        function productslist() {
            echo json_encode($this->api_model->getProducts());
            die();
            }


    function add_product() {

        $post = $this->input->post();

        $prd_name_en = isset($post['prd_name_en'])?$post['prd_name_en']:null;
        $prd_moq = isset($post['prd_moq'])?$post['prd_moq']:null;
        $prd_is_stock_prod = isset($post['prd_is_stock_prod'])?$post['prd_is_stock_prod']:null;
        $prd_unit = isset($post['prd_unit'])?$post['prd_unit']:null;
        $prd_qty = isset($post['prd_qty'])?$post['prd_qty']:null;
        $prd_price_min = isset($post['prd_price_min'])?$post['prd_price_min']:null;
        $prd_price_max = isset($post['prd_price_max'])?$post['prd_price_max']:null;
        $prd_offer_price_min = isset($post['prd_offer_price_min'])?$post['prd_offer_price_min']:null;
        $prd_offer_price_max = isset($post['prd_offer_price_max'])?$post['prd_offer_price_max']:null;
        $prd_category = isset($post['categories'])?$post['categories']:null;
        $prd_desc = isset($post['prd_desc'])?$post['prd_desc']:null;
   
        
    
        if($prd_name_en && ctype_digit($prd_moq) && ctype_digit($prd_is_stock_prod) && ctype_digit($prd_unit)
         && ctype_digit($prd_qty) && ctype_digit($prd_price_min) && ctype_digit($prd_price_max)  && ctype_digit($prd_offer_price_min)
         && ctype_digit($prd_offer_price_max) && is_array($prd_category) && $prd_desc )
        {
             $post['prd_added_by']     = $this->uid;
            if ($prdId = $this->api_model->addNewProduct($post)) {
           
              
                 $fileCount = isset($_FILES['pictures']['name'])?count($_FILES['pictures']['name']):0;
                
                if($fileCount>0)
                {
                    $this->load->library('upload');
                    $up = array();
                    for ($j = 0; $j < $fileCount; $j++) {
                         $data = array();
                     
                         $newFileName = microtime(true) . $_FILES['pictures']['name'][$j];
                         $config['upload_path'] = FILE_UPLOAD_PATH . 'product/';
                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                         $config['file_name'] = $newFileName;
                         $this->upload->initialize($config);
                         $_FILES['prd_image_tmp']['name'] = $_FILES['pictures']['name'][$j];
                         $_FILES['prd_image_tmp']['type'] = $_FILES['pictures']['type'][$j];
                         $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['pictures']['tmp_name'][$j];
                         $_FILES['prd_image_tmp']['error'] = $_FILES['pictures']['error'][$j];
                         $_FILES['prd_image_tmp']['size'] = $_FILES['pictures']['size'][$j];
                         if (!$this->upload->do_upload('prd_image_tmp')) {
                              $up = array('error' => $this->upload->display_errors());
                         } else {
                              $data = array('upload_data' => $this->upload->data());
                            
                              $this->product_model->addImages(array('pimg_product' => $prdId, 'pimg_image' => $data['upload_data']['file_name']));
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 275, 206);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 154, 116);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 207, 180);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 173, 150);
                              resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 450, 338);
                         }
                    }
                }
                
              echo json_encode(array('msg' => 'Product added  successfully'));
              die();
                
        }
    }
        else 
        {
        $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
       
    }

    }


    function chat_list() {
        echo json_encode($this->api_model->getRecentChats($this->uid,$this->input->post()));
        die();
    }


    function get_all_chats() {


        if($usrId = $this->input->post('user'))
        {
            echo json_encode($this->api_model->getallchats($this->uid,$usrId,$this->input->post()));
            die();
        }
        else  {
                $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();  
            }
        
    }


    public function send_pushntftn() {

          $request = $this->input->post();
          $title = isset($request['pnm_title'])?$request['pnm_title']:null;
          $content = isset($request['pnm_message'])?$request['pnm_message']:null;

          if(is_null($title)||is_null($content))
          {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();  
          }
             if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
                $newFileName = uniqid(rand(1, 8888888), true) . rand(1, 9999999) . $_FILES['image']['name'];
                $config['upload_path'] = FILE_UPLOAD_PATH . 'notification/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['file_name'] = $newFileName;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    echo json_encode(array('error' => $this->upload->display_errors()));
                    die();
                } else {
                     $data = array('upload_data' => $this->upload->data());
                     //crop($this->upload->data(), $this->input->post());
                }
                $_POST['pnm_image'] = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : '';
             }
            if($stat = $this->api_model->sendPushNotification($this->input->post()))
            {
                echo json_encode(array('msg' => 'Pushnotification sent  successfully'));
                die();
                  
            }
              else
              {
                echo json_encode(array('error' => 'Something went wrong!!'));
                die();
              }
            
        
        
   }

   public function related_product()
   {
    $cat_id = $this->input->post('cat_id');
    if( isset($cat_id) && ctype_digit($cat_id))
    {
        echo json_encode($this->api_model->getRealatedProduct($cat_id));
        die(); 
    }
    else
    {
        echo json_encode(array('error' => 'Partial Content'));
        die();  
    }

   }


   public function send_rfq() {

    $rfq_id = $this->input->post('rfq_id');
    $content = $this->input->post('reply');
    $to = $this->input->post('buyr_email');
    $product = $this->input->post('product');
   

    if ($rfq_id && $content && $to && $product) {
         $_POST['rfqn_prod_sent'][0]  = $product;
         $suplier = $this->sup_id;
         $this->mail->isSMTP();
         $this->mail->Host = MAIL_HOST;
         $this->mail->SMTPAuth = true;
         $this->mail->Username = MAIL_USERNAME;
         $this->mail->Password = MAIL_PASSWORD;
         $this->mail->SMTPSecure = 'ssl';
         $this->mail->Port = 465;
         // $this->mail->SMTPDebug = 4;
         $this->mail->SMTPOptions = array(
             'ssl' => array(
                 'verify_peer' => false,
                 'verify_peer_name' => false,
                 'allow_self_signed' => true,
             ),
         );
         $this->mail->setFrom(FROM_MAIL, FROM_NAME);
         $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
         $this->mail->addAddress($to);
         $this->mail->Subject = 'RFQ Quote';
         $this->mail->isHTML(true);
         $mailContent = $content;
         $this->mail->Body = $mailContent;
         $this->mail->send();
     
         if ($stat =  $this->product_model->updateRfqStatusAndCount($suplier, $this->input->post())) {
            

              echo json_encode(array('msg' => 'rfq reply  sent  successfully'));
              die();
            
         } else {
             
            //echo json_encode(array('msg' => 'rfq reply  sent  successfully'));
            echo json_encode(array('error' => 'Something went wrong!!'));
            die();
         }
    } else {
        echo json_encode(array('error' => 'Partial Content'));
        die();  
    }
}


public function send_chat() {

    $content = $this->input->post('content');
    $to_user = $this->input->post('to_user');

    $file = $attachment = $ext = $file_attchment= ''; 
 if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'chats/', 'file', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
                     $file_attchment = site_url() .'assets/uploads/chats/' .$file;
                }


    if (($content||$file) && $to_user) {

if ($this->input->post()) {
    $enc_id = encryptor($this->uid);
     $tok_det = $this->user_model->getUserById($to_user);
     $insert = array(
         'c_from_id' => $this->uid,
         'c_to_id' => $to_user,
         'c_content' => $content,
         'c_attachment' => $file
     );
     $is_prd = 0;
     if ($attachment) {
          $is_prd = 1;
          $insert['c_attachment'] = $attachment;
          $insert['c_is_prd_details'] = 1;
     }
     $this->common_model->saveChat($insert);

    
     $chat['attachment'] = $file_attchment;
     $chat['message'] = $content;
     $chat['from'] = $this->u_name;
     $chat['ext'] = $ext;
     $chat['from_ch'] = $enc_id;
     $chat['is_prd'] = $is_prd;



     //   $tok_det['usr_pushy_token']= '6b8a304919ab8b6434fb52';
     if (isset($tok_det['usr_pushy_token'])) {

          $push['type'] = 'recieved';
          $push['msg_type'] = 'chat';
          $push['from_id'] = $this->uid == 1;
          $attach = 0;
          if ($file) {
               $attach = 1;
          }
          $push['content'] = $chat['message'];
          $push['attachment_link'] = '';
          $push['attachment_preview'] = '';
          if ($is_prd) {
               $push['attachment_link'] =  $file_attchment;
               $push['attachment_preview'] =  $file_attchment;
          } else if ($attach) {
               $push['attachment_link'] = $file_attchment;
               $push['attachment_preview'] = site_url() . 'assets/images/attachment.jpg';
               if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $push['attachment_preview'] = site_url() . 'assets/' . $file;
               }
          }
            $to = encryptor($to_user);
          $push['from_name'] = $this->uid == 1 ? 'Fujeeka' : $this->u_name;
          $push['time'] = date("Y-m-d H:i:s");
          $push['is_attachment'] = $attach;
          $push['is_prd_details'] = $is_prd;
       
          if($this->common_model->checkIsBuyer($to_user))
             {

             $push['from_id'] = encryptor($this->uid);
                  $this->common_model->sendPushNotification($push, 
                                         $tok_det['usr_pushy_token'], '');
             }
             else
             {
                 $this->common_model->sendPushNotificationForSupplier($push, $tok_det['usr_pushy_token'], '');
             }
     }



     if ($this->sendPusherMessage($to, 'chat-event', $chat)) {

        echo json_encode(array('msg' =>  $chat,'msg2' =>  $push));

        die();
      
   } else {
      echo json_encode(array('error' => 'Something went wrong!!'));

      die();
   }
}

    }
    else {
        echo json_encode(array('error' => 'Partial Content'));
        die();  
    }
}

   public function uploadFiles($files, $path, $name, $types)
    {
        $this->load->library('upload');
        $newFileName = microtime(true) . $files[$name]['name'];

        $config['upload_path'] = $path;
        $config['allowed_types'] = $types;
        $config['file_name'] = $newFileName;
        $config['max_size'] = 2100;

        $this->upload->initialize($config);
        if (!$this->upload->do_upload($name)) {
            $ext = pathinfo($files[$name]['name'], PATHINFO_EXTENSION);
            $up = $this->upload->display_errors();

            $return = array('error' => $up . ' ,type=' . $ext . ' ,name =' . $files[$name]['name']);
            echo json_encode($return);
            die();

        } else {
            $uploadData = $this->upload->data();
            return $uploadData['file_name'];
        }
    }



function sendPusherMessage($channel, $event, $chat) {
    $options = array(
        'cluster' => PUSHER_CLUSTER,
        'useTLS' => true
    );
    $pusher = new Pusher\Pusher(
            PUSHER_APP_KEY, PUSHER_APP_SECRET, PUSHER_APP_ID, $options
    );
    return $pusher->trigger($channel, $event, $chat);
}


function update_feed1() {

    $post = $this->input->post();
    $f_id = isset($post['f_id'])?$post['f_id']:null;
    $request['f_prd_id'] = isset($post['f_prd_id'])?$post['f_prd_id']:null;
    $request['f_text'] = isset($post['f_text'])?$post['f_text']:null;

   if($f_id && ctype_digit($request['f_prd_id']) &&$request['f_text'])
   {
       $this->api_model->updateFeed($request,$id);

         echo json_encode(array('msg' => 'Message successfully deleted'));
         die();
   }
   else
   {
       $return = array('error' => 'Partial Content');
       echo json_encode($return);
       die();
   }

   }

    public function send_rfq_comments()
    {
        $request = $this->input->post();
        $save['rfqn_rfq_id'] = isset($request['rfq_id']) ? $request['rfq_id'] : 0; 
        $save['rfqn_comments'] = isset($request['rfqn_comments']) ? $request['rfqn_comments'] : null; 
        $save['rfqn_supplier'] = isset($request['buyr_id']) ? $request['buyr_id'] : 0; 
        $save['rfqn_added_by'] = $this->uid; 
       
            if ( $save['rfqn_rfq_id'] && $save['rfqn_supplier'] && $save['rfqn_added_by'] && $save['rfqn_comments'] ) {

                    $this->api_model->sendRFQComments($save);

            $chat['message'] = $save['rfqn_comments'];
            $chat['type'] = 'rfq';
            $chat['time'] = date('h:i A') . ' | ' . date('M d');
            $chat['from'] = $this->u_name;
            $chat['rfq_id'] = $save['rfqn_rfq_id'];
            $chat['rfq_link'] = site_url('product/rfq-list/'. encryptor($save['rfqn_rfq_id']));
            $chat['from_img'] = 'assets/uploads/avatar/' . $this->usr_avatar;
 
            $res = $this->sendPusherMessage(encryptor($save['rfqn_supplier']), 'rfq-event', $chat);
            $det = $this->product_model->getUserById($save['rfqn_supplier']);

        if ($det['usr_pushy_token']) {
                 $push['type'] = 'recieved';
                 $push['msg_type'] = 'rfq_chat_notfication';
                 $sup_det = $this->product_model->getUserById($this->uid);
                 $push['from_id'] = encryptor($sup_det['usr_supplier']);
                 $push['content'] = $save['rfqn_comments'];
                 $push['from_name'] = $this->u_name;
                 $push['time'] = date("Y-m-d H:i:s");
                 $push['rfq_id'] = $save['rfqn_rfq_id'];
  
             if($this->common_model->checkIsBuyer($save['rfqn_supplier']))
             {

                  $this->common_model->sendPushNotification($push, 
                                         $det['usr_pushy_token'], '');
             }
            }

                echo json_encode(array('msg' => 'Comment added successfully ')); die();
                  }
            else 
                    {

                    echo json_encode(array('error' => 'Partial Content')); die();
                     
                    }        
    }



     public function istokenValid()
    {
        $method = $this->uri->segment(2);
        if ((!$this->input->get_request_header('Token', true) || !$this->input->get_request_header('Token'))) {
            $return = array('status' => false ,'error' => 'Invalid Access Token');
            echo json_encode($return);
            die();
        }else{
            if (!$this->api_auth->checkTokenExist($this->input->get_request_header('Token'), $method)) {
                $return = array('status' => false ,'error' => 'Invalid Access Token');
                echo json_encode($return);
                die();
            } else {
                 $return = array('status' => true ,'msg' => 'Valid Access Token');
                 echo json_encode($return);
            }
        }
    }


    public function followers()
    {
        $request = $this->input->post();
        $limit = isset($request['limit'])?$request['limit']:30;
        $offset = isset($request['offset'])?$request['offset']:0;
        
        $followers = $this->api_model->getFollowers($this->sup_id,$limit,$offset);
         echo json_encode($followers);
         die();
    }




    function newproduct() {

        $post = json_decode($_POST['data'],true);

        $save['prd_name_en']=$prd_name_en = isset($post['productNameEn'])?$post['productNameEn']:null;
        $save['prd_moq']=$prd_moq = isset($post['moq'])?$post['moq']:null;
        $save['prd_is_stock_prod']=$prd_is_stock_prod = isset($post['isStockProduct'])?$post['isStockProduct']:null;
        $save['prd_unit']=$prd_unit = isset($post['unitId'])?$post['unitId']:null;
        $save['prd_qty']=$prd_qty = isset($post['qty'])?$post['qty']:null;
        $save['prd_price_min']=$prd_price_min = isset($post['minPrice'])?$post['minPrice']:null;
        $save['prd_price_max']=$prd_price_max = isset($post['maxPrice'])?$post['maxPrice']:null;
        $save['prd_offer_price_min']=$prd_offer_price_min = isset($post['minOffer'])?$post['minOffer']:null;
        $save['prd_offer_price_max']=$prd_offer_price_max = isset($post['maxOffer'])?$post['maxOffer']:null;
        $save['prd_desc']=$prd_desc = isset($post['description'])?$post['description']:null;
        $save['prd_video']=isset($post['videoUrl'])?$post['videoUrl']:null;
        $save['prd_name_ar']=isset($post['productNameAr'])?$post['productNameAr']:null;
        $save['prd_name_ch']=isset($post['productNameCh'])?$post['productNameCh']:null;
        $prd_category = isset($post['categoryId'])?$post['categoryId']:null;
        $save['prd_stock_qty']=$prd_stock_qty=isset($post['prd_stock_qty'])?
                                                $post['prd_stock_qty']:0;
        $save['prd_oem']=isset($post['prd_oem'])?$post['prd_oem']:0;
        $save['prd_hot_selling']=isset($post['prd_hot_selling'])?$post['prd_hot_selling']:0;
        $save['prd_is_show_on_profile']=isset($post['prd_profile'])?$post['prd_profile']:0;
        $save['prd_is_main_product']=isset($post['prd_is_main'])?$post['prd_is_main']:0;

        $save['prd_is_flash_sale']=isset($post['prd_is_flash_sale'])?$post['prd_is_flash_sale']:0;

       $fileCount = isset($_FILES['pictures']['name'])?count($_FILES['pictures']['name']):0;
        if($prd_name_en && is_numeric($prd_moq) &&  is_numeric($prd_unit)
         && is_numeric($prd_qty) && is_numeric($prd_price_min) && is_numeric($prd_price_max)  && is_numeric($prd_offer_price_min)
         && is_numeric($prd_offer_price_max) && is_array($prd_category) && $prd_desc && 
         $fileCount>0 )
        {
          

                if($prd_is_stock_prod ==1 && $prd_stock_qty<1)
                {
                     $return = array('status'=>false,'msg' => 'Please enter product stock quanity');
                 echo json_encode($return); 
                     die();
                }


             $save['prd_added_by']     = $this->uid;
             $save['prd_supplier']     = $this->sup_id;
            if ($prdId = $this->api_model->addProduct($post,$save)) {
           
              
              
                
                if($fileCount>0)
                {
                    $this->load->library('upload');
                    $up = array();
                    for ($j = 0; $j < $fileCount; $j++) {
                         $data = array();
                     
                         $newFileName = microtime(true) . $_FILES['pictures']['name'][$j];
                         $config['upload_path'] = FILE_UPLOAD_PATH . 'product/';
                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                         $config['file_name'] = $newFileName;
                         $this->upload->initialize($config);
                         $_FILES['prd_image_tmp']['name'] = $_FILES['pictures']['name'][$j];
                         $_FILES['prd_image_tmp']['type'] = $_FILES['pictures']['type'][$j];
                         $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['pictures']['tmp_name'][$j];
                         $_FILES['prd_image_tmp']['error'] = $_FILES['pictures']['error'][$j];
                         $_FILES['prd_image_tmp']['size'] = $_FILES['pictures']['size'][$j];
                         if (!$this->upload->do_upload('prd_image_tmp')) {
                              $up = array('error' => $this->upload->display_errors());
                         } else {
                              $data = array('upload_data' => $this->upload->data());
                            
                              $this->product_model->addImages(array('pimg_product' => $prdId, 'pimg_image' => $data['upload_data']['file_name']));
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 275, 206);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 154, 116);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 207, 180);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 173, 150);
                              resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 450, 338);
                         }
                    }
                }
                
              echo json_encode(array('msg' => 'Product added  successfully'));
              die();
                
        }
    }
        else 
        {
          $return = array('error' => 'Partial Content');
            echo json_encode($return); 
             die();
       
    }

    }

   function max_image() {
    echo json_encode(array('max_prdimg'=>get_settings_by_key('prod_img_limit')));
    die();
    }
       


    public function update_prd_stat()
    {
        $request = $this->input->post();
        $prd_id = isset($request['prd_id'])?encryptor($request['prd_id'],'D'):null;
        $status = isset($request['status'])?$request['status']:null;
                    if( $prd_id  && in_array($status, ['1','2','0']) )
                    {
                        if($status==2)
                          $updt_stat = $this->product_model->deleteProduct($prd_id);
                        else
                        {
                              if ($updt_stat = $this->common_model->changeStatus($prd_id, $status, 'products', 'prd_status', 'prd_id')) {
                             generate_log(array(
                                 'log_title' => 'Status changed',
                                 'log_desc' => 'Product status changes',
                                 'log_controller' => 'product-status-changed',
                                 'log_action' => 'U',
                                 'log_ref_id' => $prd_id,
                                 'log_added_by' => $this->uid,));
                                }
                        }

                      if($updt_stat)
                         $return = array('status' => true,'msg' => 'Product status updated successfully');
                         else
                         $return = array('status' => true,'error' => 'Product status updation failed');
                    }
                     else 
                     $return = array('error' => 'Partial Content');

                 echo json_encode($return);  die();
             
       
          }

public function product_view()
{

      if($prd_id = encryptor($this->input->post('prd_id'),'D'))
      {
                 $return =  $this->product_model->getSupplierProductDetails($prd_id);


             $prdData['productNameEn'] =$return['prd_name_en'];
             $prdData['productNameAr'] =$return['prd_name_ar'];
             $prdData['productNameCh'] =$return['prd_name_ch'];
             $prdData['description'] =$return['prd_desc'];
  

             $prdData['isStockProduct'] =$return['prd_is_stock_prod'];
             $prdData['maxOffer'] =$return['prd_offer_price_max'];
             $prdData['maxPrice'] =$return['prd_price_max'];
             $prdData['prd_stock_qty'] =$return['prd_stock_qty'];
             $prdData['moq'] =$return['prd_moq'];
             $prdData['minOffer'] =$return['prd_offer_price_min'];
             $prdData['minPrice'] =$return['prd_price_min'];
             $prdData['qty'] =$return['prd_qty'];
             $prdData['unitId'] =$return['prd_unit'];

             $prdData['videoUrl'] =$return['prd_video'];
             $prdData['prd_oem'] =$return['prd_oem'];
             $prdData['prd_hot_selling'] =$return['prd_hot_selling'];
             $prdData['prd_profile'] =$return['prd_is_show_on_profile'];
             $prdData['prd_is_main'] =$return['prd_is_main_product'];
             $prdData['prd_flash_sale'] =$return['prd_is_flash_sale'];
             $prdData['default_image'] = $return['default_image'];
             $prdData['product_images'] = $return['images'];

$i=$j=$k=0;
foreach($return['catId'] as $catId)
{
    $prdData['categoryId'][$j] = intval($catId);
    $j++;
}

foreach($return['keyword'] as $pkeyword)
{
    $prdData['productKeyWord'][$i]['ch'] = $pkeyword['pkwd_val_ch'];
    $prdData['productKeyWord'][$i]['ar'] = $pkeyword['pkwd_val_ar'];
    $prdData['productKeyWord'][$i]['en'] = $pkeyword['pkwd_val_en'];
    $i++;
}
            
foreach($return['specification'] as $specification)
{
    $prdData['productSpecs'][$k]['en']['key'] = $specification['psp_key_en'];
    $prdData['productSpecs'][$k]['en']['val'] = $specification['psp_val_en'];
                                              
    $prdData['productSpecs'][$k]['ch']['key'] = $specification['psp_key_ch'];
     $prdData['productSpecs'][$k]['ch']['val']= $specification['psp_val_ch'];
                                             
    $prdData['productSpecs'][$k]['ar']['key'] =  $specification['psp_key_ar'];
     $prdData['productSpecs'][$k]['ar']['key'] = $specification['psp_val_ar'];
    $k++;
}
  echo json_encode($prdData);  die();
  }
       
  echo json_encode(array('error' => 'Partial Content'));  die();
}



    function update_product() {

        $post = json_decode($_POST['data'],true);

        $save['prd_name_en']=$prd_name_en = isset($post['productNameEn'])?$post['productNameEn']:null;
        $save['prd_moq']=$prd_moq = isset($post['moq'])?$post['moq']:null;
        $save['prd_is_stock_prod']=$prd_is_stock_prod = isset($post['isStockProduct'])?$post['isStockProduct']:null;
        $save['prd_unit']=$prd_unit = isset($post['unitId'])?$post['unitId']:null;
        $save['prd_qty']=$prd_qty = isset($post['qty'])?$post['qty']:null;
        $save['prd_price_min']=$prd_price_min = isset($post['minPrice'])?$post['minPrice']:null;
        $save['prd_price_max']=$prd_price_max = isset($post['maxPrice'])?$post['maxPrice']:null;
        $save['prd_offer_price_min']=$prd_offer_price_min = isset($post['minOffer'])?$post['minOffer']:null;
        $save['prd_offer_price_max']=$prd_offer_price_max = isset($post['maxOffer'])?$post['maxOffer']:null;
        $save['prd_desc']=$prd_desc = isset($post['description'])?$post['description']:null;
        $save['prd_video']=isset($post['videoUrl'])?$post['videoUrl']:null;
        $save['prd_name_ar']=isset($post['productNameAr'])?$post['productNameAr']:null;
        $save['prd_name_ch']=isset($post['productNameCh'])?$post['productNameCh']:null;
        $prd_category = isset($post['categoryId'])?$post['categoryId']:null;
        $save['prd_stock_qty']=$prd_stock_qty=isset($post['prd_stock_qty'])?
                                  $post['prd_stock_qty']:0;

           $save['prd_oem']=isset($post['prd_oem'])?$post['prd_oem']:0;
           $save['prd_hot_selling']=isset($post['prd_hot_selling'])?$post['prd_hot_selling']:0;
           $save['prd_is_show_on_profile']=isset($post['prd_profile'])?$post['prd_profile']:0;
           $save['prd_is_main_product']=isset($post['prd_is_main'])?$post['prd_is_main']:0;
           $save['prd_is_flash_sale']=isset($post['prd_is_flash_sale'])?$post['prd_is_flash_sale']:0;
           $post['prd_id'] = encryptor($post['prd_id'],'D');

        if($prd_name_en && is_numeric($prd_moq) &&  is_numeric($prd_unit)
         && is_numeric($prd_qty) && is_numeric($prd_price_min) && is_numeric($prd_price_max)  && is_numeric($prd_offer_price_min)
         && is_numeric($prd_offer_price_max) && is_array($prd_category) && $prd_desc &&
         $post['prd_id'])
        {
             $fileCount = isset($_FILES['pictures']['name'])?count($_FILES['pictures']['name']):0;


                if($prd_is_stock_prod == 1 && $prd_stock_qty<1)
                {
                     $return = array('status'=>false,'msg' => 'Please enter product stock quanity');
                 echo json_encode($return); 
                     die();
                }

                if($this->api_model->getImagesCount($post['prd_id']) < 1 && $fileCount < 1)
                {
                     $return = array('status'=>false,'msg' => 'Please upload product image');
                 echo json_encode($return); 
                     die();
                }

            if ($prdId = $this->api_model->updateProduct($post,$save)) {
           
              
                
                
                if($fileCount>0)
                {
                    $this->load->library('upload');
                    $up = array();
                    for ($j = 0; $j < $fileCount; $j++) {
                         $data = array();
                     
                         $newFileName = microtime(true) . $_FILES['pictures']['name'][$j];
                         $config['upload_path'] = FILE_UPLOAD_PATH . 'product/';
                         $config['allowed_types'] = 'gif|jpg|png|jpeg';
                         $config['file_name'] = $newFileName;
                         $this->upload->initialize($config);
                         $_FILES['prd_image_tmp']['name'] = $_FILES['pictures']['name'][$j];
                         $_FILES['prd_image_tmp']['type'] = $_FILES['pictures']['type'][$j];
                         $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['pictures']['tmp_name'][$j];
                         $_FILES['prd_image_tmp']['error'] = $_FILES['pictures']['error'][$j];
                         $_FILES['prd_image_tmp']['size'] = $_FILES['pictures']['size'][$j];
                         if (!$this->upload->do_upload('prd_image_tmp')) {
                              $up = array('error' => $this->upload->display_errors());
                         } else {
                              $data = array('upload_data' => $this->upload->data());
                            
                              $this->product_model->addImages(array('pimg_product' => $prdId, 'pimg_image' => $data['upload_data']['file_name']));
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 275, 206);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 154, 116);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 207, 180);
                              // resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 173, 150);
                              resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 450, 338);
                         }
                    }
                }
                
              echo json_encode(array('msg' => 'Product updated successfully'));
              die();
                
        }
    }
        else 
        {
          $return = array('error' => 'Partial Content');
            echo json_encode($return); 
             die();
       
    }

    }
 

        public function delete_image()
        {
         if($pimg_id = $this->input->post('pimg_id')) {

             $this->product_model->removePrductImage($pimg_id);    
             $return = array('status' => true, 'msg' => 'Product image successfully deleted');
                     }
            else
            $return = array('error' => 'Partial Content');

                echo json_encode($return);   die();
                    
        }


       public function change_order_status() {

            $orderId = $this->input->post('ord_id');

            if (!empty($orderId) && isset($_POST['status'])) {
                 if ($this->api_model->changeOrderStatuse($orderId, $_POST['status'])) {
                  echo json_encode(array('status' => true, 'msg' => 'Order status changed'));die();
                 } else {
                      $return = array('error' => 'Partial Content');
                 echo json_encode($return);   die();

                 }
            }
       }


       public function forgot_password() {    

        $email=isset($_POST['email'])?$_POST['email']:null;
        $code=isset($_POST['forgot_code'])?$_POST['forgot_code']:null;
        $otp=isset($_POST['forgot_otp'])?trim($_POST['forgot_otp']):null;

            if ($email) {

                      $otp = rand(1000, 9999);
                      $forgotten = $this->ion_auth->forgotten_password($email, $otp);
                      if ($forgotten) { //if there were no errors
                           $user = $this->user_model->getUserByIdentity($email);
                           $fname = isset($user['usr_first_name']) ? $user['usr_first_name'] : '';
                           $lname = isset($user['usr_last_name']) ? $user['usr_last_name'] : '';
                           $code = isset($forgotten['forgotten_password_code']) ? $forgotten['forgotten_password_code'] : '';
                           /* Reset url sent to mail */
                           $this->mail->isSMTP();
                           $this->mail->Host = MAIL_HOST;
                           $this->mail->SMTPAuth = true;
                           $this->mail->Username = MAIL_USERNAME;
                           $this->mail->Password = MAIL_PASSWORD;
                           $this->mail->SMTPSecure = 'ssl';
                           $this->mail->Port = 465;
                           $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                           $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                           $this->mail->addAddress($email);
                           $this->mail->Subject = 'Reset your fujeeka password';
                           $this->mail->isHTML(true);
                           $mailData['name'] = $fname . ' ' . $lname;
                           $mailData['email'] = $email;
                           $mailData['url'] = site_url("user/reset-password/" . $code);
                           $mailData['otp'] = $otp;
                           $this->mail->Body = $this->load->view('user/forget-mail-template', $mailData, true);
                           $res = $this->mail->send();
                           //print_r($mailData);
                         //   debug($this->mail->ErrorInfo);
                           /* Reset url sent to mail */

                     echo json_encode(array('status' => true, 'msg' => 'Please check inbox',
                               'frgtPassCode' => $forgotten['forgotten_password_code'])); die();
                      }
            else {
            echo json_encode(array('status' => false, 'msg' => 'Invalid Email')); die();
                      }

            } else {

                 if (!empty($code) && !empty($otp)){
                      $user = $this->ion_auth->forgotten_password_check($code);

                      if (isset($user->usr_forgotten_password_otp) && ($user->usr_forgotten_password_otp == $otp)) {
                       
                     echo json_encode(array('status' => true , 'msg' => 'otp verified')); die();

                      } else {
                     echo json_encode(array('status' => false , 'msg' => 'invalid otp')); die();
                      }
                 } 
                 else {
                     echo json_encode(array('error' => 'Partial Content')); die();
                      }
            }
       }


   //reset password - final step for forgotten password
       public function reset_password() {

                $code=isset($_POST['forgot_code'])?$_POST['forgot_code']:null;
                $password=isset($_POST['password'])?$_POST['password']:null;
                $cpassword=isset($_POST['confirm'])?$_POST['confirm']:null;


            if ($code && $password && $cpassword) {
             
            $user = $this->ion_auth->forgotten_password_check($code);

            if ($user) {

  $this->form_validation->set_rules('password', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm]');
                 $this->form_validation->set_rules('confirm', 'Confirm New Password', 'required');

                    if ($this->form_validation->run() == false) {
                   echo json_encode(array('status' => true , 'msg' => validation_errors())); die();
                    }
                    else
                    {
                        $identity = $user->{$this->config->item('identity', 'ion_auth')};
           
                           if ($change = $this->ion_auth->reset_password($identity, $password)) {

                                   echo json_encode(array('status' => true , 'msg' => 'password updated successfully')); die();
                           } else {
                                 echo json_encode(array('status' => false , 'msg' => 'password updation failed')); die();
                           }
                    }

  
           
        }
      
         else {
               echo json_encode(array('status' => false , 'msg' => 'invalid user code')); die();
            }
                
        }
          else{
            echo json_encode(array('error' => 'Partial Content')); die();
        }

       }

   //reset password - final step for forgotten password

           public function sendPushy($feed_id, $f_text, $f_prd_id)
    {
        $followers = $this->feed->getFollowers($this->sup_id);
        if ($followers) {
            $push['type'] = 'notification';
            $push['msg_type'] = 'feed';
            $push['content'] = $f_text;
            $push['image'] = $this->feed->getProductImage($f_prd_id);
            $push['from_name'] = $this->u_name;
            $push['time'] = date("Y-m-d H:i:s");
            $this->common_model->sendPushNotification($push, $followers, '');
        }
        return 1;
    }


    //start-function search in feeds by keyword

    public function search_feed()
    {
        $request = $this->input->post();
        $search_key =  isset($request['keyword'])?trim($request['keyword']):null;
        $offset =  isset($request['offset'])?$request['offset']:0;
        $limit =  isset($request['limit'])?$request['limit']:30; 

     if($search_key){
    echo json_encode($this->api_model->getFeeds($this->sup_id, $offset, $limit,$search_key));die();
            }        
            else
                echo json_encode(array('error' => 'Partial Content')); die();

    }


public function delete_feed()
    {

   $request = $this->input->post();
   $feed_id =  isset($request['feed_id'])?encryptor($request['feed_id'],'D'):0; 

       if($feed_id){

                  $this->feed->delete($feed_id);

       echo json_encode(array('status' => true , 'msg' => 'feed deleted successfully')); 
       die();
            }        
            else
                echo json_encode(array('error' => 'Partial Content')); die();

    }
     //end-function search in feeds by keyword


    public function change_password()

    {

            $request = $this->input->post();
            $old_password = isset($request['old_password'])?$request['old_password']:null;
            $password = isset($request['password'])?$request['password']:null;
            $confirm_password = isset($request['confirm_password'])?
                                $request['confirm_password']:null;

    if($old_password && $password && $confirm_password)
{

if($password == $confirm_password) {
    $password_length = strlen($password);
    $min_length = $this->config->item('min_password_length', 'ion_auth');
    $max_length = $this->config->item('max_password_length', 'ion_auth');
   if ($password_length >= $min_length && $password_length <= $max_length)
                        {
                        if ($change = $this->ion_auth->change_password($this->usr_email,$old_password, $password)) {
                        echo json_encode(array('status' => true , 'msg' => 'Password changed successfully')); die();
                   
                        } else {
                        echo json_encode(array('status' => false , 'msg' => 'Inavalid old password')); die();
                   
                        }

            }else {
                $msg = "Password should be minimum $min_length and maximum $max_length";
         echo json_encode(array('status' =>false,'msg' => $msg)); die();
            }

        } else {
         echo json_encode(array('status' =>false,'msg' => 'mismatch in confirm password')); die();
            }

    } else {
    echo json_encode(array('status' =>false,'error' => 'Partial Content')); die();
            }


  }


    public function read_message()

    {
          $request = $this->input->post();
          if($uid = isset($request['from_id'])?$request['from_id']:0)
          {
                $this->api_model->makeMessageSeen($this->uid,$uid);
          echo json_encode(array('status' => true , 'msg' => 'status changed successfully')); die();

          }
          else
          {
                echo json_encode(array('status' =>false,'error' => 'Partial Content')); die();
          }


    }


}
