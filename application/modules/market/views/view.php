<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Market Places</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/update", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input value="<?php echo $marketPlace['mar_id'];?>" type="hidden" name="mar_id"/>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Code</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter market code" value="<?php echo $marketPlace['mar_code'];?>" type="text" class="form-control col-md-7 col-xs-12" name="market[mar_code]" id="mar_code" placeholder="Market code"/>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter name" value="<?php echo $marketPlace['mar_name'];?>" type="text" class="form-control col-md-7 col-xs-12" name="market[mar_name]" id="cat_title" placeholder="Market Name"/>
                              </div>
                         </div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select country"  class="form-control col-md-7 col-xs-12 bindToDropdown"
                                           data-dflt-select="Select State" name="market[mar_country_id]"
                                           data-url="<?php echo site_url('states/getStatesByCountry');?>"
                                           data-bind="cmbModel">
                                        <option value=''>Select Country</option>
                                        <?php foreach ((array) $country as $key => $value) {?>
                                               <option <?php echo ($value['ctr_id'] == $marketPlace['mar_country_id']) ? 'selected="selected"' : '';?>
                                                    value="<?php echo $value['ctr_id'];?>"><?php echo $value['ctr_name'];?></option>
                                               <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select state" class="cmbModel select2_group form-control bindToDropdown" 
                                           data-url="<?php echo site_url('district/getDistrictByState');?>" data-bind="cmbDistrict"
                                           data-dflt-select="Select district" name="market[mar_state_id]">
                                        <option value=''>Select country first</option>
                                        <?php foreach ((array) $states as $key => $value) {?>
                                               <option <?php echo ($value['stt_id'] == $marketPlace['mar_state_id']) ? 'selected="selected"' : '';?>
                                                    value="<?php echo $value['stt_id'];?>"><?php echo $value['stt_name'];?></option>
                                               <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">District</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select district" 
                                           class="cmbDistrict select2_group form-control" name="market[mar_district]">
                                        <option value="">Select District</option>
                                        <?php
                                          if (!empty($district)) {
                                               foreach ($district as $key => $value) {
                                                    ?>
                                                    <option <?php echo ($value['dit_id'] == $marketPlace['mar_district']) ? 'selected="selected"' : '';?> 
                                                         value="<?php echo $value['dit_id'];?>"><?php echo $value['dit_district'];?></option>
                                                         <?php
                                                    }
                                               }
                                             ?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter number" value="<?php echo $marketPlace['mar_contact_number'];?>" type="text" 
                                          class="form-control col-md-7 col-xs-12" name="market[mar_contact_number]"  placeholder="Contact Number"
                                          required data-parsley-required-message="Please enter contact number"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="email" class="form-control col-md-7 col-xs-12" name="market[mar_email]" 
                                   value="<?php echo $marketPlace['mar_email'];?>"  placeholder="Email" 
                                   required data-parsley-required-message="Please enter email"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12 txtAddress" id="pac-input" 
                                          value="<?php echo $marketPlace['mar_address'];?>" autocomplete="off" name="market[mar_address]" placeholder="Address"/>
                              </div>
                         </div>

                         <!-- -->
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Find your location</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div id="map"></div>
                                   <div id="infowindow-content">
                                        <img src="" width="16" height="16" id="place-icon">
                                        <span id="place-name"  class="title"></span><br>
                                        <span id="place-address"></span>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Latitude</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" id="latitude" name="market[mar_lat]" 
                                          value="<?php echo $marketPlace['mar_lat'];?>" placeholder="Latitude"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Longitude</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" id="longitude" name="market[mar_long]" 
                                          value="<?php echo $marketPlace['mar_long'];?>" placeholder="Longitude"/>
                              </div>
                         </div>
                         <!-- -->
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     #map {
          height: 500px !important;
          width: 500px  !important;
          float: left !important;
     }
</style>

<script>
     $('input[type=file]').change(function (e) {
          var image = $('input[type=file]')[0].files[0].name;
          if (image)
          {
               $('#bImgAlt').attr('data-parsley-required', true);
          } else
          {
               $('#bImgAlt').attr('data-parsley-required', false);
          }
     });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR07gIkjnjfMhUqqj5WPZ3oUAjoo49wKQ&libraries=places&callback=initMap"
async defer></script>
<script>
     function initMap() {
          var latlng = new google.maps.LatLng(<?php echo $marketPlace['mar_lat'];?>, <?php echo $marketPlace['mar_long'];?>);
          var map = new google.maps.Map(document.getElementById('map'), {
               center: latlng,
               zoom: 13
          });
          var card = document.getElementById('pac-card');
          var input = document.getElementById('pac-input');
          var types = document.getElementById('type-selector');
          var strictBounds = document.getElementById('strict-bounds-selector');
          var geocoder = new google.maps.Geocoder();
//                    var myLatlng = new google.maps.LatLng(20.268455824834792,85.84099235520011);
          var autocomplete = new google.maps.places.Autocomplete(input);


          autocomplete.bindTo('bounds', map);
          autocomplete.setFields(
                  ['address_components', 'geometry', 'icon', 'name']);

          var infowindow = new google.maps.InfoWindow();
          var infowindowContent = document.getElementById('infowindow-content');
          infowindow.setContent(infowindowContent);
          var marker = new google.maps.Marker({
               map: map,
               draggable: true,
               position: {lat: <?php echo $marketPlace['mar_lat'];?>, lng: <?php echo $marketPlace['mar_long'];?>},
               animation: google.maps.Animation.DROP,
               anchorPoint: new google.maps.Point(0, -29)
          });

          autocomplete.addListener('place_changed', function () {
               infowindow.close();
               marker.setVisible(false);
               var place = autocomplete.getPlace();
               if (!place.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
               }
               if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
               } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
               }
               marker.setPosition(place.geometry.location);
               marker.setVisible(true);

               var address = '';
               if (place.address_components) {
                    address = [
                         (place.address_components[0] && place.address_components[0].short_name || ''),
                         (place.address_components[1] && place.address_components[1].short_name || ''),
                         (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
               }

               $('.txtAddress').val(address);
               $('#latitude').val(place.geometry.location.lat());
               $('#longitude').val(place.geometry.location.lng());

               infowindowContent.children['place-icon'].src = place.icon;
               infowindowContent.children['place-name'].textContent = place.name;
               infowindowContent.children['place-address'].textContent = address;
               infowindow.open(map, marker);
          });
          /**/
          google.maps.event.addListener(map, 'click', function (event) {
               placeMarker(event.latLng);
          });
          function placeMarker(location) {
               if (marker == undefined) {
                    marker = new google.maps.Marker({
                         position: location,
                         map: map,
                         animation: google.maps.Animation.DROP
                    });
               } else {
                    marker.setPosition(location);
               }
               map.setCenter(location);

               geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                         if (results[0]) {
                              console.log(results);
                              $('.txtAddress').val(results[0].formatted_address);
                              $('#latitude').val(marker.getPosition().lat());
                              $('#longitude').val(marker.getPosition().lng());
                              infowindow.setContent(results[0].formatted_address);
                              infowindow.open(map, marker);
                         }
                    }
               });
          }
          google.maps.event.addListener(marker, 'dragend', function () {
               geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                         if (results[0]) {
                              console.log(results);
                              $('.txtAddress').val(results[0].formatted_address);
                              $('#latitude').val(marker.getPosition().lat());
                              $('#longitude').val(marker.getPosition().lng());
                              infowindow.setContent(results[0].formatted_address);
                              infowindow.open(map, marker);
                         }
                    }
               });
          });
          /**/
          function setupClickListener(id, types) {
               var radioButton = document.getElementById(id);
               radioButton.addEventListener('click', function () {
                    autocomplete.setTypes(types);
               });
          }

          setupClickListener('changetype-all', []);
          setupClickListener('changetype-address', ['address']);
          setupClickListener('changetype-establishment', ['establishment']);
          setupClickListener('changetype-geocode', ['geocode']);

          document.getElementById('use-strict-bounds').addEventListener('click', function () {
               console.log('Checkbox clicked! New state=' + this.checked);
               autocomplete.setOptions({strictBounds: this.checked});
          });
     }
</script>