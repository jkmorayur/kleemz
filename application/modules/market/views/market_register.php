<!-- Banner -->
<div class="sr-banner">
     <img src="images/banners/sr-banner.jpg" alt="Register as Supplier" class="img-fluid">
</div>
<!-- Banner ends -->

<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
               <div class="col-md-7">
                    <div class="cntent">
                         <h2 class="heading2">
                              Why register as Market?
                         </h2>
                         <p style="text-align: justify;">Building which referred as market. Building is one of our main modules in B2B business. Buildings are always listed in categories wise. Fujeeka team verifies the market once they receive requests from the Building team. This helps the buyer and seller to identify their market and their potential. Buyers can have a over view about the market once its approved by our Team which helps the supplier to be known by the market. The supplier will be added under the market which they belong to by Fujeeka team in its default category list. Understanding the potential of the market helps the buyer to focus more into their business and its growth and which helps the supplier to be updated according to their Market.</p>

                         <h5 class="mt-3">How market is approved:</h5>
                         <ul>
                           <li>The team will Request to approve the Market by Fujeeka.</li>
                           <li>Fujeeka will go for verfifcation</li>
                           <li>Once verified the market is approved</li>
                           <li>When the approval is done the market will be listed in Fujeeka</li>
                         </ul>
                    </div>
               </div>

               <div class="col-md-5">
                    <div class="card card-frms">
                         <h2 class="heading2">
                              REGISTER A MARKET
                         </h2>

                         <form data-parsley-validate="true" class="frmSupplierRegister" action="<?php echo site_url('market/market-register'); ?>">
                              <div class="form-group">
                                   <input required data-parsley-required-message="Enter market name" type="text" name="mar_name" class="form-control"
                                          id="sphone" placeholder="Market name">
                              </div>
                              <div class="form-group">
                                   <input type="email" name="mar_email" class="form-control" id="sEmail1"
                                          required data-parsley-required-message="Enter valid email"
                                          aria-describedby="emailHelp" placeholder="Enter email">
                                   <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                              </div>
                              <div class="form-group">
                                   <input data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" type="number" name="mar_contact_number" class="form-control" id="sphone" placeholder="Phone">
                              </div>
                              <div class="form-group">
                                   <textarea name="mar_desc" class="form-control" id="sComments" placeholder="Comments" rows="3"></textarea>
                              </div>

                              <div class="w-100 text-center">
                                   <button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">Send Request</button>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</section>

<style>
     p.parsley-success {
          color: #468847;
          background-color: #DFF0D8;
          border: 1px solid #D6E9C6
     }

     p.parsley-error {
          color: #B94A48;
          background-color: #F2DEDE;
          border: 1px solid #EED3D7
     }

     ul.parsley-errors-list {
          list-style: none;
          color: #E74C3C;
          padding-left: 0
     }

     input.parsley-error,
     select.parsley-error,
     textarea.parsley-error {
          background: #FAEDEC;
          border: 1px solid #E85445
     }

     .btn-group .parsley-errors-list {
          display: none
     }
</style>
