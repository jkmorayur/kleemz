<!-- content area -->
<div class="total-container">
      <div class="container">
        <h2 class="heading2 mt-0 mb-2">Market Place</h2>
        <div class="main-area">

               <div class="row">
                 <div class="search-result">
                   <div class="col-9 pl-0">

                      <div class="filter-cat border-0 p-0">
                        <h3>FILTER RESULTS BY : </h3>
                        <div class="s-filter">
                          <div class="s-cont">
                            <select class="form-control" id="cate">
                              <option value="">Select Category</option>
                              <?php foreach($categories as $cat){?>
                              <option value="<?php echo $cat['cat_id']?>"><?php echo $cat['cat_title'] ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="s-filter">
                          <div class="s-cont">
                            <select class="form-control" id="country">
                                 <option value="">All Markets</option>
                                 <?php foreach ($markets as $key => $value) { ?>
                                        <option <?php if($mar==$value['mar_id']) {?> selected <?php }?> value="<?php echo $value['mar_id']?>"><?php echo $value['mar_name'] ?></option>
                                 <?php  } ?>
                            </select>
                          </div>
                        </div>
                        <div class="s-filter">
                          <div class="s-cont">
                            <select class="form-control" id="building">
                            <option value="">Select Building</option>
                              <?php foreach($buildings as $bu){?>
                              <option <?php if($build==$bu['bld_id']) {?> selected <?php }?> value="<?php echo $bu['bld_id']?>"><?php echo $bu['bld_title'] ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                      </div>

                   </div>
                   <div class="col-3">
                     <div id="btnContainer">
                       <button class="btn btn_view btn-selected" onclick="gridView()"><i class="fa fa-th-large"></i> Grid</button>
                       <button class="btn btn_view" onclick="listView()"><i class="fa fa-bars"></i> List</button>
                     </div>
                   </div>
                   <div class="clearfix"></div>
                 </div>
               </div>
               <div class="row">
                 <!-- product container -->
                 <div class="ajax_loader1"></div>
                 <!-- <div class="prodct-container" id="product_listing"> -->
                   <!-- product container -->
                   <div class="prodct-container" id="product_listing">
                     <!-- single product -->
                     
                     <!-- /single product -->

                 </div>
                 <div class="col-12">
                    <div id="pagination_link">
                    </div>
                 </div>

                 <!-- / product container -->
               </div>
        </div>
      </div>
      <!-- 360 modal -->
      <div id="Modal-360" class="modal fade modal-360" role="dialog">
               <div class="modal-dialog">
                    <!--Filter Modal content-->
                    <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <div class="modal-body 360_body">
                         </div>
                    </div>
               </div>
          </div>
          <!-- /360 modal -->
  </div>
</div>
</div>
</div>
</div>
<style>
  .btn-selected{
    background-color: #ffffff !important;
    border-color: #28a745;
  }
</style>

    <!-- /content area -->