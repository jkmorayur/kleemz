<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Market Places</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Date</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Comments</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                     foreach ((array) $marketPlaces as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url('market/view/' . $value['mar_id']);?>">
                                               <td class="trVOE"><?php echo date('d M Y h:i:s', strtotime($value['mar_added_on']));?></td>
                                               <td class="trVOE"><?php echo $value['mar_email'];?></td>
                                               <td class="trVOE"><?php echo $value['mar_contact_number'];?></td>
                                               <td class="trVOE"><?php echo $value['mar_desc'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url('market/delete/' . $value['mar_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>