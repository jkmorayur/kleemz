<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  require './vendor/autoload.php';
  
  class market extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Market';
            $this->load->library('form_validation');
            $this->load->model('market_model', 'market');
            $this->load->model('category/category_model', 'category');
            $this->load->model('country/country_model', 'country');
            
            require_once(APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php');
            $this->mail = new PHPMailer;
       }

       public function index() {
            $this->lock_in();
            $data['marketPlaces'] = $this->market->gerMarketPlaces();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       function view($id = '') {
            $this->lock_in();
            $data = $this->market->gerMarketPlaces($id);
            $data['country'] = get_country_list();
            $data['categories'] = $this->category->getRootCategories();
            $country = isset($data['marketPlace']['mar_country_id']) ? $data['marketPlace']['mar_country_id'] : 0;
            $stateId = isset($data['marketPlace']['mar_state_id']) ? $data['marketPlace']['mar_state_id'] : 0;
            $data['states'] = get_state_province('', $country);
            $data['district'] = get_district_by_state($stateId);
            $data['categoriesSelected'] = explode(',', $data['categoriesSelected']);
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function add() {
            $this->lock_in();
            if (!empty($_POST) && isset($_POST['market'])) {
                 if (isset($_FILES['marketImage']['name']) && !empty($_FILES['marketImage']['name'])) {
                      /* Category banner */
                      $config = array();
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'market_place/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = rand(9999999, 0) . $_FILES['marketImage']['name'];

                      $this->load->library('upload');
                      $this->upload->initialize($config);

                      $pixel['x1'][0] = $_POST['x1'][0];
                      $pixel['y1'][0] = $_POST['y1'][0];
                      $pixel['x2'][0] = $_POST['x2'][0];
                      $pixel['y2'][0] = $_POST['y2'][0];
                      $pixel['w'][0] = $_POST['w'][0];
                      $pixel['h'][0] = $_POST['h'][0];

                      if (!$this->upload->do_upload('marketImage')) {
                           array('error' => $this->upload->display_errors());
                      } else {
                           $uploadData = $this->upload->data();
                           crop($this->upload->data(), $pixel);
                           $_POST['market']['mar_image'] = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                      }
                 }
                 if ($this->market->newMarket($_POST)) {
                      $this->session->set_flashdata('app_success', 'Row successfully added!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't add row, may be already exists, please search before create new town!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['categories'] = $this->category->getRootCategories();
                 $data['country'] = get_country_list();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       function removeImage($marketId) {
            if ($this->market->removeImage($marketId)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Image successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete image"));
                 die();
            }
       }

       function update() {
            $this->lock_in();
            if (!empty($_POST) && isset($_POST['market'])) {
                 if (isset($_FILES['marketImage']['name']) && !empty($_FILES['marketImage']['name'])) {
                      /* Category banner */
                      $config = array();
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'market_place/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = rand(9999999, 0) . $_FILES['marketImage']['name'];

                      $this->load->library('upload');
                      $this->upload->initialize($config);

                      $pixel['x1'][0] = $_POST['x1'][0];
                      $pixel['y1'][0] = $_POST['y1'][0];
                      $pixel['x2'][0] = $_POST['x2'][0];
                      $pixel['y2'][0] = $_POST['y2'][0];
                      $pixel['w'][0] = $_POST['w'][0];
                      $pixel['h'][0] = $_POST['h'][0];

                      if (!$this->upload->do_upload('marketImage')) {
                           array('error' => $this->upload->display_errors());
                      } else {
                           $uploadData = $this->upload->data();
                           crop($this->upload->data(), $pixel);
                           $_POST['market']['mar_image'] = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';


                  $newalt= isset($_POST['mar_imgalt_new'])?$_POST['mar_imgalt_new']:null;
                 if($newalt)
                  $_POST['market']['mar_imgalt']= $newalt;

                      }
                 }
                 if ($this->market->updateMarket($_POST)) {
                      $this->session->set_flashdata('app_success', 'Row successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't updated row!");
                 }
                 redirect(strtolower(__CLASS__));
            }
       }

       public function delete($id) {
            if ($this->market->deleteMarket($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Market place successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete market place"));
                 die();
            }
       }

       function getCaregoryByMarket() {
            if (isset($_POST['marketID'])) {
                 echo json_encode($this->market->getCaregoryByMarket($_POST['marketID']));
                 die();
            } else {
                 echo 'null';
                 die();
            }
       }

       public function markets() {
            $filter = array();
            if ($this->input->get('page')) {
                 $params = $this->input->get();
                 if ($params['category']) {
                      $filter['category'] = $params['category'];
                 }
                 if ($params['market']) {
                      $filter['market'] = $params['market'];
                 }
                 if ($params['building']) {
                      $filter['building'] = $params['building'];
                 }
                 $count = $this->market->countAllMarketSupplier($filter);
                 if ($params['order_by']) {
                      $filter['order_by'] = $params['order_by'];
                 }
                 if ($params['cls']) {
                      $filter['cls'] = $params['cls'];
                 }

                 pagination($this->input->get('page'), base_url() . "market/markets", $count, 'market_model', 'getMarketSuppliers', $filter);
            }

            $this->page_title = 'Home | Markets';
            $this->template->set_layout('markets');
            $data['markets'] = $this->market->getMarkets();
            $data['buildings'] = $this->market->getBuildings();
            $data['categories'] = $this->market->getCategories();
            $data['build'] = isset($_GET['building']) ? $_GET['building'] : '';
            $data['mar'] = isset($_GET['market']) ? $_GET['market'] : '';
          //   $data['countries'] = $this->country->getCountryAssocMarket();
            $this->render_page(strtolower(__CLASS__) . '/markets', $data);
       }
       
       /**
        * New supplier request from site
        * Author : JK
        */
       function market_register() {

            if (!empty($this->input->post())) {
                 if ($this->market->marketRegister($this->input->post())) {

                      /* Mail to supplier */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress($this->input->post('mar_email'));
                      $this->mail->Subject = 'New market registration';
                      $this->mail->isHTML(true);
                      $mailData['email'] = $this->input->post('mar_email');
                      $this->mail->Body = $this->load->view('market-register-mail-template', $mailData, true);
                      $this->mail->send();
                      /* Mail end */

                      /* Mail to admin */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress(get_settings_by_key('admin_email'));
                      $this->mail->Subject = 'New market registration';
                      $this->mail->isHTML(true);
                      $this->mail->Body = $this->load->view('market-register-mail-template-admin', $this->input->post(), true);
                      $this->mail->send();
                      /* Mail end */

                      echo json_encode(array('status' => 'success', 'msg' => 'We will get back to you'));
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => 'Email already exists'));
                 }
            } else {
                 $this->page_title = 'Market registration';
                 $this->template->set_layout('common');
                 $this->render_page(strtolower(__CLASS__) . '/market_register');
            }
       }

       function registrations() {
            $data['marketPlaces'] = $this->market->gerMarketRegistrations();
            $this->render_page(strtolower(__CLASS__) . '/registrations', $data);
       }
  }  