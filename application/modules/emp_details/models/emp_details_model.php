<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class emp_details_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_city = TABLE_PREFIX . 'city';
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_state = TABLE_PREFIX . 'state';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_country = TABLE_PREFIX . 'country';
            $this->tbl_showroom = TABLE_PREFIX . 'showroom';
            $this->tbl_district = TABLE_PREFIX . 'district';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
       }

       public function getUsers() {

            if ($this->usr_grp == 'SP') {
                 $this->db->where($this->tbl_users . '.usr_supplier', $this->suplr);
                 $this->db->where($this->tbl_groups . '.id', 3);
                 $this->db->where($this->tbl_users . '.usr_active', 1);
            }
            if ($this->usr_grp == 'ST') {
                 $this->db->where($this->tbl_users . '.usr_id', $this->uid);
                 $this->db->where($this->tbl_users . '.usr_active', 1);
            }
            if (is_root_user()) {
                 $this->db->where($this->tbl_groups . '.id', 3)->or_where($this->tbl_groups . '.id', 5);
            }

            $this->db->where($this->tbl_users . '.usr_id !=', 1);
            return $this->db->select($this->tbl_users . '.*, ' .
                                    $this->tbl_users_groups . '.group_id as group_id, ' .
                                    $this->tbl_groups . '.name as group_name, ' .
                                    $this->tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                    'supp.usr_first_name AS sup_first_name, supp.usr_last_name AS sup_last_name,' .
                                    $this->tbl_supplier_master . '.*')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->join($this->tbl_users . ' branch', 'branch.usr_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_users . ' supp', 'supp.usr_id = ' . $this->tbl_users . '.usr_supplier', 'LEFT')
                            ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_users . '.usr_supplier', 'LEFT')
                            ->get($this->tbl_users)->result_array();
       }

       public function register($data) {
            if ($data) {
                 $username = isset($data['usr_first_name']) ? $data['usr_first_name'] : '';
                 $password = isset($data['usr_password']) ? $data['usr_password'] : '';
                 $email = isset($data['usr_email']) ? $data['usr_email'] : '';
                 $group = array($data['usr_group']); // Sets user to admin.

                 unset($data['usr_group']);
                 unset($data['usr_password']);
                 unset($data['usr_password_conf']);

                 if ($this->uid == 1 || $this->usr_grp == 'SP') { // Check if supper admin
                      $data['usr_active'] = 1;
                      $data['usr_is_mail_verified'] = 1;
                 }

                 $data['usr_added_by'] = $this->uid;
                 if ($lastInsertId = $this->ion_auth->register($username, $password, $email, $data, $group)) {
                      generate_log(array(
                          'log_title' => 'Create new user',
                          'log_desc' => 'New user added',
                          'log_controller' => strtolower(__CLASS__),
                          'log_action' => 'C',
                          'log_ref_id' => $lastInsertId,
                          'log_added_by' => $this->uid,
                      ));
                      return $lastInsertId;
                 } else {
                      return $this->ion_auth->errors();
                 }
            } else {
                 generate_log(array(
                     'log_title' => 'Create new user',
                     'log_desc' => 'Failed to create new user',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid,
                 ));

                 return false;
            }
       }

       public function update($data) {
            $id = isset($data['usr_id']) ? $data['usr_id'] : '';
            unset($data['usr_id']);

            /* Add supplier and buyer groups */
            if (isset($data['usr_group'])) {
                 if (is_array($data['usr_group'])) {
                      foreach ($data['usr_group'] as $value) {
                           $this->ion_auth->add_to_group($value, $id);
                      }
                 } else {
                      $this->ion_auth->add_to_group($data['usr_group'], $id);
                 }
            }
            if (isset($data['usr_password']) && empty($data['usr_password'])) {
                 unset($data['usr_password']);
            }
            unset($data['usr_password_conf']);

            $data['usr_active'] = 1;
            $data['usr_updated_by'] = $this->uid;
            $this->ion_auth->update($id, $data);

            generate_log(array(
                'log_title' => 'Update user',
                'log_desc' => 'Updated user details',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'U',
                'log_ref_id' => $id,
                'log_added_by' => $this->uid,
            ));

            return true;
       }

       public function getAllEmployees() {
            return $this->db->select(
                                    $this->tbl_users . '.*, ' .
                                    $this->tbl_users_groups . '.group_id as group_id, ' .
                                    $this->tbl_groups . '.name as group_name, ' .
                                    $this->tbl_groups . '.description as group_desc, ' . $this->tbl_showroom . '.*'
                            )
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id')
                            ->join($this->tbl_showroom, $this->tbl_showroom . '.shr_id = ' . $this->tbl_users . '.usr_supplier', 'left')
                            ->order_by($this->tbl_users . '.usr_first_name')
                            ->get($this->tbl_users)->result_array();
       }

       public function getStaffs() {
            $this->db->where($this->tbl_users . '.usr_supplier', $this->suplr);
            $this->db->where($this->tbl_users_groups . '.group_id', 3);
            $this->db->where($this->tbl_users . '.usr_active', 1);
            $this->db->where($this->tbl_users . '.usr_id !=', 1);
            return $this->db->select('usr_id,usr_first_name,usr_last_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->get($this->tbl_users)->result_array();
       }

       public function migrate($from, $to) {
            $mailbx = TABLE_PREFIX . 'mailboox_master';
            $prd = TABLE_PREFIX . 'products';
            $push = TABLE_PREFIX . 'pushnotification_master';
            // $mailbx = TABLE_PREFIX . 'user_access';
            $flag = false;
            $this->db->set('prd_added_by', $to)
                    ->where('prd_added_by', $from)
                    ->update($prd);
            if ($this->db->affected_rows()) {
                 $flag = true;
            }

            $this->db->set('mms_from', $to)
                    ->where('mms_from', $from)
                    ->update($mailbx);
            if ($this->db->affected_rows()) {
                 $flag = true;
            }

            $this->db->set('mms_to', $to)
                    ->where('mms_to', $from)
                    ->update($mailbx);
            if ($this->db->affected_rows()) {
                 $flag = true;
            }

            $this->db->set('pnm_sent_by', $to)
                    ->where('pnm_sent_by', $from)
                    ->update($push);
            if ($this->db->affected_rows()) {
                 $flag = true;
            }
            if ($flag) {
                 return true;
            } else {
                 return false;
            }
       }

       public function checkDataUnderEmployee($id) {
            $mailbx = TABLE_PREFIX . 'mailboox_master';
            $prd = TABLE_PREFIX . 'products';
            $push = TABLE_PREFIX . 'pushnotification_master';
            $count = $this->db->select('prd_id')->where('prd_added_by', $id)
                            ->get($prd)->num_rows();
            if ($count) {
                 return false;
            }

            $count = $this->db->select('mms_id')->where('mms_from', $id)
                            ->get($mailbx)->num_rows();
            if ($count) {
                 return false;
            }

            $count = $this->db->select('mms_id')->where('mms_to', $id)
                            ->get($mailbx)->num_rows();
            if ($count) {
                 return false;
            }

            $count = $this->db->select('pnm_id')->where('pnm_sent_by', $id)
                            ->get($push)->num_rows();
            if ($count) {
                 return false;
            }
            return true;
       }

  }
  