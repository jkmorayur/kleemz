<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Feeds</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Product</th>
                                        <th>Feed Text</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody> 
                                   <?php
                                          foreach ($feeds as $key => $value) {
                                               ?>
                                               <tr data-url="<?php echo site_url('feed/update/' . encryptor($value['f_id']));?>">
                                                    <td class="trVOE"><?php echo $value['prd_name_en'];?></td>
                                                    <td class="trVOE"><?php echo $value['f_text'];?></td>
                                                    <?php if (check_permission($controller, 'delete')) {?>
                                                         <td>
                                                              <a class="pencile deleteListItem" href="javascript:void(0);" data-url="<?php echo site_url('feed/delete/' . encryptor($value['f_id']));?>">
                                                                   <i class="fa fa-remove"></i>
                                                              </a>
                                                         </td>
                                                    <?php }?>
                                               </tr>
                                               <?php
                                          }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>