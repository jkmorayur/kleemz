<?php $suppliersid = isset($suppliers['supm_id']) ? $suppliers['supm_id'] : '';?>
<div class="right_col" role="main">
     <div class="">
          <div class="">
               <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="x_panel">
                         <?php echo form_open_multipart($controller . "/activate-supplier", array('id' => "frmSupplier", 'class' => "form-horizontal", 'data-parsley-validate' => "true"));?>
                         <input type="hidden" name="supm_id" value="<?php echo $suppliersid;?>" />
                         <div class="x_title">
                              <?php if (privilege_exists('SP') && isset($suppliers['supm_status']) && $suppliers['supm_status'] != 0) {?>
                                     <h2>Edit Your Profile </h2>
                                <?php } else {?>
                                     <h2>Market activation form 
                                          <small style="color: red;"><?php
                                               echo (!is_root_user() && isset($suppliers['supm_status']) && empty($suppliers['supm_status'])) ?
                                                       'Your request to activate as supplier is in progress, we will touch you soon!' : '';
                                               ?>
                                          </small>
                                     </h2>
                                <?php }?>
                              <div class="clearfix"></div>
                         </div>
                         <div class="x_content">
                              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                   <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                             <a href="javascript:;" goto="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Supplier Details</a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Personal Details
                                                  <small>(Optional)</small>
                                             </a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#tab_content3" role="tab" id="photos-tab" data-toggle="tab" aria-expanded="false">Photos</a>
                                        </li>
                                   </ul>
                                   <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Business name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter business name" 
                                                              value="<?php echo $suppliers['supm_name'];?>" required placeholder="Business name" type="text" 
                                                              name="supplier[supm_name]" id="supm_name" class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <?php
                                                         build_category_tree($this, $locations, 0);
                                                       ?>
                                                       <div class="div-category">
                                                            <ul class="li-category">
                                                                 <?php echo $locations?>
                                                            </ul>
                                                       </div>
                                                       <?php

                                                         function build_category_tree($f, &$output, $preselected, $parent = 0, $indent = "") {
                                                              $menucats = isset($f->_ci_cached_vars['suppliers']['categories']) ?
                                                                      $f->_ci_cached_vars['suppliers']['categories'] : array();
                                                              $parentCategories = $f->category_model->getCategoryChaild($parent);
                                                              foreach ($parentCategories as $key => $value) {
                                                                   $selected = (is_array($menucats) && in_array($value["cat_id"], $menucats)) ? "checked" : "";
                                                                   $output .= "<li>" . $indent . "<input " . $selected . " name='categories[]' value='" . $value["cat_id"] . "' type='checkbox' /><span>" . $value["cat_title"] . '</span></li>';
                                                                   if ($value["cat_id"] != $parent) {
                                                                        build_category_tree($f, $output, $preselected, $value["cat_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                   }
                                                              }
                                                         }
                                                       ?>
                                                  </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-12 divSelectedCategories"></div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Business Email</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input autocomplete="cutthesillyness"  value="<?php echo $suppliers['supm_email'];?>" 
                                                              placeholder="Business Email" type="text" name="supplier[supm_email]" id="supm_email" class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <?php
                                               if (isset($suppliers['contacts'])) {
                                                    foreach ((array) $suppliers['contacts'] as $key => $value) {
                                                         ?>
                                                         <div class="form-group">
                                                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Business contact number
                                                                   <span class="required">*</span>
                                                              </label>
                                                              <div class="col-md-6 col-sm-3 col-xs-12">
                                                                   <input type="text" id="mobile" name="upn_phone_number[]" required="required" data-parsley-required-message="Mobile required" maxlength="15" data-past=".usr_whatsapp" 
                                                                          value="<?php echo $value['spc_number'];?>" class="pastContent numOnly form-control col-md-7 col-xs-12">
                                                              </div>
                                                              <div class="col-md-1 col-sm-1 col-xs-12">
                                                                   <span style="cursor: pointer;" class="glyphicon glyphicon-minus btnAddRemoveKeys"></span>
                                                              </div>
                                                         </div>
                                                         <?php
                                                    }
                                               }
                                             ?>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Business contact number
                                                       <span class="required">*</span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-3 col-xs-12">
                                                       <input type="text" id="mobile" name="upn_phone_number[]" data-parsley-required-message="Mobile required" maxlength="15" data-past=".usr_whatsapp" 
                                                              class="pastContent numOnly form-control col-md-7 col-xs-12">
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-12">
                                                       <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMorePhone"></span>
                                                  </div>
                                             </div>
                                             <div class="divMobilePhone"></div>

                                             <?php if (is_root_user()) {?>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Market/Town</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <select required data-parsley-required-message="Select market"
                                                                      name="supplier[supm_market]" id="supm_market" class="form-control bindMetaTown" 
                                                                      data-dflt-select="Select Building">
                                                                   <option value="">Select Market/Town</option>
                                                                   <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                                                        <option value="<?php echo $value['mar_id'];?>"
                                                                                data-cnt="<?php echo $value['mar_country_id'];?>"
                                                                                data-stt="<?php echo $value['mar_state_id'];?>"
                                                                                data-dit="<?php echo $value['mar_district'];?>"
                                                                                data-lat="<?php echo $value['mar_lat'];?>"
                                                                                data-lan="<?php echo $value['mar_long'];?>"
                                                                                <?php echo ($value['mar_id'] == $suppliers['supm_market']) ? 'selected="selected"' : '';?>>
                                                                             <?php echo $value['mar_name'];?></option>
                                                                   <?php }?>
                                                              </select>
                                                         </div>
                                                    </div>
                                               <?php } else {?>
                                                    <input type="hidden" name="supplier[supm_market]" value="<?php echo get_logged_user('usr_market');?>"/>
                                               <?php }?>
                                             <!-- -->
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select data-parsley-required="false" data-parsley-required-message="Select country" 
                                                               class="form-control col-md-7 col-xs-12 bindToDropdown cmbCountry"
                                                               data-dflt-select="Select State" name="supplier[supm_country]"
                                                               data-url="<?php echo site_url('states/getStatesByCountry');?>"
                                                               data-bind="cmbModel" id="cmbCountry">
                                                            <option value="">Select Country</option>
                                                            <?php foreach ((array) $country as $key => $value) {?>
                                                                   <option value="<?php echo $value['ctr_id'];?>"
                                                                           <?php echo ($value['ctr_id'] == $suppliers['supm_country']) ? 'selected="selected"' : '';?>><?php echo $value['ctr_name'];?></option>
                                                                      <?php }?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select data-parsley-required="false" data-parsley-required-message="Select state" 
                                                               data-url="<?php echo site_url('district/getDistrictByState');?>" data-bind="cmbDistrict"
                                                               data-dflt-select="Select district" name="supplier[supm_state]"
                                                               class="cmbModel select2_group form-control bindToDropdown cmbState">
                                                            <option value="">Select country first</option>

                                                            <?php foreach ((array) $states as $key => $value) {?>
                                                                   <option value="<?php echo $value['stt_id'];?>"
                                                                           <?php echo ($value['stt_id'] == $suppliers['supm_state']) ? 'selected="selected"' : '';?>><?php echo $value['stt_name'];?></option>
                                                                      <?php }?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">District</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select data-parsley-required="false" data-parsley-required-message="Select district" 
                                                               class="cmbDistrict select2_group form-control cmbDistrict" name="supplier[supm_district]">
                                                            <option value="">Select state first</option>
                                                            <?php
                                                              foreach ((array) $district as $key => $value) {
                                                                   ?>
                                                                   <option value="<?php echo $value['dit_id'];?>"
                                                                           <?php echo ($value['dit_id'] == $suppliers['supm_district']) ? 'selected="selected"' : '';?>><?php echo $value['dit_district'];?></option>
                                                                           <?php
                                                                      }
                                                                    ?>
                                                       </select>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Find your location</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <div id="map"></div>
                                                       <div id="infowindow-content">
                                                            <img src="" width="16" height="16" id="place-icon">
                                                            <span id="place-name"  class="title"></span><br>
                                                            <span id="place-address"></span>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Latitude</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input type="text" class="form-control col-md-7 col-xs-12 txtLatitude" id="latitude"
                                                              name="supplier[supm_latitude]" value="<?php echo $suppliers['supm_latitude'];?>"
                                                              placeholder="Latitude"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Longitude</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input type="text" class="form-control col-md-7 col-xs-12 txtLongitude" id="longitude" 
                                                              name="supplier[supm_longitude]" value="<?php echo $suppliers['supm_longitude'];?>"
                                                              placeholder="Longitude"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <textarea style="width: 100%; height: 80px;" required data-parsley-required-message="Enter valid address" name="supplier[supm_address]" 
                                                                 id="supm_address_en" placeholder="Address"><?php echo $suppliers['supm_address'];?></textarea>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <textarea style="width: 460px; height: 80px;" name="supplier[supm_desc]" id="supm_desc"
                                                                 placeholder="Description"><?php echo $suppliers['supm_desc'];?></textarea>
                                                  </div>
                                             </div>
                                             <!-- -->
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Supplier keyword</label>
                                                  <div class="col-md-6 col-sm-3 col-xs-12">
                                                       <div class="table-responsive divVehDetailsSale">
                                                            <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                                 <thead>
                                                                      <tr>
                                                                           <th>Keyword</th>
                                                                           <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <?php
                                                                        if (!empty($suppliers['keywords'])) {
                                                                             foreach ((array) $suppliers['keywords'] as $key => $value) {
                                                                                  ?>
                                                                                  <tr>
                                                                                       <td>
                                                                                            <input value="<?php echo $value['supk_keyword_en'];?>" id="keywords_en" placeholder="Keyword" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                       </td>
                                                                                  </tr>
                                                                                  <?php
                                                                             }
                                                                        } else {
                                                                             ?>
                                                                             <tr>
                                                                                  <td>
                                                                                       <input id="keywords_en" placeholder="Keyword" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                  </td>
                                                                             </tr>
                                                                        <?php }?>
                                                                 </tbody>
                                                            </table>
                                                       </div>
                                                  </div>
                                             </div>
                                             <!-- -->

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Youtube</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Youtube" type="text" class="form-control col-md-7 col-xs-12" 
                                                              value="<?php echo $suppliers['supm_youtube'];?>" name="supplier[supm_youtube]" 
                                                              id="supm_official_video"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Facebook" type="text"
                                                              value="<?php echo $suppliers['supm_fb'];?>"
                                                              name="supplier[supm_fb]" id="supm_official_video"
                                                              class="form-control col-md-7 col-xs-12" />
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Twitter" type="text"
                                                              value="<?php echo $suppliers['supm_twitter'];?>"
                                                              name="supplier[supm_twitter]" id="supm_official_video"
                                                              class="form-control col-md-7 col-xs-12" />
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Google plus</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Google plus" type="text"
                                                              value="<?php echo $suppliers['supm_gplus'];?>"
                                                              name="supplier[supm_gplus]" id="supm_official_video"
                                                              class="form-control col-md-7 col-xs-12" />
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Linkedin</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Linkedin" type="text"
                                                              value="<?php echo $suppliers['supm_linkedin'];?>"
                                                              name="supplier[supm_linkedin]" id="supm_official_video"
                                                              class="form-control col-md-7 col-xs-12" />
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                             <input value="<?php echo $suppliers['usr_id'];?>" type="hidden" name="user[usr_id]" id="usr_id"/>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input data-parsley-required-message="Enter name" value="<?php echo $suppliers['usr_first_name'];?>" placeholder="Name" type="text" name="user[usr_first_name]" id="usr_first_name" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input data-parsley-required-message="Enter email"  data-parsley-remote="<?php echo site_url("supplier/checkIfValueExists")?>" data-parsley-remote-options='{ "type": "POST","data": { "field": "usr_email" ,"excludemail":"<?php echo $suppliers['usr_email'];?>"} }' data-parsley-remote-message="Email already exists" value="<?php echo $suppliers['usr_email'];?>" placeholder="Email" type="email" name="user[usr_email]" id="usr_email" 
                                                              class="form-control col-md-7 col-xs-12" autocomplete="new-email"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12 hidden">User name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo $suppliers['usr_first_name'];?>" placeholder="User name" type="hidden" name="user[usr_username]" id="usr_username"
                                                              class="form-control col-md-7 col-xs-12" autocomplete="new-password"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input autocomplete="new-password" placeholder="Password" type="password" name="user[usr_password]" id="usr_password" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                                  <span toggle="#usr_password" class="fa fa-eye field-icon toggle-password"></span>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input  data-parsley-equalto="#usr_password" data-parsley-trigger="change" data-parsley-equalto-message="Should be same as password" placeholder="Confirm Password" type="password" name="user[usr_password_confirm]" id="usr_password_confirm" placeholder="Confirm Password" type="password" name="user[usr_password_confirm]" id="usr_password_confirm" 
                                                               class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                                  <span toggle="#usr_password_confirm" class="fa fa-eye field-icon toggle-password"></span>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact number</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input data-parsley-required-message="Enter contact number" value="<?php echo $suppliers['usr_phone'];?>" placeholder="Contact number" type="text" name="user[usr_phone]" id="usr_phone" 
                                                              class="form-control col-md-7 col-xs-12 numOnly"/>
                                                  </div>

                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <textarea style="width: 480px; height: 80px;" name="user[usr_address]" placeholder="Address"><?php echo $suppliers['usr_address'];?></textarea>
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="photos-tab">
                                             <?php if (isset($suppliers['usr_avatar']) && !empty($suppliers['usr_avatar'])) {?>                            
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <div class="input-group">
                                                                   <?php echo img(array('src' => FILE_UPLOAD_PATH . 'avatar/' . $suppliers['usr_avatar'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                              </div>
                                                              <?php if ($suppliers['usr_avatar']) {?>
                                                                   <span class="help-block">
                                                                        <a data-url="<?php echo site_url('supplier/removeAvatar/' . $suppliers['usr_id']);?>" href="javascript:void(0);" 
                                                                           style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                   </span>
                                                              <?php }?>
                                                         </div>
                                                    </div>
                                               <?php }?>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Logo</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <div id="newupload">
                                                            <input type="hidden" id="x10" name="x10" />
                                                            <input type="hidden" id="y10" name="y10" />
                                                            <input type="hidden" id="x20" name="x20" />
                                                            <input type="hidden" id="y20" name="y20" />
                                                            <input type="hidden" id="w0" name="w0" />
                                                            <input type="hidden" id="h0" name="h0" />
                                                            <input <?php if (!isset($suppliers['usr_avatar']) || empty($suppliers['usr_avatar'])) {?> 
                                                                        data-parsley-required-message="Upload a file" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                                 type="file" class="form-control col-md-7 col-xs-12" name="userAvatar" id="image_file0" onchange="fileSelectHandler('0', '600', '300')" />
                                                            <img id="preview0" class="preview"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <!-- Panorama view -->
                                             <?php if (isset($suppliers['supm_panoramic_image']) && !empty($suppliers['supm_panoramic_image'])) {?>                            
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <div class="input-group">
                                                                   <?php echo img(array('src' => FILE_UPLOAD_PATH . 'panorama-shop/' . $suppliers['supm_panoramic_image'], 'height' => '200', 'width' => '700', 'id' => 'imgBrandImage'));?>
                                                              </div>
                                                              <?php if ($suppliers['supm_panoramic_image']) {?>
                                                                   <span class="help-block">
                                                                        <a data-url="<?php echo site_url('supplier/removePanorama/' . $suppliers['supm_id']);?>" href="javascript:void(0);" 
                                                                           style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                   </span>
                                                              <?php }?>
                                                         </div>
                                                    </div>
                                               <?php }?>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Panorama View</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <div id="newupload">
                                                            <input type="hidden" id="x11" name="x11" />
                                                            <input type="hidden" id="y11" name="y11" />
                                                            <input type="hidden" id="x21" name="x21" />
                                                            <input type="hidden" id="y21" name="y21" />
                                                            <input type="hidden" id="w1" name="w1" />
                                                            <input type="hidden" id="h1" name="h1" />
                                                            <input data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                                   type="file" class="form-control col-md-7 col-xs-12" name="panoramaImage" id="image_file1" onchange="fileSelectHandler('1', '600', '300')" />
                                                            <img id="preview1" class="preview"/>
                                                            <span class="help-inline">Choose 2160(W) X 1080(H)</span>
                                                       </div>
                                                  </div>
                                             </div>
                                             <!-- Banners -->
                                             <?php
                                               if (isset($suppliers['shop_images']) && !empty($suppliers['shop_images'])) {
                                                    $i = 0;
                                                    ?>
                                                    <div class="form-group">

                                                         <?php
                                                         foreach ($suppliers['shop_images'] as $key => $value) {
                                                              if ($i % 4 == 0) {
                                                                   ?>
                                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Shop Images</label>

                                                              <?php }?>
                                                              <div class="bannerImageGroupshop col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['ssi_id'];?>" style="padding-left: 10px;">
                                                                   <div class="input-group">
                                                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'shops/' . $value['ssi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                                   </div>
                                                                   <?php if ($value['ssi_image']) {?>
                                                                        <span class="help-block">
                                                                             <a data-url="<?php echo site_url('supplier/removeImage/' . $value['ssi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                        </span>
                                                                       <!-- <span class="help-block">
                                                                             <input <?php echo ($value['sbi_is_default'] == '1') ? 'checked="checked"' : '';?> class="btnSetDefaultImage" type="radio" 
                                                                                   name="default_home_banner" data-url="<?php echo site_url('supplier/setDefaultBannerImage/' . $value['sbi_id'] . '/' . $suppliers['supm_id']);?>"/>&nbsp; Make it default
                                                                        </span> -->
                                                                        <div id="altTag">
                                                                             <input placeholder="Alt tag for image" type="text" name="ssi_alttag[<?php echo $value['ssi_id']?>]" id="ssi_alttag" data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['ssi_alttag'];?>"
                                                                                    class="form-control col-md-9 col-xs-12"/>
                                                                        </div>
                                                                   <?php }?>
                                                              </div>

                                                              <?php
                                                              $i++;
                                                         }
                                                         ?>
                                                    </div>
                                               <?php }
                                             ?>
                                             <?php if (count($suppliers['shop_images']) < get_settings_by_key('sup_shop_img_limit')) {?>
                                                    <div class="form-group">
                                                         <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Shop Images 
                                                              <small>(maximum <?php echo get_settings_by_key('sup_shop_img_limit');?> images)</small>
                                                         </label>
                                                         <div class="col-md-5 col-sm-3 col-xs-12">
                                                              <div id="newupload">
                                                                   <input type="hidden" id="x13" name="x13[]" />
                                                                   <input type="hidden" id="y13" name="y13[]" />
                                                                   <input type="hidden" id="x23" name="x23[]" />
                                                                   <input type="hidden" id="y23" name="y23[]" />
                                                                   <input type="hidden" id="w3" name="w3[]" />
                                                                   <input type="hidden" id="h3" name="h3[]" />
                                                                   <input <?php if (!isset($suppliers['shop_images']) || empty($suppliers['shop_images'])) {?> 
                                                                             data-parsley-required-message="Please Upload image" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                                        type="file" class="form-control col-md-7 col-xs-12" name="shopImages[]" 
                                                                        id="image_file3" onchange="fileSelectHandler('3', '1800', '800', true)" />
                                                                   <img id="preview3" class="preview"/>
                                                                   <span class="help-inline">Choose 1800(W) X 800(H)</span>
                                                              </div>
                                                              <div id="altTag">
                                                                   <input placeholder="Alt tag for image" type="text" name="altshopImgs[]" id="altshopImg0" data-parsley-required-message="Please enter an alt tag for the image"
                                                                          class="form-control col-md-9 col-xs-12"/>
                                                              </div>
                                                         </div>

                                                         <div class="col-md-1 col-sm-1 col-xs-12">
                                                              <span style="float: right;cursor: pointer;" data-limit="<?php echo get_settings_by_key('sup_shop_img_limit') - count($suppliers['shop_images']);?>"
                                                                    class="glyphicon glyphicon-plus btnMoreShopImages"></span>

                                                         </div>

                                                    </div>

                                                    <div id="divMoreProductImages"></div>
                                               <?php }?>
                                             <!-- Banners -->
                                             <?php
                                               if (isset($suppliers['banner_images_web']) && !empty($suppliers['banner_images_web'])) {
                                                    $i = 0;
                                                    ?>
                                                    <div class="form-group">
                                                         <?php
                                                         foreach ($suppliers['banner_images_web'] as $key => $value) {
                                                              if ($i % 4 == 0) {
                                                                   ?>
                                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for website</label>
                                                              <?php }?>
                                                              <div class="bannerImageGroup col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['sbi_id'];?>" style="padding-left: 10px;">
                                                                   <div class="input-group">
                                                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'home_banner/' . $value['sbi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                                   </div>
                                                                   <?php if ($value['sbi_image']) {?>
                                                                        <span class="help-block">
                                                                             <a data-url="<?php echo site_url('supplier/removeSupplierBanner/' . $value['sbi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                        </span>
                                                                        <span class="help-block">
                                                                             <input class="btnSetDefaultImage" type="radio" name="2default_home_bannerweb" 
                                                                             <?php echo ($value['sbi_is_default'] == '1') ? 'checked="checked"' : '';?>
                                                                                    data-url="<?php echo site_url('supplier/setDefaultBannerImage/' . $value['sbi_id'] . '/' . $suppliers['supm_id']);?>"/>&nbsp; Make it default
                                                                        </span>


                                                                        <div id="altTag">
                                                                             <input placeholder="Alt tag for image" type="text" name="sbi_alttag[<?php echo $value['sbi_id']?>]" id="ssi_alttag" data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['sbi_alttag'];?>"
                                                                                    class="form-control col-md-9 col-xs-12"/>
                                                                        </div>

                                                                   <?php }?>
                                                              </div>

                                                              <?php
                                                              $i++;
                                                         }
                                                         ?>
                                                    </div>
                                               <?php }
                                             ?>

                                             <?php if (count($suppliers['banner_images_web']) < get_settings_by_key('sup_home_bnr_img_limit')) {?>

                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for website with 1280(W) X 400(H)
                                                              <small>(maximum <?php echo get_settings_by_key('sup_home_bnr_img_limit');?> images)</small>
                                                         </label>
                                                         <div class="col-md-5 col-sm-3 col-xs-12">
                                                              <div id="newupload">
                                                                   <input type="hidden" id="x19" name="x12[]" />
                                                                   <input type="hidden" id="y19" name="y12[]" />
                                                                   <input type="hidden" id="x29" name="x22[]" />
                                                                   <input type="hidden" id="y29" name="y22[]" />
                                                                   <input type="hidden" id="w9" name="w2[]" />
                                                                   <input type="hidden" id="h9" name="h2[]" />
                                                                   <input <?php if (!isset($suppliers['banner_images_web']) || empty($suppliers['banner_images_web'])) {?> 
                                                                             data-parsley-required-message="Please upload banner image" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                                        type="file" class="form-control col-md-7 col-xs-12" name="supm_home_banner[]" 
                                                                        id="image_file9" onchange="fileSelectHandler('9', '1280', '400', '1')" />
                                                                   <img id="preview9" class="preview"/>
                                                              </div>
                                                              <div id="altTag"><input placeholder="Alt tag for image" type="text" name="altSupBanImgs[]"id="altSupBanImg0" <?php if (!isset($suppliers['banner_images_web']) || empty($suppliers['banner_images_web'])) {?> 

                                                                                           data-parsley-required-message="Please enter an alt tag for the image" <?php }?> class="form-control col-md-9 col-xs-12"/></div>

                                                         </div>
                                                         <div class="col-md-1 col-sm-1 col-xs-12">
                                                              <span style="float: right;cursor: pointer;" data-limit="<?php echo get_settings_by_key('sup_home_bnr_img_limit');?>" 
                                                                    class="glyphicon glyphicon-plus btnMoreImages" data-h="400" data-w="1280" data-dest="#divMorePageBanner"
                                                                    data-file-name="supm_home_banner[]" 
                                                                    data-file-name2="altSupBanImgs[]" 
                                                                    data-index="2" data-image-group=".bannerImageGroup"></span>
                                                         </div>
                                                    </div>

                                                    <div id="divMorePageBanner"></div>
                                               <?php }?>
                                             <?php
                                               if (isset($suppliers['banner_images_app']) && !empty($suppliers['banner_images_app'])) {
                                                    $i = 0;
                                                    ?>
                                                    <div class="form-group">
                                                         <?php
                                                         foreach ($suppliers['banner_images_app'] as $key => $value) {
                                                              if ($i % 4 == 0) {
                                                                   ?>
                                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for mobile/app</label>
                                                              <?php }?>
                                                              <div class="bannerImageGroup2 col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['sbi_id'];?>" style="padding-left: 10px;">
                                                                   <div class="input-group">
                                                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'home_banner/' . $value['sbi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                                   </div>
                                                                   <?php if ($value['sbi_image']) {?>
                                                                        <span class="help-block">
                                                                             <a data-url="<?php echo site_url('supplier/removeSupplierBanner/' . $value['sbi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                        </span>
                                                                        <span class="help-block">
                                                                             <input class="btnSetDefaultImage" type="radio" name="2default_home_bannerapp" 
                                                                             <?php echo ($value['sbi_is_default'] == '1') ? 'checked="checked"' : '';?>
                                                                                    data-url="<?php echo site_url('supplier/setDefaultBannerImage/' . $value['sbi_id'] . '/' . $suppliers['supm_id']);?>"/>&nbsp; Make it default
                                                                        </span>

                                                                        <div id="altTag">
                                                                             <input placeholder="Alt tag for image" type="text" name="sbi_alttag[<?php echo $value['sbi_id']?>]" id="ssi_alttag" data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['sbi_alttag'];?>"
                                                                                    class="form-control col-md-9 col-xs-12"/>
                                                                        </div>
                                                                   <?php }?>
                                                              </div>

                                                              <?php
                                                              $i++;
                                                         }
                                                         ?>
                                                    </div>
                                               <?php }
                                             ?>

                                             <?php if (count($suppliers['banner_images_app']) < get_settings_by_key('sup_home_bnr_img_limit')) {?>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for mobile/app with 800(W) X 400(H)
                                                              <small>(maximum <?php echo get_settings_by_key('sup_home_bnr_img_limit');?> images)</small>
                                                         </label>
                                                         <?php
                                                         $rand = rand(1, 100);
                                                         ?>
                                                         <div class="col-md-5 col-sm-3 col-xs-12">
                                                              <div id="newupload">
                                                                   <input type="hidden" id="x1<?php echo $rand;?>" name="x14[]" />
                                                                   <input type="hidden" id="y1<?php echo $rand;?>" name="y14[]" />
                                                                   <input type="hidden" id="x2<?php echo $rand;?>" name="x24[]" />
                                                                   <input type="hidden" id="y2<?php echo $rand;?>" name="y24[]" />
                                                                   <input type="hidden" id="w<?php echo $rand;?>" name="w4[]" />
                                                                   <input type="hidden" id="h<?php echo $rand;?>" name="h4[]" />
                                                                   <input <?php if (!isset($suppliers['banner_images_app']) || empty($suppliers['banner_images_app'])) {?> 

                                                                             data-parsley-required-message="Please upload banner image" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                                        type="file" class="form-control col-md-7 col-xs-12" name="supm_home_banner_mobile[]" 
                                                                        id="image_file<?php echo $rand;?>" onchange="fileSelectHandler('<?php echo $rand;?>', '800', '400', '1')" />
                                                                   <img id="preview<?php echo $rand;?>" class="preview"/>
                                                              </div>
                                                              <div id="altTag"><input placeholder="Alt tag for image" type="text" name="altSupBanmobImgs[]"id="altSupBanImg0" <?php if (!isset($suppliers['banner_images_app']) || empty($suppliers['banner_images_app'])) {?> 

                                                                                           data-parsley-required-message="Please enter an alt tag for the image" <?php }?>class="form-control col-md-9 col-xs-12"/></div>
                                                         </div>
                                                         <div class="col-md-1 col-sm-1 col-xs-12">
                                                              <span style="float: right;cursor: pointer;" data-limit="<?php echo get_settings_by_key('sup_home_bnr_img_limit');?>" 
                                                                    class="glyphicon glyphicon-plus btnMoreImages" data-h="400" data-w="800" data-dest="#divMorePageBannerapp"
                                                                    data-file-name="supm_home_banner_mobile[]"
                                                                    data-file-name2="altSupBanmobImgs[]"  
                                                                    data-index="4" data-image-group=".bannerImageGroup2"></span>
                                                         </div>
                                                    </div>
                                                    <div id="divMorePageBannerapp"></div>
                                               <?php }?>
                                             <!-- Banners -->                  

                                             <div class="ln_solid"></div>

                                             <div class="form-group">

                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>

                                                       <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>

                                                  </div>

                                             </div>

                                        </div>

                                   </div>

                              </div>
                              <!-- Settings panel -->
                              <?php if (check_permission('business', 'verifysupplier')) {?>
                                     <div class="theme-panel theme-panel-lg">
                                          <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
                                          <div class="theme-panel-content">
                                               <h5 class="m-t-0 text-center">Settings</h5>
                                               <div class="divider"></div>
                                               <div>
                                                    <div class="form-group">
                                                         <div class="col-md-12 col-sm-6 col-xs-12">
                                                              <div class="row m-t-10">
                                                                   <div class="col-md-2">
                                                                        <input  type="checkbox" class="chkSupVerify" name="chkStatus" value="1" 
                                                                                data-switchery="true" <?php echo $suppliers['supm_status'] == 1 ? 'checked' : '';?>>
                                                                   </div>
                                                                   <div class="col-md-10"><div class="fullwidth">Verify</div></div>
                                                              </div>
                                                         </div>
                                                    </div>
                                                    <?php if ($suppliers['supm_status'] != 1 && check_permission('business', 'rejectsupplier')) {?>
                                                         <div class="form-group">
                                                              <div class="col-md-12 col-sm-6 col-xs-12">
                                                                   <textarea class="form-control col-md-7 col-xs-12 txtReason" 
                                                                             placeholder="If rejected, give the reson for rejection" name="txtReason"><?php
                                                                                  echo isset($suppliers['businessCurStatus']['ssl_status_message']) ?
                                                                                          $suppliers['businessCurStatus']['ssl_status_message'] : '';
                                                                                  ?></textarea>
                                                                   <span style="font-style: italic;color: red;font-size: 10px;">
                                                                        Please give reason for reject the business.
                                                                   </span>
                                                              </div>
                                                         </div>
                                                    <?php }?>
                                                    <div class="form-group">
                                                         <div class="col-md-12 col-sm-6 col-xs-12">
                                                              <button type="button" class="btn btn-primary pars btnSupVerify"
                                                                      data-url="<?php echo site_url($controller . '/verifySupplier/' . encryptor($suppliersid) . '/' . encryptor($suppliers['usr_id']));?>">Submit</button>
                                                         </div>
                                                    </div>
                                               </div>
                                          </div>
                                     </div>
                                <?php }?>
                              <!--Settings panel-->

                              <?php echo form_close()?>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
     #map {
          height: 500px !important;
          width: 500px  !important;
          float: left !important;
     }
</style>
<script>
     $("#supm_domain_prefix").keyup(function () {
          _val = $(this).val();
          if (_val) {
               $("#domain_eg").html("Your Website on Fujeeka Will be <i>http://" + _val + ".halal86.com</i>")
          }
     });
</script>
<script>
     $(document).ready(function () {

          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

               if ($('#tab_content1').find('.parsley-error').length > 0) {

                    $('#home-tab').trigger('click');

                    return false;

               } else if ($('#tab_content2').find('.parsley-error').length > 0) {

                    $('#profile-tab').trigger('click');

                    return false;

               } else if ($('#tab_content3').find('.parsley-error').length > 0) {

                    $('#photos-tab').trigger('click');

                    return false;

               }

          });

     });
</script>

<script>
     $('#image_file9').change(function (e) {
          var image = $('#image_file9')[0].files[0].name;
          if (image)
          {
               $('#altSupBanImg0').attr('data-parsley-required', true);
          } else
          {
               $('#altSupBanImg0').attr('data-parsley-required', false);
          }
     });
</script>

<script>
     $('supm_home_banner_mobile[]').change(function (e) {
          var image = $('#image_file9')[0].files[0].name;
          if (image)
          {
               $('#altSupBanmobImg1').attr('data-parsley-required', true);
          } else
          {
               $('#altSupBanmobImg1').attr('data-parsley-required', false);
          }
     });
</script>

<script type="text/template" class="tmpMobile">
     <div class="form-group">
     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Business contact number <span class="required">*</span>
     </label>
     <div class="col-md-6 col-sm-3 col-xs-12">
     <input type="text" id="mobile" name="upn_phone_number[]" required="required" data-parsley-required-message="Mobile required" maxlength="15" data-past=".usr_whatsapp" 
     class="pastContent numOnly form-control col-md-7 col-xs-12">
     </div>
     <div class="col-md-1 col-sm-1 col-xs-12">
     <span style="cursor: pointer;" class="glyphicon glyphicon-minus btnAddRemoveKeys"></span>
     </div>
     </div>
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR07gIkjnjfMhUqqj5WPZ3oUAjoo49wKQ&libraries=places&callback=initMap" async defer>
</script>
<script>
     function initMap(latt, lann) {
          latt = latt || <?php echo $suppliers['supm_latitude'];?>;
          lann = lann || <?php echo $suppliers['supm_longitude'];?>;
          var map = new google.maps.Map(document.getElementById('map'), {
               center: {lat: latt, lng: lann},
               zoom: 20
          });
          var card = document.getElementById('pac-card');
          var input = document.getElementById('pac-input');
          var types = document.getElementById('type-selector');
          var strictBounds = document.getElementById('strict-bounds-selector');
          var geocoder = new google.maps.Geocoder();

          var autocomplete = new google.maps.places.Autocomplete(input);


          autocomplete.bindTo('bounds', map);
          autocomplete.setFields(
                  ['address_components', 'geometry', 'icon', 'name']);

          var infowindow = new google.maps.InfoWindow();
          var infowindowContent = document.getElementById('infowindow-content');
          infowindow.setContent(infowindowContent);
          var marker = new google.maps.Marker({
               map: map,
               draggable: true,
               animation: google.maps.Animation.DROP,
               position: {lat: latt, lng: lann},
               anchorPoint: new google.maps.Point(0, -29)
          });

          autocomplete.addListener('place_changed', function () {
               infowindow.close();
               marker.setVisible(false);
               var place = autocomplete.getPlace();
               if (!place.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
               }
               if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
               } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
               }
               marker.setPosition(place.geometry.location);
               marker.setVisible(true);

               var address = '';
               if (place.address_components) {
                    address = [
                         (place.address_components[0] && place.address_components[0].short_name || ''),
                         (place.address_components[1] && place.address_components[1].short_name || ''),
                         (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
               }

               $('.txtAddress').val(address);
               $('#latitude').val(place.geometry.location.lat());
               $('#longitude').val(place.geometry.location.lng());

               console.log(place);
               infowindowContent.children['place-icon'].src = place.icon;
               infowindowContent.children['place-name'].textContent = place.name;
               infowindowContent.children['place-address'].textContent = address;
               infowindow.open(map, marker);
          });
          /**/
          google.maps.event.addListener(map, 'click', function (event) {
               placeMarker(event.latLng);
          });
          function placeMarker(location) {
               if (marker == undefined) {
                    marker = new google.maps.Marker({
                         position: location,
                         map: map,
                         animation: google.maps.Animation.DROP
                    });
               } else {
                    marker.setPosition(location);
               }
               map.setCenter(location);

               geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                         if (results[0]) {
                              console.log(results);
                              $('.txtAddress').val(results[0].formatted_address);
                              $('#latitude').val(marker.getPosition().lat());
                              $('#longitude').val(marker.getPosition().lng());
                              infowindow.setContent(results[0].formatted_address);
                              infowindow.open(map, marker);
                         }
                    }
               });
          }
          google.maps.event.addListener(marker, 'dragend', function () {
               geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                         if (results[0]) {
                              console.log(results);
                              $('.txtAddress').val(results[0].formatted_address);
                              $('#latitude').val(marker.getPosition().lat());
                              $('#longitude').val(marker.getPosition().lng());
                              infowindow.setContent(results[0].formatted_address);
                              infowindow.open(map, marker);
                         }
                    }
               });
          });
     }
</script>