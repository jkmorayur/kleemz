<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Stock list <?php echo isset($suppliers['supm_name']) ? ' of ' . $suppliers['supm_name'] . ' (' . count($products) . ')' : '';?></h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                        <a href="<?php echo site_url('product/add_stock?sup=' . $supplier . '&callback=' . encryptor('business/myproducts/' . $supplier));?>" class="btn btn-round btn-primary">
                                             <i class="fa fa-pencil-square-o"></i>New Stock
                                        </a>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="Shop charge" type="text" name="supm_shop_charge" 
                                               value="<?php echo isset($suppliers['supm_shop_charges']) ? $suppliers['supm_shop_charges'] : '';?>"
                                               id="supm_email" class="txtShopCharge decimalOnly form-control col-md-7 col-xs-12"/>
                                   </div>
                                   <a href="javascript:;" data-url="<?php echo site_url('business/update_shop_charge/' . $supplier);?>" class="btn btn-round btn-primary btnUpdateShopCharge">
                                        <i class="fa fa-pencil-square-o"></i>Update shop charge
                                   </a>
                              </div>
                         </div>
                         <form class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="Search keyword" type="text" name="productName" 
                                               value="<?php echo isset($productName) ? $productName : ''; ?>"
                                               id="supm_email" class="form-control col-md-7 col-xs-12"/>
                                   </div>
                                   <input type="submit" class="btn btn-round btn-primary" value="Search"/>
                              </div>
                         </form>
                         <table id="tbl" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <?php if (is_root_user() || $this->usr_grp == 'TA') {?><th>Supplier</th><?php }?>
                                        <?php if (is_root_user()) {?><th>Added By</th><?php }?>
                                        <th>Product Name</th>
                                        <th>Local Name</th>
                                        <th>priority</th>
                                        <th>Delete</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php foreach ((array) $products as $key => $value) {
                                        $priority = ($value['psm_product_priority'] == 999999) ? NULL : $value['psm_product_priority'];
                                        ?>
                                          <tr data-url="<?php echo site_url('product/view_stock/' . encryptor($value['psm_id']) . '?calbak=' . $callBack);?>">
                                               <?php if (is_root_user() || $this->usr_grp == 'TA') {?><td class="trVOE"><?php echo $value['supm_name'];?></td><?php }?>
                                               <?php if (is_root_user()) {?><td class="trVOE"><?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?></td><?php }?>
                                               <td class="trVOE"><?php echo $value['prd_name_en'];?></td>
                                               <td class="trVOE"><?php echo $value['psm_product_local_name'];?></td>
                                               <th><input class="priority" prdId="<?php echo $value['psm_id'];?>" type="text" name="priority" value=<?php echo $priority;?>></th>
                                               <td>
                                                    <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                       data-url="<?php echo site_url('product/deleteStock/' . encryptor($value['psm_id']));?>">
                                                         <i class="fa fa-remove"></i>
                                                    </a>
                                               </td>
                                          </tr>
                                     <?php }?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
     $(document).off('blur', '.priority');
     $(document).on('blur', '.priority', function () {
          var url = site_url +'business/updateProductpriorityBasdStore';
          var prdId = $(this).attr('prdId');
          var priority = $(this).val();
          $.ajax({
               type: 'post',
               url: url,
               dataType: 'json',
               data: {
                    prdId: prdId,
                    priority:priority
               },
               success: function (resp) {
                    console.log(resp,"resp");
                    window.location.reload()
               }
          });
     })
</script>