<div class="right_col" role="main"><input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" /></form>
<div class="row">
     <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
               <div class="x_title">
                    <h2>Business</h2>
                    <div class="clearfix"></div>
               </div>
               <div class="x_content lg-bg">
                    <form id="demo-form2" class="form-inline f_tp" method="get" action="">
                         <div class="form-group">
                              <label class="control-label" for="first-name">Search <span class="required">*</span>
                              </label>
                              <input type="text" required="required" class="form-control" data-parsley-required-message="Enter name for search" 
                                     value="<?php echo isset($_GET['supm_name']) ? $_GET['supm_name'] : '';?>"
                                     placeholder="Enter name for search" name="supm_name">

                              <button type="submit" class="btn btn-success">Search</button>
                         </div>
                    </form>
               </div>
               <?php if (check_permission('supplier', 'report')) {?> 
                      <!--                      <div class="x_content lg-bg">
                                                 <form id="demo-form2" class="form-inline f_tp" method="post" action="<?php echo site_url($controller . '/report');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                                                      <input value="0" type="hidden" name="" id="usr_id"/>
                                                      <div class="form-group">
                                                           <label class="control-label" for="first-name">Start Date<span class="required">*</span>
                                                           </label>
                                                           <input type="text" id="date1" required="required" class="form-control" data-parsley-required-message="Start Date is required" name="start_date">
                                                      </div>
                                                      <div class="form-group">
                                                           <label class="control-label" for="last-name">End Date <span class="required">*</span>
                                                           </label>
                                                           <input type="text" id="date2" name="end_date" required="required" data-parsley-required-message="End Date is required" class="form-control">
                                                      </div>
                      
                                                      <button type="submit"  name='type' value="excel" class="btn btn-success">Excel</button>
                                                      <button type="submit" name='type' value="pdf" class="btn btn-danger">Pdf</button>
                                                      <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                                                 </form>
                                            </div>-->
                 <?php }?>

          </div>
          <div class="x_panel">
 <!-- <form id="testfrm"><input type="hidden" value="<?php echo base_url();?>" name="" id="url">
 <input type="file" name="" id="file">
   <input type="text"  id="message"><input type="button" value="Send" id="btn-chat">
 </form> -->
               <div class="x_content table-responsive">
                    <table class="table table-striped table-bordered">
                         <thead>
                              <tr>
                                   <th>Suggestion Name</th>
                                   <th>Category</th>
                                   <th>Address</th>
                                   <th>District</th>
                                   <th>Location</th>
                                   <th>DEO</th>
                                   <th>Products</th>
                                   <?php if (check_permission('supplier_privilege', 'setpermission')) {?>
                                          <th>Action</th>
                                     <?php }?>
                              </tr>
                         </thead>
                         <tbody>
                              <?php
                                foreach ((array) $suppliers as $key => $value) {
                                     ?>
                                     <tr data-url="<?php echo site_url($controller . '/view/' . encryptor($value['supm_id']));?>">
                                          <td class="trVOE"><?php echo $value['supm_name'];?></td>
                                          <td class="trVOE"><?php echo $value['category'];?></td>
                                          <td class="trVOE"><?php echo $value['supm_address'];?></td>
                                          <td class="trVOE"><?php echo $value['dit_district'];?></td>
                                          <td class="trVOE"><?php echo $value['mar_name'];?></td>
                                          <td class="trVOE"><?php echo $value['dfo_first_name'];?></td>
                                          <td>
                                               <a class="pencile" href="<?php echo site_url($controller . '/myproducts/' . encryptor($value['supm_id']));?>">
                                                    <i class="fa fa-cart-plus"></i>
                                               </a>
                                          </td>
                                          <td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                       data-url="<?php echo site_url($controller . '/delete/' . $value['supm_id']);?>">
                                                         <i class="fa fa-remove"></i>
                                                    </a>
                                               <?php } if (check_permission('supplier_privilege', 'setpermission')) {?>
                                                    <a title="Set permission" class="pencile" href="<?php echo site_url('supplier_privilege/index/' . encryptor($value['usr_id']));?>">
                                                         <i class="fa fa-lock"></i>
                                                    </a>
                                               <?php }?>
                                          </td>
                                     </tr>
                                     <?php
                                }
                              ?>
                         </tbody>
                    </table>
                    <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo $pageIndex;?> to <?php echo $limit;?> of <?php echo $totalRow;?> entries</div>
                    <div style="float: right;">
                         <?php echo $links;?>
                    </div>
               </div>
          </div>
     </div>
</div>
</div>
<script>
     $(function () {
          $('input[name="start_date"]').daterangepicker({
               singleDatePicker: true,
               showDropdowns: true,
               minYear: 1901,
               maxYear: parseInt(moment().format('YYYY'), 10),
               locale: {
                    format: 'YYYY-MM-DD'
               }
          });
     });

     $(function () {
          $('input[name="end_date"]').daterangepicker({
               singleDatePicker: true,
               showDropdowns: true,
               minYear: 1901,
               maxYear: parseInt(moment().format('YYYY'), 10),
               locale: {
                    format: 'YYYY-MM-DD'
               }
          });
     });
</script>