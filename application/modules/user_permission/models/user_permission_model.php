<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class User_permission_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getAllUserGroups() {

            $this->db->where(tbl_users . '.usr_active !=', 0);
            $this->db->where(tbl_users . '.usr_id !=', 1);
            $this->db->where('(' . tbl_users . '.usr_supplier IS null')->or_where(tbl_users . '.usr_supplier = 0)');
            return $this->db->select(tbl_users . '.*, ' .
                                    tbl_users_groups . '.group_id as group_id, ' .
                                    tbl_groups . '.name as group_name, ' .
                                    tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                    tbl_market_places . '.*')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                            ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_market', 'LEFT')
                            ->where(tbl_groups . '.id', 6)->get(tbl_users)->result_array();
       }

       function addUserPermission($data) {
            if (!empty($data)) {
                 if ($this->getUserPermission($data['cua_group_id'])) {
                      $this->db->where('cua_group_id', $data['cua_group_id'])->delete(tbl_user_access);
                 }
                 if(isset($data['cua_access']['category'])) {
                    if(count(array_intersect($data['cua_access']['category'], array('add','update'))) > 0){
                         array_push($data['cua_access']['category'],"insert");
                         array_push($data['cua_access']['category'],"assign_virtual_shop");
                    }
                 }
               //   print_r($data);die;
                 $data['cua_access'] = !empty($data['cua_access']) ? serialize($data['cua_access']) : serialize(array());
                 $this->db->insert(tbl_user_access, $data);
            } else {
                 return false;
            }
       }

       function getUserPermission($id) {
            if (!empty($id)) {
                 return $this->db->select('*')->where('cua_group_id', $id)->get(tbl_user_access)->row_array();
            } else {
                 return false;
            }
       }

  }
  