<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Town admin privilege</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("user_permission/setPermission", array('id' => "frmPermission", 'class' => "form-horizontal form-label-left up-check", 'data-parsley-validate' => "true"))?>
                         <div class="form-group">
                              <label for="usr_showroom" class="control-label col-md-3 col-sm-3 col-xs-12">Staff</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($users)) {?>
                                          <select name="cua_group_id" required="" data-parsley-required-message="Select a user" 
                                                  class="select2_group form-control cmbUser" data-url="<?php echo site_url('user_permission/getPermission');?>">
                                               <option value="">Select User</option>
                                               <?php
                                               foreach ($users as $key => $value) {
                                                    $name = !empty($value['usr_username']) ? $value['usr_username'] : $value['usr_first_name'];
                                                    $grp = isset($value['group_name']) ? $value['group_name'] : '';
                                                    ?>
                                                    <option value="<?php echo $value['usr_id'];?>"
                                                            <?php echo ($usrSelected == $value['usr_id']) ? 'selected="selected"' : '';?>>
                                                                 <?php echo $name . ' ' . $value['usr_last_name'] . ' (' . $grp . ')';?>
                                                    </option>
                                               <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>

                         <?php
                           if (!is_root_user()) {
                                foreach ((array) $modules as $module => $permissions) {
                                     $modulesShow = isset($permittedModules[$module]) ? $permittedModules[$module] : array();
                                     if (!empty($modulesShow)) {
                                          ?>
                                          <div class="form-group">
                                               <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                                    <?php echo clean_text($module); ?>
                                               </label>
                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <?php foreach ((array) $modulesShow as $key => $method) {
                                                         ?>
                                                         <input class="chkPermissions" id="<?php echo $module . '-' . $method?>" 
                                                                type="checkbox" value="<?php echo $method;?>" 
                                                                name="cua_access[<?php echo $module;?>][]"/>
                                                                <?php echo isset($permissions[$method]) ? $permissions[$method] : '';?>
                                                           <?php }?>
                                               </div>
                                          </div>
                                          <div class="clearfix"></div>
                                          <?php
                                     }
                                }
                           } else {
                                foreach ((array) $modules as $module => $permissions) {
                                     ?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                               <?php echo clean_text($module);?>
                                          </label>
                                          <div class="col-md-7 col-sm-7 col-xs-12">
                                               <?php foreach ((array) $permissions as $controller => $val) {?>
                                                    <input class="chkPermissions" id="<?php echo $module . '-' . $controller?>" type="checkbox" value="<?php echo $controller;?>" 
                                                           name="cua_access[<?php echo $module;?>][]"/>
                                                           <?php echo $val;?>
                                                      <?php }?>
                                          </div>
                                     </div>
                                     <div class="clearfix"></div>
                                     <?php
                                }
                           }
                         ?>

                         <!-- <div class="ln_solid"></div> -->
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button class="btn btn-primary" type="reset">Reset</button>
                                   <button type="submit" class="btn btn-success">Submit</button>
                              </div>
                         </div>
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
     $(document).ready(function () {
          $('.cmbUser').trigger('change');
     });
</script>