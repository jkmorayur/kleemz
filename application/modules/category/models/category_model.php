<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class category_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_category = TABLE_PREFIX . 'category';
       }

       public function addNewCategory($datas) {
            $datas['cat_slug'] = slugify($datas['cat_title']);
            $datas['cat_added_by'] = $this->uid;
            if ($this->db->insert($this->tbl_category, $datas)) {
                 $lastId = $this->db->insert_id();

                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'New category inserted',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $lastId,
                     'log_added_by' => $this->uid
                 ));
                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'Error while create category',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid
                 ));
                 return false;
            }
       }

       public function getCategoryChaild($parent, $idNotin = '') {

            $this->db->select("cat_id, cat_title");
            $this->db->where('cat_parent', $parent);
            if (!empty($idNotin)) {
                 $this->db->where('cat_id !=', $idNotin);
            }
            $this->db->order_by('cat_title', 'ASC');
            $result = $this->db->get(tbl_category)->result_array();
            return $result;
       }

       public function getCategories($id = '') {

            $this->db->select("gcat.*, gcat.cat_show_on_home_page, gcat.cat_image AS cat_image, gcat.cat_parent AS cat_parent,"
                    . " gcat.cat_desc AS category_desc, gcat.cat_title AS category_name, "
                    . "gcat.cat_id AS cat_id, gcat2.cat_title AS parent_category,"
                    . "gcat3.cat_title AS root_category, vcta.vsc_id");
            $this->db->from($this->tbl_category . ' gcat');
            $this->db->join($this->tbl_category . ' gcat2', 'gcat.cat_parent = gcat2.cat_id', 'left');
            $this->db->join($this->tbl_category . ' gcat3', 'gcat2.cat_parent = gcat3.cat_id', 'left');
            $this->db->join(tbl_virtshop_catog_town_assoc. ' vcta', 'vcta.vsc_cat_id = gcat.cat_id AND vcta.vsc_added_by = '.$this->uid, 'left');
            
            if (!empty($id)) {
                 $this->db->where('gcat.cat_id', $id);
                 return $this->db->get()->row_array();
            } else {
                 return $this->db->get()->result_array();
            }
       }

       public function updateCategory($datas) {

            //$dataWithOldPriority = $this->db->get_where($this->tbl_category, array('cat_order' => $datas['cat_order']))->row_array();
            //$dataWithNewPriority = $this->db->get_where($this->tbl_category, array('cat_id' => $datas['cat_id']))->row_array();
            $catId = isset($datas['cat_id']) ? $datas['cat_id'] : 0;
            unset($datas['cat_id']);
            $datas['cat_slug'] = slugify($datas['cat_title']);
            $this->db->where('cat_id', $catId);

            if ($this->db->update($this->tbl_category, $datas)) {

//                 if (!empty($dataWithOldPriority) && !empty($dataWithNewPriority)) {
//                      $this->db->where('cat_id', $dataWithOldPriority['cat_id']);
//                      $this->db->update($this->tbl_category, array('cat_order' => $dataWithNewPriority['cat_order']));
//                 }
                 generate_log(array(
                     'log_title' => 'Record updated',
                     'log_desc' => 'Updated category',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $catId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            } else {

                 generate_log(array(
                     'log_title' => 'Record updated',
                     'log_desc' => 'Error while updated category',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $catId,
                     'log_added_by' => $this->uid
                 ));

                 return false;
            }
       }

       public function deleteCategory($id) {

            $this->removeCategoryImage($id);
            //remove form virtual shop
            $this->db->where('vsc_cat_id', $id);
            $this->db->delete(tbl_virtshop_catog_town_assoc);

            $this->db->where('cat_id', $id);
            if ($this->db->delete($this->tbl_category)) {
                 $this->db->where('cat_parent', $id);
                 $this->db->delete($this->tbl_category);

                 generate_log(array(
                     'log_title' => 'Delete a record',
                     'log_desc' => 'Category deleted',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'D',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid
                 ));
                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Delete a record',
                     'log_desc' => 'Error while delete category',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'D',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid
                 ));
                 return false;
            }
       }

       public function categoryTree() {

            $this->db->select("cat.cat_id AS category_id, cat.cat_title AS category_name, cat2.cat_title AS parent_category_name, cat2.cat_id AS parent_category_id")
                    ->from($this->tbl_category . ' cat')
                    ->join($this->tbl_category . ' cat2', 'cat.cat_parent = cat2.cat_id', 'LEFT')
                    ->order_by('parent_category_name');
            $tree = $this->db->get()->result_array();
            return $tree;
       }

       public function removeCategoryImage($id) {
            if ($id) {
                 $this->db->where('cat_id', $id);
                 $image = $this->db->get($this->tbl_category)->result_array();
                 $image = isset($image['0']) ? $image['0'] : array();
                 if (isset($image['cat_image']) && !empty($image['cat_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'category/' . $image['cat_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'category/' . $image['cat_image']);
                           unlink(FILE_UPLOAD_PATH . 'category/thumb_' . $image['cat_image']);
                      }
                      $this->db->where('cat_id', $id);
                      $this->db->update($this->tbl_category, array('cat_image' => ''));
                      generate_log(array(
                          'log_title' => 'Delete image',
                          'log_desc' => 'Delete category image',
                          'log_controller' => strtolower(__CLASS__),
                          'log_action' => 'D',
                          'log_ref_id' => $id,
                          'log_added_by' => $this->uid
                      ));
                      return true;
                 }
            }
            return false;
       }

       function removeBannerImage($id) {
            if ($id) {
                 $this->db->where('cat_id', $id);
                 $image = $this->db->get($this->tbl_category)->row_array();
                 if (isset($image['cat_page_banner']) && !empty($image['cat_page_banner'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'category/' . $image['cat_page_banner'])) {
                           unlink(FILE_UPLOAD_PATH . 'category/' . $image['cat_page_banner']);
                           unlink(FILE_UPLOAD_PATH . 'category/thumb_' . $image['cat_page_banner']);
                      }
                      $this->db->where('cat_id', $id);
                      $this->db->update($this->tbl_category, array('cat_page_banner' => ''));
                      generate_log(array(
                          'log_title' => 'Delete image',
                          'log_desc' => 'Delete category banner image',
                          'log_controller' => strtolower(__CLASS__),
                          'log_action' => 'D',
                          'log_ref_id' => $id,
                          'log_added_by' => $this->uid
                      ));
                      return true;
                 }
            }
            return false;
       }

       function getNextOrder($max = false) {
            if ($max) {
                 return $this->db->count_all_results($this->tbl_category);
            } else {
                 return $this->db->select_max('cat_order')->get($this->tbl_category)->row()->cat_order + 1;
            }
       }

       function checkCategoryExists($data) {
            $cateslug = isset($data['category']) ? slugify($data['category']) : '';
            if (isset($data['exclude']) && !empty($data['exclude'])) {
                 $this->db->where('cat_id !=', $data['exclude']);
            }
            $data['repeting'] = $this->db->get_where($this->tbl_category, array('cat_slug' => $cateslug))->result_array();
            $data['suggestion'] = $this->db->like('cat_slug', $cateslug)->get($this->tbl_category)->result_array();
            return $data;
       }

       function getRootCategories() {
            return $this->db->get_where($this->tbl_category, array('cat_parent' => '0'))->result_array();
       }

       function isProductUnderThisCategory($id){
            return$this->db->get_where(TABLE_PREFIX . 'products_categories', array('pcat_category' => $id))->num_rows();
       }
       
     public function assign_virtual_shop($id) {
          
          $this->db->where('vsc_cat_id',$id );
          $this->db->where('vsc_added_by',$this->uid );          
          $count = $this->db->count_all_results(tbl_virtshop_catog_town_assoc);
         // print_r($count);die;
          if($count == 0) {
               $data = array();
               $this->db->select('tta_town_id');
               $this->db->from(tbl_townadmin_town_assoc);
               $town_id = $this->db->where('tta_user_id', $this->uid)->get()->row()->tta_town_id;
          
               $data['vsc_cat_id'] = $id;
               $data['vsc_town_id'] = $town_id;
               $data['vsc_added_by'] = $this->uid;
               $this->db->insert(tbl_virtshop_catog_town_assoc, $data);
                    generate_log(array(
                    'log_title' => 'Virtual shop category',
                    'log_desc' => 'Virtual shop category added',
                    'log_controller' => strtolower(__CLASS__),
                    'log_action' => 'D',
                    'log_ref_id' => $id,
                    'log_added_by' => $this->uid
                    ));
                    return true;     
          } else {
               $this->db->where('vsc_cat_id',$id);
               $this->db->where('vsc_added_by',$this->uid);
               $this->db->delete(tbl_virtshop_catog_town_assoc);
                    generate_log(array(
                    'log_title' => 'Virtual shop category',
                    'log_desc' => 'Removed category from virtual shop',
                    'log_controller' => strtolower(__CLASS__),
                    'log_action' => 'D',
                    'log_ref_id' => $id,
                    'log_added_by' => $this->uid
                    ));
                    return true;
          }          
     }
  }
  