<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Update Category</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("category/update", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" class="txtCategoryid" name="category[cat_id]" value="<?php echo $categories['cat_id'];?>" />
<!--                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Parent Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php
                                     //build_category_tree($categories['cat_id'], $categories['cat_parent'], $this, $locations, 0);
                                   ?>

                                   <select name="category[cat_parent]" id="cat_parent" class="form-control col-md-7 col-xs-12">
                                        <option value="0">Select Parent</option> 
                                        <?php echo $locations?>
                                   </select>
                                   <?php

//                                     function build_category_tree($catId, $selectedId, $f, &$output, $preselected, $parent = 0, $indent = "") {
//                                          $parentCategories = $f->category->getCategoryChaild($parent, $catId);
//                                          foreach ($parentCategories as $key => $value) {
//                                               $selected = ($value["cat_id"] == $selectedId) ? "selected=\"selected\"" : "";
//                                               $output .= "<option value=\"" . $value["cat_id"] . "\" " . $selected . ">" . $indent . $value["cat_title"] . "</option>";
//                                               if ($value["cat_id"] != $parent) {
//                                                    build_category_tree($catId, $selectedId, $f, $output, $preselected, $value["cat_id"], $indent . "&nbsp;&nbsp;");
//                                               }
//                                          }
//                                     }
                                   ?>
                              </div>
                         </div>-->
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo ($categories['cat_parent'] == 0) ? 'Category Title' : 'Sub Category Title';?></label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter category title" autocomplete="false" type="text" class="form-control col-md-7 col-xs-12 txtCategoryName" 
                                          data-url="<?php echo site_url('category/checkCategoryExists');?>"
                                          value="<?php echo $categories['category_name'];?>" name="category[cat_title]" 
                                          id="cat_title" placeholder="Category Title"/>
                              </div>
                              <label class="lblMsgCategoryExists"></label>
                         </div>
                         <div class="form-group divCategoryMatching"></div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Priority</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($order)) {?>
                                          <select name="category[cat_order]" id="exp_order" class="form-control col-md-7 col-xs-12">
                                               <option value="0">Select Priority</option>
                                               <?php for ($i = 1; $i <= $order; $i++) {?>
                                                    <option <?php echo ($i == $categories['cat_order']) ? "selected='selected'" : '';?> 
                                                         value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="category[cat_desc]" class='editor'><?php echo $categories['category_desc'];?></textarea>
                              </div>
                         </div>
                         <div id="mydiv">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                             <?php echo img(array('src' => FILE_UPLOAD_PATH . 'category/' . $categories['cat_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                        </div>
                                        <?php if ($categories['cat_image']) {?>
                                               <span class="help-block">
                                                    <a data-url="<?php echo site_url('category/removeImage/' . $categories['cat_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                               </span>
                                          <?php }?>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Icon</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x1[]" />
                                             <input type="hidden" id="y10" name="y1[]" />
                                             <input type="hidden" id="x20" name="x2[]" />
                                             <input type="hidden" id="y20" name="y2[]" />
                                             <input type="hidden" id="w0" name="w[]" />
                                             <input type="hidden" id="h0" name="h[]" />
                                        <input  <?php if (!$categories['cat_image']) {?> data-parsley-required-message="upload image" <?php } ?> data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="cat_image" id="image_file0" onchange="fileSelectHandler('0', '816', '720','1')" />
                                             <img id="preview0" class="preview"/>
                                        </div>
                                   </div>
                              </div>

                              <!-- Category banner -->
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                             <?php echo img(array('src' => FILE_UPLOAD_PATH . 'category/' . $categories['cat_page_banner'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                        </div>
                                        <?php if ($categories['cat_page_banner']) {?>
                                               <span class="help-block">
                                                    <a data-url="<?php echo site_url('category/removeBanner/' . $categories['cat_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                               </span>
                                          <?php }?>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Banner</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x11" name="x1[]" />
                                             <input type="hidden" id="y11" name="y1[]" />
                                             <input type="hidden" id="x21" name="x2[]" />
                                             <input type="hidden" id="y21" name="y2[]" />
                                             <input type="hidden" id="w1" name="w[]" />
                                             <input type="hidden" id="h1" name="h[]" />
                                             <input data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="cat_page_banner" id="image_file1" onchange="fileSelectHandler('1', '500', '268')" />
                                             <img id="preview1" class="preview"/>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <!-- Category Banner -->
                         
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success btnSubmitCategory">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>