<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New Category</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("category/insert", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
<!--                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Parent Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php
                                     //uild_category_tree($this, $locations, 0);
                                   ?>

                                   <select name="category[cat_parent]" id="cat_parent" class="form-control col-md-7 col-xs-12">
                                        <option value="0">Select Parent</option> 
                                        <?php echo $locations?>
                                   </select>
                                   <?php

//                                     function build_category_tree($f, &$output, $preselected, $parent = 0, $indent = "") {
//                                          $ser_parent = '';
//                                          $parentCategories = $f->category->getCategoryChaild($parent);
//                                          foreach ($parentCategories as $key => $value) {
//                                               $selected = ($value["cat_id"] == $ser_parent) ? "selected=\"selected\"" : "";
//                                               $output .= "<option value=\"" . $value["cat_id"] . "\" " . $selected . ">" . $indent . $value["cat_title"] . "</option>";
//                                               if ($value["cat_id"] != $parent) {
//                                                    build_category_tree($f, $output, $preselected, $value["cat_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//                                               }
//                                          }
//                                     }
                                   ?>
                              </div>
                         </div>-->
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Title</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter category title" type="text" class="form-control col-md-7 col-xs-12 txtCategoryName" autocomplete="true"
                                          data-url="<?php echo site_url('category/checkCategoryExists');?>"
                                          name="category[cat_title]" id="cat_title" 
                                          placeholder="Category Title"/>
                              </div>
                              <label class="lblMsgCategoryExists"></label>
                         </div>
                         <div class="form-group divCategoryMatching"></div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Priority</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($order)) {?>
                                          <select name="category[cat_order]" id="exp_order" class="form-control col-md-7 col-xs-12">
                                               <option value="0">Select Priority</option>
                                               <?php for ($i = 1; $i <= $order; $i++) {?>
                                                    <option  
                                                         value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="category[cat_desc]" class='editor'></textarea>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Image</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div id="newupload">
                                        <input type="hidden" id="x10" name="x1[]" />
                                        <input type="hidden" id="y10" name="y1[]" />
                                        <input type="hidden" id="x20" name="x2[]" />
                                        <input type="hidden" id="y20" name="y2[]" />
                                        <input type="hidden" id="w0" name="w[]" />
                                        <input type="hidden" id="h0" name="h[]" />
                                        <input data-parsley-required-message="upload image" data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="cat_image" id="image_file0" onchange="fileSelectHandler('0', '816', '720','1')" />
                                        <img id="preview0" class="preview"/>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Page Banner</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div id="newupload">
                                        <input type="hidden" id="x11" name="x1[]" />
                                        <input type="hidden" id="y11" name="y1[]" />
                                        <input type="hidden" id="x21" name="x2[]" />
                                        <input type="hidden" id="y21" name="y2[]" />
                                        <input type="hidden" id="w1" name="w[]" />
                                        <input type="hidden" id="h1" name="h[]" />
                                        <input data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="cat_page_banner" id="image_file1" onchange="fileSelectHandler('1', '500', '268')" />
                                        <img id="preview1" class="preview"/>
                                   </div>
                              </div>
                         </div>
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success btnSubmitCategory" disabled="true">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>