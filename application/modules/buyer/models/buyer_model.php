<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class buyer_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
       }

       function getUsers() {
            if ($this->usr_grp == 'BY') {
                 $this->db->where($this->tbl_users . '.usr_id', $this->uid);
            }
            $this->db->where($this->tbl_users . '.usr_active !=', 0);
            $this->db->where($this->tbl_users . '.usr_id !=', 1);
            $this->db->where('(' . $this->tbl_users . '.usr_supplier IS null')->or_where($this->tbl_users . '.usr_supplier = 0)');
            return $this->db->select(
                                    $this->tbl_users . '.*, ' .
                                    $this->tbl_users_groups . '.group_id as group_id, ' .
                                    $this->tbl_groups . '.name as group_name, ' .
                                    $this->tbl_groups . '.description as group_desc, branch.usr_username AS branch'
                            )
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->join($this->tbl_users . ' branch', 'branch.usr_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->where($this->tbl_groups . '.id', 4)
                            ->get($this->tbl_users)->result_array();
       }

       function register($data) {
            if ($data) {
                 $username = isset($data['usr_first_name']) ? $data['usr_first_name'] : '';
                 $password = isset($data['usr_password']) ? $data['usr_password'] : '';
                 $email = isset($data['usr_email']) ? $data['usr_email'] : '';
                 $group = array($data['usr_group']); // Sets user to admin.

                 unset($data['usr_group']);
                 unset($data['usr_password']);
                 unset($data['usr_password_conf']);

                 $data['usr_added_by'] = $this->uid;
                 $lastInsertId = $this->ion_auth->register($username, $password, $email, $data, $group);

                 generate_log(array(
                     'log_title' => 'Create new user',
                     'log_desc' => 'New user added',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $lastInsertId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Create new user',
                     'log_desc' => 'Failed to create new user',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid
                 ));

                 return false;
            }
       }

       function update($data) {

            $id = isset($data['usr_id']) ? $data['usr_id'] : '';
            unset($data['usr_id']);

            if (isset($data['usr_password']) && empty($data['usr_password'])) {
                 unset($data['usr_password']);
            }
            unset($data['usr_password_conf']);

            $data['usr_active'] = 1;

            $data['usr_updated_by'] = $this->uid;
            $this->ion_auth->update($id, $data);

            generate_log(array(
                'log_title' => 'Update user',
                'log_desc' => 'Updated user details',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'U',
                'log_ref_id' => $id,
                'log_added_by' => $this->uid
            ));

            return true;
       }

  }
  