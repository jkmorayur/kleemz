<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class buyer extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'New Buyer';
            $this->load->model('buyer/buyer_model', 'buyer');
            $this->load->model('buyer_model', 'buyer');
            $this->load->model('user_permission/user_permission_model', 'user_permission');
       }

       public function index() {
            $data['users'] = $this->buyer->getUsers();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       function view($id) {
            $id = encryptor($id, 'D');

            $data['userDetails'] = $this->ion_auth->user($id)->row_array();
            $data['user_group'] = $this->ion_auth->get_users_groups($id)->row_array();
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       function update() {
            if (isset($_FILES['usr_avatar']['name'])) {
                 $this->load->library('upload');
                 $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'avatar/';
                 $config['allowed_types'] = 'jpg|jpeg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);
                 if ($this->upload->do_upload('usr_avatar')) {
                      $uploadData = $this->upload->data();
                      $_POST['usr_avatar'] = $uploadData['file_name'];
                 }
            }
            if ($this->buyer->update($_POST)) {
                 /* Add default permission */
                 if (isset($_POST['usr_id'])) {
                      $permission['cua_group_id'] = $_POST['usr_id'];
                      $permission['cua_access']['product'] = array('rfq', 'rfq_list', 'pending_rfq_list', 'new_rfq');
                      $permission['cua_access']['order'] = array('index', 'order_summery');
                      $this->user_permission->addUserPermission($permission);
                 }
                 $this->session->set_flashdata('app_success', 'Row successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', 'Row successfully updated!');
            }
            redirect(strtolower(__CLASS__));
       }

       function updateProfile($id = '') {
            if (!empty($_POST)) {
                 if (isset($_FILES['usr_avatar']['name'])) {
                      $this->load->library('upload');
                      $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                      $config['upload_path'] = './assets/uploads/avatar/';
                      $config['allowed_types'] = 'jpg|jpeg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      if ($this->upload->do_upload('usr_avatar')) {
                           $uploadData = $this->upload->data();
                           $_POST['usr_avatar'] = $uploadData['file_name'];
                      }
                 }
                 if ($this->buyer->update($_POST)) {
                      $this->session->set_flashdata('app_success', 'Row successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', 'Row successfully updated!');
                 }
                 redirect(__CLASS__ . '/updateProfile/' . encryptor($_POST['usr_id']));
            } else {
                 $id = encryptor($id, 'D');
                 $data['userDetails'] = $this->ion_auth->user($id)->row_array();
                 $data['user_group'] = $this->ion_auth->get_users_groups($id)->row_array();
                 $this->render_page(strtolower(__CLASS__) . '/update_profile', $data);
            }
       }

       function delete($id) {
            $this->ion_auth->deactivate($id);
            echo json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted'));
            die();
       }

       function salesExecutivesByShowroom() {
            $showroomId = isset($_POST['id']) ? $_POST['id'] : 0;
            echo json_encode($this->buyer->salesExecutivesByShowroom($showroomId));
            die();
       }

  }
  