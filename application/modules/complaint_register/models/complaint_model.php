<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class complaint_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->complaints = TABLE_PREFIX . 'complaints';
        $this->complaint_types = TABLE_PREFIX . 'complaint_types';
        $this->users = TABLE_PREFIX . 'users';
        $this->users_group = TABLE_PREFIX . 'users_group';
        $this->complaints_pptyinfrngmnt = TABLE_PREFIX . 'property_infringement';
        $this->banner = TABLE_PREFIX . 'banner';
       

    }

    public function insertCompalint($data)
    {
        $data = array_filter($data);
        $this->db->insert($this->complaints, $data);
        return $this->db->insert_id();
      
    }

    
    public function getAdminEmail()
    {
        return $this->db->select('usr_email')
              ->from($this->users)
              //->join($this->users, $this->users .'.usr_id = ' . $this->users_group . '.user_id', 'INNER')
              //->where($this->users_group.'.group_id',1)
              ->where($this->users.'.usr_id',1)->get()->row()->usr_email;

    
    }



    public function getComplaintName($c_type)
    {
        return $this->db->select('c_name')
              ->from($this->complaint_types)
              ->where('c_type',$c_type)->get()->row()->c_name;

    
    }

    public function insertpptyInfrngmntComplaint($data)
    {
 
        unset($data['trade_dispute']);
        $data = array_filter($data);
        $this->db->insert($this->complaints_pptyinfrngmnt, $data);
        return $this->db->insert_id();
      
    }

    public function getComplaintBanner()
    {
        return $this->db->select('bnr_image,bnr_desc')->where('bnr_category', 'complaints')
        ->order_by('bnr_id',"desc")
		->limit(1)->get($this->banner)->row_array();
    }

}
