
        <link rel="stylesheet" href="css/compliant_form.css">
<?php echo form_open_multipart($controller . "/register_property_infringement", array('class' => "mob-form", 'data-parsley-validate' => "true"))?>
     <div class="container-fluid complaintform_back">
     	<div class="container complaintform_cont">
     		<div class="row">
     			<div class="col-md-12">
     				<h4 class="heading2">Report an Intellectual Property Infringement</h4>
     			</div>
		
				 
				<div class="col-md-12 ">
     			<h2 class="heading3">Complaint Details</h2>
     			<div class="complaintform_form">
     			<div class="row">
     			
     		
				<div class="col-md-4 fo required">
     			<p>Type of Complaint 
</p>
					</div>
					<div class="col-md-8 fo" >
					
					<div class="input-group" >
  
    <select name="type" class="form-control" data_liveedit_tagid="000000001EAF2EB0">
<option class="option" value="Picture Embezzlement" <?php if($property_infringement == "picture_embezzlement") { ?> selected <?php }?> >Picture Embezzlement</option>
						<option class="option" value="Copyright Infringement" <?php if($property_infringement == "copyright_infringement") { ?> selected <?php }?> >Copyright Infringement</option>
						<option class="option" value="Trademark Infringement" <?php if($property_infringement == "trademark_infringement") { ?> selected <?php }?> >Trademark Infringement</option>
						<option class="option" value="Patent Infringement" <?php if($property_infringement == "patent_infringement") { ?> selected <?php }?> >Patent Infringement</option>
					
                   </select>

  
</div>

				</div>
				<div class="col-md-4 fo required">
     			<p>Infringement Product Link
</p>
					</div>
					
				
					<div class="col-md-8 fo">
					<p id="demo"></p>
					<a href="javascript:;" data-toggle="modal" data-target="#prodModal">Add Infringement Products</a>
					<input type="hidden" class="form-control" name="product_link" id="product_link"   data-parsley-required ="true" class="required"  data-parsley-required-message=" Product link  is required" value="">
					</div>
					
  		<div class="col-md-4 fo required">
     			<p>Description
</p>
					</div>
					<div class="col-md-8 fo">
					<textarea name="complaint_desc" required data-parsley-required-message="Complaint description is required" class="form-control" rows="5"></textarea>
					</div>
					
					
			
					
					
   		
    		
    		
    		
    		</div>
     		</div>
     		</div>





     			<div class="col-md-12">
     			<h2 class="heading3">Complainant Information</h2>
     			<div class="complaintform_form">
     			<div class="row">
     			
   	
     			<div class="col-md-4 fo required">
     			<p> Company Name
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" class="form-control" name="company_name" required data-parsley-required-message="Enter company name" value="">
					</div>


					
					<div class="col-md-4 fo required">
     			<p> Complainant Name
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" class="form-control" name="complainant_name" required data-parsley-required-message="Complainant name is required" value="">
					</div>
					
				<div class="col-md-4 fo required">



				
     			<p> Email 
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" data-parsley-type-message="Invalid  email format" data-parsley-type="email" class="form-control" name="complainant_email" data-parsley-type-message="Invalid  email format" data-parsley-type="email" required data-parsley-required-message="Please enter mail" value="">
					</div>

   		<div class="col-md-4 fo required">
     			<p>Telephone
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" data-parsley-type-message="Phone should be numeric" data-parsley-type="number"  class="form-control" name="complainant_phone"  required data-parsley-required-message="Please enter phone" value="">
					</div>
			
					
					<div class="col-md-4 fo required">
     			<p>Type of Identification
</p>
					</div>
					<div class="col-md-8 fo required">
					
					<div class="input-group">
  
    <select name="id_type" class="form-control">
                        <option class="option" value="Bussiness Registration">Bussiness Registration</option>
						<option class="option" value="ID Card" >ID Card</option>
					
                   </select>
  
  
</div>
				
				</div>


				<div class="col-md-4 fo required">
     			<p>Business Registration Number
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" class="form-control" name="b_id" placeholder="Business registration number/ ID card number" required data-parsley-required-message="Business registration number/ ID card number is required" value="">
					</div>
			
							
					<div class="col-md-4 fo required">
     			<p>Attachments
</p>
					</div>
					<div class="col-md-8 fo ">
					<input type="file"  required data-parsley-required-message="Attachments required" data-parsley-errors-container="#file_err"  data-parsley-fileextension="doc,docx,pdf,jpg,jpeg" class="form-control" style="
    padding: .19rem .75rem;
"  id="attach_file" name="attach_file" /><span id="file_name"></span>  <div id="file_err"></div>
					<p style="margin-top: 10px;color: #444444">Upload doc,docx,pdf,jpg,jpeg,png format only.</p>
					</div>
					<div class="input-checkbox">
                                                    <label class="input-wrap ">
                                                        <input type="checkbox" class="J-sync-compl" name="iprcheck" id="iprcheck">
                                                      
                                                        <span class="input-ctnr"></span> I am the IPR Holder
                                                    </label>

											
                                                </div>
    		
    		</div>
     		</div>
     		</div>
     		
     		
     		
   
 
     		
     		
     		
     		<div class="col-md-12" >
     		
     			<div class="complaintform_form">
     			<div class="row"  id="iprcheckdiv" >
     			<div class="col-md-4 fo required">
     			<p>IPR Holder
</p>
					</div>
					<div class="col-md-8 fo ">
					<input type="text" class="form-control" name="ipr_name" id="ipr_name" data-parsley-required ="true" data-parsley-required-message="Please enter name" value="">
					</div>
					
					<div class="col-md-4 fo required">
     			<p>Type of Identification
</p>
					</div>
					<div class="col-md-8 fo">
					
					<div class="input-group">
  
    <select name="ipr_id_type" data-parsley-required ="true" id="ipr_id_type" class="form-control">
                        <option class="option" value="Bussiness Registration">Bussiness Registration</option>
						<option class="option" value="ID Card" >ID Card</option>
					
                   </select>
  
  
</div>
				
				</div>


				<div class="col-md-4 fo required">
     			<p>Business Registration Number
</p>
					</div>
					<div class="col-md-8 fo">
					<input type="text" class="form-control" id="ipr_b_id" name="ipr_b_id" placeholder="Business Registration / ID card number"  data-parsley-required-message="Id card number / bussiness registration number is required" data-parsley-required ="true"  value="">
					</div>
			
							
					<div class="col-md-4 fo required">
     			<p>Attachments
</p>
					</div>
					<div class="col-md-8 fo">
					<input type="file"  data-parsley-required ="true"   data-parsley-errors-container="#file_err2"  data-parsley-fileextension="doc,docx,pdf,jpg,jpeg"  data-parsley-required-message="Document is required" class="form-control" style="
    padding: .19rem .75rem;
"  id="ipr_attachment" name="ipr_attachment"  /><span id="file_name"></span>  <div id="file_err2"></div>
					<p style="margin-top: 10px;color: #444444">Upload doc,docx,pdf,jpg,jpeg,png format only.</p>
					</div>
					
							
					<div class="col-md-4 fo required">
     			<p>Power of Attorney
</p>
					</div>
					<div class="col-md-8 fo">
					<input type="file"  data-parsley-required ="true" data-parsley-required-message="Please upload Power of Attorney"  data-parsley-errors-container="#file_err3"  data-parsley-fileextension="doc,docx,pdf,jpg,jpeg" class="form-control" style="
    padding: .19rem .75rem;
"  id="ipr_poa" name="ipr_poa" /><span id="file_name"></span>  <div id="file_err3"></div>
						<p style="margin-top: 10px;color: #444444">Upload doc,docx,pdf,jpg,jpeg,png format only.</p>
				
					</div>
					</div>
                    
					<div class="row">
								
					<div class="col-md-4 fo required">
     			<p>Captcha
</p>
					</div>
					<div class="col-md-4 bo required">
					<div class="input-group captcha-center ">
                                                            <div id="captcha_img">
                                                            <?php site_url() . '/assets/images/captcha/'.$captcha['image'];?>
                                                            <?php echo $captcha['image']; ?>
                                                         
                                                        </div>
                                                            <input value="<?=$captcha['word'] ?>" type="hidden" name="captcha_code" id="captcha_code" required data-parsley-required-message="Please Enter Captch" id="captcha_code">
															<div class="input-group-append">
                                                                 <a  class="btn btn-default btn-sm refresh_captcha">
                                                                      <i class="fas fa-redo"></i>
                                                                 </a>
                                                            </div>
                                                           
                                                       </div></div>
                          <div class="col-md-4">                             
					<input type="text" required data-parsley-required-message="Captcha is required"  class="form-control"  name="captcha" id="captcha" name="captcha_code" data-parsley-equalto="#captcha_code" data-parsley-equalto-message="Invalid captcha code">
						</div>
					</div>
					
					
				
					
   		<div class="col-md-12 text-right"><button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">submit</button></div>
    		
		   </div>
    		</div>
    		</div>
     		</div>
     		</div>
     		
     		
     		
     		
     		
     		</div>
     	</div>
		 <?php echo form_close()?>

		  <!-- The Modal -->
		  <div class="modal infrModal" id="prodModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <!-- <h4 class="modal-title">Modal Heading</h4> -->
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
              <div class="inf-cont">
                <div class="sbar">
                  <div class="input-group mb-3">
                    <input type="text" autocomplete="true"   class="form-control commonAutoComplete Infrengemntprdct" name="productkey" id="productkey" placeholder="Search Product by Keyword" aria-label="Search Product">
                    <div class="input-group-append">
                      <button class="btn srch-btn btnProductSearch" data-url="<?php echo site_url('home/search');?>" type="button"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
<div id="datadiv" name="datadiv">


</div>
   

              </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" id="closemodal" class="closemodal btn btn-success">Finish</button>
            </div>

          </div>
        </div>
      </div>
	  

<style>
.mob-form .form-control {
	border: 1px solid #ced4da;
}


  .autocomplete-suggestions {
    border: 1px solid #e4e4e4;
    background: #F4F4F4;
    cursor: default;
    overflow: auto
}

.autocomplete-suggestion {
    padding: 2px 5px;
    font-size: 1.2em;
    white-space: nowrap;
    overflow: hidden
}

.autocomplete-selected {
    background: #f0f0f0
}

.autocomplete-suggestions strong {
    color: #39f;
    font-weight: bolder
}
p.parsley-success {
    color: #468847;
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6
}

p.parsley-error {
    color: #B94A48;
    background-color: #F2DEDE;
    border: 1px solid #EED3D7
}

ul.parsley-errors-list {
    list-style: none;
    color: #E74C3C;
    padding-left: 0
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
    background: #FAEDEC;
    border: 1px solid #E85445
}

.btn-group .parsley-errors-list {
    display: none
}
</style>
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		  <script src="js/productSearch.js"></script>
          <script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
	<script>
	$(function() {
   
   $(document).on('click', '.refresh_captcha', function() {
	   $.ajax({
		   url: site_url + "user/refreshCaptcha",
		   method: "GET",
		   dataType: "json",
		 
		   success: function(data) {
			   $('#captcha_code').val('');
			   $('#captcha_img').html(data.data.image);
			   $('#captcha_code').val(data.data.word);
			   $('#captcha').val('');
		   }
	   });
   });
 
});
	</script>
		<script>
$(document).ready(function() {
    $('#iprcheck').change(function() {
		if(this.checked)
		{
			$('#iprcheckdiv').hide();
			$('#ipr_name').attr('data-parsley-required',false);

		
          $('#ipr_poa').attr('data-parsley-required',false);
		  $('#ipr_attachment').attr('data-parsley-required',false);
          $('#ipr_b_id').attr('data-parsley-required',false);
		  $('#ipr_id_type').attr('data-parsley-required',false);
        
		}
          
        else
		{
			$('#iprcheckdiv').show();
		
		}
           
      
    });
});


window.ParsleyConfig = {
    excluded: 'input[type=button], input[type=submit], input[type=reset]',
    inputs: 'input, textarea, select, input[type=hidden], :hidden',
};

</script>