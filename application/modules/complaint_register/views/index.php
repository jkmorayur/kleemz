
        <link rel="stylesheet" href="css/submit.css">
   
   <!--<div class="container-fluid banner">
       <img src="<?php echo site_url('assets/images/banners/submit.jpg'); ?>">
   </div> -->

   <div class="container-fluid banner">
     <div class="container">
          <div class="logictitle2">
               <div class="row">
                    <div class="col-md-12">
                         <?php echo isset($banner['bnr_desc']) ? $banner['bnr_desc'] : '';?>
                    </div>
               </div>
          </div>
     </div>
     <?php
       if (!empty($banner) && isset($banner['bnr_image'])) {
            echo img(array('src' => UPLOAD_PATH . 'banner/' . $banner['bnr_image'], 'alt' => ''));
       }
     ?>
</div>

   
    
<?php echo form_open_multipart("complaint_register/register",  array('class' => "mob-form", 'data-parsley-validate' => "true"))?>
    <div class="container-fluid contback">
        <div class="container contcont">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading2">Submit a Complaint</h2>
                    <p>Fujeeka.com is a platform connecting global buyers and China suppliers. To avoid misunderstanding and provide better support for our members, Fujeeka.com have a professional team to handle complaints of trade dispute and Intellectual Property Infringement.</p>
                </div>
              <div class="col-md-12"><hr></div>
                
                <div class="col-md-12">
               <h5 class="heading2">Service Process</h5>
           </div>
           
           <div class="col-md-3">
           <div class="service text-center">
               <img src="<?php echo site_url('assets/images/icons/icons8-submit-resume-96.png'); ?>" class="img-circle img-thumbnail" width="80" alt="">
               <p>Submit a Complaint</p>
           </div>
              </div>
          <div class="col-md-3">
           <div class="service text-center">
               <img src="<?php echo site_url('assets/images/icons/icons8-process-96.png'); ?>" class="img-circle img-thumbnail" width="80" alt="">
               <p>Confirm to process</p>
           </div>
              </div>
          <div class="col-md-3">
           <div class="service text-center">
               <img src="<?php echo site_url('assets/images/icons/icons8-voice-recognition-96.png'); ?>" class="img-circle img-thumbnail" width="80" alt="">
               <p>Communication and Resolving</p>
           </div>
              </div>
          <div class="col-md-3">
           <div class="service text-center">
               <img src="<?php echo site_url('assets/images/icons/icons8-report-card-96.png'); ?>" class="img-circle img-thumbnail" width="80" alt="">
               <p>Receive Results and Resolutions</p>
           </div>
              </div>
           
                
                
                <!--<div class="col-md-12">
              <p><span>To get the help and support, please tell us which type of your complaint is: </span></p>
          </div>-->
           
              <div class="col-md-12"><h6 class="heading2">Report a Trade Dispute </h6></div>
             
              <div class="col-md-6">
              <div class="subty">
      <ul>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" data-parsley-errors-container="#file_err" required data-parsley-required-message="Please Select One option  to proceed" class="custom-control-input" id="customRadio"  value="Products not received after payment" name="complaintoption">
                              <label class="custom-control-label" for="customRadio"> Products not received after payment</label>
                          </div>
                      </li>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio1"  value="Quantity not match with contract" name="complaintoption">
                              <label class="custom-control-label" for="customRadio1"> Quantity not match with contract</label>
                          </div>
                      </li>
                      <!--<li><div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="customRadio3" name="example1">
                          <label class="custom-control-label" for="customRadio3"> Shipment problem</label>
                          </div></li>-->
                  </ul>
              </div>
         </div>
  

     <div class="col-md-6">
              <div class="subty">
                  <ul>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio4" value="Quality not as agreed" name="complaintoption">
                              <label class="custom-control-label" for="customRadio4"> Quality not as agreed</label>
                          </div>
                      </li>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio5" value="Packaging not as agreed" name="complaintoption">
                              <label class="custom-control-label" for="customRadio5">  Packaging not as agreed</label>
                          </div>
                      </li>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio11" value="otherdispute" name="complaintoption">
                              <label class="custom-control-label" for="customRadio11">  Other trade dispute</label>
                          </div>
                      </li>

    <div id="otherdisputetextbox" style="display: none">

					<div class="col-md-12 fo">
					<input type="text" class="form-control textinp"  data-parsley-required ="false" id="dispute_type"  name="dispute_type" value="">
					</div>


                  </ul>
            
 
                       </div>
                  <!--<li><div class="custom-control custom-radio">
                      <input type="radio" class="custom-control-input" id="customRadio6" name="example1">
                      <label class="custom-control-label" for="customRadio6">  Other trade dispute</label>
                      </div></li>-->
              </div>
          </div>
           
           <div class="row">
           <div class="col-md-12"><h6 class="heading2">Report an Intellectual Property Infringement</h6></div>
              <div class="col-md-6">
              <div class="subty">
      <ul>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio6" value="picture_embezzlement" name="complaintoption">
                              <label class="custom-control-label" for="customRadio6"> Picture Embezzlement</label>
                          </div>
                      </li>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio7"value="copyright_infringement"  name="complaintoption">
                              <label class="custom-control-label" for="customRadio7"> Copyright Infringement</label>
                          </div>
                      </li>
                  </ul>
              </div>
         </div>
       <div class="col-md-6">
              <div class="subty">
                  <ul>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio8" value="trademark_infringement"  name="complaintoption">
                              <label class="custom-control-label" for="customRadio8"> Trademark Infringement</label>
                          </div>
                      </li>
                      <li>
                          <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="customRadio9" value="patent_infringement"  name="complaintoption">
                              <label class="custom-control-label" for="customRadio9"> Patent Infringement</label>
                          </div>
                      </li>


               
 
                       </div>
                  </ul>
              </div>
              </div>
              
              <div class="row">
              <div class="col-md-12 text-center">
               <div class="w-100 text-center mb-2"><span id="file_err"></span> </div>
           <button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">Continue</button>
              </div>
          </div>
          
          </div>
                    </div>
           
           

     

           <!--service-->
           
           
           
            </div>
        </div>
    </div>






    <?php echo form_close()?>
    <script src="assets/js/parsley.js"></script>
  


<style>
.mob-form .form-control {
  border: 1px solid #ced4da;
}


.autocomplete-suggestions {
  border: 1px solid #e4e4e4;
  background: #F4F4F4;
  cursor: default;
  overflow: auto
}

.autocomplete-suggestion {
  padding: 2px 5px;
  font-size: 1.2em;
  white-space: nowrap;
  overflow: hidden
}

.autocomplete-selected {
  background: #f0f0f0
}

.autocomplete-suggestions strong {
  color: #39f;
  font-weight: bolder
}
p.parsley-success {
  color: #468847;
  background-color: #DFF0D8;
  border: 1px solid #D6E9C6
}

p.parsley-error {
  color: #B94A48;
  background-color: #F2DEDE;
  border: 1px solid #EED3D7
}

ul.parsley-errors-list {
  list-style: none;
  color: #E74C3C;
  padding-left: 0
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
  background: #FAEDEC;
  border: 1px solid #E85445
}

.btn-group .parsley-errors-list {
  display: none
}

</style>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script>
$(function() {
  $('input[name="complaintoption"]').on('click', function() {
 
      if ($(this).val() == 'otherdispute') {
          $('#otherdisputetextbox').show();
          $('#otherinfrgmnttextbox').hide();
          $('#dispute_type').attr('data-parsley-required',true);
      }
      else if ($(this).val() == 'otherinfrgmnt') {
          $('#otherinfrgmnttextbox').show();
          $('#otherdisputetextbox').hide();
          $('#infringement_type').attr('data-parsley-required',true);
      }
      else {
          $('#otherinfrgmnttextbox').hide();
          $('#otherdisputetextbox').hide();
          $('#dispute_type').attr('data-parsley-required',false);
          $('#infringement_type').attr('data-parsley-required',false);
      }
  });





});
</script>
