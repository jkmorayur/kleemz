<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class category_stock_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_category = TABLE_PREFIX . 'category';
            $this->tbl_category_stock = TABLE_PREFIX . 'category_stock';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
       }

       function getCategoryStock($id = '') {
            if (!empty($id)) {
                 return $this->db->select($this->tbl_category_stock . '.*,' . $this->tbl_supplier_master . '.*,' . $this->tbl_category . '.*')
                                 ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_category_stock . '.cst_supplier', 'LEFT')
                                 ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_category_stock . '.cst_category', 'LEFT')
                                 ->where($this->tbl_category_stock . '.cst_id', $id)->get($this->tbl_category_stock)->row_array();
            } else {
                 return $this->db->select($this->tbl_category_stock . '.*,' . $this->tbl_supplier_master . '.*,' . $this->tbl_category . '.*')
                                 ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_category_stock . '.cst_supplier', 'LEFT')
                                 ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_category_stock . '.cst_category', 'LEFT')
                                 ->get($this->tbl_category_stock)->result_array();
            }
       }

       function insert($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_category_stock, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function update($data) {
            if (!empty($data) && isset($data['cst_id'])) {
                 $id = $data['cst_id'];
                 unset($data['cst_id']);
                 $this->db->where('cst_id', $id);
                 $this->db->update($this->tbl_category_stock, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function delete($id) {
            if (!empty($id)) {
                 $this->db->where('cst_id', $id);
                 $this->db->delete($this->tbl_category_stock);
                 return true;
            } else {
                 return false;
            }
       }

  }
  