<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class category_stock extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Category stock';
            $this->load->library('form_validation');

            $this->load->model('business/business_model', 'supplier');
            $this->load->model('category_stock_model', 'category_stock');
            $this->load->model('category/category_model', 'category_model');
            $this->lock_in();
       }

       public function index() {
            $data['stock'] = $this->category_stock->getCategoryStock();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function view($id = '') {
            $id = encryptor($id, 'D');
            $data['suppliers'] = $this->supplier->suppliers();
            $data['stock'] = $this->category_stock->getCategoryStock($id);
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       function update() {
            if (!empty($_POST)) {
                 if ($this->category_stock->update($_POST)) {
                      $this->session->set_flashdata('app_success', 'Quantity successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't update quantity!");
                 }
                 redirect(strtolower(__CLASS__));
            }
       }

       function add() {
            if (!empty($_POST)) {
                 if ($this->category_stock->insert($_POST)) {
                      $this->session->set_flashdata('app_success', 'Quantity successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't update quantity!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['suppliers'] = $this->supplier->suppliers();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       public function delete($id) {
            if ($this->category_stock->delete($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Stock successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete stock"));
            }
       }

  }
  