<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class product_schedule extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Product schedule';
            $this->load->model('product_schedule_model', 'product_schedule');
            $this->load->model('business/business_model', 'business');
            $this->lock_in();
       }

       public function index() {
            $data['units'] = $this->product_schedule->get();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function add() {
            if (!empty($_POST)) {
                 if ($this->product_schedule->create($_POST)) {
                      $this->session->set_flashdata('app_success', 'New schedule created!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't create new schedule!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data = array();
                 if (is_root_user()) {
                      $data['suppliers'] = $this->product_schedule->suppliers();
                 }
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       public function view($id) {
            $id = encryptor($id, 'D');
            if (is_root_user()) {
                 $data['suppliers'] = $this->product_schedule->suppliers();
            }
            $data['unit'] = $this->product_schedule->get($id);
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function update() {
            if ($this->product_schedule->update($this->input->post())) {
                 $this->session->set_flashdata('app_success', 'Schedule successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update schedule!");
            }
            redirect(strtolower(__CLASS__));
       }

       function delete($id = '') {
            if ($this->product_schedule->delete($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Schedule successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete schedule")));
            }
       }

  }
  