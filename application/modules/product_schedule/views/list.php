<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Product schedule</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                              <a href="<?php echo site_url($controller . '/add');?>" class="btn btn-round btn-primary">
                                   <i class="fa fa-pencil-square-o"></i>
                                   New Schedule
                              </a>
                         </div>
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Schedule name</th>
                                        <th>Start time</th>
                                        <th>End time</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $units as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/view/' . encryptor($value['prs_id']));?>">
                                               <td class="trVOE"><?php echo $value['prs_schedule_name'];?></td>
                                               <td class="trVOE"><?php echo $value['prs_start_time'];?></td>
                                               <td class="trVOE"><?php echo $value['prs_end_time'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . $value['prs_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>