<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New Schedule</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/add", array('id' => "frmNewsEvents", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" value="<?php echo $this->uid;?>" name="prs_supplier_id"/>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Schedule name</label>
                              <div class="form-group">
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <input type="text" required class="form-control col-md-9 col-xs-12"  
                                               name="prs_schedule_name" id="prs_schedule_name" />
                                        <small>Eg : Breakfast</small>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Start time</label>
                              <div class="form-group">
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <input type="text" required class="form-control col-md-9 col-xs-12 hourPicker"  
                                               name="prs_start_time" id="prs_start_time" />
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">End time</label>
                              <div class="form-group">
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <input type="text" required class="form-control col-md-9 col-xs-12 hourPicker"  
                                               name="prs_end_time" id="prs_end_time" />
                                   </div>
                              </div>
                         </div>
                         <!-- -->
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="form-group">
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <textarea type="text" class="form-control col-md-9 col-xs-12"  
                                                  name="prs_desc" id="prs_schedule_name"></textarea>
                                   </div>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>