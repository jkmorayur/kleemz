<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class product_schedule_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getActiveSchedule() {
            $select = array(
                tbl_products_schedule . '.*',
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_id'
            );
            $this->db->select(implode(',', $select))
                    ->join(tbl_supplier_master, tbl_products_schedule . '.prs_supplier_id = ' . tbl_supplier_master . '.supm_id', 'LEFT');

            $this->db->where(tbl_products_schedule . '.prs_status', 1);
            return $this->db->get(tbl_products_schedule)->result_array();
       }

       function get($id = '') {
            if (!empty($id)) {
                 return $this->db->get_where(tbl_products_schedule, array('prs_id' => $id))->row_array();
            } else {
                 $select = array(
                     tbl_products_schedule . '.*',
                     tbl_supplier_master . '.supm_name',
                     tbl_supplier_master . '.supm_id'
                 );
                 $this->db->select(implode(',', $select))
                         ->join(tbl_supplier_master, tbl_products_schedule . '.prs_supplier_id = ' . tbl_supplier_master . '.supm_id', 'LEFT');
                 if (!is_root_user()) {
                      $this->db->where(tbl_products_schedule . '.prs_supplier_id', $this->uid);
                 }
                 return $this->db->get(tbl_products_schedule)->result_array();
            }
       }

       function create($data) {
            if (!empty($data)) {
                 $this->db->insert(tbl_products_schedule, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function update($data) {
            if ($data) {
                 $id = isset($data['prs_id']) ? $data['prs_id'] : '';
                 unset($data['prs_id']);
                 $this->db->where('prs_id', $id);
                 $this->db->update(tbl_products_schedule, $data);
                 return true;
            } else {
                 return FALSE;
            }
       }

       function delete($id) {
            if (!empty($id)) {
                 $this->db->where('prs_id', $id);
                 $this->db->delete(tbl_products_schedule);
                 return true;
            } else {
                 return false;
            }
       }

       function suppliers() {
            $select = array(
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_id',
                tbl_users . '.usr_id',
                tbl_users . '.usr_first_name',
                tbl_users . '.usr_last_name',
                tbl_countries . '.ctr_name',
                tbl_states . '.stt_name',
                tbl_market_places . '.mar_name'
            );
            return $this->db->select(implode(',', $select))
                            ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                            ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                            ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                            ->where(tbl_users_groups . '.group_id', 2)->get(tbl_supplier_master)->result_array();
       }

  }
  