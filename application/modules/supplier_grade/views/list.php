<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Supplier Grades</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <?php if (check_permission($controller, 'add', $this->userAccess)) {?>
                                <table style="margin: 10px 10px;">
                                     <tr>
                                          <td>
                                               <a href="<?php echo site_url($controller . '/add');?>" class="btn btn-round btn-primary">
                                                    <i class="fa fa-pencil-square-o"></i> New Grade</a>
                                          </td>
                                     </tr>
                                </table>
                           <?php }?>
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Grade English</th>
                                        <th>Grade Arabic</th>
                                        <th>Grade Chinese</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $countries as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/view/' . $value['sgrd_id']);?>">
                                               <td class="trVOE"><?php echo $value['sgrd_grade_en'];?></td>
                                               <td class="trVOE"><?php echo $value['sgrd_grade_ar'];?></td>
                                               <td class="trVOE"><?php echo $value['sgrd_grade_ch'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/deleteGrade/' . $value['sgrd_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>