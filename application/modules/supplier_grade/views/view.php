<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Update Supplier Grade</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/update", array('id' => "frmNewsEvents", 'class' => "form-horizontal"))?>
                         <input type="hidden" name="grade[sgrd_id]" value="<?php echo $supplierGrade['sgrd_id']; ?>"/>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Grade English</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                   <input value="<?php echo $supplierGrade['sgrd_grade_en']; ?>" required placeholder="Supplier Grade English" type="text" name="grade[sgrd_grade_en]" id="sgrd_grade_en"
                                          class="form-control col-md-9 col-xs-12"/>
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-1">
                                   <span style="float: right;cursor: pointer;font-size: 28px;" 
                                         class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                              </div>
                         </div>
                         <div class="divOtherLanguage" style="display: none;">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Grade Arabic</label>
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <input value="<?php echo $supplierGrade['sgrd_grade_ar']; ?>" placeholder="Supplier Grade Arabic" type="text" name="grade[sgrd_grade_ar]" id="sgrd_grade_ar"
                                               class="form-control col-md-9 col-xs-12"/>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Grade Chinese</label>
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <input value="<?php echo $supplierGrade['sgrd_grade_ch']; ?>" placeholder="Supplier Grade Chinese" type="text" name="grade[sgrd_grade_ch]" id="sgrd_grade_ch"
                                               class="form-control col-md-9 col-xs-12"/>
                                   </div>
                              </div>
                         </div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div class="input-group">
                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'icon/' . $supplierGrade['sgrd_icon'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                   </div>
                                   <?php if ($supplierGrade['sgrd_icon']) {?>
                                          <span class="help-block">
                                               <a data-url="<?php echo site_url($controller . '/removeImage/' . $supplierGrade['sgrd_id'] . '/' . $supplierGrade['sgrd_icon']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                          </span>
                                     <?php }?>
                              </div>
                         </div>
                         <br />
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">New Banner</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div id="newupload">
                                        <input type="hidden" id="x10" name="x1[]" />
                                        <input type="hidden" id="y10" name="y1[]" />
                                        <input type="hidden" id="x20" name="x2[]" />
                                        <input type="hidden" id="y20" name="y2[]" />
                                        <input type="hidden" id="w0" name="w[]" />
                                        <input type="hidden" id="h0" name="h[]" />
                                        <input type="file" class="form-control" name="icon" id="image_file0" 
                                               onchange="fileSelectHandler('0', 15, 15)" />
                                        <img id="preview0" class="preview"/>
                                   </div>
                                   <span class="help-inline">Upload approximate this dimension 15px × 15px</span>
                              </div>
                         </div>
                         
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>