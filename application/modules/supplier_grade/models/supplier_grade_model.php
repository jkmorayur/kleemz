<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class supplier_grade_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_supplier_grade = TABLE_PREFIX . 'supplier_grade';
       }

       function get($id = '') {
            if (!empty($id)) {
                 return $this->db->get_where($this->tbl_supplier_grade, array('sgrd_id' => $id))->row_array();
            }
            return $this->db->get($this->tbl_supplier_grade)->result_array();
       }

       function newGrade($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_supplier_grade, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function updateGrade($data) {
            if ($data) {
                 $id = isset($data['sgrd_id']) ? $data['sgrd_id'] : '';
                 unset($data['sgrd_id']);
                 $this->db->where('sgrd_id', $id);
                 $this->db->update($this->tbl_supplier_grade, $data);
                 return true;
            } else {
                 return FALSE;
            }
       }

       function deleteGrade($id) {
            if (!empty($id)) {
                 $this->db->where('sgrd_id', $id);
                 $this->db->delete($this->tbl_supplier_grade);
                 return true;
            } else {
                 return false;
            }
       }

       function removeImage($id, $image) {
            $this->db->where('sgrd_id', $id);
            $this->db->update($this->tbl_supplier_grade, array('sgrd_icon' => ''));

            if (file_exists(FILE_UPLOAD_PATH . 'icon/' . $image)) {
                 unlink(FILE_UPLOAD_PATH . 'icon/' . $image);
            }
       }
  }