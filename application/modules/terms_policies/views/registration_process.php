<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
               <div class="col-md-9">
                    <div class="cntent trms">
                         <h2 class="heading2">
                              Registration Process
                         </h2>
                         <p>Fujeeka takes the security of its users very importantly hence the sign-up process for supplier is somewhat extensive, which gives our platform adequate information to properly vet and ensure that people providing wholesale goods services on our platform are real people who have no fraudulent intentions.</p>
                         <p>When a supplier signs up to our platform to partner with us, his or her application is sent to the platform admins to ensure that said wholesaler meets the quality standard set by our platform to ensure only the trading of premium quality goods.</p>
                         <p>After the platform admins go through the individual applications sent in by the supplier and they are satisfied that said application meets the appropriate standards, the application is then approved and the account of the approved supplier is then activated</p>
                         <p>A personal account is created, and that account will be accessed by the approved supplier via his or her own personally set username and password.</p>
                         <p>The requirements that will be needed from the supplier looking to partner with our platform to vet/verify his/her identity and qualification, which would make for the approval of a personal account for said supplier includes:</p>

                         <ul>
                           <li>An authentic and valid license</li>
                          <li>Certification and categorization of products to be traded under the wholesale market category.</li>
                          <li>High-quality photos of products to be traded on the platform to be displayed in individual wholesaler's website.</li>
                          <li>An acceptance of the Fujeeka platform terms and conditions that covers the listing of supplier shop and products.</li>
                         </ul>

                         <p>Each wholesale product to be listed on our website by the supplier needs to be appropriately categorized under specific markets; a typical example is a supplier who solely deals on phone accessories having to list his products under the phone accessories market. If he or she deals on more than one type of wholesale product, he or she would also have to list the rest of these products under the appropriate market category.</p>
                         <p>Our platform has default markets and categories for specific products, and if a supplier is looking to sell a product that doesn't fall under any of our default categories, said supplier would be required to apply for said new category/market to be listed; this application would be overseen and approved by our admins.</p>
                         <p>For the buyers, the sign-up process is a lot more straightforward as all they need do is create a free account only, they can access with their username and password.</p>
                    </div>
               </div>

          </div>
     </div>
</section>
