<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
               <div class="col-md-9">
                 <div class="cntent trms">
                   <h2 class="heading2">
                     Terms &amp; Conditions
                   </h2>
                   <h5 class="sub-heading">Overview</h5>
                   <p>Welcome to Fujeeka Technologies Limited, which facilitates business between suppliers and buyers globally. This website is operated by Fujeeka Technologies Limited and the Terms and conditions govern your access to the use of the Fujeeka website, including any functionality and services offered on or through www.fujeeka.com. Thus, throughout the website, Fujeeka Technologies Limited is referred to as www.Fujeeka.com and users are referred to as User.</p>
                   <p>Please, read these terms and conditions thoroughly prior to accessing any part of the website before clicking to acknowledge or consent to any choice made accessible to you on the grounds that any use, implies that you have acknowledged and consented to be bound by the Terms and conditions on this site. Along these lines, if you don't consent to these terms and conditions, you should not access or use the Website in any capacity.</p>
                   <p>By accepting to use www.fujeeka.com, you have acknowledged and consented to be bound by the Terms and conditions on this site as it governs your relationship with www.fujeeka.com. In addition, we reserve the right to update, replace or change any part of these Terms and conditions by posting updates and/or changes to our website. It is your responsibility to check this page periodically to take note of any changes that we may have made to the Terms of Service. Following the posting of any changes and your continued access to the website or the use of the services establishes acceptance of those changes.</p>

                   <h5 class="sub-heading">User Registration</h5>
                   <p>In order to access some features of this Site, you may be required to register and We may assign to you, or you may be required to select, a password and username or account identification. If you register, you agree to provide us with accurate and complete registration information and to inform us immediately of any updates or other changes to such information. You are solely responsible for protecting the security and confidentiality of the password and identification assigned to you. You shall immediately notify Us of any unauthorized use of your password or identification or any other breach or threatened breach of this Site's security. Each time you use a password or identification, you will be deemed to be authorized to access and use the Site in a manner consistent with these Terms, and We have no obligation to investigate the authorization or source of any such access or use of this Site. <b class="blk-txt">You will be solely responsible for all access to and use of this site by anyone using the password and identification originally selected by, or assigned to, You whether or not such access to and use of this site is actually authorized by You, including without limitation, all communications and transmissions and all obligations (including without limitation financial obligations) incurred through such access or use. </b> In regards to account usage, after account activation Suppliers can access their account with their login credential. They have a good dashboard for uploading their product. The following options are inside suppliers’ dashboard: </p>

                   <ul>
                     <li>Suppliers can add staff for managing their dashboard and also give a permission for accessing a particular module (like product add, manage etc.,)</li>
                     <li>Suppliers can update their basic features and upload Shop images and 360-degree images of their shops.</li>
                     <li>Product adding: Suppliers can add product with all details and manage the added product.</li>
                     <li>Stock Updation : suppliers can update stock on daily basis.</li>
                     <li>Supplier offer: supplier can update their offers which can be visible to the buyers.</li>
                     <li>Inquiry: Suppliers can view the product inquiry from buyers. </li>
                     <li>RFQ: Suppliers will get RFQ (Request for a quote) from various buyer and option for replying.</li>
                     <li>Chatting system: Suppliers can reply to the chats from buyers.</li>
                     <li>Communication system: The mobile app is enabled with a communication feature to help suppliers communicate with buyers.</li>
                     <li>Reports:  The website enables users to access on demand product reports. </li>
                     <li>Supplier page: All suppliers are assigned a dedicated web page on the Fujeeka website.</li>
                   </ul>

                   <p>Buyers can also use their accounts to achieve the following:</p>
                   <ul>
                     <li>Buyers can log in and search for products by:
                         <ol>
                           <li>Supplier</li>
                           <li>Product</li>
                           <li>Stock</li>
                         </ol>
                     </li>
                     <li>Buyers can sort their search according to their requirement.</li>
                     <li>Product inquiry and chatting option.</li>
                     <li>Option for chatting with supplier.</li>
                     <li>Product and supplier favorite option.</li>
                     <li>Buyer’s favorite supplier. The buyer will get all new product alert from that supplier.</li>
                     <li>RFQ: Buyer can submit RFQ (Request for Quote) and receive the reply from various supplier in their inbox.</li>
                     <li>Buyers can also search for product in particular marketplaces while they filter by various options.</li>
                     <li>Buyers can view the 360-degree view of supplier’s shop.</li>
                   </ul>

                   <h5 class="sub-heading">User Representations</h5>
                   <p>By using this Site, you represent and warrant that: </p>
                   <ol>
                     <li>all registration information you submit will be true, accurate, current, and complete; </li>
                     <li>you will maintain the accuracy of such information and promptly update such registration information as necessary; </li>
                     <li>you have the legal capacity and you agree to comply with these Terms of Use;</li>
                     <li>you are not under the age of 13]; </li>
                     <li>you are not a minor in the jurisdiction in which you reside [, or if a minor, you have received parental permission to use the Site]; </li>
                     <li>you will not access the Site through automated or non-human means, whether through a bot, script or otherwise; </li>
                     <li>you will not use the Site for any illegal or unauthorized purpose; </li>
                     <li>your use of the Site will not violate any applicable law or regulation.</li>
                   </ol>
                   <p>If you provide any information that is untrue, inaccurate, not current, or incomplete, we have the right to suspend or terminate your account and refuse any and all current or future use of the Site (or any portion thereof).</p>

                   <h5 class="sub-heading">User Content</h5>
                   <p>This Site may include features and functionality ("Interactive Features") that allow users to create, post, transmit or store any content, such as text, photos, graphics or code on the Sites ("User Content"). User Content is publicly-viewable and may include your profile information and any content you post pursuant to your profile. You agree that you are solely responsible for your User Content and for your use of Interactive Features, and that you use any Interactive Features, at your own risk. By using any Interactive Areas, you agree not to post, upload to, transmit, distribute, store, create, publish or send through the Sites any of the following:</p>
                   <ul>
                     <li>User Content that is unlawful, libelous, defamatory, obscene, pornographic, indecent, lewd, suggestive, harassing, threatening, abusive, inflammatory, fraudulent or otherwise objectionable;</li>
                     <li>User Content that would constitute, encourage or provide instructions for a criminal offense, violate the rights of any party or that would otherwise create liability or violate any local, state, national or international law;</li>
                     <li>User Content that displays, describes or encourages usage of any product We sell in a manner that could be offensive, inappropriate or harmful to Us or any user or consumer;</li>
                     <li>User Content that may infringe upon or violate the publicity, privacy or data protection rights of others, including pictures, videos, images or information about another individual where you have not obtained such individual's consent;</li>
                     <li>User Content that makes false or misleading statements, claims or depictions about a person, company, product or service;</li>
                     <li>User Content that may infringe any patent, trademark, trade secret, copyright or other intellectual or proprietary right of any party;</li>
                     <li>User Content that impersonates any person or entity or otherwise misrepresents your affiliation with a person or entity; and</li>
                     <li>Viruses, malware of any kind, corrupted data or other harmful, disruptive or destructive files or code.</li>
                   </ul>

                   <h5 class="sub-heading">Description and use services</h5>
                   <p>Fujeeka Technologies Limited Website is strictly a platform where wholesale suppliers and wholesale buyers communicate. The payments made to the supplier without Fujeeka’s SECURE PAYMENT SYSTEM , fujeeka will not owe any responsibility . However, this Site provides User with access to a variety of resources, including download areas, communication services and product information (collectively "Services"). User agrees to use the Communication Services only to post, send and receive messages and material that are proper and, when applicable, related to the particular Communication Service. By way of example, and not as a limitation, User agrees that when using the Communication Services, User will not:</p>
                   <ul>
                     <li>Use the Communication Services in connection with surveys, contests, pyramid schemes, chain letters, junk email, spamming or any duplicative or unsolicited messages (commercial or otherwise).</li>
                     <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others.</li>
                     <li>Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, obscene, indecent or unlawful topic, name, material or information.</li>
                     <li>Upload, or otherwise make available, files that contain images, photographs, software or other material protected by intellectual property laws, including, by way of example, and not as limitation, copyright or trademark laws (or by rights of privacy or publicity) unless User own or control the rights thereto or have received all necessary consent to do the same.</li>
                     <li>Use any material or information, including images or photographs, which are made available through the Services in any manner that infringes any copyright, trademark, patent, trade secret, or other proprietary right of any party.</li>
                     <li>Upload files that contain viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files, or any other similar software or programs that may damage the operation of another's computer or property of another.</li>
                     <li>Advertise or offer to sell or buy any goods or services for any business purpose, unless such Communication Services specifically allows such messages.</li>
                     <li>Download any file posted by another user of a Communication Service that User know, or reasonably should know, cannot be legally reproduced, displayed, performed, and/or distributed in such manner.</li>
                     <li>Falsify or delete any copyright management information, such as author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded.</li>
                     <li>Restrict or inhibit any other user from using and enjoying the Communication Services.</li>
                     <li>Violate any code of conduct or other guidelines which may be applicable for any particular Communication Service.</li>
                     <li>Harvest or otherwise collect information about others, including email addresses.</li>
                     <li>Violate any applicable laws or regulations.</li>
                     <li>Create a false identity for the purpose of misleading others.</li>
                     <li>Use, download or otherwise copy, or provide (whether or not for a fee) to a person or entity any directory of users of the Services or other user or usage information or any portion thereof.</li>
                   </ul>

                   <p>Fujeeka Technologies Limited has no obligation to monitor the Communication Services. However, Fujeeka Technologies reserves the right to review materials posted to the Communication Services and to remove any materials in its sole discretion. Fujeeka Technologies Limited reserves the right to terminate User’s access to any or all of the Communication Services at any time, without notice, for any reason whatsoever. Fujeeka Technologies Limited reserves the right at all times to disclose any information as it deems necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in Fujeeka Technologies Limited's sole discretion.</p>
                   <p>Materials uploaded to the Communication Services may be subject to posted limitations on usage, reproduction and/or dissemination; User is responsible for adhering to such limitations if User downloads the materials. Always use caution when giving out any personally identifiable information in any Communication Services. Fujeeka Technologies Limited does not control or endorse the content, messages or information found in any Communication Services and, therefore, Fujeeka Technologies Limited specifically disclaims any liability with regard to the Communication Services and any actions resulting from User’s participation in any Communication Services. Managers and hosts are not authorized Fujeeka Technologies Limited spokespersons, and their views do not necessarily reflect those of Fujeeka Technologies Limited.</p>
                   <p>The Services, including any updates, enhancements, new features, and/or the addition of any new Web properties, are subject to the TOU.</p>

                   <h5 class="sub-heading">Termination</h5>
                   <p>You may terminate the Terms at any time by closing your account, discontinuing your use of this Site and providing Us with a notice of termination. We reserve the right, without notice and in our sole discretion, to terminate your right to use this Site, or any portion of this Site, and to block or prevent your future access to and use of this Site or any portion of this Site.</p>

                   <h5 class="sub-heading">Indemnification</h5>
                   <p>To the fullest extent permitted by applicable law, you agree to defend, indemnify and hold harmless Us and our subsidiaries and affiliates, and our respective officers, directors, agents, partners, members, employees, independent contractors, service providers and consultants ("Our Related Parties"), from and against any claims, damages, costs, including reasonable attorneys' fees, liabilities and expenses (collectively, "Claims") arising out of or related to (1) this Agreement or the breach of your warranties, representations and obligations under this Agreement; (2) the Website content or your use of the Website content; (3) the Products or your use of the Products (including Trial Products); (4) any intellectual property or other proprietary right of any person or entity; (5) your violation of any provision of this Agreement; or (6) any information or data you supplied to Us. When Fujeeka Technologies Limited is threatened with suit or sued by a third party, We may seek written assurances from you concerning your promise to indemnify Us; your failure to provide such assurances may be considered by Us to be a material breach of this Agreement. We will have the right to participate in any defense by you of a third-party claim related to your use of any of the Website content or Products, with counsel of Fujeeka Technologies Limited choice at its expense. We will reasonably cooperate in any defense by you of a third-party claim at your request and expense. You will have sole responsibility to defend Us against any claim, but you must receive Fujeeka Technologies Limited prior written consent regarding any related settlement. The terms of this provision will survive any termination or cancellation of this Agreement or your use of the Website or Products.</p>

                   <h5 class="sub-heading">Disclaimers</h5>
                   <p>Except as expressly provided, this Site, including all Site Content, and services provided on or in connection with this Site is provided on an <b class="blk-txt">"AS IS"</b> and <b class="blk-txt">"WITH ALL FAULTS"</b> basis without representations, warranties or conditionqus of any kind, either express or implied. <b class="blk-txt">WE DISCLAIM ALL OTHER REPRESENTATIONS, WARRANTIES, CONDITIONS, AND DUTIES, EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTIES, DUTIES OR CONDITIONS: (A) OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR USE, RESULTS, TITLE, AND NON-INFRINGEMENT; AND (B) CREATED BY TRADE USAGE, COURSE OF DEALING OR COURSE OF PERFORMANCE.</b> We do not represent or warrant that this Site is accurate, complete, reliable, current or error-free. We do not represent or warrant that this Site or our servers are free of viruses or other harmful components.</p>

                   <h5 class="sub-heading">Miscellaneous</h5>
                   <p>No agency, partnership, joint venture, or employment relationship is created as a result of these Terms, and you do not have any authority of any kind to bind Us in any respect whatsoever. We may provide you with notices, including those regarding changes to these Terms, by email, regular mail, or postings on this Site. These Terms, which shall be deemed accepted by you upon your use of the Site, constitute the entire agreement among you and Us regarding the use of this Site. Our failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of the enforcement of such right or provision. If any provision of these Terms is found to be unenforceable or invalid, that provision shall be limited or eliminated to the minimum extent necessary so that these Terms shall otherwise remain in full force and effect and enforceable. These Terms are not assignable, transferable or sublicensable by you, except with our prior written consent. These Terms include and incorporate by reference Our Privacy Policy, which can be found at <b><a href="<?=site_url('privacy-policies')?>">PRIVACY POLICY</a></b>, and any notices regarding the Site.</p>

                   <h5 class="sub-heading">Questions</h5>
                   <p>Questions regarding these Terms, Our Privacy Policy, or other policy related material can be directed to our support staff by emailing us at: <a href="mailto:help_tp@fujeeka.com." target="_blank">help_tp@fujeeka.com</a>.</p>
                 </div>
               </div>

          </div>
     </div>
</section>
