<div class="container-fluid quality_back">
     <?php
       if (!empty($banner) && isset($banner['bnr_image'])) {
            echo img(array('src' => UPLOAD_PATH . 'banner/' . $banner['bnr_image'], 'alt' => ''));
       }
     ?>
</div>


<!--<div class="container">
     <div class="logistic_about2">
          <div class="row">

          </div>
     </div>
</div>-->

<div class="container-fluid">
     <div class="container">
          <div class="row">

          </div>
     </div>
</div>

<div class="container-fluid quality_about">
     <div class="container">
          <div class="row">
                <div class="col-md-12 text-center"><h2 class="heading2">QUALITY CONTROL</h2>
     		<div class="lighthead">
     		<p class="light">The era of paying for goods that are a far cry from what its description promised is over as we are a b2b online platform that is like no other, ensuring that all the pieces that make a successful and mutually beneficial transaction for both wholesalers and retailers, check out adequately.
</p></div>
     		<p class="letterheight">We employ revolutionary quality control metrics that ensure the absolute quality of the products you purchase, making sure that you get precisely what you paid for.
Before we approve of a wholesaler to partner up with us on our platform, we ensure that both the individual and his or her products are vetted to acceptable industry standards. When a wholesaler applies to partner with us and distribute his or her goods on our platform, we ensure all the products that are proposed to be listed for sale are thoroughly tackled with our world standard quality measures, and if said product doesn't meet all our requirements, the product would be denied the access to be sold on our platform.
If you are looking to find a b2b platform that makes available only the highest quality whole sale goods, then you have found just the place.
</p>
			<p class="letterheight">As long as the product you require in wholesale quantity is listed on our website, you can be sure that you are going to be purchasing only the most authentic version of said product and hence you have no worries. Once you have made buying from our platform paramount in your business, you can be confident that your retail business will never be the same.
Let’s us help you set a premium quality standard in your trade niche by providing your customers with only the best of their demands.
</p>

     		</div>


          </div>
     </div>
</div>

<?php if (!empty($qualityCtrl)) {?>
       <div class="container" id="qpartners">
            <div class="row">
                 <div class="col-md-12">
                      <div class="col-md-12 logidown"><h2 class="heading2">QC PARTNERS</h2></div>
                      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0" style="height:auto;">
                           <div class="carousel-inner">
                                <?php
                                foreach ((array) $qualityCtrl as $key => $value) {
                                     echo ($key != 0 && $key % 4 == 0) ? '</div></div>' : '';
                                     $active = $key == 0 ? 'active' : '';
                                     echo ($key == 0 || $key % 4 == 0) ? '<div class="item carousel-item ' . $active . '"><div class="row">' : '';
                                     ?>
                                     <div class="col-sm-3">
                                          <div class="thumb-wrapper">
                                               <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                               <div class="img-box">
                                                    <?php
                                                    $img = $this->quality_control->getDefaultImage($value['usr_id']);
                                                    echo img(array('src' => $img, 'alt' => ''));
                                                    ?>
                                               </div>
                                               <div class="thumb-content">
                                                    <h6><?php echo $value['usr_first_name'];?></h6>
                                                    <div class="star-rating"></div>
                                                    <a class="icons-bx btn btn-primary btnGetQuote" href="<?php echo site_url('quality-control' . '/get-quote/' . encryptor($value['usr_id']));?>">View Details</a>
                                               </div>
                                          </div>
                                     </div>
                                     <?php
                                     echo ($key == (count($qualityCtrl) - 1)) ? '</div></div>' : '';
                                }
                                ?>
                           </div>
                           <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                           </a>
                           <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                           </a>
                      </div>
                 </div>
            </div>
       </div>
  <?php }?>

<div class="container-fluid quality_register">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <h2>Quality Control Partner</h2>
                    <p>If you can assure precise quality control checking, Register as a quality control partner with fujeeka now</p>
                    <a href="<?= site_url('quality-control/register')?>">REGISTER</a>
               </div>
          </div>
     </div>
</div>
