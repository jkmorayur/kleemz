<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit logistics service</h2>
                         <a href="<?php echo site_url($controller . '/services');?>" class="btn btn-round btn-primary" style="float: right">
                                                    <i class="fa fa-arrow-left"></i> Back</a>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/edit_service');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input type="hidden" name="qcs_id" value="<?php echo $service['qcs_id']; ?>"/>
                              <div class="form-group">
                                   <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Logistics
                                        <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_group form-control" data-parsley-required-message="Select a logistics" 
                                                name="service[qcs_qc_id]" required="true">
                                             <option value="">Select Logistics</option>
                                             <?php foreach ($qc as $key => $value) {?>
                                                    <option <?php echo $value['usr_id'] == $service['qcs_qc_id'] ? 'selected="selected"' : '';?>
                                                         value="<?php echo $value['usr_id'];?>">
                                                              <?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?>
                                                    </option>
                                                    <?php
                                               }
                                             ?>
                                        </select>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" 
                                               value="<?php echo $service['qcs_service_title'];?>"
                                               data-parsley-required-message="First Name required" name="service[qcs_service_title]">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="service[qcs_desc]" class='editor'><?php echo $service['qcs_desc'];?></textarea>
                                   </div>
                              </div>
                              <!-- -->
                              <?php
                                if (isset($service['images']) && !empty($service['images'])) {
                                     $i = 0;
                                     ?>
                                     <div class="form-group">
                                          <?php
                                          foreach ($service['images'] as $key => $value) {
                                               if ($i % 4 == 0) {
                                                    ?>
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                               <?php }?>
                                               <div class="col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['qcsi_id'];?>" style="padding-left: 10px;">
                                                    <div class="input-group">
                                                         <?php echo img(array('src' => FILE_UPLOAD_PATH . 'qc/' . $value['qcsi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                    </div>
                                                    <?php if ($value['qcsi_image']) {?>
                                                         <span class="help-block">
                                                              <a data-url="<?php echo site_url($controller . '/removeServiceImage/' . $value['qcsi_id']);?>" href="javascript:void(0);" style="width: 100px;" 
                                                                 class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                         </span>
                                                    <?php }?>
                                               </div>
                                               <?php
                                               $i++;
                                          }
                                          ?>
                                     </div>
                                <?php }
                              ?>
                              <!-- -->
                              <div class="form-group">
                                   <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Images</label>
                                   <div class="col-md-5 col-sm-3 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x12[]" />
                                             <input type="hidden" id="y10" name="y12[]" />
                                             <input type="hidden" id="x20" name="x22[]" />
                                             <input type="hidden" id="y20" name="y22[]" />
                                             <input type="hidden" id="w0" name="w2[]" />
                                             <input type="hidden" id="h0" name="h2[]" />
                                             <input data-parsley-required-message="upload atleast one image" data-parsley-fileextension="jpg,png,gif,jpeg,JPG,JPEG" type="file" class="form-control col-md-7 col-xs-12" name="shopImages[]" 
                                                    id="image_file0" onchange="fileSelectHandler('0', '1000', '882', '0')" />
                                             <img id="preview0" class="preview"/>
                                             <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                        </div>
                                   </div>

<!--                                   <div class="col-md-1 col-sm-1 col-xs-12">
                                        <span style="float: right;cursor: pointer;" data-limit="-1"
                                              class="glyphicon glyphicon-plus btnMoreProductImages"></span>
                                   </div>-->
                              </div>
                              <div  id="divMoreProductImages"></div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>