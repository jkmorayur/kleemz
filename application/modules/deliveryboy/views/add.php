<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Add new delivery boy</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/add');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="0" type="hidden" name="" id="usr_id"/>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" data-parsley-required-message="First Name required" name="user[usr_first_name]">
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="last-name" name="user[usr_last_name]" required="required" data-parsley-required-message="Last Name required" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <?php if (is_root_user()) {?>
                                     <div class="form-group">
                                          <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Town admin</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <select class="select2_group form-control" data-parsley-required-message="Select a town admin" name="user[usr_town_admin]" required="true">
                                                    <option value="">Select town admin</option>
                                                    <?php
                                                    foreach ($townAdmins as $key => $value) {
                                                         ?>
                                                         <option value="<?php echo $value['usr_id'];?>">
                                                              <?php echo $value['usr_first_name'] . ' (' . $value['usr_last_name'] . ')';?>
                                                         </option>
                                                         <?php
                                                    }
                                                    ?>
                                               </select>
                                          </div>
                                     </div>
                                <?php } else {?>
                                     <input value="<?php echo $this->uid;?>" type="hidden" name="user[usr_town_admin]"/>
                                <?php }?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="mobile" name="user[usr_phone]" required="required" data-parsley-required-message="Mobile required" maxlength="15" data-past=".usr_whatsapp" 
                                               autocomplete="mobile" class="pastContent numOnly form-control col-md-7 col-xs-12">
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input autocomplete="off" type="email" id="email" name="user[usr_email]" required="required" 
                                               data-parsley-trigger="change" data-parsley-required-message="Email required" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_password">Password<span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="usr_password" name="user[usr_password]" required="required" 
                                               autocomplete="off" data-parsley-required-message="Enter a strong password" data-parsley-trigger="change" 
                                               minlength="6" data-parsley-minlength-message="Password should be more than 5 character" class="form-control col-md-7 col-xs-12 usr_password">
                                   </div>
                                   <span toggle="#usr_password" class="fa fa-eye field-icon toggle-password"></span>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_password_conf">Re enter Password<span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="usr_password_conf" name="user[usr_password_conf]" required="required" 
                                               data-parsley-equalto="#usr_password" data-parsley-trigger="change" data-parsley-equalto-message="Should be same as password"  
                                               data-parsley-required-message="Confirm your password" class="form-control col-md-7 col-xs-12 usr_password_conf">
                                   </div>
                                   <span toggle="#usr_password_conf" class="fa fa-eye field-icon toggle-password"></span>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_address">Address</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="usr_address" name="user[usr_address]" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city">ID proof</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input required data-parsley-required-message="Upload ID proof" type="file" name="usr_avatar" 
                                               data-parsley-fileextension="jpg,png,gif,jpeg" class="form-control col-md-7 col-xs-12">
                                        <span class="help-inline">ID proof with address</span>
                                   </div>
                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>