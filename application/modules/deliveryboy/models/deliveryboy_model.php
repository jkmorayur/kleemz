<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class deliveryboy_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       public function getUsers() {
            if ($this->usr_grp == 'TA') {
                 $myTows = array_column($this->db->select('tta_town_id')->get_where(tbl_townadmin_town_assoc, array('tta_user_id' => $this->uid))
                                 ->result_array(), 'tta_town_id');
                 $this->db->where_in(tbl_users . '.usr_db_town', $myTows);
            }
            $this->db->where(tbl_groups . '.id', 10);
            $this->db->where(tbl_users . '.usr_active', 1);
            return $this->db->select(tbl_users . '.*, ' .
                                    tbl_users_groups . '.group_id as group_id, ' .
                                    tbl_groups . '.name as group_name, ' . tbl_groups . '.description as group_desc, ' .
                                    'branch.usr_username AS branch, townadmin.usr_first_name AS ta_first_name, ' .
                                    'townadmin.usr_last_name AS ta_last_name, ' . tbl_market_places . '.*')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                            ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_users . ' townadmin', 'townadmin.usr_id = ' . tbl_users . '.usr_town_admin', 'LEFT')
                            ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_db_town', 'LEFT')
                            ->get(tbl_users)->result_array();
       }

       public function getPendingAproval() {
            $this->db->where(tbl_groups . '.id', 10);
            $this->db->where(tbl_users . '.usr_active', 0);
            return $this->db->select(tbl_users . '.*, ' .
                                    tbl_users_groups . '.group_id as group_id, ' .
                                    tbl_groups . '.name as group_name, ' .
                                    tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                    'supp.usr_first_name AS sup_first_name, supp.usr_last_name AS sup_last_name')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                            ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_users . ' supp', 'supp.usr_id = ' . tbl_users . '.usr_supplier', 'LEFT')
                            ->get(tbl_users)->result_array();
       }

       public function register($data) {
            if ($data) {
                 $username = isset($data['usr_first_name']) ? $data['usr_first_name'] : '';
                 $password = isset($data['usr_password']) ? $data['usr_password'] : '';
                 $email = isset($data['usr_email']) ? $data['usr_email'] : '';
                 $group = array($data['usr_group']); // Sets user to admin.

                 unset($data['usr_group']);
                 unset($data['usr_password']);
                 unset($data['usr_password_conf']);

                 if ($this->uid == 1 || $this->usr_grp == 'SP' || $this->usr_grp == 'TA') { // Check if supper admin
                      $data['usr_active'] = 1;
                      $data['usr_is_mail_verified'] = 1;
                 }

                 $data['usr_added_by'] = $this->uid;

                 //DB Code
                 $townDetails = $this->db->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_townadmin_town_assoc . '.tta_town_id', 'LEFT')
                                 ->get_where(tbl_townadmin_town_assoc, array('tta_user_id' => $data['usr_town_admin']))->row_array();

                 $data['usr_db_town'] = isset($townDetails['tta_town_id']) ? $townDetails['tta_town_id'] : 0;
                 $townCode = isset($townDetails['mar_code']) ? strtoupper(trim($townDetails['mar_code'])) : 0;
                 $int = $this->generateUserCode();
                 $data['usr_code_in_part'] = $int;
                 $data['usr_code'] = $townCode . $int;

                 if ($lastInsertId = $this->ion_auth->register($username, $password, $email, $data, $group)) {
                      generate_log(array(
                          'log_title' => 'Create delivery boy',
                          'log_desc' => 'New delivery boy',
                          'log_controller' => 'delivery-boy',
                          'log_action' => 'C',
                          'log_ref_id' => $lastInsertId,
                          'log_added_by' => $this->uid,
                      ));
                      return $lastInsertId;
                 } else {
                      return $this->ion_auth->errors();
                 }
            } else {
                 generate_log(array(
                     'log_title' => 'Create delivery boy',
                     'log_desc' => 'New delivery boy',
                     'log_controller' => 'delivery-boy',
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid,
                 ));

                 return false;
            }
       }

       public function update($data) {
            $id = isset($data['usr_id']) ? $data['usr_id'] : '';
            unset($data['usr_id']);

            /* Add supplier and buyer groups */
            if (isset($data['usr_group'])) {
                 if (is_array($data['usr_group'])) {
                      foreach ($data['usr_group'] as $value) {
                           $this->ion_auth->add_to_group($value, $id);
                      }
                 } else {
                      $this->ion_auth->add_to_group($data['usr_group'], $id);
                 }
            }
            if (isset($data['usr_password']) && empty($data['usr_password'])) {
                 unset($data['usr_password']);
            }
            unset($data['usr_password_conf']);

            $data['usr_active'] = 1;
            $data['usr_updated_by'] = $this->uid;

            //DB Code
            $townDetails = $this->db->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_townadmin_town_assoc . '.tta_town_id', 'LEFT')
                            ->get_where(tbl_townadmin_town_assoc, array('tta_user_id' => $data['usr_town_admin']))->row_array();

            $data['usr_db_town'] = isset($townDetails['tta_town_id']) ? $townDetails['tta_town_id'] : 0;
            $townCode = isset($townDetails['mar_code']) ? strtoupper(trim($townDetails['mar_code'])) : 0;

            if (!empty($data['usr_code_in_part'])) {
                 $data['usr_code'] = $townCode . $data['usr_code_in_part'];
            } else {
                 $int = $this->generateUserCode();
                 $data['usr_code_in_part'] = $int;
            }

            $this->ion_auth->update($id, $data);

            generate_log(array(
                'log_title' => 'Create delivery boy',
                'log_desc' => 'New delivery boy',
                'log_controller' => 'delivery-boy',
                'log_action' => 'U',
                'log_ref_id' => $id,
                'log_added_by' => $this->uid,
            ));

            return true;
       }

       function generateUserCode() {
            return $this->db->select('MAX(usr_code_in_part) AS usr_code_in_part')
                            ->where('usr_code_in_part IS NOT NULL')->get(tbl_users)->row()->usr_code_in_part + 1;
       }

  }
  