<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class deliveryboy extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Deliveryboy';
            $this->load->model('deliveryboy_model', 'deliveryboy');
            $this->load->model('townadmin/townadmin_model', 'townadmin');
       }
       
       function index() {
            $data['deliveryBoys'] = $this->deliveryboy->getUsers();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }
       
       public function add() {
            if (!empty($_POST)) {
                 if (isset($_FILES['usr_avatar']['name'])) {
                      $this->load->library('upload');
                      $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                      $config['upload_path'] = './assets/uploads/avatar/';
                      $config['allowed_types'] = 'jpg|jpeg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      if ($this->upload->do_upload('usr_avatar')) {
                           $uploadData = $this->upload->data();
                           $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                      }
                 }
                 $_POST['user']['usr_group'] = 10; // sub admin
                 $userid = $this->deliveryboy->register($_POST['user']);
                 if(is_numeric($userid)){
                    $this->session->set_flashdata('app_success', 'Delivery boy added successfully!');
                 }else{
                    $this->session->set_flashdata('app_error', $userid);
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['townAdmins'] = $this->townadmin->getUsers();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       public function view($id) {
            $id = encryptor($id, 'D');
            $data['userDetails'] = $this->ion_auth->user($id)->row_array();
            $data['townAdmins'] = $this->townadmin->getUsers();
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function update() {
            if (isset($_FILES['usr_avatar']['name'])) {
                 $this->load->library('upload');
                 $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                 $config['upload_path'] = './assets/uploads/avatar/';
                 $config['allowed_types'] = 'jpg|jpeg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);
                 if ($this->upload->do_upload('usr_avatar')) {
                      $uploadData = $this->upload->data();
                      $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                 }
            }

            if ($this->deliveryboy->update($_POST['user'])) {
                 $this->session->set_flashdata('app_success', 'Row successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', 'Row successfully updated!');
            }
            redirect(strtolower(__CLASS__));
       }

       public function delete($id) {
            if ($this->ion_auth->delete_user($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Delivery boy successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => 'Something went wrong, try again')));
            }
       }
       
       /**
        * Change employee status
        * @param type $userId
        * Author : jk
        */
       public function changeuserstatus($userId) {
            $userId = encryptor($userId, 'D');
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            if ($this->common_model->changeStatus($userId, $ischecked, 'users', 'usr_active', 'usr_id')) {
                 $logMessage = ($ischecked == 1) ? 'Delivery boy status activated' : 'Delivery boy status de-activated';
                 generate_log(array(
                     'log_title' => 'Status changed',
                     'log_desc' => $logMessage,
                     'log_controller' => 'chg-status-delivery-boy',
                     'log_action' => 'U',
                     'log_ref_id' => $userId,
                     'log_added_by' => $this->uid,
                 ));

                 $msg = ($ischecked == 1) ? "Activated this record successfully" : "De-activated this record successfully";
                 die(json_encode(array('status' => 'success', 'msg' => $msg)));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
            }
       }
       
       function pending_aproval() {
            $data['deliveryBoys'] = $this->deliveryboy->getPendingAproval();
            $this->render_page(strtolower(__CLASS__) . '/pending_aproval', $data);
       }
  } 