<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class cronjobs extends CI_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Cron jobs';
            $this->load->model('cronjobs_model', 'cronjobs');
       }

       /**
        * Author : JK
        * Date   : 05-03-2020
        * About  : Function for active and de-active stock products due to schedule
        */
       function resetScheduledProducts() {
            echo 'Here';exit;
            $now = DateTime::createFromFormat('H:i', date('H:i'));
            $this->cronjobs->testCronJob(date('H:i'));
            $scheduledProducts = $this->cronjobs->getScheduledStockProducts();
            foreach ($scheduledProducts as $key => $value) {

                 $start = DateTime::createFromFormat('H:i', date("H:i", strtotime($value['prs_start_time'])));
                 $end = DateTime::createFromFormat('H:i', date("H:i", strtotime($value['prs_end_time'])));

                 if ($now > $start && $now < $end) {
                      $this->cronjobs->updateStockSchedule($value['psm_id'], 1);
                 } else {
                      $this->cronjobs->updateStockSchedule($value['psm_id'], 0);
                 }
            }
       }

  }
  