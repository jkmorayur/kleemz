<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class cronjobs_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getScheduledStockProducts() {
            $selArray = array(
                tbl_products_stock_master . '.psm_id',
                tbl_products_stock_master . '.psm_product_schedule',
                tbl_products_schedule . '.*'
            );
            return $this->db->select($selArray)->join(tbl_products_schedule, tbl_products_schedule . '.prs_id = ' . tbl_products_stock_master . '.psm_product_schedule', 'LEFT')
                            ->where(tbl_products_stock_master . '.psm_product_schedule > 0')->get(tbl_products_stock_master)->result_array();
       }

       function updateStockSchedule($psmId, $status) {
            $this->db->where('psm_id', $psmId)->update(tbl_products_stock_master, array('psm_status' => $status));
            return true;
       }
       
       function testCronJob($tim) {
            $this->db->insert('test', array('va' => $tim));
       }
  }  