<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Contact Enquiry </h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="overflow-x:auto;">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                      
                                        <th>Name</th>
                                        <th>Email </th>
                                        <th>Phone</th>
                                        <th>Country</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Date</th>
                                      
                                        
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $enquiry as $key => $value) {
                                   
                                          ?>
                                          <tr>
                                               <!-- <td class="trVOE"><?php echo "CMPL".$value['id'];?></td> -->
                                             
                                          
                                               <td><?php echo $value['c_name'];?></td>
                                               <td><?php echo $value['c_email'];?></td>
                                               <td><?php echo $value['c_phone'];?></td>
                                               <td><?php echo $value['c_country'];?></td>
                                               <td><?php echo $value['c_subject'];?></td>
                                               <td><?php echo $value['c_message'];?></td>
                                               <td><?php echo $value['c_date'];?></td>

                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>