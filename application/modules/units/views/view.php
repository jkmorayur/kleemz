<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Update Unit</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/update", array('id' => "frmNewsEvents", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" name="unt_id" id="unt_id" class="form-control col-md-7 col-xs-12" value="<?php echo $unit['unt_id'];?>"/>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Unit</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" name="unt_unit_en" id="unt_name_en" value="<?php echo $unit['unt_unit_en'];?>"
                                          data-parsley-required-message="Enter Unit" class="form-control col-md-9 col-xs-12"/>
                                   <small>Eg : Kg</small>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Unit Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" name="unt_unit_name_en" id="unt_unit_name_en" value="<?php echo $unit['unt_unit_name_en'];?>"
                                          data-parsley-required-message="Enter unit name" class="form-control col-md-9 col-xs-12"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">How many number in one unit, if any</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input placeholder="How many number in one unit, if any" type="number" name="unt_numbers" id="unt_numbers" 
                                          value="<?php echo $unit['unt_numbers'];?>" class="form-control col-md-7 col-xs-12 numOnly" autocomplete="off"/>
                                   <span>Eg : 1 dozen is 12 Nos</span>
                              </div>
                         </div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" name="unt_desc_en" id="unt_desc_en" 
                                          value="<?php echo $unit['unt_desc_en'];?>" class="form-control col-md-9 col-xs-12"/>
                                   
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>