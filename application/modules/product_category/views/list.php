<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Product categories</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                              <a href="<?php echo site_url($controller . '/add');?>" class="btn btn-round btn-primary">
                                   <i class="fa fa-pencil-square-o"></i>
                                   New Category
                              </a>
                         </div>
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Icon</th>
                                        <th>Market category</th>
                                        <th>Priority</th>
                                        <th>Category</th>
                                        <th>Parent Category</th>
                                        <th>Status</th>
                                        <th>Delete</th>     
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $categories as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/view/' . encryptor($value['pcat_id']));?>">
                                               <td class="trVOE">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'category/' . $value['pcat_image'], 'width' => '30'));?>
                                               </td>
                                               <td class="trVOE"><?php echo $value['mkt_cat'];?></td>
                                               <td class="trVOE"><?php echo $value['pcat_order'];?></td>
                                               <td class="trVOE"><?php echo $value['pcat_title'];?></td>
                                               <td class="trVOE"><?php
                                                    $root = ($value['root_category']) ? $value['root_category'] . ' >> ' : '';
                                                    echo $root . $value['parent_category'];
                                                    ?>
                                               </td>
                                               <td>
                                                    <label class="switch">
                                                         <input type="checkbox" value="1" class="chkOnchange" <?php echo ($value['pcat_status'] == 1) ? "checked " : '';?>
                                                                data-url="<?php echo site_url($controller . '/changestatus/' . encryptor($value['pcat_id']));?>">
                                                         <span class="slider round"></span>
                                                    </label>
                                               </td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . $value['pcat_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>