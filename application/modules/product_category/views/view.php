<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit product category</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br/>
                         <?php echo form_open_multipart($controller . "/update", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" name="category[pcat_id]" value="<?php echo $categories['pcat_id'];?>" />

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($order)) {?>
                                          <select required data-parsley-required-message="Select market category" name="category[pcat_market_category]" 
                                                  id="exp_order" class="form-control col-md-7 col-xs-12 cmbMarketCategory search-box">
                                               <option value="">Select market category</option>
                                               <?php foreach ($marketCategories as $key => $value) {?>
                                                    <option <?php echo ($value['cat_id'] == $categories['pcat_market_category']) ? 'selected="selected"' : '';?>
                                                         value="<?php echo $value['cat_id'];?>"><?php echo $value['cat_title'];?></option>
                                                    <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Parent Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php
                                     build_category_tree($categories['pcat_id'], $categories['pcat_parent'], $this, $locations, 0);
                                   ?>

                                   <select name="category[pcat_parent]" id="pcat_parent" class="form-control col-md-7 col-xs-12">
                                        <option value="0">Select Parent</option> 
                                        <?php echo $locations?>
                                   </select>
                                   <?php

                                     function build_category_tree($catId, $selectedId, $f, &$output, $preselected, $parent = 0, $indent = "") {
                                          $parentCategories = $f->category->getCategoryChaild($parent, $catId);
                                          foreach ($parentCategories as $key => $value) {
                                               $selected = ($value["pcat_id"] == $selectedId) ? "selected=\"selected\"" : "";
                                               $output .= "<option value=\"" . $value["pcat_id"] . "\" " . $selected . ">" . $indent . $value["pcat_title"] . "</option>";
                                               if ($value["pcat_id"] != $parent) {
                                                    build_category_tree($catId, $selectedId, $f, $output, $preselected, $value["pcat_id"], $indent . "&nbsp;&nbsp;");
                                               }
                                          }
                                     }
                                   ?>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo ($categories['pcat_parent'] == 0) ? 'Category Title' : 'Sub Category Title';?></label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter category title" type="text" 
                                          data-parsley-remote="<?php echo site_url($controller . "/checkIfValueExists")?>" 
                                          data-parsley-remote-options='{ "type": "POST","data": { "field": "pcat_title", "id": "<?php echo $categories['pcat_id'];?>" } }' 
                                          data-parsley-remote-message="Category already exists" data-parsley-trigger="change"
                                          class="form-control col-md-7 col-xs-12" name="category[pcat_title]" 
                                          placeholder="Category Title" id="pcat_title" value="<?php echo $categories['category_name'];?>"/>
                                   <!--<input type="text" class="form-control col-md-7 col-xs-12" value="<?php echo $categories['category_name'];?>" name="category[pcat_title]" id="pcat_title" placeholder="Category Title"/>-->
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Priority</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($order)) {?>
                                          <select name="category[pcat_order]" id="exp_order" class="form-control col-md-7 col-xs-12">
                                               <option value="0">Select Priority</option>
                                               <?php for ($i = 1; $i <= $order; $i++) {?>
                                                    <option <?php echo ($i == $categories['pcat_order']) ? "selected='selected'" : '';?> 
                                                         value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="category[pcat_desc]" class='editor'><?php echo $categories['pcat_desc'];?></textarea>
                              </div>
                         </div>
                         <div id="mydiv">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                             <?php echo img(array('src' => FILE_UPLOAD_PATH . 'category/' . $categories['pcat_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                        </div>
                                        <?php if ($categories['pcat_image']) {?>
                                               <span class="help-block">
                                                    <a data-url="<?php echo site_url('category/removeImage/' . $categories['pcat_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                               </span>
                                          <?php }?>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Icon</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x1[]" />
                                             <input type="hidden" id="y10" name="y1[]" />
                                             <input type="hidden" id="x20" name="x2[]" />
                                             <input type="hidden" id="y20" name="y2[]" />
                                             <input type="hidden" id="w0" name="w[]" />
                                             <input type="hidden" id="h0" name="h[]" />
                                             <input type="file" class="form-control col-md-7 col-xs-12" name="pcat_image" id="image_file0" onchange="fileSelectHandler('0', '500', '268')" />
                                             <img id="preview0" class="preview"/>
                                             <span class="help-inline">Choose 500(W) X 268(H)</span>
                                        </div>
                                   </div>
                              </div>

                              <!-- Category banner -->
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                             <?php echo img(array('src' => FILE_UPLOAD_PATH . 'category/' . $categories['pcat_page_banner'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                        </div>
                                        <?php if ($categories['pcat_page_banner']) {?>
                                               <span class="help-block">
                                                    <a data-url="<?php echo site_url('category/removeBanner/' . $categories['pcat_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                               </span>
                                          <?php }?>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Banner</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x11" name="x1[]" />
                                             <input type="hidden" id="y11" name="y1[]" />
                                             <input type="hidden" id="x21" name="x2[]" />
                                             <input type="hidden" id="y21" name="y2[]" />
                                             <input type="hidden" id="w1" name="w[]" />
                                             <input type="hidden" id="h1" name="h[]" />
                                             <input type="file" class="form-control col-md-7 col-xs-12" name="pcat_page_banner" id="image_file1" onchange="fileSelectHandler('1', '500', '268')" />
                                             <img id="preview1" class="preview"/>
                                             <span class="help-inline">Choose 500(W) X 268(H)</span>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <!-- Category Banner -->
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
