<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class product_category extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Product categories';
            $this->load->library('form_validation');
            $this->load->model('category/category_model', 'marketCategory');
            $this->load->model('product_category_model', 'category');
       }

       public function index() {
            $categories['categories'] = $this->category->getCategories();
            $this->render_page(strtolower(__CLASS__) . '/list', $categories);
       }

       public function add() {
            if (!empty($_POST)) {
                 $data = array();
                 if (isset($_FILES['pcat_image']['name']) && !empty($_FILES['pcat_image']['name'])) {
                      /* Category image */
                      $newFileName = rand(9999999, 0) . $_FILES['pcat_image']['name'];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = $newFileName;
                      $this->load->library('upload', $config);

                      if (!$this->upload->do_upload('pcat_image')) {
                           array('error' => $this->upload->display_errors());
                      } else {
                           $data = array('upload_data' => $this->upload->data());
                           crop($this->upload->data(), $this->input->post());
                      }
                      $_POST['category']['pcat_image'] = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : '';
                      resize_thumb_proportion(FILE_UPLOAD_PATH . 'category/' . $_POST['category']['pcat_image'], 80, 80);
                 }

                 if (isset($_FILES['pcat_page_banner']['name']) && !empty($_FILES['pcat_page_banner']['name'])) {
                      /* Category banner */
                      $config = array();
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = rand(9999999, 0) . $_FILES['pcat_page_banner']['name'];

                      $this->load->library('upload');
                      $this->upload->initialize($config);

                      $pixel['x1'][0] = $_POST['x1'][1];
                      $pixel['y1'][0] = $_POST['y1'][1];
                      $pixel['x2'][0] = $_POST['x2'][1];
                      $pixel['y2'][0] = $_POST['y2'][1];
                      $pixel['w'][0] = $_POST['w'][1];
                      $pixel['h'][0] = $_POST['h'][1];

                      if (!$this->upload->do_upload('pcat_page_banner')) {
                           array('error' => $this->upload->display_errors());
                      } else {
                           $uploadData = $this->upload->data();
                           crop($this->upload->data(), $pixel);
                           $_POST['category']['pcat_page_banner'] = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                           resize_thumb_proportion(FILE_UPLOAD_PATH . 'category/' . $_POST['category']['pcat_page_banner'], 30, 50);
                      }
                 }
                 if ($this->category->addNewCategory($_POST['category'])) {
                      $this->session->set_flashdata('app_success', 'Category successfully added!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't add category!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $categories['marketCategories'] = $this->marketCategory->getCategories();
                 $categories['order'] = $this->category->getNextOrder();
                 $this->render_page(strtolower(__CLASS__) . '/add', $categories);
            }
       }

       public function update() {
            /**/
            $data = array();
            if (isset($_FILES['pcat_image']['name']) && !empty($_FILES['pcat_image']['name'])) {
                 $newFileName = rand(9999999, 0) . $_FILES['pcat_image']['name'];
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                 $config['allowed_types'] = 'gif|jpg|png';
                 $config['file_name'] = $newFileName;
                 $this->load->library('upload', $config);

                 if (!$this->upload->do_upload('pcat_image')) {
                      $data = array('error' => $this->upload->display_errors());
                 } else {
                      $data = array('upload_data' => $this->upload->data());
                      crop($this->upload->data(), $this->input->post());
                 }
                 /**/
                 $_POST['category']['pcat_image'] = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : '';
                 resize_thumb_proportion(FILE_UPLOAD_PATH . 'category/' . $_POST['category']['pcat_image'], 80, 80);
            }
            if (isset($_FILES['pcat_page_banner']['name']) && !empty($_FILES['pcat_page_banner']['name'])) {
                 /* Category banner */
                 $config = array();
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                 $config['allowed_types'] = 'gif|jpg|png';
                 $config['file_name'] = rand(9999999, 0) . $_FILES['pcat_page_banner']['name'];
                 $this->load->library('upload');
                 $this->upload->initialize($config);
                 $pixel['x1'][0] = $_POST['x1'][1];
                 $pixel['y1'][0] = $_POST['y1'][1];
                 $pixel['x2'][0] = $_POST['x2'][1];
                 $pixel['y2'][0] = $_POST['y2'][1];
                 $pixel['w'][0] = $_POST['w'][1];
                 $pixel['h'][0] = $_POST['h'][1];

                 if (!$this->upload->do_upload('pcat_page_banner')) {
                      array('error' => $this->upload->display_errors());
                 } else {
                      $uploadData = $this->upload->data();
                      crop($this->upload->data(), $pixel);
                      $_POST['category']['pcat_page_banner'] = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                      resize_thumb_proportion(FILE_UPLOAD_PATH . 'category/' . $_POST['category']['pcat_page_banner'], 30, 50);
                 }
            }
            if ($this->category->updateCategory($_POST['category'])) {
                 $this->session->set_flashdata('app_success', 'Category successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update category!");
            }
            redirect(strtolower(__CLASS__));
       }

       public function delete($id) {
            if ($this->category->deleteCategory($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete category"));
            }
       }

       public function view($id) {
            $id = encryptor($id, 'D');
            $categories['order'] = $this->category->getNextOrder(true);
            $categories['categories'] = $this->category->getCategories($id);
            $categories['marketCategories'] = $this->marketCategory->getCategories();
            $this->render_page(strtolower(__CLASS__) . '/view', $categories);
       }

       public function removeImage($id) {
            if ($this->category->removeCategoryImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category image successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete category image"));
            }
       }

       function removeBanner($id) {
            if ($this->category->removeBannerImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category banner successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete category logo"));
            }
       }

       function isShowOnMenu($id) {
            if ($this->category->isShowOnMenu($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category status successfully updated'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't update category status"));
            }
       }
       
       public function checkIfValueExists() {
            if ($this->category->checkIfValueExists($_POST)) {
                 header("HTTP/1.1 200 Ok"); //please dont change this.
            } else {
                 header("HTTP/1.0 404 Not Found"); //this too.
            }
       }
       
       /**
        * Function for change category status.
        * @param type $prodId
        * Author : JK
        */
       public function changestatus($pcatId) {
            $pcatId = encryptor($pcatId, 'D');
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            if ($this->common_model->changeStatus($pcatId, $ischecked, 'products_category', 'pcat_status', 'pcat_id')) {
                 generate_log(array(
                     'log_title' => 'Status changed',
                     'log_desc' => 'Product category status changes',
                     'log_controller' => 'pro-cat-status-changed',
                     'log_action' => 'U',
                     'log_ref_id' => $pcatId,
                     'log_added_by' => $this->uid,
                 ));
                 $msg = ($ischecked == 1) ? "Activated this record successfully" : "De-activated this record successfully";
                 die(json_encode(array('status' => 'success', 'msg' => $msg)));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
            }
       }
  }
  