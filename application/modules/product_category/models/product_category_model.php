<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class product_category_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       public function addNewCategory($datas) {
            $datas['pcat_slug'] = slugify($datas['pcat_title']);
            $datas['pcat_title'] = trim($datas['pcat_title']);
            //$datas['pcat_order'] = $this->getNextOrder();

            if ($this->db->insert(tbl_products_category, $datas)) {
                 return true;
            } else {
                 return false;
            }
       }

       public function getCategoryChaild($parent, $idNotin = '') {

            $this->db->select("pcat_id, pcat_title");
            $this->db->where('pcat_parent', $parent);
            if (!empty($idNotin)) {
                 $this->db->where('pcat_id !=', $idNotin);
            }
            $this->db->order_by('pcat_order', 'ASC');
            $result = $this->db->get(tbl_products_category)->result_array();
            return $result;
       }

       public function getCategories($id = '') {

            $this->db->select("gcat.*, gcat.pcat_show_on_home_page, gcat.pcat_image AS pcat_image, gcat.pcat_parent AS pcat_parent,"
                    . " gcat.pcat_desc AS category_desc, gcat.pcat_title AS category_name, "
                    . "gcat.pcat_id AS pcat_id, gcat2.pcat_title AS parent_category,"
                    . "gcat3.pcat_title AS root_category," . tbl_category . '.cat_title AS mkt_cat');
            $this->db->from(tbl_products_category . ' gcat');
            $this->db->join(tbl_products_category . ' gcat2', 'gcat.pcat_parent = gcat2.pcat_id', 'left');
            $this->db->join(tbl_category, tbl_category . '.cat_id = gcat.pcat_market_category', 'left');
            $this->db->join(tbl_products_category . ' gcat3', 'gcat2.pcat_parent = gcat3.pcat_id', 'left');
            if (!empty($id)) {
                 $this->db->where('gcat.pcat_id', $id);
                 return $this->db->get()->row_array();
            } else {
                 return $this->db->get()->result_array();
            }
       }

       public function updateCategory($datas) {

            //$dataWithOldPriority = $this->db->get_where(tbl_products_category, array('pcat_order' => $datas['pcat_order']))->row_array();
            //$dataWithNewPriority = $this->db->get_where(tbl_products_category, array('pcat_id' => $datas['pcat_id']))->row_array();
            $datas['pcat_slug'] = slugify($datas['pcat_title']);
            $datas['pcat_title'] = trim($datas['pcat_title']);
            $this->db->where('pcat_id', $datas['pcat_id']);
            $datas['pcat_is_name_pendent'] = isset($datas['pcat_is_name_pendent']) ? 1 : 0;
            unset($datas['pcat_id']);
            if ($this->db->update(tbl_products_category, $datas)) {

//                 if (!empty($dataWithOldPriority) && !empty($dataWithNewPriority)) {
//                      $this->db->where('pcat_id', $dataWithOldPriority['pcat_id']);
//                      $this->db->update(tbl_products_category, array('pcat_order' => $dataWithNewPriority['pcat_order']));
//                 }

                 return true;
            } else {
                 return false;
            }
       }

       public function deleteCategory($id) {

            $this->removeCategoryImage($id);
            $this->db->where('pcat_id', $id);
            if ($this->db->delete(tbl_products_category)) {
                 $this->db->where('pcat_parent', $id);
                 $this->db->delete(tbl_products_category);
                 return true;
            } else {
                 return false;
            }
       }

       public function categoryTree() {

            $this->db->select("cat.pcat_id AS category_id, cat.pcat_title AS category_name, cat2.pcat_title AS parent_category_name, cat2.pcat_id AS parent_category_id")
                    ->from(tbl_products_category . ' cat')
                    ->join(tbl_products_category . ' cat2', 'cat.pcat_parent = cat2.pcat_id', 'LEFT')
                    ->order_by('parent_category_name');
            $tree = $this->db->get()->result_array();
            return $tree;
       }

       public function removeCategoryImage($id) {
            if ($id) {
                 $this->db->where('pcat_id', $id);
                 $image = $this->db->get(tbl_products_category)->result_array();
                 $image = isset($image['0']) ? $image['0'] : array();
                 if (isset($image['pcat_image']) && !empty($image['pcat_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'category/' . $image['pcat_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'category/' . $image['pcat_image']);
                           unlink(FILE_UPLOAD_PATH . 'category/thumb_' . $image['pcat_image']);
                      }
                      $this->db->where('pcat_id', $id);
                      $this->db->update(tbl_products_category, array('pcat_image' => ''));
                      return true;
                 }
            }
            return false;
       }

       function removeBannerImage($id) {
            if ($id) {
                 $this->db->where('pcat_id', $id);
                 $image = $this->db->get(tbl_products_category)->row_array();
                 if (isset($image['pcat_page_banner']) && !empty($image['pcat_page_banner'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'category/' . $image['pcat_page_banner'])) {
                           unlink(FILE_UPLOAD_PATH . 'category/' . $image['pcat_page_banner']);
                           unlink(FILE_UPLOAD_PATH . 'category/thumb_' . $image['pcat_page_banner']);
                      }
                      $this->db->where('pcat_id', $id);
                      $this->db->update(tbl_products_category, array('pcat_page_banner' => ''));
                      return true;
                 }
            }
            return false;
       }

       function getNextOrder($max = false) {
            if ($max) {
                 return $this->db->count_all_results(tbl_products_category);
            } else {
                 return $this->db->select_max('pcat_order')->get(tbl_products_category)->row()->pcat_order + 1;
            }
       }

       function isShowOnMenu($id) {
            if (!empty($id)) {
                 $newStstue = $this->db->where('pcat_id', $id)->get(tbl_products_category)->row()->pcat_show_on_menu == 1 ? 0 : 1;
                 $this->db->where('pcat_id', $id)->update(tbl_products_category, array('pcat_show_on_menu' => $newStstue));
                 return true;
            } else {
                 return false;
            }
       }

       function checkIfValueExists($data) {
            $field = isset($data['field']) ? $data['field'] : '';
            $title = isset($data['category']['pcat_title']) ? strtolower(trim($data['category']['pcat_title'])) : '';
            if (isset($data['id']) && !empty($data['id'])) {
                 $this->db->where('pcat_id != ' . $data['id']);
            }
            $return = $this->db->like($field, $title, 'none')->get(tbl_products_category)->row_array();
            return (empty($return)) ? true : false;
       }
  }  