<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Complaints List</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="overflow-x:auto;">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Complaint </th>
                                        <th>Company</th>
                                        <th>Product</th>
                                        <!--<th>Company email</th>
                                        <th>Company website</th>
                                        <th>Transaction amount</th>
                                         
                                        <th>Complainted Date</th>
                                        <th>Complaint type</th>
                                            <th>Contact email</th>
                                                <th>Attachments </th>
                                                
                                                  <th>Contact person</th>
                                    
                                        <th>Contact phone</th>
                                    
                                        <th>View More</th>-->
                                      
   
                                        
                                      
                                        <?php echo check_permission($controller, 'update') ? '<th>Status</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $complaints as $key => $value) {
                                   
                                          ?>
                                          
                                          <tr data-url = "<?= base_url('complaint/complaint_details') ?>/<?= encryptor($value['id'],'E')?>" >
                                             
                                          <td class="trVOE"><?php echo "CMPL".$value['id'];?> </td>
                                             
                                               <td class="trVOE"><?php echo $value['usr_first_name'] ." ".$value['usr_last_name'] ;?></td>
                                               <td class="trVOE"><?php echo $value['trade_dispute'];?></td>
                                               <td class="trVOE"><?php echo $value['company_name'];?></td>
                                               <td class="trVOE"><?php echo $value['product_name'];?></td>
                                               <?php if (check_permission($controller, 'update')) {?>
                                                  <td>
                                                              <label class="switch">
                                                                   <input type="checkbox" value="1" class="chkOnchange"
                                                                   <?php echo $value['status'] == 1 ? 'checked' : '';?>
                                                                          data-url="<?php echo site_url($controller . '/updatestatus/' . encryptor($value['id']));?>">
                                                                   <span class="slider round"></span>
                                                              </label>
                                                         </td>
                                               <?php }?>
                                               
                                          </tr>
                                         
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>