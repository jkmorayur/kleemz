<?php

  defined('BASEPATH') or exit('No direct script access allowed');
  require './vendor/autoload.php';

  class supplier extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Supplier';
            $this->load->library('upload');
            $this->load->model('supplier_model', 'supplier');
            $this->load->model('market/market_model', 'market');
            $this->load->model('building/building_model', 'building');
            $this->load->model('emp_details/emp_details_model', 'emp_details');
            $this->load->model('supplier_grade/supplier_grade_model', 'supplier_grade');
            $this->load->model('category/category_model', 'category_model');
            require_once(APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php');
            $this->mail = new PHPMailer;
       }

       public function index($status = '') {
            $this->lock_in();

            $this->load->library("pagination");
            $limit = 20;
            $page = !isset($_GET['page']) ? 0 : $_GET['page'];
            $linkParts = explode('&page=', current_url() . '?' . $_SERVER['QUERY_STRING']);
            $link = $linkParts[0];
            $config = getPaginationDesign();

            $data = $_GET;
            $business = $this->supplier->suppliers('', $limit, $page, $_GET);
            $data['suppliers'] = $business['data'];

            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config["base_url"] = $link;
            $config["total_rows"] = $business['count'];
            $config["per_page"] = $limit;
            $config["uri_segment"] = 3;

            /* Table info */
            $data['pageIndex'] = $page + 1;
            $data['limit'] = $page + $limit;
            $data['totalRow'] = number_format($business['count']);
            /* Table info */

            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       function pendingApproval() {
            $this->lock_in();

            $this->load->library("pagination");
            $limit = 20;
            $page = !isset($_GET['page']) ? 0 : $_GET['page'];
            $linkParts = explode('&page=', current_url() . '?' . $_SERVER['QUERY_STRING']);
            $link = $linkParts[0];
            $config = getPaginationDesign();

            $data = $_GET;
            $business = $this->supplier->pendingApproval('', $limit, $page, $_GET);
            $data['suppliers'] = $business['data'];

            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config["base_url"] = $link;
            $config["total_rows"] = $business['count'];
            $config["per_page"] = $limit;
            $config["uri_segment"] = 3;

            /* Table info */
            $data['pageIndex'] = $page + 1;
            $data['limit'] = $page + $limit;
            $data['totalRow'] = number_format($business['count']);
            /* Table info */

            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function report() {

            $date1 = isset($_POST['start_date']) ? $_POST['start_date'] : null;
            $date2 = isset($_POST['end_date']) ? $_POST['end_date'] : null;

            if ($date1 && $date2) {
                 $exportData = $this->supplier->suppliersbyDates($date1, $date2);

                 if (empty($exportData)) {

                      $this->session->set_flashdata('app_error', "No Data exist between $date1 and $date2");
                      redirect(strtolower(__CLASS__));
                 }
                 if ($_POST['type'] == 'excel') {
                      $this->load->library('excel');
                      $this->excel->stream("Suppliers Report ($date1 -$date2)-" . now() . " .xls", $exportData);
                 } else {
                      $html = $this->getHtmlTable($exportData);
                      $pdfFilePath = "supplierReport (" . $date1
                              . "-" . $date2 . ")-" . now() . ".pdf";
                      $this->load->library('m_pdf');
                      $this->m_pdf->pdf->WriteHTML($html);
                      $this->m_pdf->pdf->Output($pdfFilePath, "D");
                      //$serverpath = "export/pdfreport/supplierReport-($date1 
                      //-$date2)-".now().".pdf";
                      //$this->m_pdf->pdf->Output($serverpath,'F');
                 }

                 $this->session->set_flashdata('app_success', 'Report Generated Succesfully');
                 redirect(strtolower(__CLASS__));
            } else {
                 $this->session->set_flashdata('app_error', 'Invalid dates');
                 redirect(strtolower(__CLASS__));
            }
       }

       public function getHtmlTable($exportData) {

            $start = "<style type='text/css'>
.tg  {border-collapse:collapse;border-spacing:0;border-width:1px;border-style:solid;border-color:#aaa;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
.tg .tg-7d57{background-color:#FCFBE3;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
<div class='tg-wrap'><table class='tg'>
  <tr>
   <th class='tg-0pky'><span style='font-weight:bold'>Sl No</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier Name</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier Refno</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier Country</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier State</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier Email</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier Phone</span></th>
    <th class='tg-0pky'><span style='font-weight:bold'>Supplier Website</span></th>

  </tr>";
            $tr = '';
            $k = 1;
            foreach ($exportData as $key => $td) {

                 $tr = $tr . "<tr>
    <td class='tg-7d57'>" . $k . "</td>
    <td class='tg-7d57'>" . $td['supplier name'] . "</td>
    <td class='tg-7d57'>" . $td['supplier refno'] . "</td>
    <td class='tg-7d57'>" . $td['supplier country'] . "</td>  
    <td class='tg-7d57'>" . $td['supplier state'] . "</td>
    <td class='tg-7d57'>" . $td['supplier email'] . "</td>
    <td class='tg-7d57'>" . $td['supplier phone'] . "</td>  
    <td class='tg-7d57'>" . $td['supplier website'] . "</td>  

  </tr>";
                 $k++;
            }



            return $start . $tr . "</table></div>";
       }

       public function view($id) {
            $this->lock_in();
            $id = encryptor($id, 'D');
            $data['suppliers'] = $this->supplier->suppliers($id);
            $this->selectedCategories = $data['suppliers']['categories'];
            $countryId = isset($data['suppliers']['supm_country']) ? $data['suppliers']['supm_country'] : 0;
            $stateId = isset($data['suppliers']['supm_state']) ? $data['suppliers']['supm_state'] : 0;
            $data['businessTypes'] = $this->supplier->getBusinessTypes();
            $data['states'] = get_state_province('', $countryId);
            $data['country'] = get_country_list();
            $data['district'] = get_district_by_state($stateId);
            $data['grades'] = $this->supplier_grade->get();
            $data['buildings'] = $this->building->getBuilding();
            $data['marketPlaces'] = $this->market->gerMarketPlaces();
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function add() {
            $this->lock_in();
            if (!empty($_POST)) {
                 /* Panoramic view */
                 if (isset($_FILES['panoramaImage']['name']) && !empty($_FILES['panoramaImage']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      /* Panorama image */
                      $newFileName = microtime(true) . clean_image_name($_FILES['panoramaImage']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'panorama-shop/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];
                      if ($this->upload->do_upload('panoramaImage')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['supplier']['supm_panoramic_image'] = $uploadData['file_name'];
                      } else {
                           //debug($this->upload->display_errors(), 0);
                      }
                 }
                 /* Company logo */
                 if ($supplierID = $this->supplier->newSupplier($_POST)) {
                      /* Banner image */
                      $x12 = $this->input->post('x12');
                      $fileCount = count($x12);
                      if ($fileCount > 0) {
                           for ($j = 0; $j < $fileCount; $j++) {
                                if (isset($_FILES['supm_home_banner']['name'][$j]) && !empty($_FILES['supm_home_banner']['name'][$j])) {
                                     $pixel = array();
                                     $uploadData = array();
                                     $newFileName = microtime(true) . clean_image_name($_FILES['supm_home_banner']['name'][$j]);
                                     $config['upload_path'] = FILE_UPLOAD_PATH . 'home_banner/';
                                     $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                     $config['file_name'] = $newFileName;
                                     $this->upload->initialize($config);
                                     $pixel['x1'][0] = $_POST['x12'][$j];
                                     $pixel['y1'][0] = $_POST['y12'][$j];
                                     $pixel['x2'][0] = $_POST['x22'][$j];
                                     $pixel['y2'][0] = $_POST['y22'][$j];
                                     $pixel['w'][0] = $_POST['w2'][$j];
                                     $pixel['h'][0] = $_POST['h2'][$j];
                                     $_FILES['prd_image_tmp']['name'] = $_FILES['supm_home_banner']['name'][$j];
                                     $_FILES['prd_image_tmp']['type'] = $_FILES['supm_home_banner']['type'][$j];
                                     $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['supm_home_banner']['tmp_name'][$j];
                                     $_FILES['prd_image_tmp']['error'] = $_FILES['supm_home_banner']['error'][$j];
                                     $_FILES['prd_image_tmp']['size'] = $_FILES['supm_home_banner']['size'][$j];
                                     if ($this->upload->do_upload('prd_image_tmp')) {

                                          $alttag = isset($_POST['altSupBanImgs'][$j]) ? $_POST['altSupBanImgs'][$j] : 'Banner image';
                                          $uploadData = $this->upload->data();
                                          crop($uploadData, $pixel);
                                          $this->supplier->addBannerImages(array('sbi_supplier' => $supplierID, 'sbi_image' => $uploadData['file_name']
                                              , 'sbi_alttag' => $alttag
                                          ));
                                     } else {
                                          //debug($this->upload->display_errors(), 0);
                                     }
                                }
                           }
                      }
                      /* Company logo */
                      if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                           $pixel = array();
                           $uploadData = array();
                           $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'avatar/';
                           $config['allowed_types'] = 'gif|jpg|png|jpeg';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);
                           $pixel['x1'][0] = $_POST['x10'];
                           $pixel['y1'][0] = $_POST['y10'];
                           $pixel['x2'][0] = $_POST['x20'];
                           $pixel['y2'][0] = $_POST['y20'];
                           $pixel['w'][0] = $_POST['w0'];
                           $pixel['h'][0] = $_POST['h0'];
                           if ($this->upload->do_upload('userAvatar')) {
                                $uploadData = $this->upload->data();
                                crop($uploadData, $pixel);
                                $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                           } else {
                                //debug($this->upload->display_errors(), 0);
                           }
                      }
                      $x13 = $this->input->post('x13');
                      $fileCount = count($x13);
                      for ($j = 0; $j < $fileCount; $j++) {
                           if (isset($_FILES['shopImages']['name'][$j]) && !empty($_FILES['shopImages']['name'][$j])) {
                                $uploadData = array();
                                $pixel = array();
                                $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                                $config['upload_path'] = FILE_UPLOAD_PATH . 'shops/';
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                $config['file_name'] = $newFileName;
                                $this->upload->initialize($config);
                                $pixel['x1'][0] = $_POST['x13'][$j];
                                $pixel['y1'][0] = $_POST['x13'][$j];
                                $pixel['x2'][0] = $_POST['x13'][$j];
                                $pixel['y2'][0] = $_POST['x13'][$j];
                                $pixel['w'][0] = $_POST['w3'][$j];
                                $pixel['h'][0] = $_POST['h3'][$j];
                                $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                                $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                                $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                                $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                                $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];
                                if (!$this->upload->do_upload('prd_image_tmp')) {
                                     //debug($this->upload->display_errors(), 0);
                                } else {
                                     $uploadData = $this->upload->data();
                                     crop($uploadData, $pixel);
                                     $imgId = $this->supplier->addImages(array('ssi_supplier' => $supplierID,
                                         'ssi_image' => $uploadData['file_name'],
                                         'ssi_alttag' => $_POST['altshopImgs'][$j]));
                                     //for home page banner
                                     resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 724, 378);
                                     //for view all supplier
                                     resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 260, 100);
                                     if ($j == 0) {
                                          $this->supplier->setDefaultImage($imgId, $supplierID);
                                     }
                                }
                           }
                      }

                      /* Web or app banner */
                      $x14 = $this->input->post('x14');
                      $fileCount = count($x14);
                      if ($fileCount > 0) {
                           for ($j = 0; $j < $fileCount; $j++) {
                                if (isset($_FILES['supm_home_banner_mobile']['name'][$j]) && !empty($_FILES['supm_home_banner_mobile']['name'][$j])) {
                                     $pixel = array();
                                     $uploadData = array();
                                     $newFileName = microtime(true) . clean_image_name($_FILES['supm_home_banner_mobile']['name'][$j]);
                                     $config['upload_path'] = FILE_UPLOAD_PATH . 'home_banner/';
                                     $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                     $config['file_name'] = $newFileName;
                                     $this->upload->initialize($config);
                                     $pixel['x1'][0] = $_POST['x14'][$j];
                                     $pixel['y1'][0] = $_POST['y14'][$j];
                                     $pixel['x2'][0] = $_POST['x24'][$j];
                                     $pixel['y2'][0] = $_POST['y24'][$j];
                                     $pixel['w'][0] = $_POST['w4'][$j];
                                     $pixel['h'][0] = $_POST['h4'][$j];
                                     $_FILES['prd_image_tmp']['name'] = $_FILES['supm_home_banner_mobile']['name'][$j];
                                     $_FILES['prd_image_tmp']['type'] = $_FILES['supm_home_banner_mobile']['type'][$j];
                                     $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['supm_home_banner_mobile']['tmp_name'][$j];
                                     $_FILES['prd_image_tmp']['error'] = $_FILES['supm_home_banner_mobile']['error'][$j];
                                     $_FILES['prd_image_tmp']['size'] = $_FILES['supm_home_banner_mobile']['size'][$j];
                                     if ($this->upload->do_upload('prd_image_tmp')) {
                                          $alttag = isset($_POST['altSupBanmobImgs'][$j]) ? $_POST['altSupBanmobImgs'][$j] : 'Banner image';
                                          $uploadData = $this->upload->data();
                                          crop($uploadData, $pixel);
                                          $this->supplier->addBannerImages(array('sbi_type' => 2, 'sbi_supplier' => $supplierID, 'sbi_image' =>
                                              $uploadData['file_name'],
                                              'sbi_alttag' => $alttag
                                          ));
                                     } else {
                                          //debug($this->upload->display_errors(), 0);
                                     }
                                }
                           }
                      }

                      $_POST['user']['usr_group'] = 2;
                      $_POST['user']['usr_supplier'] = $supplierID;
                      $_POST['user']['usr_username'] = $_POST['user']['usr_first_name'];
                      if ($id = $this->emp_details->register($_POST['user'])) {
                           // default permissions for supplier - only added when registered by admin
                           if ($this->usr_grp == 'SA') {
                                $this->load->model('supplier_privilege/supplier_privilege_model', 'supplier_privilege');
                                $permission['cua_group_id'] = $id;
                                $permission['cua_access']['emp_details'] = array('index', 'add', 'view', 'update', 'delete');
                                $permission['cua_access']['product'] = array('index', 'add', 'view', 'update', 'delete', 'rfq', 'rfq_list', 'pending_rfq_list');
                                $permission['cua_access']['user_permission'] = array('index');
                                $permission['cua_access']['order'] = array('index', 'order_summery', 'changeorderstatuse');
                                $this->supplier_privilege->addUserPermission($permission);
                           }
                           $this->session->set_flashdata('app_success', 'Row successfully added!');
                      } else {
                           $this->session->set_flashdata('app_error', 'Row successfully added!');
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Row successfully added!');
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['businessTypes'] = $this->supplier->getBusinessTypes();
                 $data['grades'] = $this->supplier_grade->get();
                 $data['country'] = get_country_list();
                 $data['states'] = get_state_province();
                 $data['district'] = get_district_by_state();
                 $data['buildings'] = $this->building->getBuilding();
                 $data['marketPlaces'] = $this->market->gerMarketPlaces();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       public function update() {
            $updateSupplierProfile = isset($_POST['update_by_admin']) ? $_POST['update_by_admin'] : 0;
            if ($updateSupplierProfile == 1) {
                 $this->lock_in();
            }
            $supplierID = isset($_POST['supm_id']) ? $_POST['supm_id'] : 0;
            if (!empty($supplierID)) {
                 /* Panoramic view */
                 if (isset($_FILES['panoramaImage']['name']) && !empty($_FILES['panoramaImage']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      /* Panorama image */
                      $newFileName = microtime(true) . clean_image_name($_FILES['panoramaImage']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'panorama-shop/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];
                      if ($this->upload->do_upload('panoramaImage')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['supplier']['supm_panoramic_image'] = $uploadData['file_name'];
                      }
                 }
                 /* Banner image */
                 $x12 = $this->input->post('x12');
                 $fileCount = count($x12);
                 if ($fileCount > 0) {
                      for ($j = 0; $j < $fileCount; $j++) {
                           if (isset($_FILES['supm_home_banner']['name'][$j]) && !empty($_FILES['supm_home_banner']['name'][$j])) {
                                $pixel = array();
                                $uploadData = array();
                                $newFileName = microtime(true) . clean_image_name($_FILES['supm_home_banner']['name'][$j]);
                                $config['upload_path'] = FILE_UPLOAD_PATH . 'home_banner/';
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                $config['file_name'] = $newFileName;
                                $this->upload->initialize($config);
                                $pixel['x1'][0] = $_POST['x12'][$j];
                                $pixel['y1'][0] = $_POST['y12'][$j];
                                $pixel['x2'][0] = $_POST['x22'][$j];
                                $pixel['y2'][0] = $_POST['y22'][$j];
                                $pixel['w'][0] = $_POST['w2'][$j];
                                $pixel['h'][0] = $_POST['h2'][$j];
                                $_FILES['prd_image_tmp']['name'] = $_FILES['supm_home_banner']['name'][$j];
                                $_FILES['prd_image_tmp']['type'] = $_FILES['supm_home_banner']['type'][$j];
                                $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['supm_home_banner']['tmp_name'][$j];
                                $_FILES['prd_image_tmp']['error'] = $_FILES['supm_home_banner']['error'][$j];
                                $_FILES['prd_image_tmp']['size'] = $_FILES['supm_home_banner']['size'][$j];
                                if ($this->upload->do_upload('prd_image_tmp')) {
                                     $alttag = isset($_POST['altSupBanImgs'][$j]) ? $_POST['altSupBanImgs'][$j] : 'Banner image';

                                     $uploadData = $this->upload->data();
                                     crop($uploadData, $pixel);
                                     $this->supplier->addBannerImages(array('sbi_supplier' => $supplierID, 'sbi_image' => $uploadData['file_name'],
                                         'sbi_alttag' => $alttag

                                             //$uploadData['file_name']
                                     ));
                                }
                           }
                      }
                 }

                 /* Web or app banner */
                 $x12 = $this->input->post('x14');
                 $fileCount = count($x12);
                 if ($fileCount > 0) {
                      for ($j = 0; $j < $fileCount; $j++) {
                           if (isset($_FILES['supm_home_banner_mobile']['name'][$j]) && !empty($_FILES['supm_home_banner_mobile']['name'][$j])) {
                                $pixel = array();
                                $uploadData = array();
                                $newFileName = microtime(true) . clean_image_name($_FILES['supm_home_banner_mobile']['name'][$j]);
                                $config['upload_path'] = FILE_UPLOAD_PATH . 'home_banner/';
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                $config['file_name'] = $newFileName;
                                $this->upload->initialize($config);
                                $pixel['x1'][0] = $_POST['x14'][$j];
                                $pixel['y1'][0] = $_POST['y14'][$j];
                                $pixel['x2'][0] = $_POST['x24'][$j];
                                $pixel['y2'][0] = $_POST['y24'][$j];
                                $pixel['w'][0] = $_POST['w4'][$j];
                                $pixel['h'][0] = $_POST['h4'][$j];
                                $_FILES['prd_image_tmp']['name'] = $_FILES['supm_home_banner_mobile']['name'][$j];
                                $_FILES['prd_image_tmp']['type'] = $_FILES['supm_home_banner_mobile']['type'][$j];
                                $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['supm_home_banner_mobile']['tmp_name'][$j];
                                $_FILES['prd_image_tmp']['error'] = $_FILES['supm_home_banner_mobile']['error'][$j];
                                $_FILES['prd_image_tmp']['size'] = $_FILES['supm_home_banner_mobile']['size'][$j];
                                if ($this->upload->do_upload('prd_image_tmp')) {
                                     $alttag = isset($_POST['altSupBanmobImgs'][$j]) ? $_POST['altSupBanmobImgs'][$j] : 'Banner image';
                                     $uploadData = $this->upload->data();
                                     crop($uploadData, $pixel);
                                     $this->supplier->addBannerImages(array('sbi_type' => 2, 'sbi_supplier' => $supplierID, 'sbi_image' =>
                                         $uploadData['file_name'],
                                         'sbi_alttag' => $alttag
                                     ));
                                }
                           }
                      }
                 }

                 $this->supplier->updateSupplier($_POST);
                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'avatar/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x10'];
                      $pixel['y1'][0] = $_POST['y10'];
                      $pixel['x2'][0] = $_POST['x20'];
                      $pixel['y2'][0] = $_POST['y20'];
                      $pixel['w'][0] = $_POST['w0'];
                      $pixel['h'][0] = $_POST['h0'];
                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                      }
                 }

                 $_POST['user']['usr_group'] = 6;
                 $_POST['user']['usr_supplier'] = $supplierID;
                 $this->emp_details->update($_POST['user']);
                 $x13 = $this->input->post('x13');
                 $fileCount = count($x13);
                 for ($j = 0; $j < $fileCount; $j++) {
                      if (isset($_FILES['shopImages']['name'][$j]) && !empty($_FILES['shopImages']['name'][$j])) {
                           $uploadData = array();
                           $pixel = array();
                           $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'shops/';
                           $config['allowed_types'] = 'gif|jpg|png|jpeg';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);
                           $pixel['x1'][0] = $_POST['x13'][$j];
                           $pixel['y1'][0] = $_POST['y13'][$j];
                           $pixel['x2'][0] = $_POST['x23'][$j];
                           $pixel['y2'][0] = $_POST['y23'][$j];
                           $pixel['w'][0] = $_POST['w3'][$j];
                           $pixel['h'][0] = $_POST['h3'][$j];
                           $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                           $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                           $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                           $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                           $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];
                           if (!$this->upload->do_upload('prd_image_tmp')) {
                                $this->upload->display_errors();
                           } else {
                                $uploadData = $this->upload->data();
                                crop($uploadData, $pixel);
                                $this->supplier->addImages(array('ssi_supplier' => $supplierID, 'ssi_image' => $uploadData['file_name'], 'ssi_alttag' => $_POST['altshopImgs'][$j]));

                                //for home page banner
                                resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 724, 378);
                                //for view all supplier
                                resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 260, 100);
                           }
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Supplier updated successfully!');
                 if ($updateSupplierProfile == 1) {
                      redirect(strtolower(__CLASS__));
                 } else {
                      redirect('supplier/myprofile');
                 }
            } else {
                 $this->session->set_flashdata('app_success', 'Error occured!');
                 redirect(strtolower(__CLASS__));
            }
       }

       public function setDefaultBannerImage($imageId, $supId) {
            if (!empty($imageId)) {
                 if ($this->supplier->setDefaultBannerImage($imageId, $supId)) {
                      echo json_encode(array('status' => 'success', 'msg' => 'This image set as default'));
                      die();
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => "Can't set this image as default"));
                      die();
                 }
            }
       }

       public function setDefaultImage($imageId, $supplierId) {
            if (!empty($imageId) && !empty($supplierId)) {
                 if ($this->supplier->setDefaultImage($imageId, $supplierId)) {
                      echo json_encode(array('status' => 'success', 'msg' => 'This image set as default'));
                      die();
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => "Can't set this image as default"));
                      die();
                 }
            }
       }

       public function removeAvatar($userId) {
            if (!empty($userId)) {
                 if ($this->supplier->removeAvatar($userId)) {
                      echo json_encode(array('status' => 'success', 'msg' => 'Avatar removed'));
                      die();
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => "Can't remove avatar"));
                      die();
                 }
            }
       }

       function removeSupplierBanner($supplierId) {
            if (!empty($supplierId)) {
                 if ($this->supplier->removeSupplierBanner($supplierId)) {
                      die(json_encode(array('status' => 'success', 'msg' => 'Banner removed')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't remove banner")));
                 }
            }
       }

       public function removeImage($id) {
            if ($this->supplier->removeImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Building image successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete project image"));
                 die();
            }
       }

       /* To check given value exist or not using parsley remote validation.
         --edited by irfan.
        */

       public function checkIfValueExists() {
            if ($this->supplier->checkIfValueExists($_POST)) {
                 header("HTTP/1.1 200 Ok"); //please dont change this.
            } else {
                 header("HTTP/1.0 404 Not Found"); //this too.
            }
       }

       /**
        * Function for verify supplier
        * @param int $id
        * Author : JK
        */
       function changestatus($supplierId, $userId) {
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            $supplierId = encryptor($supplierId, 'D');
            $userId = encryptor($userId, 'D');

            $msg = ($ischecked == 1) ? "Supplier successfully activated" : "Supplier successfully de-activated";
            $logMsg = $msg . ', supplierID:' . $supplierId . ' & userID:' . $userId;
            if ($this->supplier->verifySupplier($supplierId, $userId, $ischecked)) {
                 generate_log(array(
                     'log_title' => 'Status changed',
                     'log_desc' => $logMsg,
                     'log_controller' => 'supplier-status-changed',
                     'log_action' => 'U',
                     'log_ref_id' => $supplierId,
                     'log_added_by' => $this->uid
                 ));
                 die(json_encode(array('status' => 'success', 'msg' => $msg)));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't varify supplier")));
            }
       }

       function supplier_list() {
            $filter = '';
            if ($this->input->get('page')) {
                 $params = $this->input->get();
                 if ($params['category']) {
                      $filter['category'] = $params['category'];
                 }
                 $count = $this->supplier->countAllSupplier($filter);
                 if ($params['order_by']) {
                      $filter['order_by'] = $params['order_by'];
                 }
                 pagination($this->input->get('page'), base_url() . "supplier/supplier_list", $count, 'supplier', 'getSupplierListing', $filter);
            }
            $data['categories'] = $this->common_model->getAllCategories();
            $this->page_title = 'Home | Supplier Listing';
            $this->template->set_layout('supplier');
            $this->render_page(strtolower(__CLASS__) . '/supplier_list.php', $data);
       }

       /**
        * Function for verify supplier
        * @param int $id
        * Author : JK
        */
       function verifySupplier($supplierId, $userId) {
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            $supplierId = encryptor($supplierId, 'D');
            $userId = encryptor($userId, 'D');

            $msg = ($ischecked == 1) ? "Supplier successfully activated" : "Supplier successfully de-activated";

            if ($this->supplier->verifySupplier($supplierId, $userId, $ischecked)) {
                 die(json_encode(array('status' => 'success', 'msg' => $msg)));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't varify supplier")));
            }
       }

       public function activate_supplier($id = '') {

            if (!$this->ion_auth->logged_in()) {
                 $this->session->set_userdata(array('callback' => site_url(strtolower(__CLASS__)) . '/activate_supplier' . '/' . $id));
                 redirect('user/login');
            }

            if (!privilege_exists('SP') && empty($_POST) && !is_root_user()) {
                 $count = $this->supplier->checkSupplierActivation($id);
                 if ($count <= 0) {
                      redirect('dashboard');
                 } else {
                      $id = '';
                 }
            }
            $supplierID = isset($_POST['supm_id']) ? $_POST['supm_id'] : 0;
            $this->load->library('upload');
            $enc_id = $id;
            $id = encryptor($id, 'D');
            if (privilege_exists('SP') && $id != $this->session->userdata('usr_supplier')) {
                 $id = $this->session->userdata('usr_supplier');
            }
            if (privilege_exists('SP') && $this->usr_grp != 'SP') {
                 $this->session->set_userdata('grp_slug', 'SP');
                 redirect(strtolower(__CLASS__) . '/activate-supplier/' . $enc_id);
            }
            if (!empty($_POST)) {
                 /* Panoramic view */
                 if (isset($_FILES['panoramaImage']['name']) && !empty($_FILES['panoramaImage']['name'])) {
                      $pixel = array();
                      $uploadData = array();

                      /* Panorama image */
                      $newFileName = microtime(true) . clean_image_name($_FILES['panoramaImage']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'panorama-shop/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];

                      if ($this->upload->do_upload('panoramaImage')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['supplier']['supm_panoramic_image'] = $uploadData['file_name'];
                      }
                 }

                 /* Banner image */
                 $x12 = $this->input->post('x12');
                 $fileCount = count($x12);
                 if ($fileCount > 0) {
                      for ($j = 0; $j < $fileCount; $j++) {
                           if (isset($_FILES['supm_home_banner']['name'][$j]) && !empty($_FILES['supm_home_banner']['name'][$j])) {
                                $pixel = array();
                                $uploadData = array();
                                $newFileName = microtime(true) . clean_image_name($_FILES['supm_home_banner']['name'][$j]);
                                $config['upload_path'] = FILE_UPLOAD_PATH . 'home_banner/';
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                $config['file_name'] = $newFileName;
                                $this->upload->initialize($config);
                                $pixel['x1'][0] = $_POST['x12'][$j];
                                $pixel['y1'][0] = $_POST['y12'][$j];
                                $pixel['x2'][0] = $_POST['x22'][$j];
                                $pixel['y2'][0] = $_POST['y22'][$j];
                                $pixel['w'][0] = $_POST['w2'][$j];
                                $pixel['h'][0] = $_POST['h2'][$j];
                                $_FILES['prd_image_tmp']['name'] = $_FILES['supm_home_banner']['name'][$j];
                                $_FILES['prd_image_tmp']['type'] = $_FILES['supm_home_banner']['type'][$j];
                                $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['supm_home_banner']['tmp_name'][$j];
                                $_FILES['prd_image_tmp']['error'] = $_FILES['supm_home_banner']['error'][$j];
                                $_FILES['prd_image_tmp']['size'] = $_FILES['supm_home_banner']['size'][$j];
                                if ($this->upload->do_upload('prd_image_tmp')) {
                                     $alttag = isset($_POST['altSupBanImgs'][$j]) ? $_POST['altSupBanImgs'][$j] : 'Banner image';

                                     $uploadData = $this->upload->data();
                                     crop($uploadData, $pixel);
                                     $this->supplier->addBannerImages(array('sbi_supplier' => $supplierID, 'sbi_image' => $uploadData['file_name'],
                                         'sbi_alttag' => $alttag

                                             //$uploadData['file_name']
                                     ));
                                }
                           }
                      }
                 }

                 /* Web or app banner */

                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();

                      $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'avatar/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $pixel['x1'][0] = $_POST['x10'];
                      $pixel['y1'][0] = $_POST['y10'];
                      $pixel['x2'][0] = $_POST['x20'];
                      $pixel['y2'][0] = $_POST['y20'];
                      $pixel['w'][0] = $_POST['w0'];
                      $pixel['h'][0] = $_POST['h0'];

                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                      }
                 }

                 /* Web or app banner */
                 $x14 = $this->input->post('x14');
                 $fileCount = count($x14);
                 if ($fileCount > 0) {
                      for ($j = 0; $j < $fileCount; $j++) {
                           if (isset($_FILES['supm_home_banner_mobile']['name'][$j]) && !empty($_FILES['supm_home_banner_mobile']['name'][$j])) {
                                $pixel = array();
                                $uploadData = array();
                                $newFileName = microtime(true) . clean_image_name($_FILES['supm_home_banner_mobile']['name'][$j]);
                                $config['upload_path'] = FILE_UPLOAD_PATH . 'home_banner/';
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                $config['file_name'] = $newFileName;
                                $this->upload->initialize($config);
                                $pixel['x1'][0] = $_POST['x14'][$j];
                                $pixel['y1'][0] = $_POST['y14'][$j];
                                $pixel['x2'][0] = $_POST['x24'][$j];
                                $pixel['y2'][0] = $_POST['y24'][$j];
                                $pixel['w'][0] = $_POST['w4'][$j];
                                $pixel['h'][0] = $_POST['h4'][$j];
                                $_FILES['prd_image_tmp']['name'] = $_FILES['supm_home_banner_mobile']['name'][$j];
                                $_FILES['prd_image_tmp']['type'] = $_FILES['supm_home_banner_mobile']['type'][$j];
                                $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['supm_home_banner_mobile']['tmp_name'][$j];
                                $_FILES['prd_image_tmp']['error'] = $_FILES['supm_home_banner_mobile']['error'][$j];
                                $_FILES['prd_image_tmp']['size'] = $_FILES['supm_home_banner_mobile']['size'][$j];
                                if ($this->upload->do_upload('prd_image_tmp')) {
                                     $alttag = isset($_POST['altSupBanmobImgs'][$j]) ? $_POST['altSupBanmobImgs'][$j] : 'Banner image';
                                     $uploadData = $this->upload->data();
                                     crop($uploadData, $pixel);
                                     $this->supplier->addBannerImages(array('sbi_type' => 2, 'sbi_supplier' => $supplierID, 'sbi_image' =>
                                         $uploadData['file_name'],
                                         'sbi_alttag' => $alttag
                                     ));
                                }
                           }
                      }
                 }

                 if (isset($_POST['supm_id']) && !empty($_POST['supm_id'])) { // Update if exists already.
                      if (!empty($supplierID)) {
                           $this->supplier->updateSupplier($_POST);
                           $_POST['user']['usr_group'] = 2; // changed 6 to 2 
                           $_POST['user']['usr_supplier'] = $supplierID;
                           $this->emp_details->update($_POST['user']);
                      }
                 } else {// Add as new row if not exists already.
                      if ($supplierID = $this->supplier->newSupplier($_POST)) {
                           if ($this->session->userdata('usr_supplier') == 0) {
                                $this->session->set_userdata('usr_supplier', $supplierID);
                           }
                           $_POST['user']['usr_group'] = array(4, 2);
                           $_POST['user']['usr_supplier'] = $supplierID;
                           $this->emp_details->update($_POST['user']);
                      }
                 }

                 $x13 = $this->input->post('x13');
                 $fileCount = count($x13);
                 for ($j = 0; $j < $fileCount; $j++) {
                      if (isset($_FILES['shopImages']['name'][$j]) && !empty($_FILES['shopImages']['name'][$j])) {
                           $uploadData = array();
                           $pixel = array();
                           $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'shops/';
                           $config['allowed_types'] = 'gif|jpg|png|jpeg';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);

                           $pixel['x1'][0] = $_POST['x13'][$j];
                           $pixel['y1'][0] = $_POST['x13'][$j];
                           $pixel['x2'][0] = $_POST['x13'][$j];
                           $pixel['y2'][0] = $_POST['x13'][$j];
                           $pixel['w'][0] = $_POST['w3'][$j];
                           $pixel['h'][0] = $_POST['h3'][$j];

                           $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                           $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                           $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                           $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                           $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];
                           if (!$this->upload->do_upload('prd_image_tmp')) {
                                $this->upload->display_errors();
                           } else {
                                $uploadData = $this->upload->data();
                                crop($uploadData, $pixel);
                                //for home page banner
                                resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 724, 378);
                                //for view all supplier
                                resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 260, 100);
                                $this->supplier->addImages(array('ssi_supplier' => $supplierID, 'ssi_image' => $uploadData['file_name']));
                           }
                      }
                 }
                 if (privilege_exists('SP')) {
                      $this->session->set_flashdata('app_success', 'Updated successfully');
                 } else {
                      $this->session->set_flashdata('app_success', 'Your request to activate as supplier is successfully completed, we will touch you soon!');
                 }

                 redirect(strtolower(__CLASS__) . '/activate-supplier/' . encryptor($supplierID));
            } else {

                 if (!empty($id)) {
                      $data['suppliers'] = $this->supplier->suppliers($id);
                      $countryId = isset($data['suppliers'] ['supm_country']) ? $data['suppliers']['supm_country'] : 0;
                      $data['states'] = get_state_province('', $countryId);
                      $data['country'] = get_country_list();
                      $data['grades'] = $this->supplier_grade->get();
                      $data['buildings'] = $this->building->getBuilding();
                      $data['marketPlaces'] = $this->market->gerMarketPlaces();
                      $data['buildingsCategories'] = array();
                      if (isset($data['suppliers']['supm_building']))
                           $data['buildingsCategories'] = $this->supplier->getBuildingsCategories($data['suppliers']['supm_building']);
                 } else {
                      $data['country'] = get_country_list();
                      $data['buildings'] = $this->building->getBuilding();
                      $data['marketPlaces'] = $this->market->gerMarketPlaces();
                      $data['suppliers'] = get_logged_user();
                 }
                 $this->render_page(strtolower(__CLASS__) . '/activate-supplier', $data);
            }
       }

       function getSupplierCategories() {
            $supId = isset($_POST['supID']) ? $_POST['supID'] : 0;
            if (!empty($supId)) {
                 echo json_encode($this->supplier->getSupplierCategories($supId));
                 die();
            }
       }

       function home($supplierName = '', $id = '') {
            $flag = true;
            if ($supplierName && $id && $supplierName == $id) {
                 $res = $this->supplier->getSupDetUsingDomainPrefix($supplierName);
                 if ($res) {
                      $id = $res['supm_id'];
                      $flag = false;
                 }
            }
            if ($id && $supplierName != $id) {
                 $id = $flag ? encryptor($id, 'D') : $id;
                 $data['suppliers'] = $this->supplier->supplierHome($id);
                 $data['categories'] = $this->supplier->getSupplierCategories($id);
                 $data['stock'] = $this->supplier->getProducts(12, $id, 'stock');
                 $data['featured'] = $this->supplier->getProducts(12, $id);
                 $data['latest'] = $this->supplier->getMyLatestProducts(12, $id);
                 $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($id);
                 $data['suppliersProducts'] = $this->supplier->getMyProducts(12, $id);
                 $this->template->set_layout('supplier_home');
                 $this->render_page(strtolower(__CLASS__) . '/home', $data);
            } else {
                 redirect('error404');
                 //   $this->render_page('user_permission/nop');
            }
       }

       function contact($id = '') {
            if ($id) {
                 $id = encryptor($id, 'D');
                 $data['suppliers'] = $this->supplier->supplierHome($id);
                 $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($id);
                 $this->template->set_layout('supplier_home');
                 $this->render_page(strtolower(__CLASS__) . '/contact', $data);
            } else {
                 $this->render_page('user_permission/nop');
            }
       }

       function profile($id = '', $type = '') {
            $flag = true;
            $id_flag = true;
            if ($type == 'sub_domain') {
                 $res = $this->supplier->getSupDetUsingDomainPrefix($id);
                 if ($res) {
                      $id = $res['supm_id'];
                      $flag = false;
                 } else {
                      $id_flag = false;
                 }
            }
            if ($id && $id_flag) {
                 $id = $flag ? encryptor($id, 'D') : $id;
                 $data['suppliers'] = $this->supplier->supplierHome($id);
                 $data['categories'] = $this->supplier->getSupplierCategories($id);
                 $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($id);
                 $this->template->set_layout('supplier_home');
                 $this->render_page(strtolower(__CLASS__) . '/profile', $data);
            } else {
                 redirect('error404');
            }
       }

       function supplier_product($id = '', $cat_id = '') {
            $flag = true;
            $id_flag = true;
            if ($cat_id == 'sub_domain') {
                 $res = $this->supplier->getSupDetUsingDomainPrefix($id);
                 if ($res) {
                      $id = $res['supm_id'];
                      $flag = false;
                      $cat_id = '';
                 } else {
                      $id_flag = false;
                 }
            }

            if ($id && $id_flag) {
                 $data['cat_id'] = $cat_id;
                 $data['supplier'] = $flag ? encryptor($id, 'D') : $id;
                 $data['suppliers'] = $this->supplier->supplierHome($data['supplier']);
                 $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($data['supplier']);
                 $filter = '';
                 if ($this->input->get('page')) {
                      $this->load->model('product/product_model');
                      $params = $this->input->get();
                      if ($params['category']) {
                           $filter['category'] = $params['category'];
                      }
                      if ($params['suplier']) {
                           $filter['suplier'] = $params['suplier'];
                      }
                      $count = $this->product_model->countAllProduct($filter);
                      if ($params['order_by']) {
                           $filter['order_by'] = $params['order_by'];
                      }
                      pagination($this->input->get('page'), base_url() . "product/product_listing", $count, 'product_model', 'getProductListing', $filter);
                 }
                 $this->page_title = 'Supplier Products';
                 $data['categories'] = $this->supplier->getSupplierCategories($data['supplier']); //$this->common_model->getAllCategories();
                 $this->template->set_layout('supplier_home');
                 $this->render_page(strtolower(__CLASS__) . '/products', $data);
            } else {
                 redirect('error404');
            }
       }

       /**
        * Add to favorite
        * @param type $supName
        * @param type $supId
        * Author : JK
        */
       function addToFavorate($supName, $supId) {
            if (!$this->ion_auth->logged_in()) {
                 $this->session->set_userdata('callback', site_url('supplier/addToFavorate/' . $supName . '/' . $supId));
                 redirect('user/login');
            } else {
                 //Add to favorate list
                 $this->load->model('favorites/favorites_model', 'favorites');
                 $this->favorites->addSupplierToFavoriteList($supId);
                 $this->session->set_flashdata('app_success', 'This supplier added to your favorite list');
                 $this->template->set_layout('supplier_home');
                 redirect('supplier/home/' . $supName . '/' . $supId);
            }
       }

       function get_360($id = '') {
            $data['suppliers'] = $this->supplier->supplierHome(encryptor($id, 'D'));
            if (!($data['suppliers']) || !$data['suppliers']['supm_panoramic_image']) {
                 echo '<h3 style="text-align: center;
                 top: 50%;
                 position: absolute;
                 width: 100%;
                 transform: translate(0,50%);">No 360 image added this supplier';
                 die();
            }
            $this->template->set_layout('');
            $this->render_page(strtolower(__CLASS__) . '/360', $data);
       }

       function removePanorama($supplierId) {
            if (!empty($supplierId)) {
                 if ($this->supplier->removePanorama($supplierId)) {
                      die(json_encode(array('status' => 'success', 'msg' => 'Image removed')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't remove image")));
                 }
            }
       }

       function enquiry() {
            $details = $this->input->post();
            if ($this->supplier->addEnquiry($details)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Enquiry submitted, We will contact you soon!!')));
            }
            die(json_encode(array('status' => 'fail', 'msg' => "Something went wrong")));
       }

       function enquiries() {
            $this->page_title = "Enquiries";
            $data['enquiries'] = $this->supplier->getEnquiries();
            $this->render_page(strtolower(__CLASS__) . '/enquiries', $data);
       }

       /**
        * New supplier request from site
        * Author : JK
        */
       function supplier_register() {
            if (!empty($this->input->post())) {
                 if ($this->supplier->supplierRegister($this->input->post())) {
                      /* Mail to supplier */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress($this->input->post('supm_email'));
                      $this->mail->Subject = 'New supplier registration';
                      $this->mail->isHTML(true);
                      $mailData['email'] = $this->input->post('supm_email');
                      $this->mail->Body = $this->load->view('supplier-register-mail-template', $mailData, true);
                      $this->mail->send();
                      /* Mail end */
                      /* Mail to admin */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress(get_settings_by_key('admin_email'));
                      $this->mail->Subject = 'New supplier registration';
                      $this->mail->isHTML(true);
                      $this->mail->Body = $this->load->view('supplier-register-mail-template-admin', $this->input->post(), true);
                      $this->mail->send();
                      /* Mail end */
                      die(json_encode(array('status' => 'success', 'msg' => 'We will get back to you')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => 'Email already exists')));
                 }
            } else {
                 $this->page_title = 'Supplier registration';
                 $this->template->set_layout('common');
                 $this->render_page(strtolower(__CLASS__) . '/supplier_register');
            }
       }

       /**
        * Send supplier verification
        * Author : JK
        */
       function sendSupplierVerifyLink($supplierId, $userId) {
            $supplierId = encryptor($supplierId, 'D');
            $supp = $this->supplier->suppliers($supplierId);
            if (!empty($supp)) {
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($supp['supm_email']);
                 $this->mail->Subject = 'Supplier confirmation';
                 $this->mail->isHTML(true);

                 $mailData['name'] = $supp['supm_email'];
                 $mailData['activationUrl'] = site_url("supplier/activate-supplier/" . encryptor($supplierId));
                 $this->mail->Body = $this->load->view('user/become-seller-mail-template', $mailData, true);
                 $this->mail->send();
                 echo json_encode(array('status' => 'success', 'msg' => 'Activation mail successfully sent'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => 'Supplier not found'));
            }
       }

       function feeds() {
            $this->page_title = 'Feeds from suppliers';
            $this->load->model('product/product_model', 'product');
            $data['list'] = $this->supplier->feeds();
            $this->render_page(strtolower(__CLASS__) . '/feed-list', $data);
       }

       function get_360_list() {
            $this->page_title = 'Fujeeka 360 Shop Images';
            $this->template->set_layout('common');
            $this->load->model('market_model', 'market');

            // debug($data);
            $offset = $this->input->get('offset', true);
            $limit = $this->input->get('limit', true);
            $cat_id = $this->input->get('category', true);
            $market = $this->input->get('market', true);
            $building = $this->input->get('building', true);
            $class = $this->input->get('class', true);
            if (!$class) {
                 $class = "grid-style";
            }

            if (!$offset) {
                 $offset = 0;
            }

            if (!$limit) {
                 $limit = 40;
            }
            $isAjax = $this->input->get('type', true);
            if (!$isAjax) {
                 $data['markets'] = $this->market->getMarkets();
                 $data['buildings'] = $this->market->getBuildings();
                 $data['categories'] = $this->market->getCategories();
                 if (!$market) {
                      $market = '';
                 }
            }
            $data['details'] = $this->supplier->get360List($offset, $limit, $cat_id, $market, $building, $class, $isAjax);
            if ($isAjax) {
                 $return = array('details' => $data['details']);
                 echo json_encode($return);
                 die();
            } else {
                 $data['mkt_id'] = $market;
                 $this->render_page(strtolower(__CLASS__) . '/360-list', $data);
            }
       }

       function myprofile() {
            $supplierId = $this->session->userdata('usr_supplier');
            $data['suppliers'] = $this->supplier->suppliers($supplierId);
            $countryId = isset($data['suppliers']['supm_country']) ? $data['suppliers']['supm_country'] : 0;
            $data['states'] = get_state_province('', $countryId);
            $data['country'] = get_country_list();
            $data['grades'] = $this->supplier_grade->get();
            $data['buildings'] = $this->building->getBuilding();
            $data['marketPlaces'] = $this->market->gerMarketPlaces();
            $data['buildingsCategories'] = $this->supplier->getBuildingsCategories($data['suppliers']['supm_building']);
            $this->render_page(strtolower(__CLASS__) . '/myprofile', $data);
       }

  }
  