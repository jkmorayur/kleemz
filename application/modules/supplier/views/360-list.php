      <!-- Filter -->
      <section class="list-filter mt-3">
        <div class="container">
          <div class="row">
            <div class="search-result border-bottom-0">
              <div class="col-9 pl-0">

                 <div class="filter-cat border-0 p-0">
                   <h3>FILTER RESULTS BY : </h3>
                   <div class="s-filter">
                     <div class="s-cont">
                       <select class="form-control filter" id="category">
                         <option value="">Select a category</option>
                         <?php foreach($categories as $cat) {?>
                          <option value="<?=$cat['cat_id']?>"><?=$cat['cat_title']?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="s-filter">
                     <div class="s-cont">
                       <select class="form-control filter" id="market">
                         <option value="">Select Market</option>
                         <?php foreach($markets as $mar) {?>
                          <option <?php if($mkt_id == $mar['mar_id']){?> selected <?php } ?> value="<?=$mar['mar_id']?>"><?=$mar['mar_name']?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="s-filter">
                     <div class="s-cont">
                       <select class="form-control filter" id="building">
                         <option value="">Select Building</option>
                         <?php foreach($buildings as $build) {?>
                          <option value="<?=$build['bld_id']?>"><?=$build['bld_title']?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                 </div>

              </div>
              <div class="col-3">
                <div id="btnContainer">
                  <button class="btn btn_list" onclick="listView()"><i class="fa fa-bars"></i> List</button>
                  <button class="btn btn_grid btn-selected" onclick="gridView()"><i class="fa fa-th-large"></i> Grid</button>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </section>
      <!--/ Filter -->


      <section class="list-360">
        <div class="container">
          <div class="row">
          <?php if($details){ ?>
            <div class="col-12 d-flex">
              <div class="list-sec">
                <div class="hdr">
                  <div class="searchContainer">
                    <!-- <img src="images/icons/search-icon.png" alt="search" class="searchIcon"> -->
                    <input class="searchBox" type="search" name="search" placeholder="Search...">
                    <i class="demo-icon icon-search searchIcon"></i>
                  </div>

                </div>
                <div class="single-prod grid-style">
                  <ul id="results">
                  <?php foreach($details as $key => $row){ ?>
                    <li class="<?php if($key==0){ ?>select <?php } ?> 360_li" data-fsrc="<?=$row['360_image']?>" data-sup_link = "<?="http://".$row['supm_domain_prefix'] .'.'.$this->http_host?>" >
                      <div class="single-360">
                        <div class="img">
                          <img src="<?=$row['image']?>" class="img-fluid" alt="">
                        </div>
                        <div class="con">
                          <h6 class="names"><?=$row['name']?></h6>
                          <!-- <p></p> -->
                        </div>
                      </div>
                    </li>
                  <?php } ?>  
                  </ul>
                </div>
              </div>

            <div class="panorama-container">
                <div class="sup-link">
                  <a id="sup_href" href="<?="http://".$details[0]['supm_domain_prefix'] .'.'.$this->http_host?>">Go to Supplier Page</a>
                </div>
                <iframe width="100%" height="569" id="360_frame" frameborder="0" scrolling="no" allowtransparency="true" src="<?=$details[0]['360_image']?>"></iframe>
            </div>

          </div>
          <?php } ?>

          </div>
        </div>
      </section>
<style>
  .btn-selected{
    background-color: #ffffff !important;
    border-color: #28a745;
  }
</style>