<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Feed from suppliers</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Image</th>
                                        <th>Product</th>
                                        <th>Feed Text</th>
                                        <th>Supplier</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $list as $key => $value) {
                                          ?>
                                          <tr>
                                               <td>
                                                    <?php echo $this->product->getProductImage($value['prd_id'], '', 'width:50px;');?>
                                               </td>
                                               <td><?php echo $value['prd_name_en'];?></td>
                                               <td><?php echo $value['f_text'];?></td>
                                               <td><?php echo $value['supm_name_en'];?></td>
                                           </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>