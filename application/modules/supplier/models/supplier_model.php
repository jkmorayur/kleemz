<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class supplier_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function pendingApproval($id = '', $limit = 0, $page = 0, $filter = array()) {

            $this->db->where(tbl_supplier_master . '.supm_status', 0);
            // if($this->usr_grp == 'SBA') { //if(!is_root_user()) {
            //   $this->db->where(tbl_supplier_master . '.supm_added_by', $this->uid);
            // }
            if (empty($id)) {
                 if (isset($filter['supm_name']) && !empty($filter['supm_name'])) {
                      $supmName = trim($filter['supm_name']);
                      $this->db->like(tbl_supplier_master . '.supm_name', $supmName, 'both');
                 }
                 $suppliers['count'] = $this->db->get(tbl_supplier_master)->num_rows();

                 if ($limit) {
                      $this->db->limit($limit, $page);
                 }
                 if (isset($filter['supm_name']) && !empty($filter['supm_name'])) {
                      $supmName = trim($filter['supm_name']);
                      $this->db->like(tbl_supplier_master . '.supm_name', $supmName, 'both');
                 }
                 $suppliers['data'] = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*,' .
                                         tbl_users . '.*, ' . tbl_countries . '.*, ' . tbl_states . '.*, ' .
                                         tbl_users_groups . '.group_id as group_id, ' . tbl_groups . '.name as group_name,' .
                                         tbl_district . '.*,' . tbl_market_places . '.*, dfo.usr_first_name AS dfo_first_name')
                                 ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                 ->join(tbl_users . ' dfo', 'dfo.user_id_tmp = ' . tbl_supplier_master . '.supm_b_userid', 'LEFT')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                                 ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                                 ->join(tbl_district, tbl_district . '.dit_id = ' . tbl_supplier_master . '.supm_district', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                 ->where(tbl_groups . '.id', 2)->order_by('supm_added_on', 'desc')->get(tbl_supplier_master)->result_array();
                 return $suppliers;
            } else {
                 $suppliers = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*,' . tbl_countries . '.*,' . tbl_states . '.*,' .
                                         tbl_market_places . '.*')
                                 ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                                 ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                 ->where(tbl_supplier_master . '.supm_id', $id)
                                 ->where(tbl_users_groups . '.group_id', 2)
                                 ->get(tbl_supplier_master)->row_array();
                 if ($suppliers) {
                      $suppliers['keywords'] = $this->db->get_where(tbl_supplier_keywords, array('supk_supplier' => $suppliers['supm_id']))->result_array();
                      $suppliers['shop_images'] = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $suppliers['supm_id']))->result_array();

                      $suppliers['banner_images_app'] = $this->db->order_by('sbi_is_default', 'DESC')
                                      ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 2))->result_array();
                      $suppliers['banner_images_web'] = $this->db->order_by('sbi_is_default', 'DESC')
                                      ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 1))->result_array();
//                      $suppliers['building'] = $this->db->select(tbl_buildings_market_categoey_assoc . '.*,' . tbl_category . '.*')
//                                      ->join(tbl_category, tbl_buildings_market_categoey_assoc . '.bcat_category_id = ' . tbl_category . '.cat_id')
//                                      ->where(tbl_buildings_market_categoey_assoc . '.bcat_building_id', $suppliers['supm_building'])
//                                      ->get(tbl_buildings_market_categoey_assoc)->result_array();
                      $suppliers['markets'] = $this->db->select(tbl_market_cate_assoc . '.*,' . tbl_category . '.*')
                                      ->join(tbl_category, tbl_market_cate_assoc . '.mca_category_id = ' . tbl_category . '.cat_id')
                                      ->where(tbl_market_cate_assoc . '.mca_market_id', $suppliers['supm_market'])
                                      ->get(tbl_market_cate_assoc)->result_array();

                      $suppliers['contacts'] = $this->db->get_where(tbl_supplier_contacts, array('spc_master_id' => $id))->result_array();

                      $suppliers['categories'] = explode(',', $this->db->select('GROUP_CONCAT(scat_category) AS categories')
                                      ->where('scat_master_id', $id)->get(tbl_supplier_categories)->row()->categories);

                      $suppliers['buildingCates'] = explode(',', $this->db->select('GROUP_CONCAT(sbca_category) AS sbca_category')
                                      ->where('sbca_supplier', $id)->get(tbl_supplier_buildings_cate_assoc)->row()->sbca_category);
                      //supplier user id -> to use as channel name in pusher chat added by irfan
                      $suppliers['supplier_user_id'] = $this->common_model->getUserIdFromSupId($suppliers['supm_id']);
                 }

                 return $suppliers;
            }
       }

       public function suppliers($id = '', $limit = 0, $page = 0, $filter = array()) {

            
            // if($this->usr_grp == 'SBA') { //if(!is_root_user()) {
            //   $this->db->where(tbl_supplier_master . '.supm_added_by', $this->uid);
            // }
            if (empty($id)) {
                 $this->db->where(tbl_supplier_master . '.supm_status', 1);
                 if (isset($filter['supm_name']) && !empty($filter['supm_name'])) {
                      $supmName = trim($filter['supm_name']);
                      $this->db->like(tbl_supplier_master . '.supm_name', $supmName, 'both');
                 }
                 $suppliers['count'] = $this->db->get(tbl_supplier_master)->num_rows();

                 if ($limit) {
                      $this->db->limit($limit, $page);
                 }
                 if (isset($filter['supm_name']) && !empty($filter['supm_name'])) {
                      $supmName = trim($filter['supm_name']);
                      $this->db->like(tbl_supplier_master . '.supm_name', $supmName, 'both');
                 }
                 $suppliers['data'] = $this->db->select(tbl_supplier_master . '.*,' . 
                                         tbl_users . '.usr_id, ' . tbl_countries . '.*, ' . tbl_states . '.*, ' .
                                         tbl_users_groups . '.group_id as group_id, ' . tbl_groups . '.name as group_name,' .
                                         tbl_district . '.*,' . tbl_market_places . '.*, dfo.usr_first_name AS dfo_first_name')
                                 ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                 ->join(tbl_users . ' dfo', 'dfo.user_id_tmp = ' . tbl_supplier_master . '.supm_b_userid', 'LEFT')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                                 ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                                 ->join(tbl_district, tbl_district . '.dit_id = ' . tbl_supplier_master . '.supm_district', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                 ->where(tbl_groups . '.id', 2)->order_by('supm_added_on', 'desc')->get(tbl_supplier_master)->result_array();
                 return $suppliers;
            } else {
                 $suppliers = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*,' . tbl_countries . '.*,' . tbl_states . '.*,' .
                                         tbl_market_places . '.*')
                                 ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                                 ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                 ->where(tbl_supplier_master . '.supm_id', $id)
                                 ->where(tbl_users_groups . '.group_id', 2)
                                 ->get(tbl_supplier_master)->row_array();
                 if ($suppliers) {
                      $suppliers['keywords'] = $this->db->get_where(tbl_supplier_keywords, array('supk_supplier' => $suppliers['supm_id']))->result_array();
                      $suppliers['shop_images'] = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $suppliers['supm_id']))->result_array();

                      $suppliers['banner_images_app'] = $this->db->order_by('sbi_is_default', 'DESC')
                                      ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 2))->result_array();
                      $suppliers['banner_images_web'] = $this->db->order_by('sbi_is_default', 'DESC')
                                      ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 1))->result_array();
//                      $suppliers['building'] = $this->db->select(tbl_buildings_market_categoey_assoc . '.*,' . tbl_category . '.*')
//                                      ->join(tbl_category, tbl_buildings_market_categoey_assoc . '.bcat_category_id = ' . tbl_category . '.cat_id')
//                                      ->where(tbl_buildings_market_categoey_assoc . '.bcat_building_id', $suppliers['supm_building'])
//                                      ->get(tbl_buildings_market_categoey_assoc)->result_array();
                      $suppliers['markets'] = $this->db->select(tbl_market_cate_assoc . '.*,' . tbl_category . '.*')
                                      ->join(tbl_category, tbl_market_cate_assoc . '.mca_category_id = ' . tbl_category . '.cat_id')
                                      ->where(tbl_market_cate_assoc . '.mca_market_id', $suppliers['supm_market'])
                                      ->get(tbl_market_cate_assoc)->result_array();

                      $suppliers['contacts'] = $this->db->get_where(tbl_supplier_contacts, array('spc_master_id' => $id))->result_array();

                      $suppliers['categories'] = explode(',', $this->db->select('GROUP_CONCAT(scat_category) AS categories')
                                      ->where('scat_master_id', $id)->get(tbl_supplier_categories)->row()->categories);

                      $suppliers['buildingCates'] = explode(',', $this->db->select('GROUP_CONCAT(sbca_category) AS sbca_category')
                                      ->where('sbca_supplier', $id)->get(tbl_supplier_buildings_cate_assoc)->row()->sbca_category);
                      //supplier user id -> to use as channel name in pusher chat added by irfan
                      $suppliers['supplier_user_id'] = $this->common_model->getUserIdFromSupId($suppliers['supm_id']);
                 }

                 return $suppliers;
            }
       }

       public function setDefaultBannerImage($imgId, $supId) {

            $sbi_type = $this->db->select('sbi_type')->where('sbi_id', $imgId)
                            ->get(tbl_supplier_banner_images)->row()->sbi_type;

            $this->db->where('sbi_supplier', $supId);
            $this->db->where('sbi_type', $sbi_type);
            $this->db->update(tbl_supplier_banner_images, array('sbi_is_default' => 0));

            $this->db->where('sbi_id', $imgId);
            $this->db->where('sbi_type', $sbi_type);
            if ($this->db->update(tbl_supplier_banner_images, array('sbi_is_default' => 1))) {
                 generate_log(array(
                     'log_title' => 'Set default property',
                     'log_desc' => 'Set defult supplier banner image',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $imgId,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            } else {
                 return false;
            }
       }

       function addBannerImages($data) {

            if (empty($data['sbi_alttag']))
                 $data['sbi_alttag'] = 'Banner image';

            if (!empty($data)) {
                 $this->db->insert(tbl_supplier_banner_images, $data);
                 return true;
            }
            return false;
       }

       public function newSupplier($data) {
            if (!empty($data)) {
                 $supplier = isset($data['supplier']) ? array_filter($data['supplier']) : '';
                 $keywords = isset($data['keywords']) ? array_filter($data['keywords']) : '';
                 if ($this->usr_grp == 'SA') {
                      $supplier['supm_status'] = 1;
                 }
                 $supplier['supm_ref_number'] = gen_random();
                 $supplier['supm_added_by'] = $this->uid;
                 $this->db->insert(tbl_supplier_master, $supplier);
                 $supplierId = $this->db->insert_id();
                 $this->load->library('ciqrcode');
                 $params['data'] = encryptor($supplierId);
                 $params['level'] = 'H';
                 $params['size'] = 10;
                 $params['savename'] = FILE_UPLOAD_PATH . 'QR/' . $supplier['supm_ref_number'] . '.png';
                 $this->ciqrcode->generate($params);
                 //category//
                 if (isset($data['categories']) && !empty($data['categories'])) {
                      foreach ($data['categories'] as $key => $value) {
                           $this->db->insert(tbl_supplier_categories, array(
                               'scat_master_id' => $supplierId,
                               'scat_category' => $value
                           ));
                      }
                 }

                 //Contacts//
                 if (isset($data['upn_phone_number']) && !empty($data['upn_phone_number'])) {
                      foreach ($data['upn_phone_number'] as $key => $value) {
                           if (!empty($value)) {
                                $this->db->insert(tbl_supplier_contacts, array(
                                    'spc_master_id' => $supplierId,
                                    'spc_number' => $value
                                ));
                           }
                      }
                 }

                 if (!empty($keywords)) {
                      $count = isset($keywords['en']) ? count($keywords['en']) : 0;
                      for ($i = 0; $i < $count; $i++) {
                           if ((isset($keywords['en'][$i])) && (!empty($keywords['en'][$i]))) {
                                $this->db->insert(tbl_supplier_keywords, array(
                                    'supk_supplier' => $supplierId,
                                    'supk_keyword_en' => clean_text($keywords['en'][$i], 0),
                                ));
                           }
                      }
                 }
                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'New supplier created',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $supplierId,
                     'log_added_by' => $this->uid,
                 ));
                 return $supplierId;
            } else {
                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'Error while create supplier',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid,
                 ));
                 return false;
            }
       }

       public function addImages($data) {
            if ($this->db->insert(tbl_supplier_shop_images, $data)) {
                 $insertId = $this->db->insert_id();
                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'Added new image',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $insertId,
                     'log_added_by' => $this->uid,
                 ));
                 return $insertId;
            }
            return false;
       }

       public function setDefaultImage($imgId, $supplierId) {
            $this->db->where('supm_id', $supplierId);
            if ($this->db->update(tbl_supplier_master, array('supm_default_image' => $imgId))) {
                 generate_log(array(
                     'log_title' => 'Set defauld property',
                     'log_desc' => 'Set defult supplier image',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $imgId,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            } else {
                 return false;
            }
       }

       public function getBuildingsCategories($buildingId) {
            return $this->db->select(tbl_buildings_market_categoey_assoc . '.*,' . tbl_category . '.*')
                            ->join(tbl_category, tbl_buildings_market_categoey_assoc . '.bcat_category_id = ' . tbl_category . '.cat_id')
                            ->where(tbl_buildings_market_categoey_assoc . '.bcat_building_id', $buildingId)
                            ->get(tbl_buildings_market_categoey_assoc)->result_array();
       }

       public function updateSupplier($data) {

            foreach ($data['ssi_alttag'] as $ssi_id => $ssi_alttag) {
                 $this->db->where('ssi_id', $ssi_id)
                         ->update(tbl_supplier_shop_images, array('ssi_alttag' => $ssi_alttag));
            }


            foreach ($data['sbi_alttag'] as $sbi_id => $sbi_alttag) {
                 $this->db->where('sbi_id', $sbi_id)
                         ->update(tbl_supplier_banner_images, array('sbi_alttag' => $sbi_alttag));
            }


            $supplierId = $data['supm_id'];
            if (!empty($data)) {
                 $supplier = isset($data['supplier']) ? array_filter($data['supplier']) : '';
                 $keywords = isset($data['keywords']) ? array_filter($data['keywords']) : '';

                 $this->db->where('supm_id', $supplierId);
                 $this->db->update(tbl_supplier_master, $supplier);

                 /* TODO:delete the code in future */
                 $f = $this->db->get_where(tbl_supplier_master, array('supm_id' => $supplierId))->row_array();
                 $this->load->library('ciqrcode');
                 $params['data'] = site_url('supplier/addToFavorate/' . slugify($f['supm_name']) . '/' . encryptor($f['supm_id']));
                 $params['level'] = 'H';
                 $params['size'] = 10;
                 $params['savename'] = FILE_UPLOAD_PATH . 'QR/' . $f['supm_ref_number'] . '.png';
                 $this->ciqrcode->generate($params);

                 //category//
                 if (isset($data['categories']) && !empty($data['categories'])) {
                      $this->db->where('scat_master_id', $supplierId);
                      $this->db->delete(tbl_supplier_categories);
                      foreach ($data['categories'] as $key => $value) {
                           $this->db->insert(tbl_supplier_categories, array(
                               'scat_master_id' => $supplierId,
                               'scat_category' => $value
                           ));
                      }
                 }

                 //Contacts//
                 if (isset($data['upn_phone_number']) && !empty($data['upn_phone_number'])) {
                      $this->db->where('spc_master_id', $supplierId);
                      $this->db->delete(tbl_supplier_contacts);
                      foreach ($data['upn_phone_number'] as $key => $value) {
                           if (!empty($value)) {
                                $this->db->insert(tbl_supplier_contacts, array(
                                    'spc_master_id' => $supplierId,
                                    'spc_number' => $value
                                ));
                           }
                      }
                 }

                 if (!empty($keywords)) {
                      $count = isset($keywords['en']) ? count($keywords['en']) : 0;
                      $this->db->where('supk_supplier', $supplierId);
                      $this->db->delete(tbl_supplier_keywords);
                      for ($i = 0; $i < $count; $i++) {
                           if ((isset($keywords['en'][$i])) && (!empty($keywords['en'][$i]))) {
                                $this->db->insert(tbl_supplier_keywords, array(
                                    'supk_supplier' => $supplierId,
                                    'supk_keyword_en' => clean_text($keywords['en'][$i], 0),
                                ));
                           }
                      }
                 }
                 generate_log(array(
                     'log_title' => 'Record updated',
                     'log_desc' => 'Update supplier',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $supplierId,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Record updated',
                     'log_desc' => 'Error while update supplier the data variable is empty',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $supplierId,
                     'log_added_by' => $this->uid,
                 ));
                 return false;
            }
       }

       public function removeAvatar($userId) {

            $user = $this->db->get_where(tbl_users, array('usr_id' => $userId))->row_array();
            if (isset($user['usr_avatar']) && !empty($user['usr_avatar'])) {
                 if (file_exists(FILE_UPLOAD_PATH . 'avatar/' . $user['usr_avatar'])) {
                      unlink(FILE_UPLOAD_PATH . 'avatar/' . $user['usr_avatar']);
                 }
                 if (file_exists(FILE_UPLOAD_PATH . 'avatar/thumb_' . $user['usr_avatar'])) {
                      unlink(FILE_UPLOAD_PATH . 'avatar/thumb_' . $user['usr_avatar']);
                 }
            }
            $this->db->where('usr_id', $userId);
            $this->db->update(tbl_users, array('usr_avatar' => ''));
            generate_log(array(
                'log_title' => 'Delete image',
                'log_desc' => 'User avatar removed',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'D',
                'log_ref_id' => $userId,
                'log_added_by' => $this->uid,
            ));
            return true;
       }

       public function removeImage($id) {
            if ($id) {
                 $this->db->where('ssi_id', $id);
                 $data = $this->db->get(tbl_supplier_shop_images)->row_array();
                 if (isset($data['ssi_image']) && !empty($data['ssi_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'shops/' . $data['ssi_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'shops/' . $data['ssi_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'shops/thumb_' . $data['ssi_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'shops/thumb_' . $data['ssi_image']);
                      }
                      $this->db->where('ssi_id', $id);
                      if ($this->db->delete(tbl_supplier_shop_images)) {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Removed shop image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return true;
                      } else {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Error while removing shop image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return false;
                      }
                 }
            }
            return false;
       }

       public function checkIfValueExists($data) {

            if (isset($data['in']) && !empty($data['in'])) {
                 $excludedomain = isset($data['excludedomain']) && !empty($data['excludedomain']) ? $data['excludedomain'] : 0;
                 if (!empty($excludedomain)) {
                      $this->db->where('supm_domain_prefix != ', $excludedomain);
                 }
                 $return = $this->db->like('supm_domain_prefix', $data['supplier']['supm_domain_prefix'], 'none')->get(tbl_supplier_master)->row_array();
                 return (empty($return)) ? true : false;
            }
            if (isset($data['new_mail']) && !empty($data['new_mail'])) {
                 $data['user']['usr_email'] = $data['new_mail'];
            }
            $excludeId = isset($data['excludeId']) && !empty($data['excludeId']) ? $data['excludeId'] : 0;
            $excludeMail = isset($data['excludemail']) && !empty($data['excludemail']) ? $data['excludemail'] : '';
            $field = isset($data['field']) && !empty($data['field']) ? $data['field'] : 0;

            if (!empty($excludeId)) {
                 $this->db->where('usr_id != ', $excludeId);
            }
            if (!empty($excludeMail)) {
                 $this->db->where('usr_email != ', $excludeMail);
            }
            if (isset($data['user']['usr_username']) && !empty($data['user']['usr_username'])) {
                 if ($field == 'usr_username') {
                      $return = $this->db->like('usr_username', $data['user']['usr_username'], 'none')->get(tbl_users)->row_array();
                      return (empty($return)) ? true : false;
                 }
            }

            if (isset($data['user']['usr_email']) && !empty($data['user']['usr_email'])) {
                 if ($field == 'usr_email') {
                      $return = $this->db->like('usr_email', $data['user']['usr_email'], 'none')->get(tbl_users)->row_array();
                      return (empty($return)) ? true : false;
                 }
            }
       }

       /**
        * Function for verify the supplier
        * @param int $supplierid
        * @param int $userId
        * @param int $ischecked
        * @return boolean
        * Author : JK
        */
       function verifySupplier($supplierid, $userId, $ischecked) {
            if ($supplierid) {
                 $this->load->model('supplier_privilege/supplier_privilege_model', 'supplier_privilege');
                 /* Activate supplier */
                 $this->db->where('supm_id', $supplierid);
                 $this->db->update(tbl_supplier_master, array('supm_status' => $ischecked));
                 if ($ischecked == 1) {
                      /* Set default user as supplier */
                      $group = $this->db->get_where(tbl_users_groups, array('user_id' => $userId, 'group_id' => 2))->row_array();
                      if (empty($group)) {
                           $this->db->insert(tbl_users_groups, array('user_id' => $userId, 'group_id' => 2));
                      }
                      /* Update defaulf permission for supplier */
                      $permission['cua_group_id'] = $userId;
                      $permission['cua_access']['emp_details'] = array('index', 'add', 'view', 'update', 'delete');
                      $permission['cua_access']['product'] = array('index', 'add', 'view', 'update', 'delete', 'rfq', 'rfq_list',
                          'pending_rfq_list');
                      $permission['cua_access']['user_permission'] = array('index');
                      $permission['cua_access']['order'] = array('index', 'order_summery', 'changeorderstatuse');
                      $permission['cua_access']['units'] = array('index', 'add', 'view', 'update', 'delete');
                      $this->supplier_privilege->addUserPermission($permission);
                      /* Sent message */
                      $this->load->model('mailbox/mailbox_model', 'mailbox');
                      $msg = 'Your supplier account has been activated now you can sell your product on fujeeka';
                      $this->mailbox->send('', $userId, 'Your supplier account activated', $msg);
                 } else {
                      $permission = $this->db->get_where(tbl_user_access, array('cua_group_id' => $userId))->row_array();
                      if (!empty($permission) && isset($permission['cua_access']) && is_serialized($permission['cua_access'])) {
                           $permission = unserialize($permission['cua_access']);
                           if (isset($permission['emp_details'])) {
                                unset($permission['emp_details']);
                           }
                           if (isset($permission['product'])) {
                                unset($permission['product']);
                           }
                           if (empty($permission)) {
                                $this->db->where('cua_group_id', $userId);
                                $this->db->delete(tbl_user_access);
                           } else {
                                $newPermission['cua_group_id'] = $userId;
                                $newPermission['cua_access'] = $permission;
                                $this->supplier_privilege->addUserPermission($newPermission);
                           }
                      }
                 }
                 return true;
            }
            return false;
       }

       // to get total count for pagination
       function countAllSupplier($filter = '') {
            $this->db->select(tbl_supplier_master . '.*');
            if (isset($filter['category'])) {
                 $this->db->join(tbl_supplier_buildings_cate_assoc, tbl_supplier_buildings_cate_assoc . '.sbca_supplier = ' . tbl_supplier_master . '.supm_id');
                 $this->db->where(tbl_supplier_buildings_cate_assoc . '.sbca_category', $filter['category']);
            }
            $this->db->where(tbl_supplier_master . '.supm_status', 1);
            $products = $this->db->get(tbl_supplier_master);
            return $products->num_rows();
       }

       public function getSupplierListing($limit, $start, $filter) {
            $output = '';

            $this->db->select(tbl_supplier_master . '.*,' . tbl_countries . '.*,' . tbl_states . '.*,');
            // $this->db->join(tbl_supplier_buildings_cate_assoc, tbl_supplier_buildings_cate_assoc . '.sbca_supplier = ' . tbl_supplier_master . '.supm_id');
            // $this->db->join(tbl_category, tbl_category . '.cat_id = ' . tbl_supplier_buildings_cate_assoc . '.sbca_category');
            $this->db->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT');
            $this->db->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT');
            // if (isset($filter['category'])) {
            //      $this->db->where(tbl_supplier_buildings_cate_assoc . '.sbca_category', $filter['category']);
            // }
            $this->db->limit($limit, $start);
            // if (isset($filter['order_by'])) {
            //     if ($filter['order_by'] == "price_desc")
            //         $this->db->order_by("prd_price_min", "desc");
            //     if ($filter['order_by'] == "price_asc")
            //         $this->db->order_by("prd_price_min", "asc");
            // }
            $this->db->order_by(tbl_supplier_master . '.supm_added_on', 'desc');
            $this->db->where(tbl_supplier_master . '.supm_status', 1);
            $suppliers = $this->db->get(tbl_supplier_master)->result_array();

            foreach ($suppliers as $key => $val) {
                 $this->db->select(tbl_category . '.cat_id,cat_title');
                 $this->db->join(tbl_category, tbl_category . '.cat_id = ' . tbl_supplier_buildings_cate_assoc . '.sbca_category');
                 $this->db->where('sbca_supplier', $val['supm_id']);
                 $catg = $this->db->get(tbl_supplier_buildings_cate_assoc)->result_array();

                 if (isset($filter['category'])) {
                      if (!in_array($filter['category'], array_column($catg, 'cat_id'))) {
                           unset($suppliers[$key]);
                           continue;
                      }
                 }
                 $suppliers[$key]['categories'] = $catg;
                 if (isset($suppliers[$key]['supm_default_image']) && !empty($suppliers[$key]['supm_default_image'])) {
                      $images = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
                 } else {
                      $images = $this->db->limit(1)->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id']))->row_array();
                 }
                 $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
                 $suppliers[$key]['default_image'] = 'assets/uploads/shops/' . $defaultImg;
            }
            // debug($suppliers);
            foreach ($suppliers as $sup) {
                 $ecn_id = encryptor($sup['supm_id']);
                 $output .= "<div class='market-place'>
                <div class='prod-box'>
                    <div class='prod-img'>
                    <a href='" . site_url('supplier/home/' . slugify($sup['supm_name_en']) . '/' . $ecn_id) . "'>" . img(array('src' => $sup['default_image'], 'id' => '', 'alt' => '')) . "</a>
                    </div>
                    <div class='prod-detail txt-height'>
                        <h4 class='prod-title-s'><a href='" . site_url('supplier/home/' . slugify($sup['supm_name_en']) . '/' . $ecn_id) . "'>" . $sup['supm_name_en'] . "</a></h4>
                        <div class='company-details'>
                            <p class='loc'>Location : <span>" . $sup['ctr_name'] . " (" . $sup['stt_name'] . ")</span></br>
                                Categories :" . implode(', ', array_column($sup['categories'], 'cat_title')) . "</br>
                                Contact No : <span>" . $sup['supm_number'] . "</span></p>
                            <div class='clearfix'></div>
                            <p class='check'><a href='" . site_url('supplier/supplier_product/' . $ecn_id) . "'>View all products under this supplier</a></p>
                        </div>
                        <div class='contcts'>
                            <h5>Address</h5>
                            <p>" . $sup['supm_address_en'] . "</p>
                            <div class='clearfix'></div>
                        </div>
                    </div>
                </div>
            </div>";
            }
            if (!$output) {
                 $output = "<h3 class='text-center'>No Suppliers Found!!</h3>";
            }
            return $output;
       }

       function getSupplierCategories($supId) {
            return $this->db->select(tbl_supplier_buildings_cate_assoc . '.*,' . tbl_category . '.*')
                            ->join(tbl_category, tbl_category . '.cat_id = ' . tbl_supplier_buildings_cate_assoc . '.sbca_category')
                            ->where(tbl_supplier_buildings_cate_assoc . '.sbca_supplier', $supId)
                            ->get(tbl_supplier_buildings_cate_assoc)->result_array();
       }

       public function supplierHome($id) {

            $suppliers = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*,' . tbl_countries . '.*,' .
                                    tbl_states . '.*, ')
                            ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                            ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')

                            // ->join(tbl_buildins, tbl_buildins . '.bld_id = ' . tbl_supplier_master . '.supm_building', 'LEFT')
                            // ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                            ->where(tbl_supplier_master . '.supm_id', $id)
                            ->where(tbl_users_groups . '.group_id', 2)
                            ->get(tbl_supplier_master)->row_array();
            if ($suppliers) {
                 $suppliers['shop_images'] = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $suppliers['supm_id']))->result_array();
                 $suppliers['isFollowed'] = $this->db->get_where(tbl_supplier_followers, array('sfol_supplier' => $id, 'sfol_followed_by' => $this->uid))->row_array();
                 $suppliers['banner_images'] = $this->db->order_by('sbi_is_default', 'DESC')
                                 ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 1))->result_array();
            }
            return $suppliers;
       }

       public function getProducts($limit = '', $sup = '', $type = '') {
            $this->db->select('*');
            $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
            $this->db->where('prd_status', 1);
            $this->db->where('prd_supplier', $sup);
            $this->db->where('prd_status', 1);
            $this->db->where('prd_is_show_on_profile', 1);
            if ($type == 'stock') {
                 $this->db->where('prd_is_stock_prod', 1);
            }
            $this->db->order_by('prd_added_on', 'DESC');
            if ($limit) {
                 $this->db->limit($limit);
            }
            $products = $this->db->get(tbl_products)->result_array();
            foreach ($products as $key => $val) {
                 if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                      $products[$key]['images'] = $this->db->get_where(tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                 } else {
                      $products[$key]['images'] = $this->db->limit(1)->get_where(tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                 }
                 $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
                 $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;

                 $products[$key]['pimg_alttag'] = isset($products[$key]['images']['pimg_alttag']) ? $products[$key]['images']['pimg_alttag'] : 'product image';

                 $products[$key]['isFavorite'] = $this->db->get_where(tbl_favorite_list, array(
                             'fav_consign' => 'PRD', 'fav_consign_id' => $val['prd_id'], 'fav_added_by' => $this->uid))->row_array();
            }


            return $products;
       }

       function checkSupplierActivation($id) {

            $this->db->select('*');
            $this->db->where('sr_usr_id', encryptor($id, 'D'));
            $this->db->where('status', 1);
            $res = $this->db->get(TABLE_PREFIX . 'seller_requests');
            return $res->num_rows();
       }

       function removeSupplierBanner($id) {
            $supplier = $this->db->get_where(tbl_supplier_banner_images, array('sbi_id' => $id))->row_array();
            if (isset($supplier['sbi_image']) && !empty($supplier['sbi_image'])) {
                 if (file_exists(FILE_UPLOAD_PATH . 'home_banner/' . $supplier['sbi_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'home_banner/' . $supplier['sbi_image']);
                 }
                 if (file_exists(FILE_UPLOAD_PATH . 'home_banner/thumb_' . $supplier['sbi_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'home_banner/thumb_' . $supplier['sbi_image']);
                 }

                 $this->db->where('sbi_id', $id);
                 $this->db->delete(tbl_supplier_banner_images);
                 generate_log(array(
                     'log_title' => 'Delete supplier banner image',
                     'log_desc' => 'Delete supplier banner image',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'D',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            }
            return false;
       }

       function removePanorama($id) {
            $supplier = $this->db->get_where(tbl_supplier_master, array('supm_id' => $id))->row_array();
            if (isset($supplier['supm_panoramic_image']) && !empty($supplier['supm_panoramic_image'])) {
                 if (file_exists(FILE_UPLOAD_PATH . 'panorama-shop/' . $supplier['supm_panoramic_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'panorama-shop/' . $supplier['supm_panoramic_image']);
                 }
                 if (file_exists(FILE_UPLOAD_PATH . 'panorama-shop/thumb_' . $supplier['supm_panoramic_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'panorama-shop/thumb_' . $supplier['supm_panoramic_image']);
                 }

                 $this->db->where('supm_id', $id);
                 $this->db->update(tbl_supplier_master, array('supm_panoramic_image' => ''));
                 generate_log(array(
                     'log_title' => 'Delete supplier 360 image',
                     'log_desc' => 'Delete supplier 360 image',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'D',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            }
            return false;
       }

       function addEnquiry($details) {
            $details['enq_sup_id'] = encryptor($details['enq_sup_id'], 'D');
            return $this->db->insert(TABLE_PREFIX . 'enquiry', $details);
       }

       function getEnquiries() {
            if ($this->usr_grp != 'SA') {
                 $this->db->where('enq_sup_id', $this->suplr);
            }
            return $this->db->get(TABLE_PREFIX . 'enquiry')->result_array();
       }

       function getMyProducts($limit = '', $sup = '', $type = '') {
            $this->db->select('*');
            $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
            $this->db->where('prd_status', 1);
            $this->db->where('prd_is_show_on_profile', 1);
            $this->db->where('prd_supplier', $sup);
            if ($type == 'stock') {
                 $this->db->where('prd_is_stock_prod', 1);
            }
            $this->db->order_by('prd_added_on', 'DESC');
            if ($limit) {
                 $this->db->limit($limit);
            }
            $products = $this->db->get(tbl_products)->result_array();
            foreach ($products as $key => $val) {
                 if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                      $products[$key]['images'] = $this->db->get_where(tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                 } else {
                      $products[$key]['images'] = $this->db->limit(1)->get_where(tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                 }
                 $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
                 $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;

                 $products[$key]['pimg_alttag'] = isset($products[$key]['images']['pimg_alttag']) ? $products[$key]['images']['pimg_alttag'] : 'product image';


                 $products[$key]['isFavorite'] = $this->db->get_where(tbl_favorite_list, array(
                             'fav_consign' => 'PRD', 'fav_consign_id' => $val['prd_id'], 'fav_added_by' => $this->uid))->row_array();
            }
            return $products;
       }

       function supplierRegister($data) {
            if (!empty($data)) {
                 if (isset($data['supm_email'])) { // check if already registerd
                      $existance = $this->db->get_where(tbl_supplier_master, array('supm_email' => trim($data['supm_email'])))->row_array();
                      if (empty($existance)) {
                           $data = array_filter($data);
                           $data['supm_status'] = 0;
                           $data['supm_ref_number'] = gen_random();
                           $this->db->insert(tbl_supplier_master, $data);
                           $supId = $this->db->insert_id();

                           $this->db->insert(tbl_users, array('usr_supplier' => $supId, 'usr_email' => $data['supm_email'], 'usr_active' => 0));
                           $userId = $this->db->insert_id();

                           $this->db->insert(tbl_users_groups, array('user_id' => $userId, 'group_id' => 2));
                           $this->db->insert(tbl_seller_requests, array('sr_usr_id' => $userId, 'status' => 0));

                           generate_log(array(
                               'log_title' => 'Supplier register',
                               'log_desc' => 'New supplier register',
                               'log_controller' => 'supplier-registration-site',
                               'log_action' => 'C',
                               'log_ref_id' => $supId,
                               'log_added_by' => $this->uid,
                           ));
                           return true;
                      } else { // already exists
                           return false;
                      }
                 }
            }
            return false;
       }

       public function getMyLatestProducts($limit = '', $sup = '') {
            $this->db->select('*');
            $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
            $this->db->where('prd_status', 1);
            $this->db->where('prd_supplier', $sup);
            $this->db->where('prd_is_show_on_profile', 1);
            $this->db->order_by('prd_added_on', 'DESC');
            if ($limit) {
                 $this->db->limit($limit);
            }
            $products = $this->db->get(tbl_products)->result_array();
            foreach ($products as $key => $val) {
                 if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                      $products[$key]['images'] = $this->db->get_where(tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                 } else {
                      $products[$key]['images'] = $this->db->limit(1)->get_where(tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                 }
                 $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
                 $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
                 $products[$key]['pimg_alttag'] = isset($products[$key]['images']['pimg_alttag']) ? $products[$key]['images']['pimg_alttag'] : 'product image';
                 $products[$key]['isFavorite'] = $this->db->get_where(tbl_favorite_list, array(
                             'fav_consign' => 'PRD', 'fav_consign_id' => $val['prd_id'], 'fav_added_by' => $this->uid))->row_array();
            }
            return $products;
       }

       function feeds() {
            $products = $this->db->select(tbl_feed . '.*,prd_name_en,prd_default_image,prd_id,supm_name_en')
                            ->join(tbl_feed, 'f_sup_id = sfol_supplier')
                            ->join(tbl_products, 'prd_id = f_prd_id', 'RIGHT')
                            ->join(tbl_supplier_master, 'supm_id = f_sup_id', 'RIGHT')
                            ->where(array('f_status' => 1, 'sfol_followed_by' => $this->uid))
                            ->order_by('f_added_on', 'DESC')->get(tbl_supplier_followers)->result_array();
            if (!empty($products)) {
                 foreach ($products as $key => $value) {
                      if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                           $images = $this->db->get_where(tbl_products_images, array('pimg_product' => $value['prd_id'], 'pimg_id' => $value['prd_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                      }
                      $products[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                      //   $products[$key]['images'] = $this->db->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                 }
            }
            return $products;
       }

       function get360List($offset = '', $limit = '', $cat_id = '', $market = '', $building = '', $class = '', $isAjax = '') {
            $sup = array();
            $this->db->select('supm_id,supm_name_en,supm_panoramic_image,supm_default_image,supm_domain_prefix')
                    ->limit($limit, $offset)
                    ->where('supm_status', 1);
            if ($market) {
                 $this->db->where('supm_market', $market);
            }
            if ($building) {
                 $this->db->where('supm_building', $building);
            }
            $res = $this->db->get(tbl_supplier_master)
                    ->result_array();
            $output = '';
            if ($isAjax) {
                 foreach ($res as $key => $val) {
                      $this->db->select(tbl_category . '.cat_id,cat_title');
                      $this->db->join(tbl_category, tbl_category . '.cat_id = ' . tbl_supplier_buildings_cate_assoc . '.sbca_category');
                      $this->db->where('sbca_supplier', $val['supm_id']);
                      $catg = $this->db->get(tbl_supplier_buildings_cate_assoc)->result_array();
                      if ($cat_id) {
                           if (!in_array($cat_id, array_column($catg, 'cat_id'))) {
                                continue;
                           }
                      }
                      if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                           $images = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id']))->row_array();
                      }
                      $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
                      $img = site_url() . 'assets/uploads/shops/' . $defaultImg;
                      $_360 = site_url() . 'supplier/get_360/' . encryptor($val['supm_id']);
                      $link = "http://" . $val['supm_domain_prefix'] . '.' . $this->http_host;
                      $output .= " <li class='360_li' data-fsrc='" . $_360 . "' data-sup_link='" . $link . "'>
                                   <div class='single-360'>
                                        <div class='img'>
                                             <img src=" . $img . " class='img-fluid' alt=''>
                                        </div>
                                        <div class='con'>
                                             <h6 class='names'>" . $val['supm_name_en'] . "</h6>
                                        </div>
                                   </div>
                                </li>";
                 }
                 return $output;
            } else {
                 foreach ($res as $key => $val) {
                      $this->db->select(tbl_category . '.cat_id,cat_title');
                      $this->db->join(tbl_category, tbl_category . '.cat_id = ' . tbl_supplier_buildings_cate_assoc . '.sbca_category');
                      $this->db->where('sbca_supplier', $val['supm_id']);
                      $catg = $this->db->get(tbl_supplier_buildings_cate_assoc)->result_array();

                      if ($cat_id) {
                           if (!in_array($cat_id, array_column($catg, 'cat_id'))) {
                                continue;
                           }
                      }
                      $sup[$key]['name'] = $val['supm_name_en'];
                      $sup[$key]['supm_domain_prefix'] = $val['supm_domain_prefix'];
                      $sup[$key]['id'] = encryptor($val['supm_id']);
                      $sup[$key]['360_image'] = site_url() . 'supplier/get_360/' . $sup[$key]['id'];
                      if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                           $images = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id']))->row_array();
                      }
                      $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
                      $sup[$key]['image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
                 }
                 return array_values($sup);
            }
       }

       function getSupDetUsingDomainPrefix($prefix) {
            return $this->db->select('supm_id,supm_name_en')->where('supm_domain_prefix', $prefix)->get(tbl_supplier_master)->row_array();
       }

       public function suppliersbyDates($date1 = '', $date2 = '') {

            $date1 = $date1 . " 00:00:00";
            $date2 = $date2 . " 11:59:59";
            $whereindates = tbl_supplier_master . ".supm_added_on between '$date1' and
                   '$date2'";
            $supplierlist = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*,' . tbl_countries . '.*,' . tbl_states . '.*,' .
                                    tbl_buildins . '.*,' . tbl_market_places . '.*')
                            ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                            ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                            ->join(tbl_buildins, tbl_buildins . '.bld_id = ' . tbl_supplier_master . '.supm_building', 'LEFT')
                            ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                            ->where($whereindates)
                            ->where(tbl_users_groups . '.group_id', 2)
                            ->get(tbl_supplier_master)->result_array();
            $excel = array();
            foreach ($supplierlist as $key => $info) {

                 $excel[$key]['supplier name'] = isset($info['supm_name_en']) ? $info['supm_name_en'] : '';
                 $excel[$key]['supplier refno'] = isset($info['supm_ref_number']) ? $info['supm_ref_number'] : '';
                 $excel[$key]['supplier email'] = isset($info['supm_email']) ? $info['supm_email'] : '';
                 $excel[$key]['supplier phone'] = isset($info['supm_number']) ? $info['supm_number'] : '';
                 $excel[$key]['supplier website'] = isset($info['supm_website']) ? $info['supm_website'] : '';
                 $excel[$key]['supplier country'] = isset($info['ctr_name']) ? $info['ctr_name'] : '';
                 $excel[$key]['supplier state'] = isset($info['stt_name']) ? $info['stt_name'] : '';
                 $excel[$key]['supplier city'] = isset($info['supm_city_en']) ? $info['supm_city_en'] : '';
                 $excel[$key]['supplier address'] = isset($info['supm_address_en']) ? strip_tags($info['supm_address_en']) : '';
                 $excel[$key]['supplier room no'] = isset($info['supm_room_number']) ? $info['supm_room_number'] : '';
                 $excel[$key]['registration year'] = isset($info['supm_reg_year']) ? $info['supm_reg_year'] : '';
                 $excel[$key]['building'] = isset($info['bld_title']) ? $info['bld_title'] : '';
                 $excel[$key]['market'] = isset($info['mar_name']) ? $info['mar_name'] : '';


                 $excel[$key]['trade sssurance'] = isset($info['supm_trade_assurance_en']) ? $info['supm_trade_assurance_en'] : '';
                 $excel[$key]['trade sssurance level'] = isset($info['supm_transaction_level_en']) ? $info['supm_transaction_level_en'] : '';
                 $excel[$key]['total evenue'] = isset($info['supm_total_revenue']) ? $info['supm_total_revenue'] : '';
                 $excel[$key]['transactions'] = isset($info['supm_transactions_en']) ? $info['supm_transactions_en'] : '';
                 $excel[$key]['video link'] = isset($info['supm_official_video']) ? $info['supm_official_video'] : '';
                 $excel[$key]['contact name'] = isset($info['usr_username']) ? $info['usr_username'] : '';
                 $excel[$key]['contact email'] = isset($info['usr_email']) ? $info['usr_email'] : '';
                 $excel[$key]['contact phone'] = isset($info['usr_phone']) ? $info['usr_phone'] : '';
                 $excel[$key]['contact address'] = isset($info['usr_address']) ? strip_tags($info['usr_address']) : '';
            }


            return $excel;
       }

       function getBusinessTypes() {
            return $this->db->get_where(tbl_business_types, array('pst_status' => 1))->result_array();
       }

  }
  