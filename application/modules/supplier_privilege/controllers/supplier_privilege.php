<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class supplier_privilege extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->load->model('supplier_privilege_model', 'supplier_privilege');
            $this->lock_in();
       }

       function access_denied() {
            $this->render_page(strtolower(__CLASS__) . '/nop');
       }

       public function index($uid = '') {
            $data['usrSelected'] = encryptor($uid, 'D');
            $data['users'] = $this->supplier_privilege->getAllUserGroups($data['usrSelected']);
            $data['modules'] = $this->config->item('modules');

            unset($data['modules']['supplier']['index']);
            unset($data['modules']['supplier']['add']);
            unset($data['modules']['supplier']['view']);
            unset($data['modules']['supplier']['report']);

            $this->render_page(strtolower(__CLASS__) . '/index', $data);
       }

       function setPermission() {
            $this->supplier_privilege->addUserPermission($this->input->post());
            $this->session->set_flashdata('app_success', 'User permission successfully completed!');
            redirect(strtolower(__CLASS__));
       }

       function getPermission() {
            $userpermission = $this->supplier_privilege->getUserPermission($this->input->post('userid'));
            $permissionArray['permissions'] = array();
            if (isset($userpermission['cua_access']) && !empty($userpermission['cua_access'])) {
                 $tmp = unserialize($userpermission['cua_access']);
                 if (!empty($tmp) && is_array($tmp)) {
                      foreach ($tmp as $k => $v) {
                           foreach ($v as $l => $w) {
                                array_push($permissionArray['permissions'], $k . '-' . $w);
                           }
                      }
                 }
            }
            echo json_encode($permissionArray);die();
       }

  }
  