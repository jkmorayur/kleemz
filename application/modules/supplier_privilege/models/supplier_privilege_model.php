<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class supplier_privilege_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getAllUserGroups($id = 0) {

            if ($id > 0) {
                 $this->db->where(tbl_users . '.usr_id', $id);
            }
            $this->db->where(tbl_users . '.usr_id !=', 1);
            return $this->db->select(tbl_users . '.*, ' .
                                    tbl_users_groups . '.group_id as group_id, ' .
                                    tbl_groups . '.name as group_name, ' .
                                    tbl_groups . '.description as group_desc, ' . tbl_supplier_master . '.supm_name'
                            )
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                            ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_users . '.usr_supplier', 'LEFT')
                            ->where(tbl_groups . '.id', 2)
                            ->get(tbl_users)->result_array();
       }

       function addUserPermission($data) {

            if (!empty($data)) {
                 if ($this->getUserPermission($data['cua_group_id'])) {
                      $this->db->where('cua_group_id', $data['cua_group_id'])->delete(tbl_user_access);
                 }

                 $data['cua_access'] = !empty($data['cua_access']) ? serialize($data['cua_access']) : serialize(array());
                 $this->db->insert(tbl_user_access, $data);
            } else {
                 return false;
            }
       }

       function getUserPermission($id) {
            if (!empty($id)) {
                 return $this->db->select('*')->where('cua_group_id', $id)->get(tbl_user_access)->row_array();
            } else {
                 return false;
            }
       }

  }
  