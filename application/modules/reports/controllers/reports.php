<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  require './vendor/autoload.php';

  class reports extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->page_title = 'Reports';
            $this->load->model('reports_model', 'reports');
            $this->lock_in();
       }

       public function srr() {
            $this->page_title = 'Reports - SRR';
            $data['reports'] = $this->reports->getSRR();
//            $data['todaysReports'] = $this->reports->isSubmittedToday();
            $data['myDB'] = $this->reports->getMyDB();
            $this->render_page(strtolower(__CLASS__) . '/srrPartOne', $data);
       }

       function generatesrr() {
            $this->page_title = 'Reports - SRR';
            $data = $_POST;
            $data = $this->reports->generatesrr($_POST);
            if (isset($data['is_already_submitted']) && $data['is_already_submitted'] == 1) {
                 $this->render_page(strtolower(__CLASS__) . '/srr_summery', $data);
            } else {
                 $this->render_page(strtolower(__CLASS__) . '/generatesrr', $data);
            }
       }

       function submitsrr() {
            if ($this->reports->submitsrr($_POST)) {
                 $this->session->set_flashdata('app_success', 'New report generated');
            } else {
                 $this->session->set_flashdata('app_success', 'Error occured');
            }
            redirect(strtolower(__CLASS__) . '/srr');
       }

       function srr_summery($id) {
            $id = encryptor($id, 'D');
            $data = $this->reports->getSRR($id);
            $this->render_page(strtolower(__CLASS__) . '/srr_summery', $data);
       }

       function exportsrr($id) {
            $footerContent = 'Name and signature &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                    . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                    . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                    . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                    . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                    . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature of town admin';

            $id = encryptor($id, 'D');
            $data = $this->reports->getSRR($id);
            $data['footer'] = $footerContent;

            $html = $this->load->view('temp_srr_part1', $data, true);
            $pdfFilePath = "SRR-report.pdf";

            $this->load->library('m_pdf');

            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->SetHTMLFooter($footerContent);
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
       }

  }
  