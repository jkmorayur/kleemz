<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class reports_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getMyDB() {
            $this->db->where(tbl_groups . '.id', 10);
            $this->db->where(tbl_users . '.usr_active', 1);
            if ($this->uid != 1) {
                 $this->db->where(tbl_users . '.usr_town_admin', $this->uid);
            }
            return $this->db->select(tbl_users . '.*, ' .
                                    tbl_users_groups . '.group_id as group_id, ' .
                                    tbl_groups . '.name as group_name, ' .
                                    tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                    'supp.usr_first_name AS sup_first_name, supp.usr_last_name AS sup_last_name,' .
                                    tbl_supplier_master . '.*')
                            ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                            ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                            ->join(tbl_users . ' supp', 'supp.usr_id = ' . tbl_users . '.usr_supplier', 'LEFT')
                            ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_users . '.usr_supplier', 'LEFT')
                            ->order_by(tbl_users . '.usr_code_in_part', 'ASC')->get(tbl_users)->result_array();
       }

       function generatesrr($data) {

            if ((isset($data['date']) && isset($data['db'])) && (!empty($data['date']) && !empty($data['db']))) {

                 //Check if already submitted purticular date
                 $date = date('Y-m-d', strtotime($data['date']));

                 $data['master'] = $this->db->select(tbl_users . '.*,' . tbl_report_srr_master . '.*,' . tbl_market_places . '.*,' .
                                         tbl_report_srr_master . '.*, ta.usr_first_name AS ta_first_name, ta.usr_last_name AS ta_last_name')
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_report_srr_master . '.rsrrm_db_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_db_town', 'LEFT')
                                 ->join(tbl_users . ' ta', 'ta.usr_id = ' . tbl_report_srr_master . '.rsrrm_added_by', 'LEFT')
                                 ->get_where(tbl_report_srr_master, array('rsrrm_db_id' => $data['db'], 'DATE(rsrrm_report_date)' => $date))->row_array();

                 if (!empty($data['master'])) {
                      $data['is_already_submitted'] = 1;
                      $data['details'] = $this->db->get_where(tbl_report_srr_details, array('rsrrd_master_id' => $data['master']['rsrrm_id']))->result_array();

                      return $data;
                 } else {
                      $data['is_already_submitted'] = 0;
                      $this->db->where('DATE(' . tbl_products_order_master . '.ordm_added_on) =', $date);
                      $this->db->where(tbl_products_order_master . '.ordm_delivery_boy', $data['db']);
                      $this->db->where(tbl_products_order_master . '.ordm_submited_srr_report', 0);

                      $selectOrderMaster = array(
                          tbl_products_order_master . '.*',
                          tbl_supplier_master . '.supm_id',
                          tbl_supplier_master . '.supm_market',
                          tbl_supplier_master . '.supm_name',
                          tbl_supplier_master . '.supm_name',
                          tbl_users . '.usr_username AS buy_usr_username',
                          tbl_users . '.usr_phone AS buy_usr_phone',
                          tbl_users . '.usr_id AS buy_usr_id',
                          tbl_products_order_statuses . '.*',
                          tbl_market_places . '.mar_name',
                          'dboy.usr_first_name AS dboy_usr_first_name',
                          'dboy.usr_last_name AS dboy_usr_last_name',
                      );
                      $data['orderMaster'] = $this->db->select(implode(',', $selectOrderMaster))
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_master . '.ordm_supplier', 'LEFT')
                                      ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                      ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                      ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->order_by(tbl_products_order_master . '.ordm_id', 'DESC')->get(tbl_products_order_master)->result_array();

                      if (!empty($data['orderMaster'])) {
                           $orderMasterId = array_column($data['orderMaster'], 'ordm_id');

                           $selectOrderDetails = array(
                               tbl_products_order_details . '.*',
                               tbl_products_stock_details . '.*',
                               tbl_products_stock_master . '.*',
                               tbl_products_master . '.*',
                               tbl_products_units . '.*',
                               tbl_supplier_master . '.supm_id',
                               tbl_supplier_master . '.supm_name',
                               tbl_supplier_master . '.supm_number',
                               tbl_supplier_master . '.supm_email',
                               tbl_supplier_master . '.supm_address',
                               tbl_supplier_master . '.supm_market',
                               tbl_supplier_master . '.supm_shop_charges',
                               tbl_market_places . '.mar_name',
                               tbl_market_places . '.mar_lat',
                               tbl_market_places . '.mar_long'
                           );

                           $data['orderDetails'] = $this->db->select(implode(',', $selectOrderDetails))
                                           ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                           ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                           ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                           ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                           ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                           ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                           ->where_in('ordd_order_master_id', $orderMasterId)->get(tbl_products_order_details)->result_array();

                           if (!empty($data['orderDetails'])) {
                                $data['srr_part_2']['ttl_shop_chg_5'] = 0;
                                $data['srr_part_2']['ttl_shop_chg_7'] = 0;
                                $data['srr_part_2']['ttl_shop_chg_10'] = 0;

                                $storeValue = array();
                                foreach ($data['orderDetails'] as $key => $value) {

                                     $amountNotActual = ($value['ordd_unit_discount'] > 0) ? $value['ordd_unit_discount'] : $value['ordd_unit_total'];
                                     $storeValue[$key] = ($value['ordd_actual_rate'] > 0) ? $value['ordd_actual_rate'] : $amountNotActual;

                                     if ($value['supm_shop_charges'] == SHOP_CHRG_SLAB_1) {
                                          $data['srr_part_2']['ttl_shop_chg_5'] += SHOP_CHRG_SLAB_1;
                                     } else if ($value['supm_shop_charges'] == SHOP_CHRG_SLAB_2) {
                                          $data['srr_part_2']['ttl_shop_chg_7'] += SHOP_CHRG_SLAB_2;
                                     } else if ($value['supm_shop_charges'] == SHOP_CHRG_SLAB_3) {
                                          $data['srr_part_2']['ttl_shop_chg_10'] += SHOP_CHRG_SLAB_3;
                                     }
                                }

                                $data['srr_part_2']['ttl_stor_val'] = array_sum($storeValue);
                                $data['srr_part_2']['ttl_dlvry_chg'] = array_sum(array_column($data['orderMaster'], 'ordm_delivery_chrg'));
                           }
                      }
                      generate_log(array(
                          'log_title' => 'SRR generated',
                          'log_desc' => serialize($data),
                          'log_controller' => 'SRR-generated',
                          'log_action' => 'C',
                          'log_ref_id' => 0,
                          'log_added_by' => $this->uid
                      ));
                      return $data;
                 }
            }
            return false;
       }

       function getSRR($id = '') {
            if (empty($id)) {
                 if ($this->usr_grp == 'DB') {
                      $this->db->where('rsrrm_db_id', $this->uid);
                 } else if ($this->usr_grp == 'TA') {
                      $this->db->where('rsrrm_added_by', $this->uid);
                 }
                 return $this->db->select(tbl_users . '.*,' . tbl_report_srr_master . '.*,' . tbl_market_places . '.*,' .
                                         tbl_report_srr_master . '.*, ta.usr_first_name AS ta_first_name, ta.usr_last_name AS ta_last_name')
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_report_srr_master . '.rsrrm_db_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_db_town', 'LEFT')
                                 ->join(tbl_users . ' ta', 'ta.usr_id = ' . tbl_report_srr_master . '.rsrrm_added_by', 'LEFT')
                                 ->get(tbl_report_srr_master)->result_array();
            } else {
                 $masterData['master'] = $this->db->select(tbl_users . '.*,' . tbl_report_srr_master . '.*,' . tbl_market_places . '.*,' .
                                         tbl_report_srr_master . '.*, ta.usr_first_name AS ta_first_name, ta.usr_last_name AS ta_last_name')
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_report_srr_master . '.rsrrm_db_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_db_town', 'LEFT')
                                 ->join(tbl_users . ' ta', 'ta.usr_id = ' . tbl_report_srr_master . '.rsrrm_added_by', 'LEFT')
                                 ->get_where(tbl_report_srr_master, array('rsrrm_id' => $id))->row_array();

                 if (!empty($masterData)) {
                      $masterData['details'] = $this->db->get_where(tbl_report_srr_details, array('rsrrd_master_id' => $id))->result_array();
                 }
                 return $masterData;
            }
       }

       function submitsrr($data) {
            if (!empty($data)) {
                 $dbDetails = $this->db->get_where(tbl_users, array('usr_id' => $data['master']['rsrrm_db_id']))->row_array();
                 $data['master']['rsrrm_db_market'] = isset($dbDetails['usr_db_town']) ? $dbDetails['usr_db_town'] : 0;
                 $data['master']['rsrrm_added_on'] = date('Y-m-d h:i:s');
                 $data['master']['rsrrm_added_by'] = $this->uid;

                 $this->db->insert(tbl_report_srr_master, $data['master']);
                 $insertId = $this->db->insert_id();
                 if (isset($data['details']) && !empty($data['details'])) {
                      foreach ($data['details'] as $key => $value) {

                           $this->db->where('ordm_id', $value['rsrrd_order_id']);
                           $this->db->update(tbl_products_order_master, array('ordm_submited_srr_report' => 1));

                           $value['rsrrd_master_id'] = $insertId;
                           $this->db->insert(tbl_report_srr_details, $value);
                      }
                 }

                 generate_log(array(
                     'log_title' => 'SRR submitted',
                     'log_desc' => serialize($data),
                     'log_controller' => 'SRR-submitted',
                     'log_action' => 'C',
                     'log_ref_id' => $insertId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            }
            return false;
       }

       function isSubmittedToday() {
            return $this->db->get_where(tbl_report_srr_master, array('rsrrm_added_by' => $this->uid, 'DATE(rsrrm_added_on)' => date('Y-m-d')))->result_array();
       }

       /**
        * Get order total include discount, shop charge and delivery charge
        * @param int $ordmId
        * @return decimal
        */
       function getOrderTotal($ordmId) {
            $delChrg = $this->db->select('ordm_delivery_chrg')->get_where(tbl_products_order_master, array('ordm_id' => $ordmId))->row()->ordm_delivery_chrg;
            $selectOrderDetails = array(
                tbl_products_order_details . '.ordd_actual_rate',
                tbl_products_order_details . '.ordd_unit_total',
                tbl_products_order_details . '.ordd_unit_discount',
                tbl_supplier_master . '.supm_shop_charges'
            );

            $orderDetails = $this->db->select(implode(',', $selectOrderDetails))
                            ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                            ->where('ordd_order_master_id', $ordmId)->get(tbl_products_order_details)->result_array();

            $shopChrg = array();
            $orderTotal = array();

            if (!empty($orderDetails)) {
                 foreach ($orderDetails as $key => $value) {
                      $amountNotActual = ($value['ordd_unit_discount'] > 0) ? $value['ordd_unit_discount'] : $value['ordd_unit_total'];
                      $orderTotal[$key] = ($value['ordd_actual_rate'] > 0) ? $value['ordd_actual_rate'] : $amountNotActual;
                      $shopChrg[$key] = $value['supm_shop_charges'];
                 }
            }
            $orderTotal = array_sum($orderTotal);
            $shopChrg = array_sum($shopChrg);

            return $orderTotal + $shopChrg + $delChrg;
       }

  }
  