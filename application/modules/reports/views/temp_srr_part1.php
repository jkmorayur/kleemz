<div style="width: 2480px; height: 98%;">
     <h1 style="text-align: center;margin-bottom: 0px;">SRR</h1>
     <div style="text-align: center;">(part-1)</div>
     <div>
          <table style="width: 100%;">
               <tr>
                    <td style="width: 100px;border: none;">DB ID : </td>
                    <td style="width: 400px;border: none;"><?php echo $master['usr_code'];?></td>
                    <td style="width: 100px;border: none;">Date : </td>
                    <td style="border: none;"><?php echo date('d-m-Y', strtotime($master['rsrrm_added_on']));?></td>
               </tr>
               <tr>
                    <td style="border: none;">DB NAME : </td>
                    <td style="border: none;"><?php echo $master['usr_username'];?></td>
                    <td style="border: none;">Closing time : </td>
                    <td style="border: none;"><?php echo date('h:i A', strtotime($master['rsrrm_added_on']));?></td>
               </tr>
               <tr>
                    <td style="border: none;">Town : </td>
                    <td style="border: none;"><?php echo $master['mar_name'];?></td>
               </tr>
          </table>
     </div>

     <table style="width: 100%;margin-top: 20px;" class="table">
          <thead>
               <tr>
                    <th>S.NO</th>
                    <th>Order No</th>
                    <th>Pay.mod</th>
                    <th>On.pay</th>
                    <th>COD</th>
               </tr>
          </thead>
          <tbody>
               <?php
                 $totalCOD = 0;
                 $totalPAY = 0;
                 if (!empty($details)) {
                      foreach ((array) $details as $key => $value) {
                           $amtCOD = ($value['rsrrd_paymeny_mod'] == 1) ? $value['rsrrd_amount'] : 0;
                           $amtPAY = ($value['rsrrd_paymeny_mod'] != 1) ? $value['rsrrd_amount'] : 0;
                           $totalCOD += $amtCOD;
                           $totalPAY += $amtPAY;
                           ?>
                           <tr>
                                <td><?php echo $key + 1;?></td>
                                <td><?php echo $value['rsrrd_order_number'];?></td>
                                <td><?php echo ($value['rsrrd_paymeny_mod'] == 1) ? 'COD' : 'On.Pay';?></td>
                                <td><?php echo $amtPAY;?></td>
                                <td><?php echo $amtCOD;?></td>
                           </tr>
                           <?php
                      }
                      ?>
                      <tr>
                           <td class="total-amt" colspan="3">Total</td>
                           <td class="total-amt"><?php echo $totalPAY;?></td>
                           <td class="total-amt"><?php echo $totalCOD;?></td>
                      </tr>
                      <?php
                 } else {
                      echo '<tr style="text-align: center;"><td colspan="9">Empty list</td></tr>';
                 }
               ?>
          </tbody>
     </table>
     <table style="margin-top: 20px;width: 50%;" class="table">
          <tr>
               <th>ONLINE PAYMENT</th>
               <td class="total-amt"><?php echo $totalPAY;?></td>
          </tr>
          <tr>
               <th>CASH ON DELIVERY</th>
               <td class="total-amt"><?php echo $totalCOD;?></td>
          </tr>
          <tr>
               <th>GRAND TOTAL</th>
               <td class="total-amt"><?php echo $totalPAY + $totalCOD;?></td>
          </tr>
     </table>
</div>
<?php echo $footer; ?>

<div style="width: 2480px; height: 100%;overflow: hidden;">
     <h1 style="text-align: center;margin-bottom: 0px;">SRR</h1>
     <div style="text-align: center;">(part-2)</div>

     <div>
          <table style="width: 100%;">
               <tr>
                    <td style="width: 100px;border: none;">DB ID : </td>
                    <td style="width: 400px;border: none;"><?php echo $master['usr_code'];?></td>
                    <td style="width: 100px;border: none;">Date : </td>
                    <td style="border: none;"><?php echo date('d-m-Y', strtotime($master['rsrrm_added_on']));?></td>
               </tr>
               <tr>
                    <td style="border: none;">DB NAME : </td>
                    <td style="border: none;"><?php echo $master['usr_username'];?></td>
                    <td style="border: none;">Closing time : </td>
                    <td style="border: none;"><?php echo date('h:i A', strtotime($master['rsrrm_added_on']));?></td>
               </tr>
               <tr>
                    <td style="border: none;">Town : </td>
                    <td style="border: none;"><?php echo $master['mar_name'];?></td>
               </tr>
          </table>
     </div>

     <table style="width: 100%;margin-top: 20px;" class="table">
          <thead>
               <tr>
                    <th>Values</th>
                    <th>Qnt</th>
                    <th>Amount</th>
               </tr>
          </thead>
          <tbody>
               <tr>
                    <td>Total Store value</td>
                    <td><?php echo $master['rsrrm_ttl_store_count'];?></td>
                    <td><?php echo $master['rsrrm_ttl_store_value'];?></td>
               </tr>
               <tr>
                    <td>Total delivery charge</td>
                    <td><?php echo $master['rsrrm_ttl_delivery_count'];?></td>
                    <td><?php echo $master['rsrrm_ttl_delivery_value'];?></td>
               </tr>

               <tr>
                    <td>Shop charge value - <?php echo SHOP_CHRG_SLAB_1; ?></td>
                    <td><?php echo $master['rsrrm_ttl_sp_chg_count_5'];?></td>
                    <td><?php echo $master['rsrrm_ttl_sp_chg_value_5'];?></td>
               </tr>

               <tr>
                    <td>Shop charge value - <?php echo SHOP_CHRG_SLAB_2; ?></td>
                    <td><?php echo $master['rsrrm_ttl_sp_chg_count_7'];?></td>
                    <td><?php echo $master['rsrrm_ttl_sp_chg_value_7'];?></td>
               </tr>

               <tr>
                    <td>Shop charge value - <?php echo SHOP_CHRG_SLAB_3; ?></td>
                    <td><?php echo $master['rsrrm_ttl_sp_chg_count_10'];?></td>
                    <td><?php echo $master['rsrrm_ttl_sp_chg_value_10'];?></td>
               </tr>

               <tr>
                    <td class="total-amt" colspan="2">Total value</td>
                    <td class="total-amt"><?php echo $master['rsrrm_grand_total'];?></td>
               </tr>

          </tbody>
     </table>
</div>

<style>
     .table {border-collapse: collapse;}
     .table, th, td {border: 1px solid black;}
     .total-amt { text-align: center;font-size: 20px;font-weight: bold;}
</style>