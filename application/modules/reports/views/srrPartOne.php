<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>SRR report</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                         <?php echo form_open_multipart($controller . "/generatesrr", array('id' => "frmNewsEvents", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Select DB</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select name="db" id="stt_country_id" class="form-control col-md-7 col-xs-12" required="true"
                                           data-parsley-required-message="Please select DB">
                                        <option value="">Select DB</option>
                                        <?php foreach ((array) $myDB as $key => $value) {?>
                                               <option value="<?php echo $value['usr_id'];?>"><?php echo $value['usr_code'] . '-' . $value['usr_username'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Date</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" name="date" id="unt_unit_name_en" value="<?php echo date('d-m-Y');?>"
                                          data-parsley-required-message="Please select date" class="dtpDatePicker form-control col-md-9 col-xs-12"/>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Generate SRR</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>

                         <table id="tbl" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>SRR Date</th>
                                        <th>Generated On</th>
                                        <th>Grand total</th>
                                        <th>Store value</th>
                                        <th>Delivery charge</th>
                                        <th>Shop charges</th>
                                        <?php if ($this->usr_grp == 'TA' || is_root_user()) {?>
                                               <th>DB Code</th>
                                               <th>DB Name</th>
                                          <?php } if (check_permission($controller, 'exportsrr')) {?>
                                               <th>Export</th>
                                          <?php }?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ($reports as $key => $value) {
                                          $shopCharge = $value['rsrrm_ttl_sp_chg_value_5'] +
                                                  $value['rsrrm_ttl_sp_chg_value_7'] + $value['rsrrm_ttl_sp_chg_value_10'];
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/srr_summery/' . encryptor($value['rsrrm_id']));?>">
                                               <td class="trVOE"><?php echo date('Y-m-d', strtotime($value['rsrrm_report_date']));?></td>
                                               <td class="trVOE"><?php echo date('Y-m-d h:i', strtotime($value['rsrrm_added_on']));?></td>
                                               <td class="trVOE"><?php echo $value['rsrrm_grand_total'];?></td>
                                               <td class="trVOE"><?php echo $value['rsrrm_ttl_store_value'];?></td>
                                               <td class="trVOE"><?php echo $value['rsrrm_ttl_delivery_value'];?></td>
                                               <td class="trVOE"><?php echo $shopCharge;?></td>
                                               <?php if ($this->usr_grp == 'TA' || is_root_user()) {?>
                                                    <td class="trVOE"><?php echo $value['usr_code'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_username'];?></td>
                                               <?php } if (check_permission($controller, 'exportsrr')) {?>
                                                    <td>
                                                         <a href="<?php echo site_url($controller . '/exportsrr/' . encryptor($value['rsrrm_id']));?>">
                                                              <img width="15" title="Export to excel" src="images/pdf.png">
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                     <?php }?>

                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>