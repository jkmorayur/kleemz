<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>SRR report</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                         <?php if (empty($todaysReports)) {?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                     <a href="<?php echo site_url($controller . '/generatesrr');?>" class="btn btn-round btn-primary">
                                          <i class="fa fa-pencil-square-o"></i> Generate SRR on <?php echo date('d-m-Y');?>
                                     </a>
                                </div>
                           <?php } else {?>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                     <h3 style="color: red;"> You had already submitted SRR for today</h3>
                                </div>
                           <?php }?>
                         <table id="tbl" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Added On</th>
                                        <th>Grand total</th>
                                        <th>Store value</th>
                                        <th>Delivery charge</th>
                                        <th>Shop charges</th>
                                        <?php if ($this->uid == 1) {?>
                                               <th>DB Code</th>
                                               <th>DB Name</th>
                                          <?php }?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ($reports as $key => $value) {
                                          $shopCharge = $value['rsrrm_ttl_sp_chg_value_5'] +
                                                  $value['rsrrm_ttl_sp_chg_value_7'] + $value['rsrrm_ttl_sp_chg_value_10'];
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/srr_summery/' . encryptor($value['rsrrm_id']));?>">
                                               <td class="trVOE"><?php echo date('Y-m-d h:i', strtotime($value['rsrrm_added_on']));?></td>
                                               <td class="trVOE"><?php echo $value['rsrrm_grand_total'];?></td>
                                               <td class="trVOE"><?php echo $value['rsrrm_ttl_store_value'];?></td>
                                               <td class="trVOE"><?php echo $value['rsrrm_ttl_delivery_value'];?></td>
                                               <td class="trVOE"><?php echo $shopCharge;?></td>
                                               <?php if ($this->uid == 1) {?>
                                                    <td class="trVOE"><?php echo $value['usr_code'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_username'];?></td>
                                               <?php }?>
                                          </tr>
                                     <?php }?>

                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>