<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-6 col-xs-12">
               <div class="x_panel">
                    <?php
                      echo form_open_multipart($controller . "/submitsrr", array('id' => "frmSupplier",
                          'class' => "frmSupplier form-horizontal", 'data-parsley-validate' => "true",
                          'onsubmit' => "return confirm('Are you sure want to submit it, because it can only once in a day');"))
                    ?>
                    <input type="hidden" name="master[rsrrm_db_id]" value="<?php echo isset($db) ? $db : 0;?>"/>
                    <input type="hidden" name="master[rsrrm_report_date]" value="<?php echo isset($date) ? date('Y-m-d', strtotime($date)) : null;?>"/>

                    <div class="x_title">
                         <h2>Generate SRR report on <?php echo date('d-m-Y');?></h2>
                         <div class="clearfix"></div>
                    </div>
                    <?php if (!empty($orderMaster)) {?>
                           <div class="col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-round btn-primary"><i class="fa fa-pencil-square-o"></i> Submit SRR Report</button>
                           </div>
                      <?php }?>
                    <div class="x_content">

                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                   <li role="presentation" class="active">
                                        <a href="javascript:;" goto="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                           aria-expanded="true">PART-I</a>
                                   </li>

                                   <li role="presentation" class="pars">
                                        <a href="javascript:;" goto="#tab_content2" role="tab" id="profile-tab"
                                           data-toggle="tab" aria-expanded="false">PART-II</a>
                                   </li>
                              </ul>

                              <div id="myTabContent" class="tab-content">

                                   <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                        <table id="tbl" class="table table-striped table-bordered">
                                             <thead>
                                                  <tr>
                                                       <th>S.NO</th>
                                                       <th>Order No</th>
                                                       <th>Pay.mod</th>
                                                       <th>On.pay</th>
                                                       <th>COD</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php
                                                    $totalCOD = 0;
                                                    $totalPAY = 0;
                                                    if (!empty($orderMaster)) {
                                                         foreach ((array) $orderMaster as $key => $value) {
                                                              $grndTotal = $this->reports->getOrderTotal($value['ordm_id']);
                                                              $amtCOD = ($value['ordm_payment_type'] == 1) ? $grndTotal : 0;
                                                              $amtPAY = ($value['ordm_payment_type'] != 1) ? $grndTotal : 0;
                                                              $totalCOD += $amtCOD;
                                                              $totalPAY += $amtPAY;
                                                              ?>
                                                              <tr>
                                                                   <td>
                                                                        <?php echo $key + 1;?>
                                                                        <input type="hidden" name="details[<?php echo $key;?>][rsrrd_order_number]" value="<?php echo $value['ordm_number'];?>"/>
                                                                        <input type="hidden" name="details[<?php echo $key;?>][rsrrd_order_id]" value="<?php echo $value['ordm_id'];?>"/>
                                                                        <input type="hidden" name="details[<?php echo $key;?>][rsrrd_paymeny_mod]" value="<?php echo $value['ordm_payment_type'];?>"/>
                                                                        <input type="hidden" name="details[<?php echo $key;?>][rsrrd_amount]" value="<?php echo $grndTotal;?>"/>
                                                                   </td>
                                                                   <td><?php echo $value['ordm_number'];?></td>
                                                                   <td><?php echo ($value['ordm_payment_type'] == 1) ? 'COD' : 'On.Pay';?></td>
                                                                   <td><?php echo $amtPAY;?></td>
                                                                   <td><?php echo $amtCOD;?></td>
                                                              </tr>
                                                              <?php
                                                         }
                                                         ?>
                                                         <tr>
                                                              <td class="total-amt" colspan="3">Total</td>
                                                              <td class="total-amt"><?php echo $totalPAY;?></td>
                                                              <td class="total-amt"><?php echo $totalCOD;?></td>
                                                         </tr>
                                                         <?php
                                                    } else {
                                                         echo '<tr style="text-align: center;"><td colspan="9">Empty list</td></tr>';
                                                    }
                                                  ?>
                                             </tbody>
                                        </table>
                                        <h2>Today's summary</h2>
                                        <table class="table table-striped table-bordered">
                                             <tr>
                                                  <th>ONLINE PAYMENT</th>
                                                  <td class="total-amt"><?php echo $totalPAY;?></td>
                                             </tr>
                                             <tr>
                                                  <th>CASH ON DELIVERY</th>
                                                  <td class="total-amt"><?php echo $totalCOD;?></td>
                                             </tr>
                                             <tr>
                                                  <th>GRAND TOTAL</th>
                                                  <td class="total-amt"><?php echo $totalPAY + $totalCOD;?></td>
                                             </tr>
                                        </table>
                                   </div>

                                   <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                        <table id="tbl" class="table table-striped table-bordered">
                                             <thead>
                                                  <tr>
                                                       <th>Values</th>
                                                       <th>Qnt</th>
                                                       <th>Amount</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php
                                                    if (!empty($srr_part_2)) {
                                                         ?>
                                                         <tr>
                                                    <input type="hidden" name="master[rsrrm_ttl_store_count]" 
                                                           value="<?php echo count(array_unique(array_column($orderDetails, 'ordd_supplier')));?>"/>
                                                    <input type="hidden" name="master[rsrrm_ttl_store_value]" 
                                                           value="<?php echo $srr_part_2['ttl_stor_val'];?>"/>
                                                    <td>Total store value</td>
                                                    <td><?php echo count(array_unique(array_column($orderDetails, 'ordd_supplier')));?></td>
                                                    <td><?php echo $srr_part_2['ttl_stor_val'];?></td>
                                                    </tr>
                                                    <tr>
                                                    <input type="hidden" name="master[rsrrm_ttl_delivery_count]" 
                                                           value="<?php echo count($orderMaster);?>"/>
                                                    <input type="hidden" name="master[rsrrm_ttl_delivery_value]" 
                                                           value="<?php echo $srr_part_2['ttl_dlvry_chg'];?>"/>
                                                    <td>Total delivery charge</td>
                                                    <td><?php echo count($orderMaster);?></td>
                                                    <td><?php echo $srr_part_2['ttl_dlvry_chg'];?></td>
                                                    </tr>
                                                    <tr>
                                                         <td>Shop charge value - <?php echo SHOP_CHRG_SLAB_1;?></td>
                                                         <td>
                                                              <?php
                                                              echo $ttl_shop_chg_5_cnt = (isset($srr_part_2['ttl_shop_chg_5']) && !empty($srr_part_2['ttl_shop_chg_5'])) ?
                                                              $srr_part_2['ttl_shop_chg_5'] / SHOP_CHRG_SLAB_1 : 0;
                                                              ?>
                                                         </td>
                                                         <td><?php echo $ttl_shop_chg_5 = isset($srr_part_2['ttl_shop_chg_5']) ? $srr_part_2['ttl_shop_chg_5'] : 0;?></td>
                                                    </tr>
                                                    <tr>
                                                         <td>Shop charge value - <?php echo SHOP_CHRG_SLAB_2;?></td>
                                                         <td>
                                                              <?php
                                                              echo $ttl_shop_chg_7_cnt = (isset($srr_part_2['ttl_shop_chg_7']) && !empty($srr_part_2['ttl_shop_chg_7'])) ?
                                                              $srr_part_2['ttl_shop_chg_7'] / SHOP_CHRG_SLAB_2 : 0;
                                                              ?>
                                                         </td>
                                                         <td><?php echo $ttl_shop_chg_7 = isset($srr_part_2['ttl_shop_chg_7']) ? $srr_part_2['ttl_shop_chg_7'] : 0;?></td>
                                                    </tr>
                                                    <tr>
                                                         <td>Shop charge value - <?php echo SHOP_CHRG_SLAB_3;?></td>
                                                         <td>
                                                              <?php
                                                              echo $ttl_shop_chg_10_cnt = (isset($srr_part_2['ttl_shop_chg_10']) && !empty($srr_part_2['ttl_shop_chg_10'])) ?
                                                              $srr_part_2['ttl_shop_chg_10'] / SHOP_CHRG_SLAB_3 : 0;
                                                              ?>
                                                         </td>
                                                         <td><?php echo $ttl_shop_chg_10 = isset($srr_part_2['ttl_shop_chg_10']) ? $srr_part_2['ttl_shop_chg_10'] : 0;?></td>
                                                    </tr>
                                                    <tr>
                                                         <td colspan="2" class="total-amt">Total value</td>
                                                         <td class="total-amt">
                                                              <?php
                                                              echo $rsrrm_grand_total = $srr_part_2['ttl_stor_val'] + $srr_part_2['ttl_dlvry_chg'] +
                                                              $srr_part_2['ttl_shop_chg_5'] + $srr_part_2['ttl_shop_chg_7'] + $srr_part_2['ttl_shop_chg_10']
                                                              ?>
                                                         </td>
                                                    </tr>
                                                    <?php
                                               } else {
                                                    echo '<tr style="text-align: center;"><td colspan="3">Empty list</td></tr>';
                                               }
                                             ?>
                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         </div>
                         <input type="hidden" name="master[rsrrm_ttl_sp_chg_count_5]" value="<?php echo isset($ttl_shop_chg_5_cnt) ? $ttl_shop_chg_5_cnt : 0;?>"/>
                         <input type="hidden" name="master[rsrrm_ttl_sp_chg_value_5]" value="<?php echo isset($ttl_shop_chg_5) ? $ttl_shop_chg_5 : 0;?>"/>

                         <input type="hidden" name="master[rsrrm_ttl_sp_chg_count_7]" value="<?php echo isset($ttl_shop_chg_7_cnt) ? $ttl_shop_chg_7_cnt : 0;?>"/>
                         <input type="hidden" name="master[rsrrm_ttl_sp_chg_value_7]" value="<?php echo isset($ttl_shop_chg_7) ? $ttl_shop_chg_7 : 0;?>"/>

                         <input type="hidden" name="master[rsrrm_ttl_sp_chg_count_10]" value="<?php echo isset($ttl_shop_chg_10_cnt) ? $ttl_shop_chg_10_cnt : 0;?>"/>
                         <input type="hidden" name="master[rsrrm_ttl_sp_chg_value_10]" value="<?php echo isset($ttl_shop_chg_10) ? $ttl_shop_chg_10 : 0;?>"/>

                         <input type="hidden" name="master[rsrrm_grand_total]" value="<?php echo isset($rsrrm_grand_total) ? $rsrrm_grand_total : 0;?>"/>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .total-amt { text-align: center;font-size: 20px;font-weight: bold;}
</style>
