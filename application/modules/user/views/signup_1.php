<section class="multiform-container log">
     <div class="container">
          <div class="row">
               <!-- Steps form -->
               <div class="card">
                    <div class="card-body">
                         <!-- Stepper -->
                         <div class="steps-form">
                              <div class="steps-row setup-panel">
                                   <div class="steps-step">
                                        <a href="#verify" type="button" class="btn btn-indigo btn-circle ver_btn">1</a>
                                        <p>Verification</p>
                                   </div>
                                   <div class="steps-step nxt_link" disabled="disabled">
                                        <a href="javascript:;" type="button" class="btn btn-default btn-circle nxt_link" disabled="disabled">2</a>
                                        <p>Info</p>
                                   </div>
                                   <div class="steps-step nxt_link">
                                        <a href="javascript:;" type="button" class="btn btn-default btn-circle nxt_link" disabled="disabled">3</a>
                                        <p>Finish</p>
                                   </div>
                              </div>
                         </div>

                         <form role="form" method="post" class="frmSignUpStepFirst">
                              <!-- First Step -->
                              <input type="hidden" id="site_url" value="<?php echo site_url();?>">
                              <div class="setup-content" id="verify">
                                   <div class="row divSignUpStepFirst">
                                        <div class="col-12">
                                             <div>
                                                  <div class="form-group md-form">
                                                       <label for="email" data-error="wrong" data-success="right">Email</label>
                                                       <input id="email" type="text" name="usr_email" required="required" class="form-control validate" placeholder="Enter your email id">
                                                       <span id="mail_exist"></span>
                                                  </div>

                                                  <div class="form-group md-form captcha">
                                                       <!--<img src="images/captcha.jpg" alt="" class="mx-auto d-block">-->
                                                       <!-- <?php echo $captcha['image'];?>
                                                       <input value="<?php echo $captcha['word'];?>" type="hidden" name="captcha_code" id="captcha_code"/>

                                                       <label for="captcha" data-error="wrong" data-success="right">Captcha</label> -->
                                                       <!--<div class="input-group ">-->
                                                       <!--   <div id="captcha_img"><?php echo $captcha['image'];?></div>-->
                                                       <!--   <input value="<?php echo $captcha['word'];?>" type="hidden" name="captcha_code" id="captcha_code"/>-->

                                                       <!--   <div class="input-group-append">-->
                                                       <!--   <a  class="btn btn-default btn-sm refresh_captcha">-->
                                                       <!--         <span class="glyphicon glyphicon-refresh "></span> Refresh-->
                                                       <!--    </a>-->
                                                       <!--    </div>-->
                                                       <!--</div>-->

                                                       <div class="input-group captcha-center ">
                                                            <div id="captcha_img"><?php echo $captcha['image'];?></div>
                                                            <input value="<?php echo $captcha['word'];?>" type="hidden" name="captcha_code" id="captcha_code"/>

                                                            <div class="input-group-append">
                                                                 <a  class="btn btn-default btn-sm refresh_captcha">
                                                                      <i class="fas fa-redo"></i>
                                                                 </a>
                                                            </div>
                                                       </div>

                                                       <label for="captcha" data-error="wrong" data-success="right"></label>

                                                       <input id="captcha" name="usr_captcha" type="text" required="required" autocomplete="off"
                                                              class="form-control validate md-form mt-3" placeholder="Enter the captcha text">

                                                       <!--<label for="captcha" data-error="wrong" data-success="right">Captcha</label>-->

                                                       <!--<input id="captcha" name="usr_captcha" type="text" required="required" autocomplete="off"-->
                                                       <!--       class="form-control validate" placeholder="Enter the text shown in image">-->
                                                  </div>
                                                  <p class="terms">Upon creating my account, I agree to - <a href="javascript:;">User Agreement</a> and receive emails related to membership and services</p>
                                                  <button class="btn btn-indigo btn-pad btn-rounded d-block mx-auto nxt_link_btn btnSubmitFirstStep" 
                                                          type="submit">Next</button>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="row divConfirmation" style="display: none;">
                                        <div class="col-12">
                                             <h4 class="pl-0">A confirmation mail has been sent to your inbox</h4> <h3><span class="spnEmail"></span>!!</h3>
                                             <p>You have been completed first step of registration.</p>
                                             <!-- <a class="btn btn-indigo btn-pad btn-rounded d-block mx-auto nxt_link_btn"
                                                 href="javascript:;">Go to mail</a> -->
                                        </div>
                                   </div>
                              </div>
                         </form>
                    </div>
               </div>
          </div>
     </div>
</section>

<style>
     span.error {
          font-size: 10px !important;
          color: red !important;
     }
     span#mail_exist {
          font-size: 10px;
          color: red;
     }
</style>