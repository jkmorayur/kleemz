<body>
     <div id="login-bg">
     </div>
     <div class="login-wrapper">
          <section class="l-header">
               <div class="container">
                    <div class="row">
                         <div class="col">
                              <div class="l-logo">
                                   <a href="<?php echo site_url(); ?>"><img src="images/fujeeka-logo-g.png" alt="Fujeeka"></a>
                              </div>
                         </div>
                    </div>
               </div>
          </section>

          <section class="l-welcome">
               <div class="container">
                    <div class="row">
                         <div class="col">
                              <h1>Welcome</h1>
                              <h4>The right solution for your purchase needs</h4>
                         </div>
                    </div>
               </div>
          </section>
     </div>

     <div class="l-container">
          <div class="row h-100">
               <div class="col-12 my-auto">
                    <div class="card card-block mx-auto">
                         <h2 class="f-pwd">Forgot password?</h2>
                         <div class="form-container">
                              <?php echo form_open('user/forgot-password', array('class' => 'frmForGotPassword'))?>
                              <div id="myWizard3">
                                   <section class="step" data-step-title="page">
                                        <div class="form-group">
                                             <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"> 
                                        </div>
                                   </section>
                                   <section class="step" data-step-title="page">
                                        <div class="form-group">
                                             <input type="text" class="form-control forgot_otp" id="email" placeholder="Enter otp" name="forgot_otp"> 
                                             <input type="hidden" class="form-control forgot_code" name="forgot_code"> 
                                        </div>
                                   </section>
                              </div>
                              
                              <?php $success = $this->session->flashdata('app_error');?>
                              <div class="text-danger msgForGotPassword"><?php echo $success;?></div>
                              
                              <div class="clearfix"></div>
                              <div class="mt-4">
                                   <button type="submit" class="btn btn-success btn-login btn-inline btnSubmit">Submit</button>
                                   <div class="new-u">
                                        New user? <a href="<?php echo site_url('user/signup');?>">Sign up</a>
                                   </div>
                              </div>
                              <?php echo form_close()?>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</body>

<style>
     span.error {
          font-size: 10px !important;
          color: red !important;
     }
     span#mail_exist {
          font-size: 10px;
          color: red;
     }
     .form-control:focus {
          outline: none !important;
          box-shadow:none !important;
     }
</style>