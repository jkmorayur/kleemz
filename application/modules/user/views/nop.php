<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="col-middle">
                    <div class="text-center text-center">
                         <h1 class="error-number">:(</h1>
                         <h2>User not found</h2>
                         <p>Invalid OTP, Please signup again!!</p>
                         <a href="javascript:history.back()" style="border: 1px solid #00b904;color: #fff;background: #00b904;
                                                                              border-radius: .25rem;padding: 10px;text-decoration: none;">
                                                                                Back</a>
                    </div>
               </div>
          </div>
     </div>
</div>