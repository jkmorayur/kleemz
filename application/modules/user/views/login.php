<body>
     <div id="login-bg">
     </div>
     <div class="login-wrapper">
          <section class="l-header">
               <div class="container">
                    <div class="row">
                         <div class="col">
                              <div class="l-logo">
                                   <a href="<?php echo site_url(); ?>"><img src="images/fujeeka-logo-g.png" alt="Fujeeka"></a>
                              </div>
                         </div>
                    </div>
               </div>
          </section>

          <section class="l-welcome">
               <div class="container">
                    <div class="row">
                         <div class="col">
                              <h1>Welcome</h1>
                              <h4>The right solution for your purchase needs</h4>
                              <a href="<?php echo site_url(); ?>" class="btn btn-success btn-green mt-2">Return to Homepage</a>
                         </div>
                    </div>
               </div>
          </section>
     </div>

     <div class="l-container">
          <div class="row h-100">
               <div class="col-12 my-auto">
                    <div class="card card-block mx-auto">
                         <h2>Login</h2>
                         <div class="form-container">
                              <?php echo form_open('user/login', array('class' => 'frmLogin'))?>
                              <div class="form-group">
                                <!-- <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"> -->
                                   <?php echo form_input($identity)?>
                              </div>
                              <div class="form-group">
                                <!-- <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd"> -->
                                   <?php echo form_input($password)?>
                              </div>
                              <?php if ($success = $this->session->flashdata('app_error')):?>
                                     <div class="text-danger"><?php echo $success;?></div>
                                <?php endif?>
                              <?php if ($message = $this->session->flashdata('message')):?>
                                     <div class="text-danger"><?php echo $message;?></div>
                                <?php endif?>

                              <div class="form-group">
                                   <a href="<?php echo site_url('user/forgot-password');?>">Forgot Password?</a>
                              </div>
                              <div class="clearfix"></div>
                              <div class="mt-4">
                                   <button type="submit" class="btn btn-success btn-login btn-inline">Login</button>
                                   <div class="new-u">
                                        New user? <a href="<?php echo site_url('user/signup');?>">Sign up</a>
                                   </div>
                              </div>
                              <?php echo form_close()?>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</body>