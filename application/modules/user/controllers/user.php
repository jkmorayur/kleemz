<?php

  defined('BASEPATH') or exit('No direct script access allowed');
  require './vendor/autoload.php';

  class user extends App_Controller {

       public $mail = '';

       public function __construct() {
            parent::__construct();
            $this->load->model('user_model');
            $this->load->model('market/market_model', 'market');
            $this->load->model('building/building_model', 'building');
            $this->load->model('business/business_model', 'supplier');
            $this->load->model('emp_details/emp_details_model', 'emp_details');
            $this->load->model('supplier_grade/supplier_grade_model', 'supplier_grade');
            $this->load->model('user_permission/user_permission_model', 'user_permission');
            require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
            $this->mail = new PHPMailer;
       }

       public function index() {

            if ($this->ion_auth->logged_in()) {
                 if ($this->usr_grp == 'SA' || $this->usr_grp == 'ST') {
                      redirect('dashboard');
                 } else {
                      redirect('home');
                 }
            } else {
                 redirect(strtolower(__CLASS__) . '/login');
            }
       }

       public function listuser() {
            $this->section = 'List users';
            $data['users'] = $this->ion_auth->get_all_users_with_group();
            $this->render_page(strtolower(__CLASS__) . '/index', $data);
       }

       public function login() {
            $this->session->set_userdata('is_login_page', 'yes');
            if ($this->ion_auth->logged_in()) {
                 redirect('dashboard');
            } else {
                 redirect('f_cpanel/login');
            }
            if (isset($_GET['caldp']) && !empty($_GET['caldp'])) {
                 $turl = encryptor($_GET['caldp'], 'D');
                 $this->session->set_userdata('callback', site_url($turl));
            }
            $this->template->set_layout('login');
            $this->page_title = 'Login';

            $this->current_section = 'login';

            // validate form input
            $this->form_validation->set_rules('identity', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $from = $this->input->post('from', true);
            if ($this->form_validation->run() == true) {
                 // check to see if the user is logging in
                 // check for "remember me"
                 $remember = (bool) $this->input->post('remember');
                 if ($this->user_model->getUserByIdentity($this->input->post('identity'), true)) {
                      $this->session->set_flashdata('app_error', "Admin can't logging here");
                      if ($from == "sup_home") {
                           redirect($_SERVER['HTTP_REFERER']);
                      }
                      redirect('user/login');
                 } else {
                      if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                           $this->session->set_flashdata('app_success', $this->ion_auth->messages());
                           if ($this->session->userdata('grp_slug') == 'SA' || $this->session->userdata('grp_slug') == 'ST' || $this->session->userdata('grp_slug') == 'SP' || $this->session->userdata('grp_slug') == 'SBA') {
                                redirect('dashboard');
                           } else {
                                if ($from == "sup_home") {
                                     redirect($_SERVER['HTTP_REFERER']);
                                }
                                $callback = $this->session->userdata('callback'); // Set callback url from any where, redirect after login
                                if (!empty($callback)) {
                                     $this->session->set_flashdata('from_login', true);
                                     $this->session->set_flashdata('app_success', null);
                                     redirect($callback);
                                }
                                redirect('home');
                           }
                      } else {
                           $this->session->set_flashdata('app_error', $this->ion_auth->errors());
                           if ($from == "sup_home") {
                                redirect($_SERVER['HTTP_REFERER']);
                           }
                           redirect('user/login');
                      }
                 }
            } else {
                 // the user is not logging in so display the login page
                 // set the flash data error message if there is one
                 $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                 $data['identity'] = array('name' => 'identity',
                     'id' => 'identity',
                     'type' => 'text',
                     'value' => $this->form_validation->set_value('identity'),
                     'class' => 'form-control',
                     'placeholder' => 'Username',
                 );
                 $data['password'] = array('name' => 'password',
                     'id' => 'password',
                     'type' => 'password',
                     'class' => 'form-control',
                     'placeholder' => 'Password',
                 );

                 $this->render_page('user/login', $data);
            }
       }

       public function sup_home() {
            $this->session->set_userdata(array('callback' => $_SERVER['HTTP_REFERER']));
            redirect('user/login');
       }

       public function quick_login() {

            if ($this->ion_auth->logged_in()) {
                 $return = array('status' => 'success', 'msg' => "", 'redirect' => site_url('dashboard'));
                 echo json_encode($return);
                 die();
            }
            if (!$this->input->post()) {
                 redirect($_SERVER['HTTP_REFERER']);
            }
            $identity = $this->input->post('identity', true);
            $password = $this->input->post('password', true);
            $redirect_url = $this->input->post('redirect_url', true);
            $redirect_to = $this->input->post('redirect_to', true);
            if (!$identity || !$password) {
                 $return = array('status' => 'failed', 'msg' => "Partial content");
                 echo json_encode($return);
                 die();
            }
            if ($this->user_model->getUserByIdentity($identity, true)) {
                 $return = array('status' => 'failed', 'msg' => "Admin can't logging here");
                 echo json_encode($return);
                 die();
            }

            if ($this->ion_auth->login($identity, $password)) {
                 $this->session->set_flashdata('app_success', $this->ion_auth->messages());

                 if ($redirect_url) {
                      if ($redirect_url == "redirect_sup_home") {
                           $return = array('status' => 'success', 'msg' => "");
                           echo json_encode($return);
                           die();
                      }
                      if ($redirect_url == "redirect_rfq") {
                           if ($this->session->userdata('grp_slug') == 'SP') {
                                $re_url = site_url('product/rfq_list');
                           } else if ($this->session->userdata('grp_slug') == 'BY') {
                                $re_url = site_url('product/rfq');
                           } else {
                                $re_url = site_url('dashboard');
                           }
                           $return = array('status' => 'success', 'msg' => "", 'redirect' => $re_url);
                           echo json_encode($return);
                           die();
                      }
                      if ($redirect_url == "redirect_inquiry") {
                           if ($this->session->userdata('grp_slug') == 'SP') {
                                $re_url = site_url('order');
                           } else if ($this->session->userdata('grp_slug') == 'BY') {
                                $re_url = $redirect_to;
                           } else {
                                $re_url = site_url('dashboard');
                           }
                           $return = array('status' => 'success', 'msg' => "", 'redirect' => $re_url);
                           echo json_encode($return);
                           die();
                      }
                      if ($redirect_url == "redirect_chat") {
                           $url_parts = explode('/', $redirect_to);
                           // if(end($url_parts) == encryptor($this->session->userdata('usr_user_id'))){
                           //     $re_url = '';
                           // }
                           if ($this->session->userdata('grp_slug') != 'BY') {
                                $this->session->set_flashdata('app_success', 'Only buyer can chat here');
                                $re_url = '';
                           } else {
                                $re_url = 'trigger_#chat';
                           }
                           $return = array('status' => 'success', 'msg' => "", 'redirect' => $re_url);
                           echo json_encode($return);
                           die();
                      }
                 }

                 if ($this->session->userdata('grp_slug') == 'SA' || $this->session->userdata('grp_slug') == 'ST' || $this->session->userdata('grp_slug') == 'SP' || $this->session->userdata('grp_slug') == 'SBA') {
                      $return = array('status' => 'success', 'msg' => "", 'redirect' => site_url('dashboard'));
                      echo json_encode($return);
                      die();
                 } else {
                      $return = array('status' => 'success', 'msg' => "", 'redirect' => '');
                      echo json_encode($return);
                      die();
                 }
            } else {
                 $return = array('status' => 'failed', 'msg' => $this->ion_auth->errors());
                 echo json_encode($return);
                 die();
            }
       }

       public function logout($redirect = 'home', $mg = 'Logout successfully') {
            // log the user out
            $this->ion_auth->logout($mg);
            // redirect them back to the login page
            redirect('f_cpanel/login');
            redirect($redirect);
       }

       public function forgot_password() {
            $this->template->set_layout('login');
            $this->form_validation->set_rules('email', 'Email Address', 'required');
            if ($this->form_validation->run() == false) {
                 //setup the input
                 $this->data['email'] = array('name' => 'email',
                     'id' => 'email',
                 );
                 //set any errors and display the form
                 $valMessage = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                 $this->session->set_flashdata('app_error', $valMessage);
                 $this->render_page('user/forgot_password', $this->data);
            } else {

                 if ((isset($_POST['forgot_code']) && !empty($_POST['forgot_code'])) &&
                         (isset($_POST['forgot_otp']) && !empty($_POST['forgot_otp']))) {
                      $code = $_POST['forgot_code'];
                      $otp = trim($_POST['forgot_otp']);
                      $user = $this->ion_auth->forgotten_password_check($code);

                      if (isset($user->usr_forgotten_password_otp) && ($user->usr_forgotten_password_otp == $otp)) {
                           $url = site_url("user/reset-password/" . $code);
                           die(json_encode(array('status' => 'success', 'msg' => '',
                               'redirectURL' => $url, 'st' => 'suc')));
                      } else {
                           die(json_encode(array('status' => 'success', 'st' => 'fail', 'msg' => 'Invalid OTP', 'frgtPassCode' => $code)));
                      }
                 } else {
                      //run the forgotten password method to email an activation code to the user
                      $email = $this->input->post('email');
                      $otp = rand(1000, 9999);
                      $forgotten = $this->ion_auth->forgotten_password($email, $otp);
                      if ($forgotten) { //if there were no errors
                           $user = $this->user_model->getUserByIdentity($email);
                           $fname = isset($user['usr_first_name']) ? $user['usr_first_name'] : '';
                           $lname = isset($user['usr_last_name']) ? $user['usr_last_name'] : '';
                           $code = isset($forgotten['forgotten_password_code']) ? $forgotten['forgotten_password_code'] : '';
                           /* Reset url sent to mail */
                           $this->mail->isSMTP();
                           $this->mail->Host = MAIL_HOST;
                           $this->mail->SMTPAuth = true;
                           $this->mail->Username = MAIL_USERNAME;
                           $this->mail->Password = MAIL_PASSWORD;
                           $this->mail->SMTPSecure = 'ssl';
                           $this->mail->Port = 465;
                           $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                           $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                           $this->mail->addAddress($email);
                           $this->mail->Subject = 'Reset your fujeeka password';
                           $this->mail->isHTML(true);
                           $mailData['name'] = $fname . ' ' . $lname;
                           $mailData['email'] = $email;
                           $mailData['url'] = site_url("user/reset-password/" . $code);
                           $mailData['otp'] = $otp;
                           $this->mail->Body = $this->load->view('forget-mail-template', $mailData, true);
                           $res = $this->mail->send();
                           //   debug($this->mail->ErrorInfo);
                           /* Reset url sent to mail */

                           echo json_encode(array('status' => 'success', 'msg' => 'OTP to reset your password has been sent to your email',
                               'frgtPassCode' => $forgotten['forgotten_password_code']));
                           die();
                      } else {
                           echo json_encode(array('status' => 'fail', 'msg' => 'Email not exist.'));
                           die();
                      }
                 }
            }
       }

       public function account() {
            $this->body_class[] = 'my_account';
            $this->page_title = 'My Account';
            $this->current_section = 'my_account';
            $user = $this->ion_auth->user()->row_array();
            $this->render_page('user/account', array('user' => $user));
       }

       public function change_password() {

            $this->section = 'Change Password';
            $this->body_class[] = 'skin-blue';
            $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
            $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

            if (!$this->ion_auth->logged_in()) {
                 redirect('user/login', 'refresh');
            }

            $user = $this->ion_auth->user()->row();

            if ($this->form_validation->run() == false) {
                 //set the flash data error message if there is one
                 $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                 $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                 $uname = isset($this->ion_auth->user()->row()->username) ? $this->ion_auth->user()->row()->username : '';
                 $this->data['old_password'] = array(
                     'name' => 'old',
                     'id' => 'old',
                     'type' => 'password',
                     'class' => 'form-control col-md-7 col-xs-12',
                 );
                 $this->data['new_password'] = array(
                     'name' => 'new',
                     'id' => 'new',
                     'type' => 'password',
                     'class' => 'form-control col-md-7 col-xs-12',
                 );
                 $this->data['new_password_confirm'] = array(
                     'name' => 'new_confirm',
                     'id' => 'new_confirm',
                     'type' => 'password',
                     'class' => 'form-control col-md-7 col-xs-12',
                 );
                 $this->data['user_id'] = array(
                     'name' => 'user_id',
                     'id' => 'user_id',
                     'type' => 'hidden',
                     'value' => $this->uid,
                 );

                 //render
                 $this->render_page('user/change_password', $this->data);
                 //$this->render_page('user/login', $data);
            } else {

                 $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

                 $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'), $this->input->post('uname'));

                 if ($change) {
                      //if the password was successfully changed
                      $this->logout('', 'Password changed successfully');
                 } else {
                      $this->session->set_flashdata('app_error', $this->ion_auth->errors());
                      redirect('user/change_password', 'refresh');
                 }
            }
       }

       public function add() {
            $this->section = 'New User';
            $this->render_page('user/add');
       }

       public function signupFirstStep() {

            if ((isset($_POST['usr_email']) && !empty($_POST['usr_email'])) &&
                    ((isset($_POST['captcha_code']) && isset($_POST['usr_captcha'])) && $_POST['captcha_code'] == $_POST['usr_captcha'])) {
                 $tomail = $_POST['usr_email'];
                 $otp = rand(1000, 9999);
                 $id = $this->ion_auth->register('', '', $tomail, array('usr_otp' => $otp), array(4));
                 $user['usr_email'] = $tomail;
                 $user['usr_id'] = $id;
                 $user['usr_active'] = 1;
                 die(json_encode(array('status' => 'success', 'msg' => "Security code miss match or email already exists!",
                     'redirect' => site_url('user/personal-information/' . encryptor($id)))));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Security code miss match or email already exists!")));
            }
       }

       public function personal_information($id) {
            $id = encryptor($id, 'D');
            $user = $this->ion_auth->user($id)->row_array();

            $data['country'] = get_country_list();
            $data['user'] = $user;
            $this->template->set_layout('signup');
            $this->render_page(strtolower(__CLASS__) . '/personal_information', $data);
       }

       public function insert() {
            if (isset($_FILES['avatar']['name']) && !empty($_FILES['avatar']['name'])) {
                 $this->load->library('upload');
                 $newFileName = rand(9999999, 0) . $_FILES['avatar']['name'];
                 $config['upload_path'] = '../assets/uploads/avatar/';
                 $config['allowed_types'] = 'gif|jpg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);

                 $angle['x1']['0'] = $_POST['x1'];
                 $angle['x2']['0'] = $_POST['x2'];
                 $angle['y1']['0'] = $_POST['y1'];
                 $angle['y2']['0'] = $_POST['y2'];
                 $angle['w']['0'] = $_POST['w'];
                 $angle['h']['0'] = $_POST['h'];

                 if ($this->upload->do_upload('avatar')) {
                      $data = $this->upload->data();
                      crop($this->upload->data(), $angle);
                      $_POST['user']['avatar'] = $data['file_name'];
                 }
            }
            $username = $_POST['user']['username'];
            $password = trim($_POST['user']['password']);
            $email = $_POST['user']['email'];

            unset($_POST['user']['password_confirm']);
            unset($_POST['user']['username']);
            unset($_POST['user']['password']);
            unset($_POST['user']['email']);

            $this->ion_auth->register($username, $password, $email, $_POST['user']);
            $this->session->set_flashdata('app_success', 'User successfully created!');
            redirect(strtolower(__CLASS__) . '/listuser');
       }

       public function activate($id) {
            // $otp = $this->input->post('otp', true);
            $id = encryptor($id, 'D');
            // $user = $this->ion_auth->userWithIdOtp($id, $otp);
            $user = $this->ion_auth->user($id)->row_array();
            if (!empty($user)) {
                 $this->session->set_userdata('usr_is_mail_verified', 1);
                 $this->ion_auth->update($user['usr_id'], array('usr_is_mail_verified' => 1));
                 generate_log(array(
                     'log_title' => 'Email verified',
                     'log_desc' => 'The user verified through email',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $id,
                     'log_added_by' => $id,
                 ));
                 $data['country'] = get_country_list();
                 $data['user'] = $user;
                 $this->template->set_layout('signup');
                 $this->render_page(strtolower(__CLASS__) . '/activate', $data);
            } else {
                 //  $this->ion_auth->delete_user($id);
                 //  $this->template->set_layout('signup');
                 $this->render_page(strtolower(__CLASS__) . '/nop');
            }
       }

       public function update() {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('usr_id', 'Who', 'required');
            $this->form_validation->set_rules('usr_password', 'Password', 'required');
            $this->form_validation->set_rules('usr_cpassword', 'Password Confirmation', 'required|matches[usr_password]');
            $this->form_validation->set_rules('usr_country', 'Country', 'required');
            $this->form_validation->set_rules('usr_state', 'State', 'required');
            $this->form_validation->set_rules('usr_first_name', 'First name', 'required');

            if ($this->form_validation->run() == false) {
                 echo json_encode(array('status' => 'fail', 'msg' => validation_errors()));
                 die();
            } else {
                 $_POST['usr_id'] = encryptor($_POST['usr_id'], 'D');
                 if (isset($_FILES['avatar']['name']) && !empty($_FILES['avatar']['name'])) {

                      $this->load->library('upload');
                      $newFileName = rand(9999999, 0) . $_FILES['avatar']['name'];
                      $config['upload_path'] = '../assets/uploads/avatar/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $angle['x1']['0'] = $_POST['x1'];
                      $angle['x2']['0'] = $_POST['x2'];
                      $angle['y1']['0'] = $_POST['y1'];
                      $angle['y2']['0'] = $_POST['y2'];
                      $angle['w']['0'] = $_POST['w'];
                      $angle['h']['0'] = $_POST['h'];

                      if ($this->upload->do_upload('avatar')) {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle);
                           $_POST['avatar'] = $data['file_name'];
                      }
                 }
                 $userId = $_POST['usr_id'];
                 $_POST['usr_phone'] = $_POST['ccode'] . ' ' . $_POST['usr_phone'];
                 $userid = $_POST['usr_id'];

                 unset($_POST['usr_id']);
                 unset($_POST['usr_cpassword']);
                 unset($_POST['ccode']);
                 $_POST['usr_active'] = 1;
                 $this->ion_auth->update($userId, $_POST);
                 if (!$this->session->userdata('user_id')) {
                      $this->ion_auth->login($_POST['reg_email'], $_POST['usr_password']);
                 }
                 generate_log(array(
                     'log_title' => 'User active',
                     'log_desc' => 'The user activated the account',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $userid,
                     'log_added_by' => $userid,
                 ));
                 $tomail = isset($_POST['reg_email']) ? $_POST['reg_email'] : '';
                 if (!empty($tomail) && (isset($_GET['sendVerification']))) {
                      /* Add default permission */
                      $permission['cua_group_id'] = $userid;
                      $permission['cua_access']['product'] = array('rfq', 'rfq_list', 'pending_rfq_list', 'new_rfq');
                      $permission['cua_access']['order'] = array('index', 'order_summery');
                      $this->user_permission->addUserPermission($permission);

                      /* Sent activation email */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress($tomail);
                      $this->mail->Subject = 'Hi ' . $tomail . ', please verify your email address';
                      $this->mail->Subject = 'Hi email verification';
                      $this->mail->isHTML(true);
                      $mailData['mail'] = $tomail;
                      $mailData['verifyurl'] = site_url("user/activate/" . encryptor($userid));
                      $mailData['usr_id'] = encryptor($userid);
                      $mailContent = $this->load->view('register-mail-template', $mailData, true);
                      $this->mail->Body = $mailContent;
                      //$this->mail->send();
                 }
                 /* Sent activation email */
                 echo json_encode(array('status' => 'success', 'msg' => ""));
                 die();
            }
       }

       public function checkEmailExists() {

            $email = isset($_POST['email']) ? $_POST['email'] : '';
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $status = $this->ion_auth->email_check($email, $id) ? 'false' : 'true';
            die(json_encode(array('status' => $status, 'msg' => 'Email Already Registered <a href="' .
                site_url('user/forgot-password') . '">Forgot </a> or <a href="' . site_url('user/login') . '">Login</a>')));
       }

       public function checkUsernameExists() {
            $username = isset($_POST['user']['username']) ? $_POST['user']['username'] : '';
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            echo ($this->ion_auth->username_check($username, $id)) ? 'false' : 'true';
            die();
       }

       public function delete($id) {
            if ($this->ion_auth->delete_user($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'User successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete user"));
                 die();
            }
       }

       public function view_user($id) {
            $this->section = "Edit user";
            $this->data['userDetails'] = $this->ion_auth->user($id)->row_array();
            $this->render_page(strtolower(__CLASS__) . '/edit', $this->data);
       }

       public function removeImage($id) {
            if ($this->ion_auth->removeAvatar($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Avatar successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete brand avatar"));
                 die();
            }
       }

       public function signup() {

            $this->template->set_layout('signup');

            $data['captcha'] = getCiCaptcha();

            if ($this->input->post()) {
                 $tomail = $_POST['usr_email'];
                 $otp = rand(1000, 9999);
                 $id = $this->ion_auth->register('', '', $tomail, array('usr_otp' => $otp), array(4));
                 if ($id) {
                      redirect(strtolower(__CLASS__) . '/activate/' . encryptor($id));
                 }

                 //  if ($this->user_model->register($_POST)) {
                 //       $this->session->set_flashdata('app_success', 'Successfully Registered!');
                 //       redirect(strtolower(__CLASS__));
                 else {
                      $this->session->set_flashdata('app_error', 'Failed to register');
                      $this->render_page(strtolower(__CLASS__) . '/signup', $data);
                 }
            } else {
                 $this->render_page(strtolower(__CLASS__) . '/signup', $data);
            }
       }

       public function insertTestValues() {
            // $this->common_model->insertSupplier(100);
            // $this->common_model->insertProducts(100);
       }

       public function checkIsAdmin() {
            if (isset($_POST['usr_email']) && !empty($_POST['usr_email'])) {
                 $tomail = $_POST['usr_email'];

                 $id = $this->ion_auth->register('', '', $tomail, array(), array(4));

                 /* Sent activation email */
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($tomail);
                 $this->mail->Subject = 'Hi ' . $tomail . ', please verify your fujeeka account';
                 $this->mail->isHTML(true);
                 $mailContent = "Hi, <br><br> Thanks for using fujeeka! Please confirm your email address by clicking on the link below. "
                         . "We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file."
                         . "<br> " . site_url() . "user/activate/" . encryptor($id);
                 $this->mail->Body = $mailContent;
                 $this->mail->send();
                 /* Sent activation email */
                 echo json_encode(array('status' => 'success', 'msg' => site_url() . "user/activate/" . encryptor($id)));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Security code miss match or email already exists!"));
                 die();
            }
       }

       public function _get_csrf_nonce() {
            $this->load->helper('string');
            $key = random_string('alnum', 8);
            $value = random_string('alnum', 20);
            $this->session->set_userdata(array('csrfkey' => $key, 'csrfvalue' => $value));
            return array($key => $value);
       }

       //reset password - final step for forgotten password
       public function reset_password($code = null) {
            if (!$code) {
                 show_404();
            }
            $this->template->set_layout('login');
            $user = $this->ion_auth->forgotten_password_check($code);

            if ($user) {
                 //if the code is valid then display the password reset form

                 $this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
                 $this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

                 if ($this->form_validation->run() == false) {
                      //display the form
                      //set the flash data error message if there is one
                      $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                      $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                      $this->data['new_password'] = array(
                          'name' => 'new',
                          'id' => 'new',
                          'class' => 'form-control',
                          'type' => 'password',
                          'placeholder' => 'New password',
                          'required' => 'true',
                          'minlength' => 8,
                      );
                      $this->data['new_password_confirm'] = array(
                          'name' => 'new_confirm',
                          'id' => 'new_confirm',
                          'class' => 'form-control',
                          'type' => 'password',
                          'placeholder' => 'Confirm new password',
                          'required' => 'true',
                          'minlength' => 8,
                          'equalTo' => 'new',
                          'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                      );
                      $this->data['user_id'] = array(
                          'name' => 'user_id',
                          'id' => 'user_id',
                          'type' => 'hidden',
                          'value' => $user->id,
                      );
                      $this->data['csrf'] = $this->_get_csrf_nonce();
                      $this->data['code'] = $code;

                      //render
                      $this->render_page('reset_password', $this->data);
                 } else {
                      // do we have a valid request?
                      if ($this->_valid_csrf_nonce() === false || $user->id != $this->input->post('user_id')) {

                           //something fishy might be up
                           $this->ion_auth->clear_forgotten_password_code($code);

                           show_error('This form post did not pass our security checks.');
                      } else {
                           // finally change the password
                           $identity = $user->{$this->config->item('identity', 'ion_auth')};

                           $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                           if ($change) {
                                //if the password was successfully changed
                                $this->ion_auth->logout();
                                $this->session->set_flashdata('message', $this->ion_auth->messages());
                                redirect('user/login');
                           } else {
                                $this->session->set_flashdata('message', $this->ion_auth->errors());
                                redirect('user/reset-password/' . $code);
                           }
                      }
                 }
            } else {
                 //if the code is invalid then send them back to the forgot password page
                 $this->session->set_flashdata('message', $this->ion_auth->errors());
                 redirect("user/forgot-password");
            }
       }

       public function _valid_csrf_nonce() {
            $csrfkey = $this->input->post($this->session->userdata('csrfkey'));
            if ($csrfkey && $csrfkey === $this->session->userdata('csrfvalue')) {
                 return true;
            }
            return false;
       }

       public function refreshCaptcha() {
            $cap = getCiCaptcha();
            $output = array(
                'data' => $cap,
            );
            echo json_encode($output);
            die();
       }

       public function changeUserPermissions($new_group) {

            if ($crnt_group == $this->usr_grp) {
                 redirect($_SERVER['HTTP_REFERER']);
            } else {
                 if ($new_group == 'BY') {
                      $this->session->set_userdata('grp_slug', $new_group);
                 }
                 if ($new_group == 'SP') {
                      $this->session->set_userdata('grp_slug', $new_group);
                 }
                 redirect("dashboard");
            }
       }

       public function chat($sup_id = '') {
            if (!$this->session->userdata('email_not_verified_callback')) {
                 $this->session->set_userdata(array('email_not_verified_callback' => $_SERVER['HTTP_REFERER']));
            }
            if (!$this->ion_auth->logged_in()) {
                 $this->session->set_userdata(array('callback' => $_SERVER['HTTP_REFERER']));
                 redirect('user/login');
            }
            if (!$this->mail_verified) {
                 $this->session->set_flashdata('app_error', 'You should verify your email first');
                 $red = $this->session->userdata('email_not_verified_callback');
                 $this->session->unset_userdata('email_not_verified_callback');
                 redirect($red);
            }
            $data['is_open'] = false;
            if ($sup_id) {
                 $sup = encryptor($sup_id, 'D');
                 if ($this->uid != $sup) {
                      $insert = array(
                          'c_from_id' => $this->uid,
                          'c_to_id' => $sup,
                          'c_content' => '',
                          'c_attachment' => '',
                      );
                      $this->common_model->saveChat($insert);
                 }
                 $data['is_open'] = true;
            }
            $this->template->set_layout('chat');
            $data['chats'] = $this->user_model->getChats();
            $this->render_page(strtolower(__CLASS__) . '/chat', $data);
       }

       public function loadChat() {
            if ($this->input->post()) {
                 $usr_id = $this->input->post('usr_id');
                 $this->user_model->loadChats(encryptor($usr_id, 'D'));
            }
       }

       //function to send pusher message
       public function pusher() {
            $file = '';
            $ext = '';
            if ($this->input->post()) {
                 if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                      $this->load->library('upload');
                      $uploadData = array();
                      $pixel = array();
                      $newFileName = uniqid(rand(1, 8888888), true) . rand(1, 9999999) . $_FILES['file']['name'];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'chats/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|doc|txt';
                      $config['file_name'] = $newFileName;
                      $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                      $this->upload->initialize($config);
                      if ($this->upload->do_upload('file')) {
                           $uploadData = $this->upload->data();
                           $file = $uploadData['file_name'];
                      } else {
                           // debug($this->upload->display_errors());
                           die('error occured');
                      }
                 }
                 $attachment = $this->input->post('attachment', true);
                 $det = $this->input->post();
                 $enc_id = encryptor($this->uid);
                 $to = encryptor($det['to_usr'], 'D');

                 $tok_det = $this->user_model->getUserById($to);
                 $insert = array(
                     'c_from_id' => $this->uid,
                     'c_to_id' => $to,
                     'c_content' => $det['content'],
                     'c_attachment' => $file,
                 );
                 $is_prd = 0;
                 if ($attachment) {
                      $is_prd = 1;
                      $insert['c_attachment'] = $attachment;
                      $insert['c_is_prd_details'] = 1;
                 }
                 $this->common_model->saveChat($insert);

                 if ($file) {
                      $file = 'uploads/chats/' . $file;
                 }
                 $chat['attachment'] = $file;
                 $chat['message'] = $det['content'];
                 $chat['from'] = $this->session->userdata('usr_username');
                 $chat['ext'] = $ext;
                 $chat['from_ch'] = $enc_id;
                 $chat['is_prd'] = $is_prd;

                 //   $tok_det['usr_pushy_token']= '6b8a304919ab8b6434fb52';
                 if ($tok_det['usr_pushy_token']) {

                      $push['type'] = 'recieved';
                      $push['msg_type'] = 'chat';
                      $push['from_id'] = $this->uid == 1 ? 'admin' : $enc_id;
                      $attach = 0;
                      if ($file) {
                           $attach = 1;
                      }
                      $push['content'] = $chat['message'];
                      $push['attachment_link'] = '';
                      $push['attachment_preview'] = '';
                      $push['is_prd_details'] = $is_prd;
                      if ($is_prd) {
                           $push['attachment_link'] = site_url() . 'assets/' . $attachment;
                           $push['attachment_preview'] = site_url() . 'assets/' . $attachment;
                      } else if ($attach) {
                           $push['is_prd_details'] = 3;
                           $push['attachment_link'] = site_url() . 'assets/' . $file;
                           $push['attachment_preview'] = site_url() . 'assets/images/attachment.jpg';
                           if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                                $push['attachment_preview'] = site_url() . 'assets/' . $file;
                                $push['is_prd_details'] = 2;
                           }
                      }
                      $push['from_name'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_fullname');
                      $push['time'] = date("Y-m-d H:i:s");
                      $push['is_attachment'] = $attach;


                      if ($this->common_model->checkIsBuyer($to)) {
                           $this->common_model->sendPushNotification($push, $tok_det['usr_pushy_token'], '');
                      } else {
                           $push['from_id'] = $this->uid;
                           $push['from_name'] = $this->session->userdata('usr_fullname');
                           if ($this->common_model->isFirstChat($this->uid, $to)) {

                                $profile = $this->user_model->getUserById($this->uid);

                                $push['profile_image'] = site_url() . 'assets/uploads/avatar/' . $profile['usr_avatar'];
                           }
                           $this->common_model->sendPushNotificationForSupplier($push, $tok_det['usr_pushy_token'], '');
                      }
                 }

                 if ($this->sendPusherMessage($det['to_usr'], 'chat-event', $chat)) {
                      $res = array(
                          'status' => true,
                      );
                 } else {
                      $res = array(
                          'status' => false,
                      );
                 }
                 echo json_encode($res);
                 die();
            }
       }

       public function sendPusherMessage($channel, $event, $chat) {
            $options = array(
                'cluster' => PUSHER_CLUSTER,
                'useTLS' => true,
            );
            $pusher = new Pusher\Pusher(
                    PUSHER_APP_KEY, PUSHER_APP_SECRET, PUSHER_APP_ID, $options
            );
            return $pusher->trigger($channel, $event, $chat);
       }

       public function become_seller($id = '') {
            $this->page_title = "Become a seller";
            if ($this->input->post()) {
                 $insert_id = $this->user_model->addToSellerRequest(array('sr_usr_id' => encryptor($id, 'D')));
                 if ($insert_id) {
                      if ($insert_id == 'duplicate') {
                           $this->session->set_flashdata('app_success', "Your request already submitted!!");
                           redirect(strtolower(__CLASS__) . '/become-seller/' . $id);
                      }
                      $to = get_settings_by_key('admin_email'); //$this->input->post('bussiness_mail');
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      // $this->mail->SMTPDebug = 4;
                      //remove this from server
                      //  $this->mail->SMTPOptions = array(
                      //      'ssl' => array(
                      //          'verify_peer' => false,
                      //          'verify_peer_name' => false,
                      //          'allow_self_signed' => true
                      //      )
                      //  );
                      //remove this from server
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress($to);
                      $this->mail->Subject = 'New Become Seller Request';
                      $this->mail->isHTML(true);
                      $mailContent = $mailContent = "<h3>New Become Seller Request</h3><br>"
                              . "Hi Admin, <br><br> you have a new become seller request from : " . $this->session->userdata('usr_fullname') . "<br><br>"
                              . "To view, click the link below:"
                              . "<br><br> " . site_url() . "user/seller_requests/" . encryptor($insert_id);
                      $this->mail->Body = $mailContent;
                      $this->mail->send();
                      $this->session->set_flashdata('app_success', "Your request successfuly submitted.. We will touch you soon!!");
                      redirect(strtolower(__CLASS__) . '/become-seller/' . $id);
                 }
            }
            $this->render_page(strtolower(__CLASS__) . '/become_seller');
       }

       public function seller_requests($id = '') {
            if (!$this->ion_auth->logged_in()) {
                 $this->session->set_userdata(array('callback' => site_url(strtolower(__CLASS__)) . '/seller_requests/' . $id));
                 redirect('f_cpanel/index');
            }
            $data['requests'] = $this->user_model->getSellerRequests($id);
            $this->render_page(strtolower(__CLASS__) . '/seller_requests', $data);
       }

       public function changeRequestStatus($sr_id, $usr_id) {
            $status = $this->input->post('status');
            if ($this->user_model->changeRequestStatus($sr_id, $status)) {
                 $userDetails = $this->ion_auth->user(encryptor($usr_id, 'D'))->row_array();
                 $to = $userDetails['usr_email'];
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 // $this->mail->SMTPDebug = 4;
                 $this->mail->SMTPOptions = array(
                     'ssl' => array(
                         'verify_peer' => false,
                         'verify_peer_name' => false,
                         'allow_self_signed' => true,
                     ),
                 );
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($to);
                 $this->mail->Subject = 'Your Seller Request Approved';
                 $this->mail->isHTML(true);
                 $mailData['name'] = $userDetails['usr_first_name'] . " " . $userDetails['usr_last_name'];
                 $mailData['activationUrl'] = site_url("supplier/activate-supplier/" . $usr_id);
                 $this->mail->Body = $this->load->view('become-seller-mail-template', $mailData, true);
                 $this->mail->send();
                 echo json_encode(array('status' => 'success', 'msg' => 'User Notified Successfully'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Failed to notify user"));
                 die();
            }
       }

       public function chat_head($usr_id = '', $img = '', $name = '', $price = '') {
            $this->template->set_layout('');
            $data['chats'] = $this->user_model->loadChats(encryptor($usr_id, 'D'), 'chat_head');
            $data['img'] = $img == "undefined" ? '' : encryptor($img, 'D');
            $data['name'] = $name == "undefined" ? '' : encryptor($name, 'D');
            $data['price'] = $price == "undefined" ? '' : encryptor($price, 'D');
            $data['usr_id'] = $usr_id;
            $this->render_page(strtolower(__CLASS__) . '/chat_box', $data);
       }

       public function verify_mail() {

            $mail = $this->session->userdata('usr_email');
            /* Sent activation email */
            $this->mail->isSMTP();
            $this->mail->Host = MAIL_HOST;
            $this->mail->SMTPAuth = true;
            $this->mail->Username = MAIL_USERNAME;
            $this->mail->Password = MAIL_PASSWORD;
            $this->mail->SMTPSecure = 'ssl';
            $this->mail->Port = 465;
            //remove this from server
            //    $this->mail->SMTPOptions = array(
            //        'ssl' => array(
            //            'verify_peer' => false,
            //            'verify_peer_name' => false,
            //            'allow_self_signed' => true
            //        )
            //    );
            //remove this from server
            $this->mail->setFrom(FROM_MAIL, FROM_NAME);
            $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
            $this->mail->addAddress($mail);
            $this->mail->Subject = 'Hi ' . $mail . ', please verify your fujeeka account';
            $this->mail->Subject = 'Hi Verify Your Fujeeka Account';
            $this->mail->isHTML(true);
            $mailData['mail'] = $mail;
            $mailData['verifyurl'] = site_url("user/activate/" . encryptor($this->uid));
            $mailData['usr_id'] = encryptor($this->uid);
            $mailContent = $this->load->view('register-mail-template', $mailData, true);
            $this->mail->Body = $mailContent;
            $this->mail->send();
            $return = array('status' => 'success', 'msg' => 'Email sent..Check your inbox');
            echo json_encode($return);
            die();
            /* Sent activation email */
       }

       public function change_mail() {
            $mail = $this->input->post('new_mail');
            if (!$this->user_model->checkIfValueExists($mail)) {
                 echo json_encode(array('status' => 'fail', 'msg' => 'Email not available..choose another'));
                 die();
            }
            if ($this->user_model->changeEmail($mail)) {
                 $this->session->set_userdata('usr_email', $mail);
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 //remove this from server
                 //    $this->mail->SMTPOptions = array(
                 //        'ssl' => array(
                 //            'verify_peer' => false,
                 //            'verify_peer_name' => false,
                 //            'allow_self_signed' => true
                 //        )
                 //    );
                 //remove this from server
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($mail);
                 $this->mail->Subject = 'Hi ' . $mail . ', please verify your fujeeka account';
                 $this->mail->Subject = 'Hi Verify Your Fujeeka Account';
                 $this->mail->isHTML(true);
                 $mailData['mail'] = $mail;
                 $mailData['verifyurl'] = site_url("user/activate/" . encryptor($this->uid));
                 $mailData['usr_id'] = encryptor($this->uid);
                 $mailContent = $this->load->view('register-mail-template', $mailData, true);
                 $this->mail->Body = $mailContent;
                 $this->mail->send();
                 echo json_encode(array('status' => 'success', 'msg' => 'Email updated, please verify your email'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => 'Failed to update'));
                 die();
            }
       }

       public function reset_password_modal() {
            $code = $this->input->post('code', true);
            $pswd = $this->input->post('pswd', true);
            $cpswd = $this->input->post('cpswd', true);
            if ($pswd != $cpswd) {
                 die(json_encode(array('status' => 'fail', 'msg' => 'Password mismatch')));
            }
            $user = $this->ion_auth->forgotten_password_check($code);
            if ($user) {
                 $identity = $user->{$this->config->item('identity', 'ion_auth')};
                 $change = $this->ion_auth->reset_password($identity, $cpswd);
                 if ($change) {
                      die(json_encode(array('status' => 'success', 'msg' => 'Password updated')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => $this->ion_auth->errors())));
                 }
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => 'Something went wrong. try again')));
            }
       }

  }
  