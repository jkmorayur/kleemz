<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Town admin</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/update');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="<?php echo $data['usr_id'];?>" type="hidden" name="usr_id" id="usr_id"/>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $data['usr_first_name'];?>" type="text" id="first-name" required="required" 
                                               class="form-control col-md-7 col-xs-12" data-parsley-required-message="First Name required" 
                                               name="user[usr_first_name]">
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $data['usr_last_name'];?>" type="text" id="last-name" name="user[usr_last_name]" 
                                               required="required" data-parsley-required-message="Last Name required" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>
                              <?php
                                if (isset($town) && !empty($town)) {
                                     foreach ($town as $keyT => $valueT) {
                                          ?>
                                          <div class="form-group">
                                               <label class="control-label col-md-3 col-sm-3 col-xs-12">Market/Town<span class="required">*</span></label>
                                               <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select required data-parsley-required-message="Select market" name="market[]" id="bld_type" class="form-control">
                                                         <option value="">Market/Town</option>
                                                         <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                                              <option value="<?php echo $value['mar_id'];?>" <?php echo ($valueT['tta_town_id'] == $value['mar_id']) ? 'selected="selected"' : '';?>><?php echo $value['mar_name'];?></option>
                                                         <?php }?>
                                                    </select>
                                               </div>
                                               <div class="col-md-1 col-sm-1 col-xs-12">
                                                    <span style="cursor: pointer;" class="glyphicon glyphicon-minus btnAddRemoveKeys"></span>
                                               </div>
                                          </div>
                                          <?php
                                     }
                                }
                              ?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Market/Town<span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select data-parsley-required-message="Select market" name="market[]" id="bld_type" class="form-control">
                                             <option value="">Market/Town</option>
                                             <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                                    <option value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                                               <?php }?>
                                        </select>
                                   </div>
                                   <div class="col-md-1 col-sm-1 col-xs-12">
                                        <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMoreTown"></span>
                                   </div>
                              </div>
                              <div class="divTown"></div>
                              <?php
                                if (isset($phone) && !empty($phone)) {
                                     foreach ($phone as $key => $value) {
                                          ?>
                                          <div class="form-group">
                                               <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile<span class="required">*</span>
                                               </label>
                                               <div class="col-md-6 col-sm-3 col-xs-12">
                                                    <input value="<?php echo $value['upn_phone_number'];?>" type="text" id="mobile" name="upn_phone_number[]" required="required" data-parsley-required-message="Mobile required" maxlength="15" data-past=".usr_whatsapp" 
                                                           class="pastContent numOnly form-control col-md-7 col-xs-12">
                                               </div>
                                               <div class="col-md-1 col-sm-1 col-xs-12">
                                                    <span style="cursor: pointer;" class="glyphicon glyphicon-minus btnAddRemoveKeys"></span>
                                               </div>
                                          </div> 
                                          <?php
                                     }
                                }
                              ?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-3 col-xs-12">
                                        <input type="text" id="mobile" name="upn_phone_number[]" maxlength="15" data-past=".usr_whatsapp" 
                                               class="pastContent numOnly form-control col-md-7 col-xs-12">
                                   </div>
                                   <div class="col-md-1 col-sm-1 col-xs-12">
                                        <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMorePhone"></span>
                                   </div>
                              </div>

                              <div class="divMobilePhone"></div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $data['usr_email'];?>" autocomplete="off" type="email" id="email" name="user[usr_email]" required="required" 
                                               data-parsley-trigger="change" data-parsley-required-message="Email required" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_password">Password<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="usr_password" name="user[usr_password]" minlength="6" data-parsley-minlength-message="Password should be more than 5 character" class="form-control col-md-7 col-xs-12 usr_password">
                                   </div>
                                   <span toggle="#usr_password" class="fa fa-eye field-icon toggle-password"></span>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_password_conf">Re enter Password<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="usr_password_conf" name="user[usr_password_conf]"  data-parsley-equalto="#usr_password" data-parsley-trigger="change" data-parsley-equalto-message="Should be same as password"  data-parsley-required-message="Confirm your password" class="form-control col-md-7 col-xs-12 usr_password_conf">
                                   </div>
                                   <span toggle="#usr_password_conf" class="fa fa-eye field-icon toggle-password"></span>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_address">Address</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $data['usr_address'];?>" type="text" id="usr_address" name="user[usr_address]" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>
                              <?php if (!empty($data['usr_avatar'])) {?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city">Avatar</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <?php
                                               echo img(array('src' => 'assets/uploads/avatar/' . $data['usr_avatar'], 'width' => '100', 'id' => 'imgBrandImage'));
                                               ?>
                                          </div>
                                     </div>
                                <?php }?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city">New avatar</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="usr_avatar" data-parsley-fileextension="jpg,png,gif,jpeg" class="autoComCity form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
<script type="text/template" class="tmpMobile">
     <div class="form-group">
     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile<span class="required">*</span>
     </label>
     <div class="col-md-6 col-sm-3 col-xs-12">
     <input type="text" id="mobile" name="upn_phone_number[]" required="required" data-parsley-required-message="Mobile required" maxlength="15" data-past=".usr_whatsapp" 
     class="pastContent numOnly form-control col-md-7 col-xs-12">
     </div>
     <div class="col-md-1 col-sm-1 col-xs-12">
     <span style="cursor: pointer;" class="glyphicon glyphicon-minus btnAddRemoveKeys"></span>
     </div>
     </div>
</script>

<script type="text/template" class="tmpTown">
     <div class="form-group">
     <label class="control-label col-md-3 col-sm-3 col-xs-12">Market/Town<span class="required">*</span></label>
     <div class="col-md-6 col-sm-6 col-xs-12">
     <select required data-parsley-required-message="Select market" name="market[]" id="bld_type" class="form-control">
     <option value="">Market/Town</option>
     <?php foreach ((array) $marketPlaces as $key => $value) {?>
            <option value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
       <?php }?>
     </select>
     </div>
     <div class="col-md-1 col-sm-1 col-xs-12">
     <span style="cursor: pointer;" class="glyphicon glyphicon-minus btnAddRemoveKeys"></span>
     </div>
     </div>
</script>