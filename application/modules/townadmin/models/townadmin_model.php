<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class townadmin_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getUsers($id = '') {
            if ($id) {
                 if ($this->usr_grp == 'BY') {
                      $this->db->where(tbl_users . '.usr_id', $this->uid);
                 }
                 $this->db->where(tbl_users . '.usr_id', $id);
                 $return['data'] = $this->db->select(tbl_users . '.*, ' .
                                         tbl_users_groups . '.group_id as group_id, ' .
                                         tbl_groups . '.name as group_name, ' .
                                         tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                         tbl_market_places . '.*')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_market', 'LEFT')
                                 ->where(tbl_groups . '.id', 6)->get(tbl_users)->row_array();
                 $return['phone'] = $this->db->get_where(tbl_users_phone_number, array('upn_user_id' => $id))->result_array();
                 $return['town'] = $this->db->get_where(tbl_townadmin_town_assoc, array('tta_user_id' => $id))->result_array();
                 return $return;
            } else {
                 if ($this->usr_grp == 'BY') {
                      $this->db->where(tbl_users . '.usr_id', $this->uid);
                 }
                 $this->db->where(tbl_users . '.usr_active !=', 0);
                 $this->db->where(tbl_users . '.usr_id !=', 1);
                 $this->db->where('(' . tbl_users . '.usr_supplier IS null')->or_where(tbl_users . '.usr_supplier = 0)');
                 return $this->db->select(tbl_users . '.*, ' .
                                         tbl_users_groups . '.group_id as group_id, ' .
                                         tbl_groups . '.name as group_name, ' .
                                         tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                         tbl_market_places . '.*')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_market', 'LEFT')
                                 ->where(tbl_groups . '.id', 6)->get(tbl_users)->result_array();
            }
       }

       function getTownByUser($id) {
            return $this->db->select('GROUP_CONCAT(' . tbl_market_places . '.mar_name) AS mar_name,' . tbl_townadmin_town_assoc . '.tta_town_id')
                            ->join(tbl_townadmin_town_assoc, tbl_market_places . '.mar_id = ' . tbl_townadmin_town_assoc . '.tta_town_id')
                            ->where(array('tta_user_id' => $id))->get(tbl_market_places)->row()->mar_name;
       }

       function register($data) {
            if ($data) {
                 $username = isset($data['user']['usr_first_name']) ? $data['user']['usr_first_name'] : '';
                 $password = isset($data['user']['usr_password']) ? $data['user']['usr_password'] : '';
                 $email = isset($data['user']['usr_email']) ? $data['user']['usr_email'] : '';
                 $data['user']['usr_phone'] = isset($data['upn_phone_number'][0]) ? $data['upn_phone_number'][0] : '';
                 $group = array(6); // Sets user to admin.

                 unset($data['user']['usr_password']);
                 unset($data['user']['usr_password_conf']);

                 $data['user']['usr_added_by'] = $this->uid;
                 $lastInsertId = $this->ion_auth->register($username, $password, $email, $data['user'], $group);
                 // Save phone numbers
                 if ($lastInsertId) {
                      if (!empty($data['upn_phone_number'])) {
                           foreach ($data['upn_phone_number'] as $key => $value) {
                                if (!empty($value)) {
                                     $this->db->insert(tbl_users_phone_number, array('upn_user_id' => $lastInsertId, 'upn_phone_number' => $value));
                                }
                           }
                      }
                      if (!empty($data['market'])) {
                           foreach ($data['market'] as $key => $value) {
                                if ($value > 0) {
                                     $this->db->insert(tbl_townadmin_town_assoc, array('tta_user_id' => $lastInsertId, 'tta_town_id' => $value));
                                }
                           }
                      }
                 }
                 generate_log(array(
                     'log_title' => 'Create new town admin',
                     'log_desc' => serialize($data),
                     'log_controller' => 'town-admin',
                     'log_action' => 'C',
                     'log_ref_id' => $lastInsertId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Create new user',
                     'log_desc' => 'Failed to create new user',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid
                 ));

                 return false;
            }
       }

       function update($data) {

            $id = isset($data['usr_id']) ? $data['usr_id'] : '';
            unset($data['usr_id']);
            $phones = $data['upn_phone_number'] ? $data['upn_phone_number'] : array();
            $markets = $data['market'] ? $data['market'] : array();
            $data = $data['user'];

            if (isset($data['usr_password']) && empty($data['usr_password'])) {
                 unset($data['usr_password']);
            }
            unset($data['usr_password_conf']);

            $data['usr_active'] = 1;

            $data['usr_updated_by'] = $this->uid;
            $this->ion_auth->update($id, $data);

            if ($id) {
                 $this->db->delete(tbl_users_phone_number, array('upn_user_id' => $id));
                 foreach ($phones as $key => $value) {
                      if (!empty($value)) {
                           $this->db->insert(tbl_users_phone_number, array('upn_user_id' => $id, 'upn_phone_number' => $value));
                      }
                 }

                 if (!empty($markets)) {
                      $this->db->delete(tbl_townadmin_town_assoc, array('tta_user_id' => $id));
                      foreach ($markets as $key => $value) {
                           if ($value > 0) {
                                $this->db->insert(tbl_townadmin_town_assoc, array('tta_user_id' => $id, 'tta_town_id' => $value));
                           }
                      }
                 }
            }

            generate_log(array(
                'log_title' => 'Update town admin',
                'log_desc' => serialize($data),
                'log_controller' => 'town-admin',
                'log_action' => 'U',
                'log_ref_id' => $id,
                'log_added_by' => $this->uid
            ));

            return true;
       }

  }
  