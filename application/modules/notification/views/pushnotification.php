<style>
     .SumoSelect {width: 100% !important;}
</style>
<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Send Push Notification</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/pushnotification", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Title" type="text" class="form-control col-md-7 col-xs-12" 
                                          name="pushnify[pnm_title]" id="cat_title" placeholder="Title"/>
                              </div>
                         </div>
                         <?php if ($this->usr_grp == 'SA') {?>
                                <div class="form-group">
                                     <label class="control-label col-md-3 col-sm-3 col-xs-12">Receiver</label>
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                          <select placeholder="Receiver" class="search-box SumoUnder" tabindex="-1" name="pushnify[pnm_user_group]">
                                               <option disabled value="0">Both</option>
                                               <option disabled value="2">Supplier</option> 
                                               <option value="4" selected>Buyer</option> 
                                          </select>
                                     </div>
                                </div>
                           <?php }?>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div id="newupload">
                                        <input type="hidden" id="x10" name="x1[]" />
                                        <input type="hidden" id="y10" name="y1[]" />
                                        <input type="hidden" id="x20" name="x2[]" />
                                        <input type="hidden" id="y20" name="y2[]" />
                                        <input type="hidden" id="w0" name="w[]" />
                                        <input type="hidden" id="h0" name="h[]" />
                                        <input data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="image" id="image_file0" onchange="fileSelectHandler('0', '816', '720', '1')" />
                                        <img id="preview0" class="preview"/>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Content</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="pushnify[pnm_message]" class='editor'></textarea>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
</style>