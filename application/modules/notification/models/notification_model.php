<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class notification_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
            $this->tbl_pushnotification_master = TABLE_PREFIX . 'pushnotification_master';
            $this->tbl_pushnotification_receiver = TABLE_PREFIX . 'pushnotification_receiver';
       }

       function getReceivers() {
            if ($this->usr_grp == 'SP') {
                 $favUsers = $this->db->distinct()->select('GROUP_CONCAT(fav_added_by) AS fav_added_by')
                                 ->get_where($this->tbl_favorite_list, array('fav_consign' => 'SUP', 'fav_consign_id' => $this->suplr))
                                 ->row()->fav_added_by;

                 $folUsers = $this->db->distinct()->select('GROUP_CONCAT(sfol_followed_by) AS sfol_followed_by')
                                 ->get_where($this->tbl_supplier_followers, array('sfol_supplier' => $this->suplr))
                                 ->row()->sfol_followed_by;

                 $merged = array_merge(explode(',', $favUsers), explode(',', $folUsers));
                 return $this->db->where_in('usr_id', $merged)
                                 ->where("usr_pushy_token != ''")->where('usr_id != 1')->get($this->tbl_users)->result_array();
            }

            return $this->db->get_where($this->tbl_users, "usr_pushy_token != ''")->result_array();
       }

       function newPushNotification($datas) {
            $masterId = 0;
            if (isset($datas['pushnify'])) {
                 $datas['pushnify']['pnm_sent_by'] = $this->uid;
                 $this->db->insert($this->tbl_pushnotification_master, $datas['pushnify']);
                 $masterId = $this->db->insert_id();
            }
            if (isset($datas['pushnify']['pnm_user_group'])) {
                 if ($datas['pushnify']['pnm_user_group'] == 2) { //Supplier
                      $suppliers = $this->getSuppliersForPushNotfy();
                      if (!empty($suppliers)) {
                           $suppliers = explode(',', $suppliers);
                           $push['id'] = $masterId;
                           $push['msg_type'] = 'general_notfication';
                           $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_fullname');
                           $push['title'] = $datas['pushnify']['pnm_title'];
                           $push['image'] = isset($datas['pushnify']['pnm_image']) && !empty($datas['pushnify']['pnm_image']) ? site_url() . 'assets/uploads/notification/' . $datas['pushnify']['pnm_image'] : '';
                           $push['time'] = date("Y-m-d H:i:s");
                           $push['chat_id'] = $this->uid == 1 ? 'admin' : encryptor($this->uid);
                           $this->common_model->sendPushNotification($push, $suppliers, '');
                      }
                 } else if ($datas['pushnify']['pnm_user_group'] == 4) { //Buyer
                    //   $buyers = $this->getBuyersForPushNotfy();
                    //   if (!empty($buyers)) {
                           $buyers = '/topics/notifications';
                           $push['id'] = $masterId;
                           $push['msg_type'] = 'general_notfication';
                           $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_fullname');
                           $push['title'] = $datas['pushnify']['pnm_title'];
                           $push['image'] = isset($datas['pushnify']['pnm_image']) && !empty($datas['pushnify']['pnm_image']) ? site_url() . 'assets/uploads/notification/' . $datas['pushnify']['pnm_image'] : '';
                           $push['time'] = date("Y-m-d H:i:s");
                           $push['chat_id'] = $this->uid == 1 ? 'admin' : encryptor($this->uid);
                           $this->common_model->sendPushNotification($push, $buyers, '');
                    //   }
                 } else { //Both
                      $suppliers = $this->getSuppliersForPushNotfy();
                      if (!empty($suppliers)) {
                           $suppliers = explode(',', $suppliers);
                           $push['id'] = $masterId;
                           $push['msg_type'] = 'general_notfication';
                           $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_fullname');
                           $push['title'] = $datas['pushnify']['pnm_title'];
                           $push['image'] = isset($datas['pushnify']['pnm_image']) && !empty($datas['pushnify']['pnm_image']) ? site_url() . 'assets/uploads/notification/' . $datas['pushnify']['pnm_image'] : '';
                           $push['time'] = date("Y-m-d H:i:s");
                           $push['chat_id'] = $this->uid == 1 ? 'admin' : encryptor($this->uid);
                           $this->common_model->sendPushNotification($push, $suppliers, '');
                      }

                    //   $buyers = $this->getBuyersForPushNotfy();
                    //   if (!empty($buyers)) {
                           $buyers = '/topics/notifications';
                           $push['id'] = $masterId;
                           $push['msg_type'] = 'general_notfication';
                           $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_fullname');
                           $push['title'] = $datas['pushnify']['pnm_title'];
                           $push['image'] = isset($datas['pushnify']['pnm_image']) && !empty($datas['pushnify']['pnm_image']) ? site_url() . 'assets/uploads/notification/' . $datas['pushnify']['pnm_image'] : '';
                           $push['time'] = date("Y-m-d H:i:s");
                           $push['chat_id'] = $this->uid == 1 ? 'admin' : encryptor($this->uid);
                           $this->common_model->sendPushNotification($push, $buyers, '');
                    //   }
                 }
            } else {
                 $favUsers = $this->db->distinct()->select('GROUP_CONCAT(fav_added_by) AS fav_added_by')
                                 ->get_where($this->tbl_favorite_list, array('fav_consign' => 'SUP', 'fav_consign_id' => $this->suplr))
                                 ->row()->fav_added_by;

                 $folUsers = $this->db->distinct()->select('GROUP_CONCAT(sfol_followed_by) AS sfol_followed_by')
                                 ->get_where($this->tbl_supplier_followers, array('sfol_supplier' => $this->suplr))
                                 ->row()->sfol_followed_by;

                 $merged = array_merge(explode(',', $favUsers), explode(',', $folUsers));
                 $recipients = $this->db->select('GROUP_CONCAT(' . $this->tbl_users . '.usr_pushy_token) AS buy_pushy_token')
                                 ->where_in($this->tbl_users . '.usr_id', $merged)->where($this->tbl_users . '.usr_active', 1)
                                 ->where($this->tbl_users . ".usr_pushy_token != ''")->get($this->tbl_users)->row()->buy_pushy_token;
                 if (!empty($recipients)) {
                      $recipients = explode(',', $recipients);
                      $push['id'] = $masterId;
                      $push['msg_type'] = 'general_notfication';
                      $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->session->userdata('usr_fullname');
                      $push['title'] = $datas['pushnify']['pnm_title'];
                      $push['image'] = isset($datas['pushnify']['pnm_image']) && !empty($datas['pushnify']['pnm_image']) ? site_url() . 'assets/uploads/notification/' . $datas['pushnify']['pnm_image'] : '';
                      $push['time'] = date("Y-m-d H:i:s");
                      $push['chat_id'] = $this->uid == 1 ? 'admin' : encryptor($this->uid);
                      $this->common_model->sendPushNotification($push, $recipients, '');
                 }
            }
            return true;
       }

       function getSuppliersForPushNotfy() {
            return $this->db->select('GROUP_CONCAT(' . $this->tbl_users . '.usr_pushy_token) AS sup_pushy_token')
                            ->join($this->tbl_users, $this->tbl_users . '.usr_supplier = ' . $this->tbl_supplier_master . '.supm_id', 'LEFT')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where($this->tbl_groups . '.id', 2)->where($this->tbl_supplier_master . '.supm_status', 1)
                            ->where($this->tbl_users . '.usr_active', 1)->where($this->tbl_users . ".usr_pushy_token != ''")
                            ->where($this->tbl_supplier_master . '.supm_status', 1)->get($this->tbl_supplier_master)->row()->sup_pushy_token;
       }

       function getBuyersForPushNotfy() {
            return $this->db->select('GROUP_CONCAT(' . $this->tbl_users . '.usr_pushy_token) AS buy_pushy_token')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where($this->tbl_groups . '.id', 4)->where($this->tbl_users . '.usr_active', 1)
                            ->where($this->tbl_users . ".usr_pushy_token != ''")->get($this->tbl_users)->row()->buy_pushy_token;
       }

  }
  