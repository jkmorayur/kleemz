<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class addons_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getRows($id = '') {
            if (!empty($id)) {
                 return $this->db->get_where(tbl_app_home_category, array('ahc_id' => $id))->row_array();
            }
            return $this->db->get(tbl_app_home_category)->result_array();
       }

       function newRow($data) {
            $data['ahc_added_by'] = $this->uid;
            $data['ahc_added_on'] = date('Y-m-d h:i:s');
            if ($this->db->insert(tbl_app_home_category, $data)) {
                 return true;
            }
            return false;
       }

       function editRow($data) {
            $id = $data['ahc_id'];
            unset($data['ahc_id']);
            $this->db->where('ahc_id', $id);
            if ($this->db->update(tbl_app_home_category, $data)) {
                 return true;
            }
            return false;
       }

       function deleteRow($id) {

            $row = $this->getRows($id);
            if (!empty($row)) {
                 if (file_exists(FILE_UPLOAD_PATH . 'app_category/' . $row['ahc_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'app_category/' . $row['ahc_image']);
                 }
                 if (file_exists(FILE_UPLOAD_PATH . 'app_category/thumb_' . $row['ahc_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'app_category/thumb_' . $row['ahc_image']);
                 }
            }
            if ($this->db->delete(tbl_app_home_category, array('ahc_id' => $id))) {
                 return true;
            }
            return false;
       }

  }
  