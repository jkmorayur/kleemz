<?php

  defined('BASEPATH') or exit('No direct script access allowed');

  class addons extends App_Controller {

       public function __construct() {

            parent::__construct();
            
            $this->load->model('addons_model', 'addons');
            $this->load->library('upload');
            $this->lock_in();
       }

       function appHomeCategory() {
            $this->page_title = 'Addons | App home category';
            if (!empty($_POST)) {
                 $angle = array();
                 if (isset($_FILES['ahc_image']['name']) && !empty($_FILES['ahc_image']['name'])) {
                      $newFileName = rand(9999999, 0) . $_FILES['ahc_image']['name'];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'app_category/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $angle['x1']['0'] = $_POST['x10'];
                      $angle['x2']['0'] = $_POST['y10'];
                      $angle['y1']['0'] = $_POST['x20'];
                      $angle['y2']['0'] = $_POST['y20'];
                      $angle['w']['0'] = $_POST['w0'];
                      $angle['h']['0'] = $_POST['h0'];

                      if (!$this->upload->do_upload('ahc_image')) {
                           $up = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle);
                           $_POST['master']['ahc_image'] = $data['file_name'];
                      }
                      if ($this->addons->newRow($_POST['master'])) {
                           $this->session->set_flashdata('app_success', 'New category successfully added!');
                           redirect(strtolower(__CLASS__) . '/appHomeCategory');
                      }
                 }
            } else {
                 $data['category'] = $this->addons->getRows();
                 $this->render_page(__CLASS__ . '/appHomeCategory', $data);
            }
       }

       function view($id) {
            $id = encryptor($id, 'D');
            $data['category'] = $this->addons->getRows($id);
            $this->render_page(__CLASS__ . '/view', $data);
       }

       function edit() {
            if (!empty($_POST)) {
                 $angle = array();
                 if (isset($_FILES['ahc_image']['name']) && !empty($_FILES['ahc_image']['name'])) {
                      $newFileName = rand(9999999, 0) . $_FILES['ahc_image']['name'];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'app_category/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $angle['x1']['0'] = $_POST['x10'];
                      $angle['x2']['0'] = $_POST['y10'];
                      $angle['y1']['0'] = $_POST['x20'];
                      $angle['y2']['0'] = $_POST['y20'];
                      $angle['w']['0'] = $_POST['w0'];
                      $angle['h']['0'] = $_POST['h0'];

                      if (!$this->upload->do_upload('ahc_image')) {
                           $up = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle);
                           $_POST['master']['ahc_image'] = $data['file_name'];
                      }
                 }
                 if ($this->addons->editRow($_POST['master'])) {
                      $this->session->set_flashdata('app_success', 'Category successfully updated!');
                      redirect(strtolower(__CLASS__) . '/appHomeCategory');
                 }
            }
       }
       public function delete($id) {
            $id = encryptor($id, 'D');
            if ($this->addons->deleteRow($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Category successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete category")));
            }
       }

  }
  