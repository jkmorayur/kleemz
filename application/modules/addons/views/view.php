<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New app home category</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <form id="demo-form2" action="<?php echo site_url($controller . '/edit');?>" method="post"  data-parsley-validate class="form-horizontal form-label-left frmEmployee" 
                               enctype="multipart/form-data">
                              <input type="hidden" name="master[ahc_id]" value="<?php echo $category['ahc_id'];?>"/>
                              <div class="form-group">
                                   <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Category title <span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="select2_group form-control" data-parsley-required-message="Please enter category title" 
                                               value="<?php echo $category['ahc_title'];?>" placeholder="Category title" name="master[ahc_title]" required="true"/>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Choose background color <span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group demo2 colorpicker-element">
                                             <input type="text" value="#e01ab5" class="form-control" data-parsley-required-message="Please choose background color" 
                                                    value="<?php echo $category['ahc_color'];?>" placeholder="choose background color" name="master[ahc_color]" required="true">
                                             <span title="Click here to choose color" class="input-group-addon"><i style="background-color: <?php echo $category['ahc_color'];?>;"></i></span>
                                        </div>
                                   </div>
                              </div>
                              <?php
                                if (isset($category['ahc_image']) && !empty($category['ahc_image'])) {
                                     ?>
                                     <div class="form-group">
                                          <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Logo</label>
                                          <div class="col-md-5 col-sm-3 col-xs-12">
                                               <div class="input-group demo2 colorpicker-element">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'app_category/' . $category['ahc_image'], 'width' => '100', 'id' => 'imgBrandImage'));?>
                                               </div>
                                          </div>
                                     </div>
                                     <?php
                                }
                              ?>
                              <div class="form-group">
                                   <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">New Logo</label>
                                   <div class="col-md-5 col-sm-3 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x10" />
                                             <input type="hidden" id="y10" name="y10" />
                                             <input type="hidden" id="x20" name="x20" />
                                             <input type="hidden" id="y20" name="y20" />
                                             <input type="hidden" id="w0" name="w0" />
                                             <input type="hidden" id="h0" name="h0" />
                                             <input data-parsley-fileextension="png,PNG" type="file" 
                                                    class="form-control col-md-7 col-xs-12" name="ahc_image" 
                                                    id="image_file0" onchange="fileSelectHandler('0', '150', '150', false)" />
                                             <img id="preview0" class="preview"/>
                                             <span class="help-inline">Choose only .png image with 150(H) X 150(W)</span>
                                        </div>
                                   </div>
                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
