<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New app home category</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <form id="demo-form2" method="post"  data-parsley-validate class="form-horizontal form-label-left frmEmployee" 
                               enctype="multipart/form-data">
                              <div class="form-group">
                                   <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Category title <span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="select2_group form-control" data-parsley-required-message="Please enter category title" 
                                               placeholder="Category title" name="master[ahc_title]" required="true"/>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Choose background color <span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group demo2 colorpicker-element">
                                             <input type="text" value="#e01ab5" class="form-control" data-parsley-required-message="Please choose background color" 
                                                    placeholder="choose background color" name="master[ahc_color]" required="true">
                                             <span title="Click here to choose color" class="input-group-addon"><i style="background-color: rgb(101, 126, 173);"></i></span>
                                        </div>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Logo <span class="required">*</span></label>
                                   <div class="col-md-5 col-sm-3 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x10" />
                                             <input type="hidden" id="y10" name="y10" />
                                             <input type="hidden" id="x20" name="x20" />
                                             <input type="hidden" id="y20" name="y20" />
                                             <input type="hidden" id="w0" name="w0" />
                                             <input type="hidden" id="h0" name="h0" />
                                             <input required data-parsley-required-message="upload atleast one image" data-parsley-fileextension="png,PNG" type="file" 
                                                    class="form-control col-md-7 col-xs-12" name="ahc_image" 
                                                    id="image_file0" onchange="fileSelectHandler('0', '150', '150', false)" />
                                             <img id="preview0" class="preview"/>
                                             <span class="help-inline">Choose only .png image with 150(H) X 150(W)</span>
                                        </div>
                                   </div>
                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                         </form>

                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $category as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/view/' . encryptor($value['ahc_id']));?>">
                                               <td style="width: 50px;background-color: <?php echo $value['ahc_color']?>;" class="trVOE">
                                                    <?php
                                                    $img = $value['ahc_image'];
                                                    echo img(array('src' => FILE_UPLOAD_PATH . 'app_category/' . $img, 'height' => '50', 'width' => '50'));
                                                    ?>
                                               </td>
                                               <td class="trVOE"><?php echo $value['ahc_title'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td style="width: 50px;">
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . encryptor($value['ahc_id']));?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
