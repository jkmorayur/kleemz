<div class="right_col" role="main">
     <div class="">
          <div class="clearfix"></div>
          <div class="row">
               <div class="col-md-12">
                    <div class="x_panel">
                         <div class="x_title">
                              <h2> <i class="fa fa-list-alt"></i> Inbox</h2>
                              <div class="clearfix"></div>
                         </div>
                         <div class="x_content">
                              <div class="row">
                                   <?php echo $this->load->view('left-menu');?>
                                   <!-- /MAIL LIST -->

                                   <!-- CONTENT MAIL -->
                                   <div class="col-sm-9 mail_view">
                                        <div class="inbox-body divMailBody">
                                             <?php
                                               if (!empty($mail)) {
                                                    foreach ($mail as $key => $value) {
                                                         $readId = empty($value['mms_parent']) ? $value['mms_id'] : $value['mms_parent'];
                                                         ?>
                                                         <a href="<?php echo site_url('mailbox/read/' . encryptor($readId));?>">
                                                              <div class="mail_list">
                                                                   <div class="">
                                                                        <h3>To&nbsp;&nbsp;: <?php echo $value['to_usr_first_name']?><small><?php echo get_timeago($value['mms_added_on']);?></small></h3>
                                                                        <h3>From&nbsp;&nbsp;: <?php echo $value['frm_usr_first_name']?></h3>
                                                                        <p>
                                                                             <?php
                                                                             if (!empty($value['mms_subject'])) {
                                                                                  echo 'Subject&nbsp;: ' . get_snippet($value['mms_subject'], 10);
                                                                             } else {
                                                                                  echo get_snippet(strip_tags($value['mms_message']), 10);
                                                                             }
                                                                             ?>
                                                                        </p>
                                                                   </div>
                                                              </div>
                                                         </a>
                                                         <?php
                                                    }
                                               } else {
                                                    ?>
                                                    <a href="javascript:;">
                                                         <div class="mail_list" style="text-align: center;border: none;">
                                                              <h3>You have no mail :(</h3>
                                                         </div>
                                                    </a>
                                               <?php }
                                             ?>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<?php echo $this->load->view('compose');?>