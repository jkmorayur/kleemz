<div class="right_col" role="main">
     <div class="">
          <div class="clearfix"></div>
          <div class="row">
               <div class="col-md-12">
                    <div class="x_panel">
                         <div class="x_title">
                              <h2> <i class="fa fa-list-alt"></i> Inbox</h2>
                              <div class="clearfix"></div>
                         </div>
                         <div class="x_content">
                              <div class="row">
                                   <?php echo $this->load->view('left-menu');?>
                                   <!-- /MAIL LIST -->
                                   <!-- CONTENT MAIL -->
                                   <div class="col-sm-9 mail_view">
                                        <div class="inbox-body divMailBody">
                                             <a href="javascript:;">
                                                  <div class="mail_list">
                                                       <div class="">
                                                            <h3>
                                                                 <?php echo $mail['mms_subject']?>
                                                                 <small><?php echo date('j M Y h:i:s A', strtotime($mail['mms_added_on']));?></small>
                                                            </h3>
                                                            <p><?php echo $mail['mms_message']?> </p>
                                                       </div>
                                                  </div>
                                                  <div class="divReply">
                                                       <?php
                                                         if (isset($mail['replay']) && !empty($mail['replay'])) {
                                                              foreach ($mail['replay'] as $key => $rply) {
                                                                   ?>
                                                                   <div class="mail_list">
                                                                        <div>
                                                                             <h3>
                                                                                  <?php echo $rply['mms_subject']?>
                                                                                  <small><?php echo date('j M Y h:i:s A', strtotime($rply['mms_added_on']));?></small>
                                                                             </h3>
                                                                             <p><?php echo $rply['mms_message']?> </p>
                                                                        </div>
                                                                   </div>
                                                                   <?php
                                                              }
                                                         }
                                                       ?>
                                                  </div>
                                                  <form class="reply-box frmMailBooxReply" data-url="<?php echo site_url('mailbox/reply');?>" method="post"
                                                        enctype="multipart/form-data" data-parsley-validate="true">
                                                       <input type="hidden" name="mms_from" value="<?php echo $mail['mms_to']?>"/>
                                                       <input type="hidden" name="mms_to" value="<?php echo $mail['mms_from']?>"/>
                                                       <input type="hidden" name="mms_parent" value="<?php echo $mail['mms_id']?>"/>
                                                       <input type="hidden" name="mms_subject" value=""/>

                                                       <div>
                                                            <h3>Reply</h3>
                                                            <textarea name="mms_message" required data-parsley-required-message="Enter message"
                                                                      class="mailEditor" placeholder="Message"></textarea>

                                                            <div class="form-group col-md-12" style="width: 100%;float: left;">
                                                                 <label class="control-label">Attachments</label>
                                                                 <div style="display:flex">
                                                                      <input type="file" name="attachments[]"/>
                                                                      <i class="fa fa-plus btnMoreAttach" style="cursor: pointer;" title="Attach another file"></i>
                                                                 </div>

                                                            </div>

                                                            <!-- <div class="form-group" style="width: 100%;float: left;">
                                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Attachments</label>
                                                                 <div class="col-md-5 col-sm-5 col-xs-5">
                                                                      <input type="file" name="attachments[]"/>
                                                                 </div>
                                                                 <div class="col-md-4 col-sm-5 col-xs-5">
                                                                      <i class="fa fa-plus btnMoreAttach" style="cursor: pointer;"></i>
                                                                 </div>
                                                            </div>
                                                             -->
                                                            <div class="divMoreAttach"></div>

                                                            <div class="col-md-12 col-sm-6 col-xs-12">
                                                                 <div class="compose-footer">
                                                                      <button id="send" class="btn btn-sm btn-success btnSentMail"
                                                                              type="submit"><i class="fa fa-reply"></i> Send</button>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                       <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                                  </form>
                                                  <?php if (isset($mail['attachments']) && !empty($mail['attachments'])) {?>
                                                         <div class="attachment">
                                                              <p>
                                                                   <span><i class="fa fa-paperclip"></i> <?php echo count($mail['attachments']);?> attachments </span>
                                                              </p>
                                                              <ul>
                                                                   <?php foreach ($mail['attachments'] as $key => $value) {?>
                                                                        <li>
                                                                             <a href="javascript:;" class="atch-thumb">
                                                                                  <?php
                                                                                  if (is_image($value['mat_attachment'])) {
                                                                                       echo img(array('src' => './assets/uploads/mail_attachments/' . $value['mat_attachment']));
                                                                                  }
                                                                                  ?>
                                                                             </a>

                                                                             <div class="file-name">
                                                                                  <a target="_blank" href="<?php echo './uploads/mail_attachments/' . $value['mat_attachment'];?>">
                                                                                       <i data-toggle="tooltip" data-original-title="Download" class="fa fa-download"></i></a>
                                                                             </div>
                                                                        </li>
                                                                   <?php }?>
                                                              </ul>
                                                         </div>
                                                    <?php }?>
                                                  <div class="btn-group">
                                                       <!--<button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Reply</button>-->
                                                       <!--<button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>-->
                                                       <!--<button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button>-->
                                                       <button class="btn btn-sm btn-default btnDeleteMail" type="button" data-placement="top"
                                                               data-url="<?php echo site_url('mailbox/deleteMail/' . encryptor($mail['mms_id']));?>"
                                                               data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                                                  </div>
                                             </a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<?php echo $this->load->view('compose');?>
