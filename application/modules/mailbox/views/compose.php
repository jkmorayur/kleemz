<!-- compose -->
<form class="frmMailBoox" data-url="<?php echo site_url('mailbox/sent');?>" method="post" enctype="multipart/form-data"
      data-parsley-validate="true">
     <div class="compose col-md-6 col-xs-12" style="max-height: 500px;height: 400px;">
          <div class="compose-header">
               New Message
               <button type="button" class="close compose-close">
                    <span>×</span>
               </button>
          </div>

          <div class="compose-body">
               <div id="alerts"></div>
               <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                         <select data-size="10" data-width="100%" id="ajax-select" class="selectpicker with-ajax" name="mms_to" 
                                 required data-parsley-required-message="Select recipient" 
                                 data-live-search="true"></select>
                    </div>
               </div>
               <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                         <input autocomplete="off" name="mms_subject" type="text" required data-parsley-required-message="Enter subject" 
                                placeholder="Enter subject" class="form-control"/>
                    </div>
               </div>

               <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                         <textarea name="mms_message" required data-parsley-required-message="Enter message" 
                                   class="mailEditor" placeholder="Message"></textarea>
                    </div>
               </div>

               <div class="form-group" style="width: 100%;float: left;">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Attachments</label>
                    <div class="col-md-5 col-sm-5 col-xs-5">
                         <input type="file" name="attachments[]"/>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-5">
                         <i class="fa fa-plus btnMoreAttach" style="cursor: pointer;"></i>
                    </div>
               </div>
               <div class="divMoreAttach"></div>
          </div>

          <div class="form-group">
               <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="compose-footer">
                         <button id="send" class="btn btn-sm btn-success btnSentMail" type="submit"><i class="fa fa-paper-plane"></i> Send</button>
                    </div>
               </div>
          </div>
     </div>
     <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
</form>