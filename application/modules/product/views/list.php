<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Products</h2>
                         <div class="clearfix"></div>
                    </div>
                    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
                    <input type="hidden" id="crnt_status">
                    <!-- card section -->
                    <section class="nbox-cont hidden">
                         <ul>
                              <li>
                                   <div class="nbox-card btn-default active st_div" data-status="">
                                        <h1 id="tot_h"><?= $total_produts?></h1>
                                        <h4>Total Products</h4>
                                        <p>No of products uploaded by you</p>
                                   </div>
                              </li>
                              <li>
                                   <div class="nbox-card btn-success green-card disable st_div"  data-status="active">
                                        <h1 id="act_h"><?= $total_active?></h1>
                                        <h4>Active</h4>
                                        <p>Products still in sale</p>
                                   </div>
                              </li>
                              <li>
                                   <div class="nbox-card btn-danger red-card disable st_div" data-status="inactive">
                                        <h1 id="inact_h"><?= $total_inactive?></h1>
                                        <h4>Inactive</h4>
                                        <p>Products currently removed from sale</p>
                                   </div>
                              </li>

                         </ul>
                    </section>
                    <!-- /card section -->
                    <?php if (check_permission('product', 'product_report')) {?> 
                           <!--                           <div class="x_content lg-bg">
                                                           <form id="demo-form2" class="form-inline f_tp" method="post" action="<?php echo site_url($controller . '/product_report');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                                                                <input value="0" type="hidden" name="" id="usr_id"/>
                                                                <div class="form-group">
                                                                     <label class="control-label" for="first-name">Start Date<span class="required">*</span>
                                                                     </label>
                                                                     <input type="text" id="date1" required="required" class="form-control" data-parsley-required-message="Start Date is required" name="start_date">
                                                                </div>
                                                                <div class="form-group">
                                                                     <label class="control-label" for="last-name">End Date <span class="required">*</span>
                                                                     </label>
                                                                     <input type="text" id="date2" name="end_date" required="required" data-parsley-required-message="End Date is required" class="form-control">
                                                                </div>
                           
                                                                <button type="submit"  name='type' value="excel" class="btn btn-success">Excel</button>
                                                                <button type="submit" name='type' value="pdf" class="btn btn-danger">Pdf</button>
                                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                                                           </form>
                                                      </div>-->
                      <?php }?>
                    <div class="x_content table-responsive">
                         <div id="datatable_filter" class="col-md-12 col-sm-12 col-xs-12 dataTables_filter"><label>Search:<input type="text" id="prd_search" class="" placeholder="Product/Supplier name" aria-controls="datatable"><input type="button" id="search_btn" value="Search"></label></div>
                         <table id="tbl" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <?php if ($this->usr_grp == 'SP' || is_root_user()) {?><th>Added By</th><?php }?>
                                        <th>Product Name</th>
                                        <th>Added Date</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                        <?php echo check_permission($controller, 'changestatus') ? '<th>Status</th>' : '';?>
                                        <?php if (!is_root_user()) {?> <th>Status</th> <?php }?>
                                   </tr>
                              </thead>
                              <tbody></tbody>
                         </table>
                         <div class="text-right" id="pagination_link">
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<link href="css/dash-cards.css" rel="stylesheet">
<script src="js/prod_list.js"></script>

<script>
     $(document).ready(function () {
          $(".nbox-cont").removeClass("hidden");
          load_page_data(1)
     });

     $(function () {
          $('input[name="start_date"]').daterangepicker({
               singleDatePicker: true,
               showDropdowns: true,
               minYear: 1901,
               maxYear: parseInt(moment().format('YYYY'), 10),
               locale: {
                    format: 'YYYY-MM-DD'
               }
          });
     });

     $(function () {
          $('input[name="end_date"]').daterangepicker({
               singleDatePicker: true,
               showDropdowns: true,
               minYear: 1901,
               maxYear: parseInt(moment().format('YYYY'), 10),
               locale: {
                    format: 'YYYY-MM-DD'
               }
          });
     });
</script>
