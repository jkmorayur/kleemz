<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New Product</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/add", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <div class="widget-body">
                              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                   <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                             <a href="javascript:;" goto="#basic_details" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Basic Details</a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#specification" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Specification</a>
                                        </li>
                                        <!--                                        <li role="presentation" class="pars">
                                                                                     <a href="javascript:;" goto="#settings" role="tab" id="settings-tab" data-toggle="tab" aria-expanded="false">Settings</a>
                                                                                </li>-->
                                   </ul>

                                   <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="basic_details" aria-labelledby="home-tab">

                                             <?php if (is_root_user() || $this->usr_grp == 'SBA') {?>
                                                    <input value="0" type="hidden" name="product[prd_supplier]"/>
                                               <?php } else {?>
                                                    <input value="<?php echo $this->suplr;?>" type="hidden" name="product[prd_supplier]"/>
                                               <?php }?>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Product name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter Name" placeholder="Product Name" type="text" name="product[prd_name_en]" id="supm_name_en"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Product category</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <?php
                                                         build_category_tree($this, $locations, 0);
                                                       ?>
                                                       <div class="div-category">
                                                            <ul class="li-category">
                                                                 <?php echo $locations?>
                                                            </ul>
                                                       </div>
                                                       <?php

                                                         function build_category_tree($f, &$output, $preselected, $parent = 0, $indent = "") {
                                                              $ser_parent = '';
                                                              $parentCategories = $f->product_category->getCategoryChaild($parent);
                                                              foreach ($parentCategories as $key => $value) {
                                                                   $selected = ($value["pcat_id"] == $ser_parent) ? "ckecked=\"true\"" : "";
                                                                   $output .= "<li>" . $indent . "<input name='categories[]' value='" . $value["pcat_id"] . "' type='checkbox' /><span>" . $value["pcat_title"] . '</span></li>';
                                                                   if ($value["pcat_id"] != $parent) {
                                                                        build_category_tree($f, $output, $preselected, $value["pcat_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                   }
                                                              }
                                                         }
                                                       ?>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product keywords</label>
                                                  <div class="col-md-6 col-sm-3 col-xs-12">
                                                       <input id="keywords_en" placeholder="Keyword" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                  </div>

                                                  <div class="col-md-1 col-sm-1 col-xs-12">
                                                       <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMoreKeys"></span>
                                                  </div>
                                             </div>
                                             <div  id="divMoreProductKeywords"></div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12"  style="width: 50%;">
                                                       <textarea required data-parsley-required-message="Enter Product Description" data-parsley-trigger="keyup" placeholder="Description" class="txtProdDesc editor" name="product[prd_desc]"></textarea>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Product video link</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input placeholder="Product Video" type="text" name="product[prd_video]" id="supm_name_en"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <!--                                                  <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product common images
                                                                                                         <small>(maximum <?php echo get_settings_by_key('prod_img_limit');?> images)</small>
                                                                                                    </label>-->
                                                  <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product common images</label>
                                                  <div class="col-md-5 col-sm-3 col-xs-12">
                                                       <div id="newupload">
                                                            <input type="hidden" id="x10" name="x12[]" />
                                                            <input type="hidden" id="y10" name="y12[]" />
                                                            <input type="hidden" id="x20" name="x22[]" />
                                                            <input type="hidden" id="y20" name="y22[]" />
                                                            <input type="hidden" id="w0" name="w2[]" />
                                                            <input type="hidden" id="h0" name="h2[]" />
                                                            <input required data-parsley-required-message="upload atleast one image" data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="shopImages[]"
                                                                   id="image_file0" onchange="fileSelectHandler('0', '1000', '882', true)" />
                                                            <img id="preview0" class="preview"/>
                                                            <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                                       </div>
                                                       <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="altTags[]" id="image_alt0" data-parsley-required="false" data-parsley-required-message="Please enter an alt tag for the image"
                                                                   class="form-control col-md-9 col-xs-12"/>
                                                       </div>
                                                  </div>

                                                  <!--                                                  <div class="col-md-1 col-sm-1 col-xs-12">
                                                                                                         <span style="float: right;cursor: pointer;"
                                                                                                               data-limit="<?php echo get_settings_by_key('prod_img_limit');?>"
                                                                                                               class="glyphicon glyphicon-plus btnMoreProductImages"></span>
                                                                                                    </div>-->
                                             </div>
                                             <div  id="divMoreProductImages"></div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="specification" aria-labelledby="home-tab">
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Specification</label>
                                                  <div class="col-md-10 col-sm-3 col-xs-12">
                                                       <div class="table-responsive divVehDetailsSale">
                                                            <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                                 <thead>
                                                                      <tr>
                                                                           <th>Specification</th>
                                                                           <th>Value</th>
                                                                           <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <tr>
                                                                           <td>
                                                                                <input id="keywords_en" placeholder="Specification" class="commonAutoComplete form-control col-md-7 col-xs-12"
                                                                                       data-url="<?php echo site_url('product/productAutoSpecification/en');?>"
                                                                                       type="text" name="specification_key[en][]">
                                                                           </td>


                                                                           <td>
                                                                                <input id="keywords_en" placeholder="Value" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[en][]">
                                                                           </td>
                                                                           <td>
                                                                                <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                           </td>
                                                                      </tr>
                                                                 </tbody>
                                                            </table>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                       <!--<button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>-->
                                                       <button type="submit" class="btn btn-success">Submit</button>
                                                  </div>
                                             </div>
                                        </div>

                                        <!-- Settings Tab -->
                                        <!--                                        <div role="tabpanel" class="tab-pane fade" id="settings" aria-labelledby="settings-tab">
                                                                                     <div class="form-group">
                                                                                          <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Settings</label>
                                                                                          <div class="col-md-12">
                                                                                               <div class="divider"></div>
                                                                                               <div class="row m-t-10">
                                                                                                    <div class="col-md-12">
                                                                                                         <input type="checkbox" class="js-switch" name="product[prd_hot_selling]" value="1" data-switchery="true">
                                                                                                         Hot Selling
                                                                                                    </div>
                                                                                               </div>
                                                                                               <div class="row m-t-10">
                                                                                                    <div class="col-md-12">
                                                                                                         <input type="checkbox" name="product[prd_is_show_on_profile]" class="js-switch " value="1" data-toggle="divStock" data-switchery="true">
                                                                                                         Show on your profile page?
                                                                                                    </div>
                                                                                               </div>
                                        
                                                                                               <div class="row m-t-10">
                                                                                                    <div class="col-md-2">
                                                                                                         <input type="checkbox" name="product[prd_is_main_product]" class="js-switch " value="1" data-toggle="divStock" data-switchery="true">
                                                                                                         Show on home page page?
                                                                                                    </div>
                                                                                               </div>
                                                                                          </div>
                                                                                     </div>
                                        
                                                                                     <div class="ln_solid"></div>
                                                                                     <div class="form-group">
                                                                                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                                                               <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                                                               <button type="submit" class="btn btn-success">Submit</button>
                                                                                          </div>
                                                                                     </div>
                                                                                </div>-->
                                        <!-- /Settings Tab -->

                                   </div>
                              </div>
                         </div>

                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
<script>

     $(".pars").click(function () {

          if (!$('#frmProduct').parsley().validate())
               return false;

     });
     $(document).ready(function () {
          $(".redactor-editor").keyup(function () {
               var content = $('.txtProdDesc').val();
               if ($(content).text()) {
                    $(".txtProdDesc").removeAttr("required");
               } else {
                    $(".txtProdDesc").attr("required", "required");
               }
               $('#frmProduct').parsley().reset();
               var $form = $('#frmProduct');
               $form.parsley('validate');
          });
     });
</script>
