<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Product Stock</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/updateStock", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <div class="widget-body">
                              <input type="hidden" value="<?php echo $data['psm_id']?>" name="psm_id"/>
                              <input type="hidden" value="<?php echo isset($_GET['calbak']) ? $_GET['calbak'] : '';?>" name="callback"/>
                              <?php if ($this->usr_grp != 'SP') {?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier</label>
                                          <div class="col-md-5 col-sm-6 col-xs-12">
                                               <input required data-parsley-required-message="Enter Supplier and select"
                                                      data-url="<?php echo site_url($controller . '/getSuppliers');?>"
                                                      value="<?php echo $data['supm_name'];?>"
                                                      placeholder="Enter Supplier and select" type="text" data-bind="psm_supplier"
                                                      id="supm_email" class="commonAutoComplete form-control col-md-7 col-xs-12"/>
                                          </div>
                                          <input type="hidden" class="psm_supplier" name="product[psm_supplier]" value="<?php echo $data['psm_supplier'];?>"/>
                                     </div>
                                <?php } else {?>
                                     <input type="hidden" name="product[psm_supplier]" value="<?php echo $this->suplr;?>"/>
                                     <input type="hidden" name="product[psm_supplier_user_id]" value="<?php echo $this->uid;?>"/>
                                <?php }?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <select required data-parsley-required-message="Select product" name="product[psm_product]" 
                                                class="form-control col-md-9 col-xs-12 search-box" id="prd_stock_number">
                                             <option value="">Select product</option>
                                             <?php foreach ((array) $products as $key => $value) {?>
                                                    <option <?php echo ($value['prd_id'] == $data['psm_product']) ? 'selected="selected"' : '';?>
                                                         value="<?php echo $value['prd_id'];?>"><?php echo $value['prd_name_en'];?></option>
                                                    <?php }?>
                                        </select>
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Product local name</label>
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <input id="txtMalayalam" placeholder="Product local name (type manglish here)" class="form-control col-md-7 col-xs-12" 
                                               value="<?php echo $data['psm_product_local_name']; ?>" type="text" name="product[psm_product_local_name]">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Product schedule</label>
                                   <div class="col-md-5 col-sm-6 col-xs-12">
                                        <select data-parsley-required-message="Select product" name="product[psm_product_schedule]" 
                                                class="form-control col-md-9 col-xs-12 search-box" id="prd_stock_number">
                                             <option value="">Select product schedule</option>
                                             <?php foreach ((array) $schedule as $key => $value) {?>
                                                    <option value="<?php echo $value['prs_id'];?>" <?php echo ($value['prs_id'] == $data['psm_product_schedule']) ? 'selected="selected"' : '';?>>
                                                         <?php
                                                         echo $value['prs_schedule_name'] . ' (' .
                                                         $value['prs_start_time'] . ' - ' . $value['prs_end_time'] . ')';
                                                         ?>
                                                    </option>
                                               <?php }?>
                                        </select>
                                   </div>
                              </div>
                              <?php
                                if (isset($data['images']) && !empty($data['images'])) {
                                     $i = 0;
                                     ?>
                                     <div class="form-group">

                                          <?php
                                          foreach ($data['images'] as $key => $value) {
                                               if ($i % 4 == 0) {
                                                    ?>
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Images</label>
                                               <?php }?>
                                               <div class="bannerImageGroupshop col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['psi_id'];?>" style="padding-left: 10px;">
                                                    <div class="input-group">
                                                         <?php echo img(array('src' => FILE_UPLOAD_PATH . 'product/' . $value['psi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                    </div>
                                                    <?php if ($value['psi_image']) {?>
                                                         <span class="help-block">
                                                              <a data-url="<?php echo site_url($controller . '/removeStockImage/' . $value['psi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                         </span>
                                                         <!--                                                         <div id="altTag">
                                                                                                                       <input placeholder="Alt tag for image" type="text" name="ssi_alttag[<?php echo $value['psi_id']?>]" id="ssi_alttag" data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['psi_alt'];?>"
                                                                                                                              class="form-control col-md-9 col-xs-12"/>
                                                                                                                  </div>-->
                                                    <?php }?>
                                               </div>
                                               <?php
                                               $i++;
                                          }
                                          ?>
                                     </div>
                                <?php } else {?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Default Images</label>
                                          <div class="col-md-2 col-sm-3 col-xs-12">
                                               <div class="input-group">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'product/' . $data['defaultImage'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                               </div>
                                          </div>
                                     </div>
                                <?php }
                              ?>
                              <div class="form-group">
                                   <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Your product images
                                        <small>(maximum <?php echo get_settings_by_key('prod_img_limit');?> images)</small>
                                   </label>
                                   <div class="col-md-5 col-sm-3 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x12[]" />
                                             <input type="hidden" id="y10" name="y12[]" />
                                             <input type="hidden" id="x20" name="x22[]" />
                                             <input type="hidden" id="y20" name="y22[]" />
                                             <input type="hidden" id="w0" name="w2[]" />
                                             <input type="hidden" id="h0" name="h2[]" />
                                             <input data-parsley-required-message="upload atleast one image" data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="shopImages[]"
                                                    id="image_file0" onchange="fileSelectHandler('0', '1000', '882', true)" />
                                             <img id="preview0" class="preview"/>
                                             <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                        </div>
                                        <!--                                        <div id="altTag">
                                                                                     <input placeholder="Alt tag for image" type="text" name="altTags[]" id="image_alt0" 
                                                                                            data-parsley-required="false" data-parsley-required-message="Please enter an alt tag for the image"
                                                                                            class="form-control col-md-9 col-xs-12"/>
                                                                                </div>-->
                                   </div>
                                   <div class="col-md-1 col-sm-1 col-xs-12">
                                        <span style="float: right;cursor: pointer;"
                                              data-limit="<?php echo get_settings_by_key('prod_img_limit');?>"
                                              class="glyphicon glyphicon-plus btnMoreProductImages"></span>
                                   </div>
                              </div>
                              <div id="divMoreProductImages"></div>

                              <div class="form-group">
                                   <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Stock details</label>
                                   <div class="col-md-10 col-sm-3 col-xs-12">
                                        <div class="table-responsive divVehDetailsSale">
                                             <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                  <thead>
                                                       <tr>
                                                            <th>Unit</th>
                                                            <th>Qty</th>
                                                            <th>Price</th>
                                                            <th>Offer Price</th>
                                                            <th>Total stock</th>
                                                            <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                                       </tr>
                                                  </thead>
                                                  <tbody>
                                                       <?php
                                                         if (isset($data['details']) && !empty($data['details'])) {
                                                              foreach ($data['details'] as $key => $det) {
                                                                   ?>
                                                              <input type="hidden" name="stock[id][]" value="<?php echo $det['pdsm_id'];?>"/>
                                                              <tr class="deleteRow<?php echo $det['pdsm_id'];?>">
                                                                   <td>
                                                                        <select required data-parsley-required-message="Select Unit" name="stock[unit][]" class="form-control col-md-9 col-xs-12 cmbUnit" id="prd_stock_number">
                                                                             <option value="">Select unit</option>
                                                                             <?php foreach ((array) $units as $key => $value) {?>
                                                                                  <option <?php echo ($value['unt_id'] == $det['pdsm_unit']) ? 'selected="selected"' : '';?>
                                                                                       data='<?php echo json_encode($value);?>' value="<?php echo $value['unt_id'];?>"><?php echo $value['unt_unit_en'] . ' (' . $value['unt_unit_name_en'] . ')';?></option>
                                                                                  <?php }?>
                                                                        </select>
                                                                   </td>
                                                                   <td>
                                                                        <input required data-parsley-required-message="Enter qty"  id="keywords_en" value="<?php echo $det['pdsm_qty'];?>"
                                                                               placeholder="Qty" class="form-control col-md-7 col-xs-12 decimalOnly" type="text" name="stock[qty][]">
                                                                   </td>
                                                                   <td>
                                                                        <input id="keywords_en" class="form-control col-md-7 col-xs-12 decimalOnly" value="<?php echo $det['pdsm_price'];?>"
                                                                               required data-parsley-required-message="Enter price" placeholder="Price" type="text" name="stock[price][]">
                                                                   </td>
                                                                   <td>
                                                                        <input id="keywords_en" class="form-control col-md-7 col-xs-12 decimalOnly" value="<?php echo $det['pdsm_offer_price'];?>"
                                                                               placeholder="Offer price" type="text" name="stock[offer_price][]">
                                                                   </td>
                                                                   <td>
                                                                        <input id="keywords_en" class="form-control col-md-7 col-xs-12 decimalOnly" value="<?php echo $det['pdsm_ttl_stock'];?>"
                                                                               required data-parsley-required-message="Enter total stock" placeholder="Total stock" type="text" name="stock[ttl_stock][]">
                                                                   </td>
                                                                   <td>
                                                                        <span style="cursor: pointer;font-size: 12px;" data-url="<?php echo site_url('product/deleteSingleStock') . '/' . $det['pdsm_id'];?>" 
                                                                              data-id="<?php echo $det['pdsm_id'];?>" class="glyphicon glyphicon-trash deleteRow"></span>
                                                                   </td>
                                                              </tr>
                                                              <?php
                                                         }
                                                    }
                                                  ?>

                                                  </tbody>
                                             </table>
                                        </div>
                                   </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <?php if (check_permission($controller, 'updatestock')) {?>
                                               <button type="submit" class="btn btn-success">Submit</button>
                                          <?php }?>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               <?php echo form_close()?>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
<script>

     $(".pars").click(function () {

          if (!$('#frmProduct').parsley().validate())
               return false;

     });
     $(document).ready(function () {
          $(".redactor-editor").keyup(function () {
               var content = $('.txtProdDesc').val();
               if ($(content).text()) {
                    $(".txtProdDesc").removeAttr("required");
               } else {
                    $(".txtProdDesc").attr("required", "required");
               }
               $('#frmProduct').parsley().reset();
               var $form = $('#frmProduct');
               $form.parsley('validate');
          });
     });
</script>
