<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="Fujishka">
  <base href="<?php echo base_url('assets/');?>/"/>
  <title><?php echo $template['title']?></title>
  <?php echo $template['metadata']?>
  <!-- favicon-->
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">

  <!-- Bootstrap CSS
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

  <!--Main Menu File-->
  <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
  <!-- fontawesome CSS -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" href="css/main.css">


  <title>Fujeeka</title>


</head>

<body>
<?php echo $template['partials']['site_default_header'];?>
<?php echo $template['partials']['flash_messages']?>
<section class="stock-list grey-bg pt-3">
    <div class="container">
        <div class="row">
            <div class="col">
                                <div class="filter-cat">
                                    <h3>FILTER RESULTS BY : </h3>
                                    <div class="s-filter">
                                        <div class="s-cont">
                                            <select class="form-control cate" id="sel1">
                                              <option value="">Select a category</option>
                                              <?php foreach ($categories as $cat) { ?>
                                                <option value="<?php echo $cat['cat_id']; ?>"><?php echo $cat['cat_title']; ?></option>
                                              <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="s-filter">
                                        <div class="s-cont sub_cate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- filters -->
               <div class="row">
                 <div class="filter-container">
                   <div class="col-9">
                     <form class="d-flex">

                      <div class="checkbox-inline">
                          <input type="checkbox" <?php if($type!='flash_sale') { ?> checked <?php } ?> onclick="loadNewSort('lat_chk')" class="new_sort"  value="latest" id="lat_chk">
                          <label for="lat_chk">Latest Stock</label>
                      </div>
                      
                      <div class="checkbox-inline">
                          <input type="checkbox" <?php if($type=='flash_sale') { ?> checked <?php } ?> onclick="loadNewSort('flash_chk')" class="new_sort" value="flash" id="flash_chk">
                          <label for="flash_chk">Flash Stock</label>
                      </div>
                      
                      <div class="checkbox-inline">
                          <input type="checkbox" class="new_sort" onclick="loadNewSort('ofr_chk')" value="offer" id="ofr_chk">
                          <label for="ofr_chk">Offers on Stock</label>
                      </div>

                    </form>
                  </div>

                  <div class="col-3">
                    <div class="sort">
                      <label for="sort_by">Sort by:</label>
                      <select class="form-control" id="sort_by">
                        <option value="">None</option>
                        <option value="price_asc">Price Low to High</option>
                        <option value="price_desc">Price High to Low</option>
                      </select>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                 </div>
               </div>
               <!--/ filters -->
               
      <div class="row">
        <div class="col-md-12">

          <div class="stock-page">
              <div class="cat-head2">
                <div class="prd-name">PRODUCT</div>
                <div class="prd-prce">PRICE</div>
                <div class="prd-update">UPDATED</div>
                <div class="prd-stck">STOCK</div>
              </div>
              <div class="stocks-container">

                <div class="list-wrpr">
                   <ul class="stk-list" id="product_listing">
                   </ul>
                   
                </div><!-- list-wrpaaer -->

              </div>
          </div>
          <div id="pagination_link"></div>
        </div>
      </div>
    </div>
  </section>
  </body>
  </html>
  <?php echo $template['partials']['site_default_footer'];?>
<script>
site_url = "<?=site_url();?>";
</script>  
<script src="js/stock_listing.js"></script>