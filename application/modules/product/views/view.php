<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>
                              <?php
                                if (isset($from)) {
                                     echo 'Duplicate Product';
                                } else {
                                     echo 'Update Product';
                                }
                              ?>
                         </h2>
                         <ul class="nav navbar-right panel_toolbox">
                              <li>
                                   <?php if (is_root_user()) {?>
                                          <input type="checkbox" class="js-switch chkOnchange" value="1" data-switchery="true"
                                                 data-url="<?php echo site_url($controller . '/changestatus/' . encryptor($products['prd_id']));?>"
                                                 <?php echo $products['prd_status'] == 1 ? 'checked' : '';?>>
                                            <?php }?>
                              </li>
                         </ul>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php
                           if (isset($from)) {
                                echo form_open_multipart($controller . "/add", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"));
                           } else {
                                echo form_open_multipart($controller . "/update", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"));
                           }
                         ?>
                         <input type="hidden" name="prd_id" value="<?php echo $products['prd_id'];?>" />
                         <div class="widget-body">
                              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                   <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                             <a href="javascript:;" goto="#basic_details" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Basic Details</a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#specification" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Specification</a>
                                        </li>
                                   </ul>
                                   <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="basic_details" aria-labelledby="home-tab">
                                             <input value="<?php echo $products['prd_supplier'];?>" type="hidden" name="product[prd_supplier]"/>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Product name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter Name" placeholder="Product Name English" type="text" value="<?php echo $products['prd_name_en'];?>" name="product[prd_name_en]" id="supm_name_en"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Product category</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <?php
                                                         // debug($products);
                                                         build_category_tree($products['catId'], $this, $locations, 0);
                                                       ?>
                                                       <div class="div-category">
                                                            <ul class="li-category">
                                                                 <?php echo $locations?>
                                                            </ul>
                                                       </div>
                                                       <?php

                                                         function build_category_tree($selected, $f, &$output, $preselected, $parent = 0, $indent = "") {
                                                              $ser_parent = '';
                                                              $parentCategories = $f->product_category->getCategoryChaild($parent);
                                                              foreach ($parentCategories as $key => $value) {
                                                                   $checked = in_array($value["pcat_id"], $selected) ? "checked" : "";
                                                                   $output .= "<li>" . $indent . "<input $checked name='categories[]' id='" . $value["pcat_id"] . "' value='" . $value["pcat_id"] . "' type='checkbox' /><span>" . $value["pcat_title"] . '</span></li>';
                                                                   if ($value["pcat_id"] != $parent) {
                                                                        build_category_tree($selected, $f, $output, $preselected, $value["pcat_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                   }
                                                              }
                                                         }
                                                       ?>
                                                  </div>
                                             </div>
                                             <?php
                                               if (!empty($products['keyword'])) {
                                                    foreach ($products['keyword'] as $key => $value) {
                                                         ?>
                                                         <div class="form-group">
                                                              <?php if ($key == 0) {?>
                                                                   <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product keywords</label>
                                                              <?php } else {?>
                                                                   <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                              <?php }?>
                                                              <div class="col-md-6 col-sm-3 col-xs-12">
                                                                   <input id="keywords_en" placeholder="Keyword" value="<?php echo $value;?>" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                              </div>
                                                              <?php if ($key == 0) {?>
                                                                   <div class="col-md-1 col-sm-1 col-xs-12">
                                                                        <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMoreKeys"></span>
                                                                   </div>
                                                              <?php } else {?>
                                                                   <div class="col-md-1 col-sm-1 col-xs-12">
                                                                        <span style="cursor: pointer;" class="glyphicon glyphicon-trash btnAddRemoveKeys"></span>
                                                                   </div>
                                                              <?php }?>
                                                         </div>
                                                         <?php
                                                    }
                                               } else {
                                                    ?>
                                                    <div class="form-group">
                                                         <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product keywords</label>
                                                         <div class="col-md-6 col-sm-3 col-xs-12">
                                                              <input id="keywords_en" placeholder="Keyword" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                         </div>
                                                         <div class="col-md-1 col-sm-1 col-xs-12">
                                                              <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMoreKeys"></span>
                                                         </div>
                                                    </div>
                                               <?php }?>   
                                             <div  id="divMoreProductKeywords"></div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12"  style="width: 50%;">
                                                       <textarea required data-parsley-required-message="Enter Product Description" placeholder="Description" class="txtProdDesc editor" name="product[prd_desc]"><?php echo $products['prd_desc'];?></textarea>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Product video link</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input placeholder="Product Video" type="text" value="<?php echo $products['prd_video'];?>" name="product[prd_video]" id="supm_name_en"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <?php
                                               if (isset($products['images']) && !empty($products['images'])) {
                                                    $i = 0;
                                                    ?>
                                                    <div class="form-group ">
                                                         <?php
                                                         foreach ($products['images'] as $key => $value) {
                                                              if ($i % 4 == 0) {
                                                                   ?>
                                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                              <?php }?>
                                                              <div class="productImages col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['pimg_id'];?>" style="padding-left: 10px;">
                                                                   <div class="input-group">
                                                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'product/' . $value['pimg_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                                   </div>
                                                                   <?php if ($value['pimg_image']) {?>
                                                                        <span class="help-block">
                                                                             <a data-url="<?php echo site_url($controller . '/removeImage/' . $value['pimg_id']);?>"
                                                                                img-count="<?php echo $i;?>"
                                                                                href="javascript:void(0);" style="width: 100px;"
                                                                                class="btn btn-block btn-danger btn-xs btnDeleteProductImage">Delete</a>
                                                                        </span>
                 <!--                                                                        <span class="help-block">
                                                                             <input   type="radio" name="supm_default_image" class="btnSetDefaultImage"
                                                                        <?php //echo ($value['pimg_id'] == $products['prd_default_image']) ? 'checked="checked"' : '';?>
                                                                                      data-url="<?php //echo site_url($controller . '/setDefaultImage/' . $value['pimg_id'] . '/' . $products['prd_id']);?>"/>&nbsp; Make it default
                                                                        </span>-->
                                                                        <div id="altTag">
                                                                             <input placeholder="Alt tag for image" type="text" name="imgaltTags[<?php echo $value['pimg_id'];?>]" id="image_alt0" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['pimg_alttag'];?>"
                                                                                    class="form-control col-md-9 col-xs-12"/>
                                                                               <?php }?>
                                                                   </div>
                                                              </div>
                                                              <?php
                                                              $i++;
                                                         }
                                                         ?>
                                                    </div>
                                                    <?php if (count($products['images']) < get_settings_by_key('prod_img_limit')) {?>
                                                         <div class="form-group">
                                                              <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product common images</label>
                                                              <div class="col-md-5 col-sm-3 col-xs-12">
                                                                   <div id="newupload">
                                                                        <input type="hidden" id="x10" name="x12[]" />
                                                                        <input type="hidden" id="y10" name="y12[]" />
                                                                        <input type="hidden" id="x20" name="x22[]" />
                                                                        <input type="hidden" id="y20" name="y22[]" />
                                                                        <input type="hidden" id="w0" name="w2[]" />
                                                                        <input type="hidden" id="h0" name="h2[]" />
                                                                        <input  <?php if (!isset($products['images']) || empty($products['images'])) {?> data-parsley-required ="false" <?php } else {?>  data-parsley-required ="false" <?php }?>
                                                                                                                                                         data-parsley-required-message="Please Upload a file"  type="file" data-parsley-fileextension="jpg,png,gif,jpeg"  class="form-control col-md-7 col-xs-12" name="shopImages[]"
                                                                                                                                                         id="image_file0" onchange="fileSelectHandler('0', '1000', '882', '1')" />
                                                                        <img id="preview0" class="preview"/>
                                                                        <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                                                   </div>
                                                                   <div id="altTag">
                                                                        <input <?php if (!isset($products['images']) || empty($products['images'])) {?> data-parsley-required ="false" <?php } else {?>  data-parsley-required ="false" <?php }?>
                                                                                                                                                        data-parsley-required-message="Please enter an alt tag for the image"   placeholder="Alt tag for image" type="text" name="altTags[]" id="image_alttag"
                                                                                                                                                        class="form-control col-md-9 col-xs-12"/>
                                                                   </div>
                                                              </div>
                                                         </div>
                                                         <div  id="divMoreProductImages"></div>
                                                         <?php
                                                    }
                                               }
                                             ?>
                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="specification" aria-labelledby="home-tab">
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Specification</label>
                                                  <div class="col-md-10 col-sm-3 col-xs-12">
                                                       <div class="table-responsive divVehDetailsSale">
                                                            <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                                 <thead>
                                                                      <tr>
                                                                           <th>Specification</th>
                                                                           <th>Value</th>
                                                                           <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <?php
                                                                        if (!empty($products['specification'])) {
                                                                             foreach ((array) $products['specification'] as $key => $value) {
                                                                                  ?>
                                                                                  <tr>
                                                                                       <td>
                                                                                            <input id="keywords_en" placeholder="Specification" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_key_en'];?>"  name="specification_key[en][]">
                                                                                       </td>

                                                                                       <td>
                                                                                            <input id="keywords_en" placeholder="Value" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_val_en'];?>" name="specification_val[en][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                       </td>
                                                                                  </tr>
                                                                                  <?php
                                                                             }
                                                                        } else {
                                                                             ?>
                                                                             <tr>
                                                                                  <td>
                                                                                       <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[en][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[ar][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[ch][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[en][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[ar][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[ch][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                  </td>
                                                                             </tr>
                                                                        <?php }?>
                                                                 </tbody>
                                                            </table>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                       <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Submit</button>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
<script>
     $(".pars").click(function () {
          if (!$('#frmProduct').parsley().validate())
               return false;
     });

     $(document).ready(function () {
          $(".redactor-editor").keyup(function () {
               var content = $('.txtProdDesc').val();
               if ($(content).text()) {
                    $(".txtProdDesc").removeAttr("required");
               } else {
                    $(".txtProdDesc").attr("required", "required");
               }
               $('#frmProduct').parsley().reset();
               var $form = $('#frmProduct');
               $form.parsley('validate');
          });
     });

</script>