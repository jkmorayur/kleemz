<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Stock list</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                         <!--<div id="datatable_filter" class="col-md-12 col-sm-12 col-xs-12 dataTables_filter"><label>Search:<input type="text" id="prd_search" class="" placeholder="Product/Supplier name" aria-controls="datatable"><input type="button" id="search_btn" value="Search"></label></div>-->
                         <div class="col-md-12 col-sm-12 col-xs-12">
                              <a href="<?php echo site_url($controller . '/add_stock');?>" class="btn btn-round btn-primary">
                                   <i class="fa fa-pencil-square-o"></i>New Stock
                              </a>
                         </div>
                         <form class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="Search keyword" type="text" name="productName" 
                                               value="<?php echo isset($productName) ? $productName : '';?>"
                                               id="supm_email" class="form-control col-md-7 col-xs-12"/>
                                   </div>
                                   <input type="submit" class="btn btn-round btn-primary" value="Search"/>
                              </div>
                         </form>
                         <table id="tbl" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <?php if (is_root_user() || $this->usr_grp == 'TA') {?><th>Supplier</th><?php }?>
                                        <?php if (is_root_user()) {?><th>Added By</th><?php }?>
                                        <th>Product Name</th>
                                        <th>Product Local Name</th>
                                        <th>Product Schedule</th>
                                        <?php echo check_permission($controller, 'changeStockStatus') ? '<th>Status</th>' : '';?>
                                        <?php echo check_permission($controller, 'deletestock') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php foreach ((array) $stockList as $key => $value) {?>
                                          <tr data-url="<?php echo site_url($controller . '/view_stock/' . encryptor($value['psm_id']));?>">
                                               <?php if (is_root_user() || $this->usr_grp == 'TA') {?><td class="trVOE"><?php echo $value['supm_name'];?></td><?php }?>
                                               <?php if (is_root_user()) {?><td class="trVOE"><?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?></td><?php }?>
                                               <td class="trVOE"><?php echo $value['prd_name_en'];?></td>
                                               <td class="trVOE"><?php echo $value['psm_product_local_name'];?></td>
                                               <td class="trVOE"><?php echo !empty($value['prs_schedule_name']) ? $value['prs_schedule_name'] . ' (' . $value['prs_start_time'] . ' - ' . $value['prs_end_time'] . ')' : '';?></td>
                                               <?php if (check_permission($controller, 'changestockstatus')) {?>
                                                    <td>
                                                         <label class='switch'>
                                                              <input <?php echo ($value['psm_status'] == 1) ? 'checked="checked"' : '';?> 
                                                                   data-url="<?php echo site_url($controller . '/changeStockStatus/' . encryptor($value['psm_id']));?>"
                                                                   type="checkbox" value="1" class="chkOnchange">
                                                              <span class="slider round"></span>
                                                         </label>
                                                    </td>
                                               <?php } if (check_permission($controller, 'deletestock')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/deleteStock/' . encryptor($value['psm_id']));?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                     <?php }?>
                              </tbody>
                         </table>
                         <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo $pageIndex;?> to <?php echo $limit;?> of <?php echo $totalRow;?> entries</div>
                         <div style="float: right;">
                              <?php echo $links;?>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>