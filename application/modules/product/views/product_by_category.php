<!-- content area -->
<div class="total-container" style="<?php echo ($rdfrom == 'profile') ? 'margin-top: 0px;' : '';?>">
     <div class="container">
          <div class="main-area">
               <!--               <ul class="nav nav-tabs" role="tablist">
                                   <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#products">Products</a>
                                   </li>
                                   <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#suppliers">Suppliers</a>
                                   </li>
                              </ul>-->

               <div class="tab-content">
                    <!-- tab 1 -->
                    <div id="products" class="tab-pane active">
                         <!--                         <div class="row">
                                                       <div class="col">
                                                            <div class="filter-cat">
                                                                 <h3>FILTER RESULTS BY : </h3>
                                                                 <div class="s-filter">
                                                                      <div class="s-cont">
                                                                           <select class="form-control" id="sel1" data-url-param="cid">
                                                                                <option value="">Select category</option>
                         <?php foreach ($categories as $cat) {?>
                                                                                                     <option <?php echo ($cid == $cat['cat_id']) ? 'selected="selected"' : '';?>
                                                                                                          value="<?php echo $cat['cat_id'];?>"><?php echo $cat['cat_title'];?></option>
                           <?php }?>
                                                                           </select>
                                                                      </div>
                                                                 </div>
                                                                 <div class="s-filter">
                                                                      <div class="s-cont sub_cate">
                                                                           <select class="form-control cmbProductFilter" id="sel1" data-url-param="scid">
                                                                                <option value="">Select subcategory</option>
                         <?php foreach ($subcategories as $cat) {?>
                                                                                                     <option <?php echo ($scid == $cat['cat_id']) ? 'selected="selected"' : '';?>
                                                                                                          value="<?php echo $cat['cat_id'];?>"><?php echo $cat['cat_title'];?></option>
                           <?php }?> 
                                                                           </select>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </div>-->

                         <!-- filters -->

                         <div class="row">
                              <div class="filter-container">
                                   <div class="col-9">
                                        <!-- <form>
                                             <label class="checkbox-inline">
                                                  <input type="checkbox" value="">Verified Manufacturers
                                             </label>
                                             <label class="checkbox-inline">
                                                  <input type="checkbox" value="">Accepts Small Orders
                                             </label>

                                             <label class="checkbox-inline">
                                                  <input type="checkbox" value="">Accepts Sample Orders
                                             </label>
                                        </form> -->
                                   </div>

                                   <div class="col-3">
                                        <div class="sort">
                                             <label for="sel1">Sort by:</label>
                                             <select class="form-control cmbProductFilter" id="sel1" data-url-param="sort">
                                                  <option value="">none</option>
                                                  <option <?php echo ($sort == 'price_asc') ? 'selected="selected"' : '';?> value="price_asc">Price Low to High</option>
                                                  <option <?php echo ($sort == 'price_desc') ? 'selected="selected"' : '';?> value="price_desc">Price High to Low</option>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="clearfix"></div>
                              </div>
                         </div>
                         <!--/ filters -->
                         <div class="row">
                              <!-- product container -->
                              <div class="prodct-container">
                                   <!-- single product -->
                                   <?php
                                     $rdfromParam = isset($rdfromParam) ? $rdfromParam : '';
                                     foreach ($serachList as $key => $value) {
                                          ?>
                                          <div class="single-prod">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <a href="<?php echo site_url('product/product-details/' . encryptor($value['prd_id']) . $rdfromParam);?>">
                                                              <?php echo img(array('src' => $value['default_image']));?>
                                                         </a>
                                                    </div>
                                                    <?php if ($value['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                    <div class="prod-detail txt-height">
                                                         <div class="details">
                                                              <h4 class="prod-title-s">
                                                                   <a href="<?php echo site_url('product/product-details/' . encryptor($value['prd_id']) . $rdfromParam);?>"><?php echo get_snippet($value['prd_name_en'], 4);?></a>
                                                              </h4>
                                                              <p>From <span class="price-text">US $<?php echo $value['prd_price_min'];?></span> / Piece</p>
                                                              <p class="moq"><?php echo $value['prd_price_min'];?> Piece  (MOQ)</p>
                                                              <!--                                                              <div class="sub-details">
                                                                                                                                 <p>Category : <span class="green-txt"><a href="">Power Bank</a></span> <br>
                                                                                                                                      Lead Time : <span>25 days</span> <br>
                                                                                                                                      FOB Port : <span>Shenzhen</span></p>
                                                                                                                            </div>-->
                                                         </div>
                                                    </div>
                                               </div>
                                          </div> 
                                     <?php }
                                   ?>
                              </div>
                         </div>
                         <!-- Pagination -->
                         <?php echo $pagination;?>
                    </div>
               </div>
          </div>
     </div>
</div>