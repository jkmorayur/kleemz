<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>RFQ Details</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                   <li role="presentation" class="active">
                                        <a href="javascript:;" goto="#rfq" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">RFQ Details</a>
                                   </li>
                                   <li role="presentation">
                                        <a class="btnComments" href="javascript:;" goto="#messages" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Comments</a>
                                   </li>
                              </ul>

                              <div id="myTabContent" class="tab-content">
                                   <div role="tabpanel" class="tab-pane fade active in" id="rfq" aria-labelledby="home-tab">
                                        <?php if (!$rfq) {?><h3>Invalid RFQ <?php } else {?>
                                                    <?php echo form_open_multipart("product/sendRfqQuote", array('id' => "frmProject", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Buyer Name</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <input disabled type="text" style="width: 500px;" value="<?php echo $rfq['usr_first_name']?>" class="form-control" >
                                                         </div>
                                                    </div>
                                                    <input name="bussiness_mail" type="hidden"  value="<?php echo $rfq['rfq_mail']?>">
                                                    <input name="rfq_id" type="hidden" class="rfq_id" value="<?php echo $rfq['rfq_id']?>">
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Category</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <input disabled type="text" style="width: 500px;" value="<?php echo $rfq['cat_title']?>" class="form-control" >
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Name</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <input disabled type="text" style="width: 500px;" value="<?php echo $rfq['prd_name_en']?>" class="form-control" >
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Quantity</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <input disabled type="text" style="width: 500px;" value="<?php echo $rfq['rfq_qty'] . ' ' . $rfq['unt_unit_en']?>" class="form-control" >
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Requirment</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <textarea name="" disabled class='form-control'><?php echo $rfq['rfq_requirment']?></textarea>
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Validity</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <input disabled type="text" style="width: 500px;" value="<?php echo $rfq['rfq_validity']?>" class="form-control" >
                                                         </div>
                                                    </div>
                                                    <?php if ($rfq['rfq_attachment']) {?>
                                                         <div class="form-group">
                                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Attachment</label>
                                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                                   <a href="uploads/rfq/<?php echo $rfq['rfq_attachment']?>">
                                                                        <h4>View Attachment</h4>
                                                                   </a>
                                                              </div>
                                                         </div>
                                                    <?php } if ($this->usr_grp == 'SP') {?>
                                                         <div class="panel panel-default">
                                                              <div class="panel-heading">RFQ Replay</div>
                                                              <div class="panel-body">
                                                                   <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Related products</label>
                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                             <select class="form-control" name="rfqn_prod_sent[]">
                                                                                  <?php foreach ((array) $rfqRelatedProducts as $key => $value) {?>
                                                                                       <option value="<?php echo $value['prd_id'];?>">
                                                                                            <?php echo $value['prd_name_en'];?></option>
                                                                                  <?php }
                                                                                  ?>
                                                                             </select>
                                                                        </div>
                                                                   </div>
                                                                   <div class="form-group">
                                                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                                                             <textarea required data-parsley-required-message="Enter Message" name="reply" 
                                                                                       placeholder="Replay" col="10" class='editor form-control'></textarea>
                                                                        </div>
                                                                   </div>		
                                                                   <div class="ln_solid"></div>
                                                                   <div class="form-group">
                                                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                                             <button type="submit" class="btn btn-success">Send Reply</button>
                                                                        </div>
                                                                   </div>
                                                                   <div class="clearfix"></div>
                                                              </div>
                                                         </div>
                                                         <?php } if ($this->usr_grp == 'BY') { ?>
                                                         <!-- <div class="panel panel-default">
                                                              <div class="panel-heading">Quotation from supplier (<?php echo count($productsSentBySupplier)?>) </div>
                                                              <div class="panel-body">
                                                                   <?php
                                                                   foreach ((array) $productsSentBySupplier as $key => $value) {
                                                                        if (isset($value['products']) && !empty($value['products'])) {
                                                                             foreach ($value['products'] as $key => $value) {
                                                                                  ?>
                                                                                  <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                                                       <div class="well profile_view">
                                                                                            <div class="col-sm-12">
                                                                                                 <h4 class="brief"><i><?php echo $value['supm_name_en']; ?></i></h4>
                                                                                                 <div class="left col-xs-7">
                                                                                                      <p><?php echo $value['prd_name_en']; ?> </p>
                                                                                                 </div>
                                                                                                 <div class="right col-xs-5 text-center">
                                                                                                      <?php echo $this->product_model->getProductImage($value['prd_id'], 
                                                                                                              'img-circle img-responsive', 'width:100%;'); ?>
                                                                                                 </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12 bottom text-center">
                                                                                                 <div class="col-xs-12 col-sm-12 emphasis">
                                                                                                      <button onclick="$('.btnComments').trigger('click');" type="button" class="btn btn-success btn-xs">
                                                                                                           </i> <i class="fa fa-comments-o"></i> Chat now</button>
                                                                                                      <button type="button" class="btn btn-primary btn-xs">
                                                                                                           <i class="fa fa-user"> </i> Start order
                                                                                                      </button>
                                                                                                 </div>
                                                                                            </div>
                                                                                       </div>
                                                                                  </div>
                                                                                  <?php
                                                                             }
                                                                        }
                                                                    }
                                                                   ?>
                                                              </div>
                                                         </div> -->
                                                         <?php
                                                    } echo form_close();
                                               }
                                             ?>
                                   </div>
                                   <div role="tabpanel" class="tab-pane fade" id="messages" aria-labelledby="profile-tab">
                                   <input type="hidden" id="lgd_img" value="<?= 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar')?>">
                                   <input type="hidden" id="test_id" data-val="done" />
                                   <input type="hidden" id="lgd_name" data-val="done" />

                                        <div class="messaging">
                                             <div class="inbox_msg">
                                                  <?php if (!privilege_exists('SP')) {?>
                                                         <div class="inbox_people">
                                                              <div class="inbox_chat">
                                                                   <?php
                                                                   if (isset($suppliers)) {
                                                                        foreach ($suppliers as $key => $value) {
                                                                             ?>
                                                                             <div class="chat_list <?php echo $key == 0 ? 'active_chat' : '';?>"
                                                                                  data-url="<?php echo site_url('product/loadSeperateRFQComment/' . $value['usr_id'] . '/' . $rfq['rfq_id']);?>"
                                                                                  data-to-id="<?php echo $value['usr_id'];?>" data-from-id="<?php echo $rfq['rfq_user'];?>">
                                                                                  <div class="chat_people">
                                                                                       <div class="chat_img"> 
                                                                                            <?php
                                                                                            echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['usr_avatar'],
                                                                                                'class' => 'img-circle'));
                                                                                            ?>
                                                                                       </div>
                                                                                       <div class="chat_ib">
                                                                                            <h5><?php echo $value['usr_first_name'];?> 
                                                                                                 <!--<span class="chat_date"></span>-->
                                                                                            </h5>
                                                                                            <p></p>
                                                                                       </div>
                                                                                  </div>
                                                                             </div>
                                                                             <?php
                                                                        }
                                                                   }
                                                                   ?>
                                                              </div>
                                                         </div>
                                                    <?php }?>
                                                  <div class="mesgs" style="<?php echo privilege_exists('SP') ? 'width:100%;' : '';?>">
                                                       <div class="msg_history">
                                                            <?php
                                                              if (isset($rfq['comments'])) {
                                                                   foreach ((array) $rfq['comments'] as $key => $value) {
                                                                        echo '<input type="hidden" name="rfqloaded[]" value="' . $value['rfqn_id'] . '"/>';
                                                                        if (($value['rfqn_added_by'] == $this->uid)) { // incoming msg
                                                                             ?>
                                                                             <div class="outgoing_msg" id="msg<?php echo $value['rfqn_id'];?>">
                                                                                  <div class="outgoing_msg_img"> 
                                                                                       <div class="chat_img"> 
                                                                                            <?php
                                                                                            echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['buy_usr_avatar'],
                                                                                                'class' => 'img-circle'));
                                                                                            ?>
                                                                                       </div>
                                                                                  </div>
                                                                                  <div class="sent_msg">
                                                                                       <p><?php echo $value['rfqn_comments'];?></p>
                                                                                       <span class="time_date txt-align-rt"> 
                                                                                            <?php echo date('h:i A', strtotime($value['rfqn_added_on']));?> | 
                                                                                            <?php echo date('M d', strtotime($value['rfqn_added_on']));?> | 
                                                                                            
                                                                                       </span> 
                                                                                  </div>
                                                                             </div>
                                                                        <?php } else { // incoming msg ?>
                                                                           <div class="incoming_msg" id="msg<?php echo $value['rfqn_id'];?>">
                                                                                  <div class="incoming_msg_img"> 
                                                                                       <div class="chat_img"> 
                                                                                            <?php
                                                                                            echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['buy_usr_avatar'],
                                                                                                'class' => 'img-circle'));
                                                                                            ?>
                                                                                       </div>
                                                                                  </div>
                                                                                  <div class="received_msg">
                                                                                       <div class="received_withd_msg">
                                                                                            <p><?php echo $value['rfqn_comments'];?></p>
                                                                                            <span class="time_date">
                                                                                                 <?php echo date('h:i A', strtotime($value['rfqn_added_on']));?> | 
                                                                                                 <?php echo date('M d', strtotime($value['rfqn_added_on']));?> | 
                                                                                                 <strong><?php echo $value['buy_usr_first_name'];?></strong>
                                                                                            </span>
                                                                                       </div>
                                                                                  </div>
                                                                             </div> 
                                                                             <?php
                                                                        }
                                                                   }
                                                              }
                                                            ?>
                                                       </div>
                                                       <div class="type_msg" style="<?php echo is_root_user() ? 'display:none;' : '';?>">
                                                            <div class="input_msg_write">
                                                                 <textarea type="text" class="write_msg txtComments" placeholder="Type a message"
                                                                           data-url="<?php echo site_url('product/sendRFQComments');?>"
                                                                           rfqn_rfq_id="<?php echo $rfq['rfq_id'];?>" to="<?php echo $rfq['rfq_user'];?>" 
                                                                           from="<?php echo $this->uid;?>"></textarea>
                                                                 <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
     $(document).ready(function () {
          if ($(".chat_list").eq(0).length > 0) {
               $(".chat_list").eq(0).trigger("click");
          }

          var anchorHash = window.location.href.toString();
          if (anchorHash.lastIndexOf('#') != -1) {
               anchorHash = anchorHash.substr(anchorHash.lastIndexOf('#'));
               if ($('a[goto="' + anchorHash + '"]').length > 0) {
                    $('a[goto="' + anchorHash + '"]').trigger('click');
               }
          }
     });
</script>