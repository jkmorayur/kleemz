<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Active Products</h2>
                         <div class="clearfix"></div>
                    </div>
                    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
                    <input type="hidden" id="crnt_status">
                    <!-- card section -->
                    <!-- /card section -->
                    <div class="x_content table-responsive">
                         <div id="datatable_filter" class="col-md-12 col-sm-12 col-xs-12 dataTables_filter"><label>Search:<input type="text" id="prd_search" class="" placeholder="Product/Supplier name" aria-controls="datatable"><input type="button" id="search_btn" value="Search"></label></div>
                         <table id="tbl" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <?php if (is_root_user()) {?><th>Supplier</th><?php }?>
                                        <?php if ($this->usr_grp == 'SP' || is_root_user()) {?><th>Added By</th><?php }?>
                                        <th>Product Name</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                        <?php echo check_permission($controller, 'changestatus') ? '<th>Status</th>' : '';?>
                                             <?php if (!is_root_user()) {?> <th>Status</th> <?php }?>
                                   </tr>
                              </thead>
                              <tbody>

                              </tbody>
                         </table>
                         <div class="text-right" id="pagination_link">
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<link href="css/dash-cards.css" rel="stylesheet">
<script src="js/prod_list.js"></script>

<script>
     $(document).ready(function () {
          $(".nbox-cont").removeClass("hidden");
          $('.crnt_status').val('active');
          load_page_data(1,'','active');
     });
</script>