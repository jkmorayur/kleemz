<label class="control-label col-md-3 col-sm-3 col-xs-12">Product categories</label>
<div class="col-md-5 col-sm-6 col-xs-12">
     <?php
       build_category_tree($this, $locations, 0);
     ?>
     <div class="div-category">
          <ul class="li-category">
               <?php echo $locations?>
          </ul>
     </div>
     <?php

       function build_category_tree($f, &$output, $preselected, $parent = 0, $indent = "") {
            $ser_parent = '';
            $parentCategories = $f->product_category->getCategoryChaild($parent);
            foreach ($parentCategories as $key => $value) {
                 $selected = ($value["pcat_id"] == $ser_parent) ? "ckecked=\"true\"" : "";
                 $output .= "<li>" . $indent . "<input name='categories[]' value='" . $value["pcat_id"] . "' type='checkbox' /><span>" . $value["pcat_title"] . '</span></li>';
                 if ($value["pcat_id"] != $parent) {
                      build_category_tree($f, $output, $preselected, $value["pcat_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                 }
            }
       }
     ?>
</div>