<?php
  if (!$products) {
       return '';
  }
  $unit = isset($products['unt_unit_en']) ? $products['unt_unit_en'] : '';
  ?>
<div class="total-container" style="<?php echo ($rdfrom == 'profile') ? 'padding-top: 0px;' : '';?>">
  <div class="container">
    <section class="detail-top pb-4">
      <div class="row">
        <div class="col d-flex">
          <div class="det-img">
            <div class="prod-img">
              <!-- image gallery-->
              <div class="app-figure" id="zoom-fig">
                <?php if (!empty($products['default_img'])) {?>
                <a id="Zoom-1" class="MagicZoom" title="" href="uploads/product/<?php echo $products['default_img'][0]['pimg_image'];?>">
                <img alt = "<?php echo $products['default_img'][0]['pimg_alttag'];?>" src="uploads/product/<?php echo $products['default_img'][0]['pimg_image'];?>?scale.height=200"  />
                </a>
                <?php } else {?>
                <a id="Zoom-1" class="MagicZoom" title="" href="uploads/product/<?php echo $products['images'][0]['pimg_image'];?>">
                <img src="uploads/product/<?php echo $products['images'][0]['pimg_image'];?>?scale.height=200" alt ="<?php echo $products['images'][0]['pimg_alttag'];?>"
                  />
                </a>
                <?php }?>  
                <div class="selectors">
                  <?php foreach ($products['default_img'] as $img) {?>
                  <a data-zoom-id="Zoom-1" href="uploads/product/<?php echo $img['pimg_image'];?>" data-image="images/uploads/product/<?php echo $img['pimg_image'];?>">
                  <img srcset="uploads/product/<?php echo $img['pimg_image'];?>" src="uploads/product/<?php echo $img['pimg_image'];?>"
                    alt ="<?php echo $img['pimg_alttag'];?>" />
                  </a>
                  <?php } foreach ($products['images'] as $img) {?>
                  <a data-zoom-id="Zoom-1" href="uploads/product/<?php echo $img['pimg_image'];?>" data-image="images/uploads/product/<?php echo $img['pimg_image'];?>">
                  <img srcset="uploads/product/<?php echo $img['pimg_image'];?>" src="uploads/product/<?php echo $img['pimg_image'];?>" alt ="<?php echo $img['pimg_alttag'];?>" />
                  </a>
                  <?php }?>
                </div>
              </div>
              <!-- /image gallery -->
              <?php if(!$this->uid) { ?>
              <div class="favourite right-15 mt-2">
                <a href="javascript:;" data-toggle="modal" data-target="#loginModal">
                <i id="fav" class="far fa-heart"></i></a>
              </div>
              <?php } if($this->uid && $this->usr_grp == 'BY') { ?> 
              <div class="favourite right-15 mt-2">
                <a is-logged="<?php echo '1';?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($products['prd_id']));?>">
                <i id="fav" class="far fa-heart <?php echo (isset($products['isFavorite']) && !empty($products['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="p-desc">
            <div class="detail-txt">
              <h2 class="prod-title-b"><?php echo $products['prd_name_en']?></h2>
              <div class="p-cont">
                <?php if ($products['prd_offer_price_min'] > 0) {?>          
                <p><del>US $<?php echo $products['prd_price_min'];?></del> <span class="green-txt font-weight-bold">US $<?php echo $products['prd_offer_price_min'];?> </span>/ <?php echo $unit; ?> <br>
                  <?php } else { ?>
                <p><span class="green-txt font-weight-bold">US $<?php echo $products['prd_price_min']?> </span>/ <?php echo $unit; ?> <br>
                  <?php } ?>     
                  <span><?php echo $products['prd_moq']?> <?php echo $unit; ?></span> Minimum Order (MOQ)<br>
                  <?php if ($products['prd_is_stock_prod']) {?>
                  <span>Stock : </span> <span class="green-txt"><?php echo floatval($products['prd_stock_qty'])?></span> <?php echo $unit; ?>
                  <?php } ?>
                </p>
              </div>
              <div class="sub-det">
                <p>Category : <span class="green-txt">
                  <?php
                    $lastElementKey = count($products['category_name'])-1;
                    foreach($products['category_name'] as $key =>$prdcat) {?>
                  <a href="<?=site_url('supplier/supplier_product/'.encryptor($products['supm_id']).'/'.$prdcat['cat_id'])?>"><?php echo $prdcat['cat_title']?></a> <?php if($key != $lastElementKey) { ?> , <?php }?>
                  <?php } ?>
                  </span> <br> 
                  <!-- Lead Time : <span>25 days</span> <br> 
                  FOB Port : <span>Shenzhen</span> -->
                </p>
              </div>
              <a <?php if ($this->uid && !get_logged_user('usr_is_mail_verified') && $this->session->userdata('grp_slug') == 'BY') {?> href="javascript:;" data-toggle="modal"  data-target="#verif-mail" <?php } else if($this->uid && get_logged_user('usr_is_mail_verified')) { ?> href="<?php echo site_url($controller . '/contact-supplier/' . encryptor($products['prd_id']));?>" <?php } else {?> href="javascript:;" data-toggle="modal" data-target="#loginModal"  data-redrct="redirect_inquiry" data-redrct_to="<?php echo site_url($controller . '/contact-supplier/' . encryptor($products['prd_id']));?>" <?php }?> class="btn btn-success btn-green float-left">Inquire now</a>
              <?php
                if ($this->uid != $supplier_user_id['usr_id']) {
                     if ($this->uid && !get_logged_user('usr_is_mail_verified')) {
                          ?> 
              <button data-toggle="modal"  data-target="#verif-mail"   class="chat float-left ml-3"><i class="fas fa-comments"></i> <span>Chat with us</span></button>
              <?php } else if ($this->usr_grp != 'SA' && $this->usr_grp != 'SP' && $this->usr_grp != 'ST' && $this->usr_grp != 'SBA' && $this->usr_grp != 'LG' && $this->usr_grp != 'QC') {?>
              <button id="chat" data-sup_usr_id="<?php echo encryptor($supplier_user_id['usr_id']);?>" <?php if (!$this->uid) {?> href="javascript:;" data-toggle="modal" data-target="#loginModal"  data-redrct="redirect_chat" data-redrct_to="<?php echo site_url('user/chat/' . encryptor($supplier_user_id['usr_id']));?>" <?php } else {?> onclick="loadChatbox()" <?php }?> class="chat float-left ml-3"><i class="fas fa-comments"></i> <span>Chat with us</span></button>
              <?php
                }
                }
                ?>   
            </div>
          </div>
        </div>
      </div>
    </section>
    <input type="hidden" id="prd_details" data-pprice="<?php echo encryptor($products['prd_price_min']);?>" data-pid="<?php echo $products['prd_id'];?>" data-pname="<?php echo encryptor($products['prd_name_en']);?>" data-src="<?php echo encryptor(!empty($products['default_img']) ? 'uploads/product/' . $products['default_img'][0]['pimg_image'] : 'uploads/product/' . $products['images'][0]['pimg_image'])?>" >
    <!-- <p><a href="<?php echo site_url($controller . '/supplier_products/' . encryptor($products['prd_supplier'], 'E'));?>">+ view all products from supplier</a></p> -->
    <section class="company-d white-bg mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="company p-0 pr-2 w-30 align-top" style="text-decoration: none;">
              <!-- <a class="company p-0 pr-2 w-30 align-top" style="text-decoration: none;" href="<?php echo site_url('supplier/home/' . encryptor($products['supm_id']));?>"> -->
              <div class="name">
                <div class="clogo">
                  <a href="<?="http://".$products['supm_domain_prefix'] .'.'.$this->http_host?>">
                    <div class="logos">
                      <?php echo img(array("src" => $products['sup_default_image'], "id" => "", "alt" => $products['supm_name_en'], 'class' => 'img-fluid align-self-center'));?>
                    </div>
                  </a>
                </div>
                <a href="<?="http://".$products['supm_domain_prefix'] .'.'.$this->http_host?>">
                  <h2 class="prod-title-b"><?php echo $products['supm_name_en']?></h2>
                </a>
                <?php if ($this->usr_grp != 'SP' && $this->usr_grp != 'ST' && $this->usr_grp != 'SBA') {?>
                <div class="right-btns">
                <?php if(!$this->uid){ ?>
                  <div class="favourite">
                    <a href="javascript:;" data-toggle="modal" data-target="#loginModal">
                    <i id="fav" class="far fa-heart"></i></a>
                  </div>
                  <div class="follow">
                    <a href="javascript:;" data-toggle="modal" data-target="#loginModal"><i class="fas fa-plus"></i></a>
                  </div>
                <?php } else { ?>
                  <div class="favourite">
                    <a data-url="<?php echo site_url('favorites/addSupplierToFavoriteList/' . encryptor($products['supm_id']));?>"
                      is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>"
                      href="javascript:;" class="fav btnFav">
                    <i id="fav" class="far fa-heart <?php echo (!empty($products['isFav'])) ? 'fas red-icn' : '';?>"></i></a>
                  </div>
                  <div class="follow btnFollow <?php echo (!empty($products['isFol'])) ? ' green-icn' : '';?>" id="foll" 
                    data-url="<?php echo site_url('favorites/addToFollow/' . encryptor($products['supm_id']));?>"
                    is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>">
                    <a href="javascript:;" class="foll"><i class="fas fa-plus"></i></a>
                  </div>
                <?php } ?>
                </div>
                <?php }?>
              </div>
            </div>
            <div class="detail w-69 border-left pl-2">
              <div class="w-70 border-right">
                <div class="c-bx">
                  <div class="yr">
                    <p class="year"><?php echo date('Y') - $products['supm_reg_year'];?> year</p>
                  </div>
                </div>
                <ul class="loc">
                  <li>Location : <span><?php echo $products['ctr_name'] . ' (' . $products['stt_name'] . ')';?></span></li>
                  <li>
                    <div class="icon-details">
                      <div class="icon verified">Verified Information</div>
                    </div>
                  </li>
                  <li>Response : <span>High / Less than 24h</span></li>
                  <li><a href="<?="http://".$products['supm_domain_prefix'] .'.'.$this->http_host?>">Go to Supplier Page</a></li>
                </ul>
              </div>
              <div class="rating-container w-30 align-top pl-2 text-center">
                <!-- <a href="" class="btn btn-success btn-green">View 360</a> -->
                <a data-toggle="modal" id="_360_a" class="nav-link btn-360" href="<?php echo site_url() . 'supplier/get_360/' . encryptor($products['supm_id']);?>" data-target="#Modal-360"> <img src="images/360-o.png" alt="" class="mt-3"></a>
                <div class="clearfix"></div>
                <!-- <div class="view-all mt-3">
                  <p><a href="">+ view all products from supplier</a></p>
                  </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="detail-area">
      <div class="row">
        <div class="col-md-9 col-sm-12">
          <!-- prod detail -->
          <div class="pr-dt-container">
            <div class="det-bx">
              <div class="dt-heading">
                <h2>product Description</h2>
              </div>
              <p><?php echo $products['prd_desc'];?></p>
            </div>
            <div class="clearfix"></div>
            <div class="det-bx">
              <div class="dt-heading">
                <h2>specification</h2>
              </div>
              <div class="det">
                <?php foreach ($products['specification'] as $spec) {?>
                <dl class="single-item-entry">
                  <dt class="d-entry-item"><?php echo $spec['psp_key_en']?> :</dt>
                  <dd class="d-entry-item-value"><?php echo $spec['psp_val_en']?></dd>
                </dl>
                <?php }?>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <!-- /prod detail -->
        </div>
        <?php if (isset($products['related_products']) && !empty($products['related_products'])) {?>
        <div class="col-md-3 col-sm-12">
          <div class="related">
            <h3 class="heading3">RELATED PRODUCTS</h3>
            <ul class="rel-prod">
              <?php foreach ($products['related_products'] as $prod) {?>
              <li>
                <a href="<?php echo site_url('product/product_details/' . $prod['prd_id']);?>" style="text-decoration:none">
                  <div class="pbox">
                    <div class="img">
                      <?php echo img(array('src' => $prod['image'], 'class' => 'img-fluid'));?>
                    </div>
                    <div class="details">
                      <h4 class="prod-title-s"><?php echo $prod['name'];?></h4>
                      <p>From <span class="price-text">US $<?php echo $prod['price'];?></span> / <?php echo $prod['unit'];?> <br>
                        <span class="moq"><?php echo $prod['moq'];?> <?php echo $prod['unit'];?>  (MOQ)</span>
                      </p>
                    </div>
                  </div>
                </a>
              </li>
              <?php }?>
            </ul>
          </div>
        </div>
        <?php }?>
      </div>
    </section>
  </div>
  <!-- 360 modal -->
  <div id="Modal-360" class="modal fade modal-360" role="dialog">
    <div class="modal-dialog">
      <!--Filter Modal content-->
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-body 360_body">
        </div>
      </div>
    </div>
  </div>
  <!-- /360 modal -->
  <!-- Chat DIV -->
  <div class="chatbox d-none" id="chatbox">
    <span class="chat-text"><?php echo substr($products['supm_name_en'], 0, 20) . '...'?></span>
    <iframe id="ddd" src="" scrolling="no" frameborder="0" width="280px" height="450px" style="border:0; margin:0; padding: 0;">
    </iframe>       
    <div id="close-chat" onclick="closeChatbox()">&times;</div>
    <div id="minim-chat" onclick="minimChatbox()"><span class="minim-button">&minus;</span></div>
    <div id="maxi-chat" onclick="maximChatbox()"><span class="maxi-button">&plus;</span></div>
  </div>
  <!-- /Chat DIV -->                
</div>
<?php if ($rdfrom == 'profile') {?>
<section class="qd mt-4 pb-4">
  <div class="container">
    <div class="row">
      <div class="col qdw text-center">
        <h2 class="heading4 mb-4 text-center">Quick Details</h2>
        <ul>
          <li>
            <div class="qdc">
              <div class="qd-img">
                <img src="images/location-icon.png" alt="">
              </div>
              <div class="qd-desc">
                Location : <span><?php echo $suppliers['ctr_name']?> (<?php echo $suppliers['stt_name']?>)</span>
              </div>
            </div>
          </li>
          <li>
            <div class="qdc">
              <div class="qd-img">
                <img src="images/year-icon.png" alt="">
              </div>
              <div class="qd-desc">
                Year Established : <span><?php echo $suppliers['supm_reg_year']?></span>
              </div>
            </div>
          </li>
          <li class="p-0">
            <div class="qdc">
              <div class="qd-img">
                <img src="images/business-icon.png" alt="">
              </div>
              <div class="qd-desc">
                Business Type : <span>Manufacturer</span>
              </div>
            </div>
          </li>
          <li>
            <div class="qdc">
              <div class="qd-img">
                <img src="images/verification-icon.png" alt="">
              </div>
              <div class="qd-desc">
                Verification : <span class="green-txt">Verified Profile</span>
              </div>
            </div>
          </li>
          <li>
            <div class="qdc">
              <div class="qd-img">
                <img src="images/rating-icon.png" alt="">
              </div>
              <div class="qd-desc">
                Customer Rating :
                <span>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
                </span>
              </div>
            </div>
          </li>
          <li>
            <div class="qdc">
              <div class="qd-img">
                <img src="images/markets-icon.png" alt="">
              </div>
              <div class="qd-desc">
                Main Market : <span>Domestic Market, Central America, Europe</span>
              </div>
            </div>
          </li>
        </ul>
        <?php if ($this->uid != $supplier_user_id['usr_id']) {?>
        <a href="<?php echo site_url('supplier/profile/' . encryptor($suppliers['supm_id']))?>" class="btn btn-success btn-green mt-4">ABOUT COMPANY</a>
        <?php }?>  
      </div>
    </div>
  </div>
</section>
<!-- /company webpage -->
<!-- contact -->
<section class="grey-bg pb-4" id="scontact">
  <div class="container">
    <h2 class="heading2 mb-4 pt-4 text-center">Contact Us</h2>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <!-- details -->
        <div class="supp-det">
          <div class="det w-750">
            <dl class="single-item-entry">
              <dt class="d-entry-item">Company Name :</dt>
              <dd class="d-entry-item-value"><?= $suppliers['supm_name_en']?></dd>
            </dl>
            <dl class="single-item-entry">
              <dt class="d-entry-item">Operational Address :</dt>
              <dd class="d-entry-item-value"><?= $suppliers['supm_address_en']?></dd>
            </dl>
            <dl class="single-item-entry">
              <dt class="d-entry-item">Country/Region :</dt>
              <dd class="d-entry-item-value"><?= $suppliers['ctr_name']?> / <?= $suppliers['stt_name']?></dd>
            </dl>
            <dl class="single-item-entry">
              <dt class="d-entry-item">Website :</dt>
              <dd class="d-entry-item-value"><?= $suppliers['supm_website']?></dd>
            </dl>
            <dl class="single-item-entry">
              <dt class="d-entry-item">Website on fujeeka :</dt>
              <dd class="d-entry-item-value">companyname.fujeeka.com</dd>
            </dl>
          </div>
        </div>
        <div class="clearfix"></div>
        <!-- /details -->
      </div>
    </div>
  </div>
</section>
<?php
  }?>