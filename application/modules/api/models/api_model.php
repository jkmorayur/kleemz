<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class api_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       /**
        * TODO: delete this function on next live
        * @param type $limit
        * @return type
        */
       function getAllCategories($limit) {
            $imageUrl = site_url('assets/uploads/category') . '/';
            if ($limit > 0) {
                 $this->db->limit($limit);
            }
            return $this->db->select(tbl_category . ".*, CONCAT('$imageUrl',cat_image) AS cat_image, CONCAT('$imageUrl',cat_page_banner) AS cat_page_banner", false)
                            ->order_by('cat_title', 'ASC')->get_where(tbl_category, array('cat_status' => 1))->result_array();
       }

       function getAllFirmCategories($limit) {
            $imageUrl = site_url('assets/uploads/category') . '/';
            if ($limit > 0) {
                 $this->db->limit($limit);
            }
            return $this->db->select(tbl_category . ".*, CONCAT('$imageUrl',cat_image) AS cat_image, CONCAT('$imageUrl',cat_page_banner) AS cat_page_banner", false)
                            ->order_by('cat_title', 'ASC')->get_where(tbl_category, array('cat_status' => 1))->result_array();
       }

       function getFirmsByCategory($catid, $limit, $page) {
            if (!empty($catid)) {
                 if ($limit) {
                      $this->db->limit($limit, $page);
                 }
                 $firms = $this->db->select('scat_master_id')->where('scat_category', $catid)
                                 ->get(tbl_supplier_categories)->result_array();
                 if (!empty($firms)) {
                      $firms = array_column($firms, 'scat_master_id');

                      /**/
                      $selectOrderDetails = array(
                          tbl_supplier_master . '.supm_id',
                          tbl_supplier_master . '.supm_name',
                          tbl_supplier_master . '.supm_number',
                          tbl_supplier_master . '.supm_email',
                          tbl_supplier_master . '.supm_address',
                          tbl_supplier_master . '.supm_market',
                          tbl_users . '.usr_id',
                          tbl_users . '.usr_first_name',
                          tbl_users . '.usr_last_name',
                          tbl_users . '.usr_username',
                          tbl_countries . '.*',
                          tbl_market_places . '.*'
                      );
                      /**/

                      $suppliers = $this->db->select($selectOrderDetails, false)
                                      ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                      ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                      ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                                      ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->where_in(tbl_supplier_master . '.supm_id', $firms)->get(tbl_supplier_master)->result_array();

                      if ($suppliers) {
                           foreach ($suppliers as $key => $value) {
                                $imUrlShop = "'" . site_url('assets/uploads/shops') . "/'";

                                $suppliers[$key]['shop_images'] = $this->db->select('CONCAT(' . $imUrlShop . ',ssi_image) AS ssi_image', false)
                                                ->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $value['supm_id']))->result_array();

                                $imUrlBaner = "'" . site_url('assets/uploads/home_banner') . "/'";
                                $suppliers[$key]['banner_images_app'] = $this->db->select('CONCAT(' . $imUrlBaner . ',sbi_image) AS sbi_image', false)->order_by('sbi_is_default', 'DESC')
                                                ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $value['supm_id'], 'sbi_type' => 2))->result_array();

                                $suppliers[$key]['banner_images_web'] = $this->db->select('CONCAT(' . $imUrlBaner . ',sbi_image) AS sbi_image', false)->order_by('sbi_is_default', 'DESC')
                                                ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $value['supm_id'], 'sbi_type' => 1))->result_array();

                                $suppliers[$key]['contacts'] = $this->db->get_where(tbl_supplier_contacts, array('spc_master_id' => $value['supm_id']))->result_array();

//                                $suppliers[$key]['markets'] = $this->db->select(tbl_market_cate_assoc . '.*,' . tbl_category . '.*')
//                                                ->join(tbl_category, tbl_market_cate_assoc . '.mca_category_id = ' . tbl_category . '.cat_id')
//                                                ->where(tbl_market_cate_assoc . '.mca_market_id', $value['supm_market'])
//                                                ->get(tbl_market_cate_assoc)->result_array();
//                                $suppliers[$key]['categories'] = explode(',', $this->db->select('GROUP_CONCAT(scat_category) AS categories')
//                                                ->where('scat_master_id', $value['supm_id'])->get(tbl_supplier_categories)->row()->categories);
//                                $suppliers[$key]['buildingCates'] = explode(',', $this->db->select('GROUP_CONCAT(sbca_category) AS sbca_category')
//                                                ->where('sbca_supplier', $value['supm_id'])->get(tbl_supplier_buildings_cate_assoc)->row()->sbca_category);
//                                $suppliers[$key]['supplier_user_id'] = $this->common_model->getUserIdFromSupId($value['supm_id']);
//                                $suppliers[$key]['keywords'] = $this->db->get_where(tbl_supplier_keywords, array('supk_supplier' => $value['supm_id']))->result_array();                                
                           }
                      }
                      return $suppliers;
                 }
            }
            return false;
       }

       function getProductParentCategories() {
//            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
//            $this->db->select("gcat.*, CONCAT('$imageUrl',gcat.pcat_image) AS pcat_image , gcat.pcat_parent AS pcat_parent, "
//                    . "gcat.pcat_desc AS pcat_desc, gcat.pcat_title AS pcat_title, "
//                    . "gcat.pcat_id AS pcat_id, gcat2.pcat_id AS child_category_id, gcat2.pcat_title AS child_category", false);
//            $this->db->from(tbl_products_category . ' gcat');
//            $this->db->join(tbl_products_category . ' gcat2', 'gcat.pcat_id = gcat2.pcat_parent', 'left');
//            $this->db->where('gcat.pcat_parent', 0);
//            $f = $this->db->get()->result_array();
//            debug($f);

            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
            $rootCategories = $this->db->select(tbl_products_category . ".*, CONCAT('$imageUrl',pcat_image) AS pcat_image", false)
                    ->order_by('pcat_order', 'ASC')->get_where(tbl_products_category, array('pcat_parent' => 0, 'pcat_status' => 1))
                    ->result_array();

            foreach ($rootCategories as $key => $value) {
                 $subCategory = $this->db->get_where(tbl_products_category, array('pcat_parent' => $value['pcat_id']))->row_array();

                 $rootCategories[$key]['child_category_id'] = isset($subCategory['pcat_id']) ? $subCategory['pcat_id'] : null;
                 $rootCategories[$key]['child_category'] = isset($subCategory['pcat_title']) ? $subCategory['pcat_title'] : null;
            }

            return $rootCategories;
       }

//       function getProductParentCategories() {
//            $imageUrl = site_url('assets/uploads/category') . '/';
//            $this->db->select("gcat.*, CONCAT('$imageUrl',gcat.pcat_image) AS pcat_image , gcat.pcat_parent AS pcat_parent, "
//                    . "gcat.pcat_desc AS pcat_desc, gcat.pcat_title AS pcat_title, "
//                    . "gcat.pcat_id AS pcat_id, gcat2.pcat_id AS parent_category_id, gcat2.pcat_title AS parent_category, "
//                    . "gcat3.pcat_id AS root_category_id, gcat3.pcat_title AS root_category", false);
//            $this->db->from(tbl_products_category . ' gcat');
//            $this->db->join(tbl_products_category . ' gcat2', 'gcat.pcat_parent = gcat2.pcat_id', 'left');
//            $this->db->join(tbl_products_category . ' gcat3', 'gcat2.pcat_parent = gcat3.pcat_id', 'left');
//            return $this->db->get()->result_array();
//       }

       function getProductSubCategories($parent_cat) {
            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
            return $this->db->select(tbl_products_category . ".*, CONCAT('$imageUrl',pcat_image) AS pcat_image, CONCAT('$imageUrl',pcat_image) AS pcat_page_banner", false)
                            ->order_by('pcat_order', 'ASC')->get_where(tbl_products_category, array('pcat_parent' => $parent_cat, 'pcat_status' => 1))->result_array();
       }

       function getProducts($cateId = 0, $limit, $page, $town = 0, $credential = 0) {
            $lat = '';
            $lan = '';
            if (!empty($town)) {
                 $townDetails = $this->db->select('mar_lat, mar_long')->get_where(tbl_market_places, array('mar_id' => $town))->row_array();
                 $lat = isset($townDetails['mar_lat']) ? $townDetails['mar_lat'] : '';
                 $lan = isset($townDetails['mar_long']) ? $townDetails['mar_long'] : '';
            }
            $imageUrl = site_url('assets/uploads/product') . '/thumb_';
            if ($cateId > 0) {
                 $products = $this->db->select('pcata_product')->where('pcata_category', $cateId)->get(tbl_products_category_assoc)->result_array();
                 $products = !empty($products) ? array_column($products, 'pcata_product') : '';

                 $this->db->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_product = ' . tbl_products_master . '.prd_id', 'RIGHT');
                 if ($town > 0) {
                      $nearestTowns = $this->nearestTown($lat, $lan, DEF_MILE, $limit);
                      $this->db->where_in(tbl_products_stock_master . '.psm_market_place', array_column($nearestTowns, 'mar_id'));
                 }
                 $this->db->where_in(tbl_products_master . '.prd_id', $products);
                 $this->db->where(tbl_products_stock_master . '.psm_status', 1);
            }
            if ($limit) {
                 $this->db->limit($limit, $page);
            }
            $products = $this->db->get(tbl_products_master)->result_array();
            if (!empty($products)) {
                 foreach ($products as $key => $value) {

                      //Check if product stock in favorite list
                      $products[$key]['fav_id'] = 0;
                      if (!empty($credential)) {
                           $isFav = $this->db->where(array('fav_consign_id' => $value['psm_id'], 'fav_added_by' => $credential))
                                           ->like('fav_consign', 'STK', 'BOTH')->get(tbl_favorite_list)->row_array();
                           $products[$key]['fav_id'] = isset($isFav['fav_id']) ? (int) $isFav['fav_id'] : 0;
                      }

                      $products[$key]['specification'] = $this->db->order_by("psp_id", "asc")->
                                      get_where(tbl_products_specification, array('psp_product' => $value['prd_id']))->result_array();
                      $products[$key]['keyword'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $value['prd_id']))->result_array();
                      if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                           $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                           ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id'],
                                               'pimg_id' => $value['prd_default_image']))->row_array();
                      } else {

                           $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                           ->limit(1)->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                      }
                      $products[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                      $products[$key]['images'] = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                      ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                      $products[$key]['categories'] = $this->db->get_where(tbl_products_category_assoc, array('pcata_product' => $value['prd_id']))->result_array();
                      $products[$key]['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcata_category) AS pcata_category')
                                      ->where(array('pcata_product' => $value['prd_id']))
                                      ->get(tbl_products_category_assoc)->row()->pcata_category);
                 }
                 return $products;
            }
            return array();
       }

       function getCategoryDetails($catId) {
            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
            return $this->db->select(tbl_products_category . ".*, CONCAT('$imageUrl',pcat_image) AS pcat_image, CONCAT('$imageUrl',pcat_image) AS pcat_page_banner", false)
                            ->get_where(tbl_products_category, array('pcat_id' => $catId))->row_array();
       }

       function townSearch($town = '', $limit, $status) {
            if (!empty($town)) {
                 $this->db->like('mar_name', $town, 'both');
            }
            return $this->db->select('mar_id, mar_name, mar_lat, mar_long, mar_status, ctr_name, dit_district')
                            ->where('mar_status', $status)->limit($limit)->join(tbl_countries, tbl_market_places . '.mar_country_id=' . tbl_countries . '.ctr_id', 'LEFT')->join(tbl_states, tbl_market_places . '.mar_state_id=' . tbl_states . '.stt_id', 'LEFT')->join(tbl_district, tbl_market_places . '.mar_district=' . tbl_district . '.dit_id', 'LEFT')->get(tbl_market_places)->result_array();
       }

       function nearestTown($lat, $lng, $miles, $limit) {
            //In miles
            return $this->db->query("SELECT mar_id, mar_name, mar_lat, mar_long, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN(($lat - mar_lat)*pi()/180/2),2)
          +COS($lat*pi()/180 )*COS(mar_lat*pi()/180)
          *POWER(SIN(($lng-mar_long)*pi()/180/2),2))) 
          as distance FROM " . tbl_market_places . " WHERE 
          mar_long between ($lng-$miles/cos(radians($lat))*69) 
          and ($lng+$miles/cos(radians($lat))*69) 
          and mar_lat between ($lat-($miles/69)) 
          and ($lat+($miles/69)) 
          having distance < $miles ORDER BY distance limit " . $limit)->result_array();
       }

       public function getHomeBanners($town = '', $cate_id) {
            $banner = array();
            $bannerLimit = 8;
            if (!empty($town)) {
                 $ifBannerSet = $this->db->get_where(tbl_banner, array('bnr_category' => $cate_id, 'bnr_town' => $town))->num_rows();

                 if ($ifBannerSet > 0) {
                      $this->db->where('bnr_town', $town);
                      $ownBanners = $this->db->select('bnr_image')->where('bnr_status', 1)->where('bnr_category', $cate_id)
                                      ->order_by('bnr_order', 'asc')->get(tbl_banner)->result_array();

                      $balanceBanner = abs($bannerLimit - $ifBannerSet);

                      $defBanners = $this->db->select('bnr_image')->where(array('bnr_status' => 1, 'bnr_town' => 0))
                                      ->where('bnr_category', $cate_id)->limit($balanceBanner)->order_by('bnr_order', 'asc')->get(tbl_banner)->result_array();
                      $banners = array_merge($ownBanners, $defBanners);
                 } else {
                      $this->db->where('bnr_town', 0);
                      $banners = $this->db->select('bnr_image')->where('bnr_status', 1)->where('bnr_category', $cate_id)->limit($bannerLimit)
                                      ->order_by('bnr_order', 'asc')->get(tbl_banner)->result_array();
                 }
            } else {
                 $this->db->where('bnr_town', 0);
                 $banners = $this->db->select('bnr_image')->where('bnr_status', 1)->where('bnr_category', $cate_id)->limit($bannerLimit)
                                 ->order_by('bnr_order', 'asc')->get(tbl_banner)->result_array();
            }
            $ads = $this->db->select('add_url,add_image')->order_by('add_order', 'asc')->where('add_show_on_home_page', 1)->get(tbl_advertisement)->result_array();
            $banner['banners'] = array();
            $banner['ads'] = array();
            if ($banners) {
                 $banner['banners'] = preg_filter('/^/', site_url() . 'assets/uploads/banner/thumb_', array_column($banners, 'bnr_image'));
            }
            foreach ($ads as $key => $ad) {
                 $ads[$key]['add_image'] = site_url() . 'assets/uploads/advt/' . $ad['add_image'];
            }
            $banner['ads'] = $ads;
            return $banner;
       }

       public function updatePushyToken($pushy_token, $col, $val) {
            if ($pushy_token) {
                 return $this->db->where($col, $val)->update(tbl_users, array('usr_pushy_token' => $pushy_token));
            }
            return array();
       }

       function getNearestFoodPlaces($lat, $lng, $miles, $limit) {
            $imageUrl = site_url('assets/uploads/product') . '/thumb_';
            $foodCategories = $this->db->select('GROUP_CONCAT(scat_master_id) AS scat_master_id')
                            ->where_in('scat_category', array(25, 23))->get(tbl_supplier_categories)->row_array();
            if (isset($foodCategories['scat_master_id']) && !empty($foodCategories['scat_master_id'])) {
                 $foodCategories = explode(',', $foodCategories['scat_master_id']);
                 $this->db->where_in(tbl_supplier_master . '.supm_id', $foodCategories);
            }
            $footCategories = (isset($footCategories['scat_master_id']) && !empty($footCategories['scat_master_id'])) ? $footCategories['scat_master_id'] : '';
            $towns = $this->db->select("supm_id, supm_name, supm_latitude, supm_shop_charges, supm_longitude, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN(($lat - supm_latitude)*pi()/180/2),2)+COS($lat*pi()/180 )*COS(supm_latitude*pi()/180)
          *POWER(SIN(($lng-supm_longitude)*pi()/180/2),2))) as distance," . tbl_market_places . '.*', false)
                            ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market')
                            ->WHERE("supm_longitude between ($lng-$miles/cos(radians($lat))*69) 
          and ($lng+$miles/cos(radians($lat))*69) 
          and supm_latitude between ($lat-($miles/69)) 
          and ($lat+($miles/69))")->having("distance < $miles")->order_by("distance limit " . $limit)
                            ->get(tbl_supplier_master)->result_array();

            foreach ($towns as $key1 => $sups) {
                 $foodProducts = $this->db->select(tbl_products_stock_master . '.*,' . tbl_products_master . '.*')
                                 ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product')
                                 ->where(tbl_products_stock_master . '.psm_supplier', $sups['supm_id'])
                                 ->where(tbl_products_stock_master . '.psm_status', 1)->get(tbl_products_stock_master)->result_array();
                 if (!empty($foodProducts)) {
                      foreach ($foodProducts as $key => $value) {
                           $foodProducts[$key]['specification'] = $this->db->order_by("psp_id", "asc")->
                                           get_where(tbl_products_specification, array('psp_product' => $value['prd_id']))->result_array();
                           $foodProducts[$key]['keyword'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $value['prd_id']))->result_array();
                           if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                                $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                                ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id'],
                                                    'pimg_id' => $value['prd_default_image']))->row_array();
                           } else {

                                $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                                ->limit(1)->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                           }
                           $foodProducts[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                           $foodProducts[$key]['images'] = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                           ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                           $foodProducts[$key]['categories'] = $this->db->get_where(tbl_products_category_assoc, array('pcata_product' => $value['prd_id']))->row_array();
                           $foodProducts[$key]['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcata_category) AS pcata_category')
                                           ->where(array('pcata_product' => $value['prd_id']))
                                           ->get(tbl_products_category_assoc)->row()->pcata_category);
                      }
                      $towns[$key1]['products'] = $foodProducts;
                 } else {
                      $towns[$key1]['products'] = '';
                 }
            }
            return $towns;
       }

       public function getProductDetails($id = '', $filter = '', $lat, $lng, $miles, $credential = 0, $supplier = 0) {
            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
            $this->db->select(tbl_products_master . '.*,' . tbl_supplier_master . '.*')
                    ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_master . '.prd_supplier', 'LEFT');
            $products = $this->db->where(tbl_products_master . '.prd_id', $id)->get(tbl_products_master)->row_array();
            $products = array_filter($products);
            if (!empty($products)) {
                 $products['specification'] = $this->db->order_by("psp_id", "asc")->get_where(tbl_products_specification, array('psp_product' => $products['prd_id']))->result_array();
                 $products['keyword'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $products['prd_id']))->result_array();
                 if (isset($products['prd_default_image']) && !empty($products['prd_default_image'])) {
                      $images = $this->db->get_where(tbl_products_images, array('pimg_product' => $products['prd_id'], 'pimg_id' => $products['prd_default_image']))->row_array();
                 } else {
                      $images = $this->db->limit(1)->get_where(tbl_products_images, array('pimg_product' => $products['prd_id']))->row_array();
                 }
                 $products['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                 $products['images'] = $this->db->get_where(tbl_products_images, array('pimg_product' => $products['prd_id']))->result_array();

                 $products['categories'] = $this->db->select(tbl_products_category_assoc . '.*,' . tbl_products_category . '.pcat_title')
                                 ->join(tbl_products_category, tbl_products_category . '.pcat_id = ' . tbl_products_category_assoc . '.pcata_category', 'LEFT')
                                 ->get_where(tbl_products_category_assoc, array('pcata_product' => $products['prd_id']))->result_array();

                 $products['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcata_category) AS pcata_category')
                                 ->where(array('pcata_product' => $products['prd_id']))
                                 ->get(tbl_products_category_assoc)->row()->pcata_category);
                 //TODO: lat lon 
                 if (!empty($lat) && !empty($lat)) {
                      if ($supplier > 0) {
                           $this->db->where(tbl_supplier_master . '.supm_id', $supplier);
                      }
                      $products['suppliers'] = $this->db->select(tbl_products_stock_master . '.*,' . tbl_supplier_master . '.*,' . tbl_supplier_shop_images . '.*, ' .
                                              "(3956 * 2 * ASIN(SQRT( POWER(SIN(($lat - " . tbl_supplier_master .
                                              ".supm_latitude)*pi()/180/2),2)+COS($lat*pi()/180 )*COS(" . tbl_supplier_master . ".supm_latitude*pi()/180) * POWER(SIN(($lng- " . tbl_supplier_master . ".supm_longitude)*pi()/180/2),2)))) as distances", false)
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_stock_master . '.psm_supplier', 'LEFT')
                                      ->join(tbl_supplier_shop_images, tbl_supplier_shop_images . '.ssi_id = ' . tbl_supplier_master . '.supm_default_image', 'LEFT')
                                      ->where(tbl_products_stock_master . '.psm_product', $id)->where(tbl_products_stock_master . '.psm_status', 1)
                                      ->where(tbl_supplier_master . ".supm_longitude between ($lng-$miles/cos(radians($lat))*69) and ($lng+$miles/cos(radians($lat))*69) and " . tbl_supplier_master . ".supm_latitude between ($lat-($miles/69)) and ($lat+($miles/69))")
                                      ->having("distances < $miles")
                                      ->get(tbl_products_stock_master)->result_array();
                 } else {
                      if ($supplier > 0) {
                           $this->db->where(tbl_supplier_master . '.supm_id', $supplier);
                      }
                      $products['suppliers'] = $this->db->select(tbl_products_stock_master . '.*,' . tbl_supplier_master . '.*,' . tbl_supplier_shop_images . '.*')
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_stock_master . '.psm_supplier', 'LEFT')
                                      ->join(tbl_supplier_shop_images, tbl_supplier_shop_images . '.ssi_id = ' . tbl_supplier_master . '.supm_default_image', 'LEFT')
                                      ->where(tbl_products_stock_master . '.psm_product', $id)->where(tbl_products_stock_master . '.psm_status', 1)->get(tbl_products_stock_master)->result_array();
                 }

                 $products['suppliers'] = array_filter($products['suppliers']);
                 if (!empty($products['suppliers'])) {
                      foreach ($products['suppliers'] as $key => $value) {

                           $products[$key]['fav_id'] = 0;
                           if (!empty($credential)) {
                                $isFav = $this->db->where(array('fav_consign_id' => $value['psm_id'], 'fav_added_by' => $credential))
                                                ->like('fav_consign', 'STK', 'BOTH')->get(tbl_favorite_list)->row_array();
                                $products['suppliers'][$key]['fav_id'] = isset($isFav['fav_id']) ? (int) $isFav['fav_id'] : 0;
                           }

                           $products['suppliers'][$key]['stock_qty'] = $this->db->select(tbl_products_stock_details . '.*,' . tbl_products_units . '.*')
                                           ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                           ->get_where(tbl_products_stock_details, array('pdsm_stock_master' => $value['psm_id']))->result_array();

                           $products['suppliers'][$key]['stock_qty'] = array_filter($products['suppliers'][$key]['stock_qty']);
                           $products['suppliers'][$key]['stock_images'] = $this->db->select(tbl_products_stock_images . ".*, CONCAT('$imageUrl',psi_image) AS psi_image", false)
                                           ->get_where(tbl_products_stock_images, array('psi_product_master' => $value['psm_id']))->result_array();

                           $products['suppliers'][$key]['stock_images'] = array_filter($products['suppliers'][$key]['stock_images']);
                      }
                 }
            }
            return $products;
       }

       public function getUserByIdentityOTP($token, $otp) {
            $now = date('Y-m-d H:i:s');
            if (!empty($token) && !empty($otp)) {
                 return $this->db->select(tbl_users . '.*,ABS(TIMESTAMPDIFF(MINUTE, ' . "'" . $now . "'" . ', usr_created_date)) AS otp_expry', false)
                                 ->like('usr_token', trim($token), 'both')->get_where(tbl_users, array('usr_otp' => trim($otp)))->row_array();
            } else {
                 return false;
            }
       }

       public function getUserByIdentityOTPForgetPassword($token, $otp) {
            $now = date('Y-m-d H:i:s');
            if (!empty($token) && !empty($otp)) {
                 return $this->db->select(tbl_users . '.*,ABS(TIMESTAMPDIFF(MINUTE, ' . "'" . $now . "'" . ', usr_forgotten_password_time)) AS otp_expry, NOW() AS n', false)
                                 ->get_where(tbl_users, array('usr_token' => $token, 'usr_forgotten_password_otp' => $otp))->row_array();
            } else {
                 return false;
            }
       }

       public function getUserByIdentity($identity) {
            if (!empty($identity)) {
                 $identityColomn = 'usr_phone';
                 //$user = $this->db->get_where(tbl_users, array($identityColomn => $identity))->row_array();

                 $user = $this->db->select(tbl_users . '.*, ' . tbl_users_groups . '.group_id as group_id, ' .
                                         tbl_groups . '.name as group_name, ' .
                                         tbl_groups . '.description as group_desc, branch.usr_username AS branch'
                                 )
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->like(tbl_users . '.' . $identityColomn, trim($identity), 'both')->where(tbl_groups . '.id', 4)->get(tbl_users)->row_array();
                 return $user;
            } else {
                 return false;
            }
       }

       function verifyUser($userId, $userdata) {
            $ifalreadyExists = $this->db->get_where(tbl_users, "usr_id != " . $userdata['usr_id'] .
                            " AND usr_active = 0 AND (usr_email = '" . trim($userdata['usr_email']) . "' OR usr_phone = '" . trim($userdata['usr_phone']) . "')")->result_array();
            if (!empty($ifalreadyExists)) {
                 foreach ($ifalreadyExists as $key => $value) {
                      $this->db->delete(tbl_users, array('usr_id' => $value['usr_id']));
                      $this->db->delete(tbl_users_groups, array('user_id' => $value['usr_id']));
                 }
            }
            $this->db->where('usr_id', $userId);
            $this->db->update(tbl_users, array('usr_active' => 1));
            return true;
       }

       public function searchProducts($keyword, $limit, $page) {

            $imageUrl = site_url('assets/uploads/product') . '/thumb_';
            $this->db->distinct(tbl_products_master . '.*,' . tbl_products_stock_master . '.psm_id');
            $this->db->join(tbl_products_stock_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product', 'LEFT');
            if (!empty($keyword)) {
                 $this->db->like(tbl_products_master . '.prd_keywords', $keyword, 'both');
            }
            if ($limit) {
                 $this->db->limit($limit, $page);
            }
            $this->db->where(tbl_products_stock_master . '.psm_id > 0');
            $this->db->where(tbl_products_stock_master . '.psm_status', 1);
            $this->db->group_by(tbl_products_master . '.prd_id');
            $products = $this->db->get(tbl_products_master)->result_array();
            if (!empty($products)) {
                 foreach ($products as $key => $value) {
                      $products[$key]['specification'] = $this->db->order_by("psp_id", "asc")->
                                      get_where(tbl_products_specification, array('psp_product' => $value['prd_id']))->result_array();
                      $products[$key]['keyword'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $value['prd_id']))->result_array();
                      if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                           $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                           ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id'],
                                               'pimg_id' => $value['prd_default_image']))->row_array();
                      } else {

                           $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                           ->limit(1)->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                      }
                      $products[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                      $products[$key]['images'] = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                      ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                      $products[$key]['categories'] = $this->db->select(tbl_products_category_assoc . '.*,' . tbl_products_category . '.pcat_title')
                                      ->join(tbl_products_category, tbl_products_category . '.pcat_id = ' . tbl_products_category_assoc . '.pcata_category', 'LEFT')
                                      ->get_where(tbl_products_category_assoc, array('pcata_product' => $value['prd_id']))->result_array();
                      $products[$key]['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcata_category) AS pcata_category')
                                      ->where(array('pcata_product' => $value['prd_id']))
                                      ->get(tbl_products_category_assoc)->row()->pcata_category);
                 }
                 return $products;
            }
            return array();
       }

       function autocompleteProduct($keyword, $limit) {
            if (!empty($keyword)) {
                 if ($limit) {
                      $this->db->limit($limit);
                 }

                 return $this->db->select('pkwd_val_en')->like('pkwd_val_en', $keyword, 'both')->get(tbl_products_keyword)->result_array();
            }
       }

       public function getUserDetailsFromToken($token) {
            return $this->db->select('usr_company,usr_username, usr_first_name,usr_last_name,usr_id, '
                                    . 'usr_active,usr_avatar,usr_email,usr_is_mail_verified')
                            ->where('usr_token', $token)->get(tbl_users)->row_array();
       }

       public function getSuppliers($id = '', $offset = '', $limit = '', $uid = '') {

            $sup = array();
            $profile = array();
            $this->db->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                    ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                    ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                    ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT');
            if ($id) {
                 $this->db->where('supm_id', $id);
            } else {
                 $this->db->where('supm_status', 1)->limit($limit, $offset);
            }

            $supliers = $this->db->where('group_id', 2)->get(tbl_supplier_master)->result_array();
            
            foreach ($supliers as $key => $val) {

                 $sup_id = $this->common_model->getUserIdFromSupId($val['supm_id']);

                 if (!$id) {
                      
                      $sup[$key]['sup_fb'] = $val['supm_fb'];
                      $sup[$key]['sup_insta'] = $val['supm_insta'];
                      $sup[$key]['supm_whatsapp'] = $val['supm_wp_app'];
                      $sup[$key]['sup_twitter'] = $val['supm_twitter'];
                      $sup[$key]['sup_youtube'] = $val['supm_youtube'];
                      $sup[$key]['sup_linkedin'] = $val['supm_linkedin'];
                      $sup[$key]['sup_web'] = $val['supm_website'];
                      
                      $sup[$key]['type'] = 'supplier';
                      $sup[$key]['sup_id'] = encryptor($val['supm_id']);
                      $sup[$key]['name'] = $val['supm_name'];
                      $sup[$key]['year'] = date('Y') - $val['supm_reg_year'];
                      $sup[$key]['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';

                      $sup[$key]['sup_email'] = $val['supm_email'];
                      $sup[$key]['reg_year'] = $val['supm_reg_year'];
                      $sup[$key]['state'] = $val['stt_name'];
                      $sup[$key]['country'] = $val['ctr_name'];
                      $sup[$key]['address'] = $val['supm_address'];
                      $sup[$key]['room'] = $val['supm_room_number'];

                      $sup[$key]['supplier_user_id'] = encryptor($sup_id['usr_id']);
                      $sup[$key]['response'] = '';
                      $sup[$key]['markets'] = 'Market 1,Market 2,Market 3';
                      if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                           $images = $this->db->get_where(tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where(tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
                      }
                      $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
                      $sup[$key]['image'] = !empty($defaultImg) ? site_url() . 'assets/uploads/shops/' . $defaultImg : '';
                      $sup[$key]['logo'] = !empty($val['usr_avatar']) ? site_url() . 'assets/uploads/avatar/' . $val['usr_avatar'] : '';
                      $sup[$key]['our_product'] = $this->db->select();
                 } else {
                      $sup['sup_fb'] = $val['supm_fb'];
                      $sup['sup_insta'] = $val['supm_insta'];
                      $sup['supm_whatsapp'] = $val['supm_wp_app'];
                      $sup['sup_twitter'] = $val['supm_twitter'];
                      $sup['sup_youtube'] = $val['supm_youtube'];
                      $sup['sup_linkedin'] = $val['supm_linkedin'];
                      $sup['sup_web'] = $val['supm_website'];
                      
                      $sup['type'] = 'supplier';
                      $sup['sup_id'] = encryptor($val['supm_id']);
                      $sup['name'] = $val['supm_name'];
                      $sup['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';

                      $sup['sup_email'] = $val['supm_email'];
                      $sup['state'] = $val['stt_name'];
                      $sup['country'] = $val['ctr_name'];
                      $sup['address'] = $val['supm_address'];

                      $sup['supplier_user_id'] = encryptor($sup_id['usr_id']);
                      $sup['logo'] = !empty($val['usr_avatar']) ? site_url() . 'assets/uploads/avatar/' . $val['usr_avatar'] : '';

                      $banner = array();
                      $banners = $this->db->select('sbi_image')->where('sbi_status', 1)->where('sbi_type', 2)->where('sbi_supplier', $val['supm_id'])
                                      ->order_by('sbi_is_default', 'desc')->get(tbl_supplier_banner_images)->result_array();
                      if ($banners) {
                           $banner = preg_filter('/^/', site_url() . 'assets/uploads/home_banner/', array_column($banners, 'sbi_image'));
                      }
                      $sup['banner'] = $banner;
                      // $sup['banner'] = array(site_url() . 'assets/uploads/home_banner/' . $val['supm_home_banner']);
                      // $sup['360_image'] = site_url() . 'assets/uploads/panorama-shop/' . $val['supm_panoramic_image'];
                      $sup['360_image'] = site_url() . 'supplier/get_360/' . $sup['sup_id'];
                      //$sup['address'] = $val['supm_address_en'];
                      $sup['company_name'] = $val['supm_name'];
                      $sup['website'] = $val['supm_website'];
                      $sup['is_fav'] = 0;
                      $sup['is_follow'] = 0;

                      $categories = $this->getSupplierCategories($id);
                      foreach ($categories as $key => $cat) {
                           $categories[$key]['cat_image'] = site_url() . 'assets/uploads/category/thumb_' . $cat['cat_image'];
                      }
                      $sup['categories'] = $categories;
//                      $sup['our_product'] = $this->getSupplierProducts($id);
                      $shop_images = $this->db->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id']))->result_array();
                      $profile['banner_images'] = array();
                      if ($shop_images) {
                           $profile['banner_images'] = preg_filter('/^/', site_url() . 'assets/uploads/shops/', array_column($shop_images, 'ssi_image'));
                      }
                      $profile['about'] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";
                      $data['supplier_home'] = $sup;
                      $data['supplier_profile'] = $profile;
                      return $data;
                 }
            }
            return $sup;
       }

       public function getSupplierCategories($supId) {
            return $this->db->select('cat_id,cat_title,cat_image')
                            ->join(tbl_category, tbl_category . '.cat_id = ' . tbl_supplier_buildings_cate_assoc . '.sbca_category')
                            ->where(tbl_supplier_buildings_cate_assoc . '.sbca_supplier', $supId)
                            ->get(tbl_supplier_buildings_cate_assoc)->result_array();
       }

       function getProductsBySupplier($supplier = 0, $limit, $page) {
            if ($limit) {
                 $this->db->limit($limit, $page);
            }
            $products = $this->db->select(tbl_products_stock_master . '.*,' . tbl_products_master . '.*')
                            ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product', 'LEFT')
                            ->where(tbl_products_stock_master . '.psm_supplier', $supplier)
                            ->where(tbl_products_stock_master . '.psm_status', 1)->get(tbl_products_stock_master)->result_array();
            $productImageUrl = site_url('assets/uploads/product') . '/thumb_';
            $categoryImageUrl = site_url('assets/uploads/category') . '/thumb_';
            if (!empty($products)) {
                 foreach ($products as $key => $value) {
                      $products[$key]['specification'] = $this->db->get_where(tbl_products_specification, array('psp_product' => $value['prd_id']))->result_array();
                      $products[$key]['images'] = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$productImageUrl',pimg_image) AS pimg_image", false)
                                      ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                      $products[$key]['keywords'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $value['prd_id']))->result_array();
                      $products[$key]['categories'] = $this->db->select(tbl_products_category . '.*, ' . "CONCAT('$categoryImageUrl'," . tbl_products_category . ".pcat_image) AS pcat_image, " .
                                              "CONCAT('$categoryImageUrl'," . tbl_products_category . ".pcat_page_banner) AS pcat_page_banner, " . tbl_products_category_assoc . '.*', false)
                                      ->join(tbl_products_category_assoc, tbl_products_category_assoc . '.pcata_category = ' . tbl_products_category . '.pcat_id', 'LEFT')
                                      ->where(tbl_products_category_assoc . '.pcata_product', $value['prd_id'])->get(tbl_products_category)->result_array();
                 }
            }
            return $products;
       }

       public function getSupplierProducts($sup = '') {
            if (!empty($sup)) {
                 $products = $this->db->select(tbl_products_stock_master . '.*,' . tbl_products_master . '.*')
                                 ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product', 'LEFT')
                                 ->where(tbl_products_stock_master . '.psm_supplier', $sup)->where(tbl_products_stock_master . '.psm_status', 1)->get(tbl_products_stock_master)->result_array();
                 $productImageUrl = site_url('assets/uploads/product') . '/thumb_';
                 $categoryImageUrl = site_url('assets/uploads/category') . '/thumb_';
                 if (!empty($products)) {
                      foreach ($products as $key => $value) {
                           $products[$key]['specification'] = $this->db->get_where(tbl_products_specification, array('psp_product' => $value['prd_id']))->result_array();
                           $products[$key]['images'] = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$productImageUrl',pimg_image) AS pimg_image", false)
                                           ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                           $products[$key]['keywords'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $value['prd_id']))->result_array();
                           $products[$key]['category'] = $this->db->select(tbl_products_category . '.*, ' . "CONCAT('$categoryImageUrl'," . tbl_products_category . ".pcat_image) AS pcat_image, " .
                                                   "CONCAT('$categoryImageUrl'," . tbl_products_category . ".pcat_page_banner) AS pcat_page_banner, " . tbl_products_category_assoc . '.*', false)
                                           ->join(tbl_products_category_assoc, tbl_products_category_assoc . '.pcata_category = ' . tbl_products_category . '.pcat_id', 'LEFT')
                                           ->where(tbl_products_category_assoc . '.pcata_product', $value['prd_id'])->get(tbl_products_category)->result_array();
                      }
                 }
                 return $products;
            }
       }

       function addToCart($data) {
            $this->db->insert(tbl_cart_master, $data);
            return $this->db->insert_id();
       }

       function getAppHomeCategory() {
            $imageUrl = site_url('assets/uploads/app_category') . '/';
            return $this->db->select(tbl_app_home_category . ".*, CONCAT('$imageUrl',ahc_image) AS ahc_image", false)
                            ->get_where(tbl_app_home_category, array('ahc_status' => 1))->result_array();
       }

       function newProductOrder($data, $paymentType, $deliveryAddress, $billingAddress, $buyerSelectedTown = 0) {
            if (isset($data['master']) && !empty($data['master'])) {

                 $orderSupplier = array();
                 $orderTown = array();
                 $townAdminNumbers = array();

                 $data['master']['ordm_current_status'] = 0;
                 $data['master']['ordm_payment_type'] = $paymentType;
                 $data['master']['ordm_delivery_address'] = $deliveryAddress;
                 $data['master']['ordm_billing_address'] = $billingAddress;
                 $data['master']['ordm_buyer_selected_town'] = $buyerSelectedTown;

                 $this->db->insert(tbl_products_order_master, $data['master']);
                 $masterId = $this->db->insert_id();
                 $this->db->insert(tbl_products_order_status_assoc, array('pssa_order_master_id' => $masterId, 'pssa_status_id' => 0));
                 $shopCharge[] = array();
                 if ((isset($data['details']) && !empty($data['details'])) && !empty($masterId)) {
                      foreach ($data['details'] as $key => $value) {
                           $orderSupplier[] = $value['ordd_supplier'];
                           $value['ordd_order_master_id'] = $masterId;
                           $value['ordd_town_admin'] = $this->db->select('supm_added_by')->get_where(tbl_supplier_master, array('supm_id' => $value['ordd_supplier']))
                                           ->row()->supm_added_by;

                           $value['ordd_town'] = $this->db->select('supm_market')->get_where(tbl_supplier_master, array('supm_id' => $value['ordd_supplier']))->row()->supm_market;
                           $shopCharge[$value['ordd_supplier']] = $this->db->select('supm_shop_charges')->get_where(tbl_supplier_master, array('supm_id' => $value['ordd_supplier']))->row()->supm_shop_charges;
                           $orderTown[] = $value['ordd_town'];

                           $stockDetails = $this->db->select(tbl_products_stock_details . '.pdsm_id, ' . tbl_products_stock_details . '.pdsm_ttl_stock, ' . tbl_products_stock_master . '.psm_supplier')
                                           ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_stock_details . '.pdsm_stock_master')
                                           ->get_where(tbl_products_stock_details, array(tbl_products_stock_details . '.pdsm_id' => $value['ordd_prd_stock_id']))->row_array();
                           $value['ordd_supplier'] = isset($stockDetails['psm_supplier']) ? $stockDetails['psm_supplier'] : 0;
                           $this->db->insert(tbl_products_order_details, $value);

                           //Update stock
                           if (!empty($stockDetails)) {
                                $ttlStock = isset($stockDetails['pdsm_ttl_stock']) ? $stockDetails['pdsm_ttl_stock'] : 0;
                                $noOfItem = isset($value['ordd_noOf_items']) ? $value['ordd_noOf_items'] : 0;
                                $newStock = $ttlStock - $noOfItem;
                                $this->db->where('pdsm_id', $stockDetails['pdsm_id'])->update(tbl_products_stock_details, array('pdsm_ttl_stock' => $newStock));
                           }
                           //Update stock
                      }
                 }

                 //Update shop charge 
//                 $delCharge = isset($data['master']['ordm_delivery_chrg']) ? $data['master']['ordm_delivery_chrg'] : 0;
//                 $this->db->where('ordm_id', $masterId);
//                 $this->db->update(tbl_products_order_master, array('ordm_total_shop_charge' => array_sum($shopCharge),
//                     'ordm_delivery_chrg' => $delCharge - array_sum($shopCharge)));

                 $shippingAddress = $this->db->select(tbl_address_book . '.*')
                                 ->where(tbl_address_book . '.adbk_id', $data['master']['ordm_delivery_address'])->get(tbl_address_book)->row_array();

                 $paytype = ($paymentType == 1) ? 'COD' : 'Online payment';
                 $whoOrderd = '<strong>Payment type : ' . $paytype . '</strong><br><br>';

                 if (!empty($shippingAddress)) {
                      $whoOrderd .= "Delivery Address <br><br>";
                      $whoOrderd .= 'Name : ' . $shippingAddress['adbk_first_name'] . '<br>';
                      $whoOrderd .= 'Email : ' . $shippingAddress['adbk_email'] . '<br>';
                      $whoOrderd .= 'Phone : ' . $shippingAddress['adbk_phone'] . '<br>';
                      $whoOrderd .= 'Address : ' . $shippingAddress['adbk_address'] . '<br>';
                      $whoOrderd .= 'Building name : ' . $shippingAddress['adbk_bulding_name'] . '<br>';
                      $whoOrderd .= 'Block name : ' . $shippingAddress['adbk_block_name'] . '<br>';
                      $whoOrderd .= 'Street : ' . $shippingAddress['adbk_street'] . '<br>';
                      $whoOrderd .= 'PIN : ' . $shippingAddress['adbk_pin_code'] . '<br>';
                      $whoOrderd .= 'Locality : ' . $shippingAddress['adbk_locality'] . '<br>';
                      $whoOrderd .= 'City : ' . $shippingAddress['adbk_city'];
                 }

                 /* Fetch order details by supplier */
                 $selectOrderMaster = array(
                     tbl_products_order_master . '.*',
                     tbl_users . '.usr_username AS buy_usr_username',
                     tbl_users . '.usr_first_name AS buy_usr_first_name',
                     tbl_users . '.usr_last_name AS buy_usr_last_name',
                     tbl_users . '.usr_phone AS buy_usr_phone',
                     tbl_users . '.usr_email AS buy_usr_email',
                     tbl_users . '.usr_address AS buy_usr_address',
                     tbl_users . '.usr_city AS buy_usr_city',
                     tbl_users . '.usr_state AS buy_usr_state',
                     tbl_users . '.usr_district AS buy_usr_district',
                     tbl_users . '.usr_id AS buy_usr_id',
                     tbl_products_order_statuses . '.*',
                     'dboy.usr_first_name AS dboy_usr_first_name',
                     'dboy.usr_last_name AS dboy_usr_last_name',
                 );

                 $orderMaster = $this->db->select(implode(',', $selectOrderMaster))
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                 ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->get_where(tbl_products_order_master, array('ordm_id' => $masterId))->row_array();

                 $selectOrderDetails = array(
                     tbl_products_order_details . '.*',
                     tbl_products_stock_details . '.*',
                     tbl_products_stock_master . '.*',
                     tbl_products_master . '.*',
                     tbl_products_units . '.*',
                     tbl_supplier_master . '.supm_id',
                     tbl_supplier_master . '.supm_name',
                     tbl_supplier_master . '.supm_number',
                     tbl_supplier_master . '.supm_email',
                     tbl_supplier_master . '.supm_address',
                     tbl_supplier_master . '.supm_market',
                     tbl_market_places . '.mar_name',
                     tbl_market_places . '.mar_lat',
                     tbl_market_places . '.mar_long'
                 );

                 /* Town admin */
                 $orderTown = array_unique($orderTown);
                 $orderTotal = 0;
                 foreach ($orderTown as $key => $ta) {

                      $townDetails = $this->db->get_where(tbl_market_places, array('mar_id' => $ta))->row_array();
                      $orderDetails = $this->db->select(implode(',', $selectOrderDetails))
                                      ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                      ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_stock_details . '.pdsm_stock_master', 'LEFT')
                                      ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $masterId,
                                          'ordd_town' => $ta))->result_array();
                      $temProductItemsTA = '<table border="1" style="border-collapse: collapse;" cellpadding="6"><tr><th>Shop</th><th>Item</th><th>Item count</th>'
                              . '<th>Product qty</th><th>Price</th></tr>';

                      if (!empty($orderDetails)) {
                           foreach ($orderDetails as $key => $prod) {
                                $localName = !empty($prod['psm_product_local_name']) ? ' (' . $prod['psm_product_local_name'] . ') ' : '';
                                $temProductItemsTA .= "<tr>" .
                                        "<td>" . $prod['supm_name'] . "</td>" .
                                        "<td>" . $prod['prd_name_en'] . $localName . "</td>" .
                                        "<td>" . $prod['ordd_noOf_items'] . "</td>" .
                                        "<td>" . $prod['pdsm_qty'] . ' ' . $prod['unt_unit_en'] . "</td>" .
                                        "<td>" . $prod['ordd_unit_total'] . "</td></tr>";
                                $orderTotal += $prod['ordd_unit_total'];
                           }
                      }

                      $temProductItemsTA .= "</table><br>Order total : " . $orderTotal;

                      //mail To tOWN ADMIN 
                      $toMail = '';
                      $orderNumber = isset($orderMaster['ordm_number']) ? $orderMaster['ordm_number'] : '';
                      if (!empty($townDetails)) {
                           $toMail = isset($townDetails['mar_email']) ? $townDetails['mar_email'] : '';

                           $this->mail->isSMTP();
                           $this->mail->Host = MAIL_HOST;
                           $this->mail->SMTPAuth = true;
                           $this->mail->Username = MAIL_USERNAME;
                           $this->mail->Password = MAIL_PASSWORD;
                           $this->mail->SMTPSecure = 'ssl';
                           $this->mail->Port = 465;
                           $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                           $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                           $this->mail->addAddress($toMail);
                           $this->mail->SMTPOptions = array(
                               'ssl' => array(
                                   'verify_peer' => false,
                                   'verify_peer_name' => false,
                                   'allow_self_signed' => true,
                               ),
                           );
                           $this->mail->Subject = 'New order, order number: ' . $orderNumber;
                           $this->mail->isHTML(true);
                           $this->mail->Body = $temProductItemsTA . ' <br><br><br>' . $whoOrderd;
                           $this->mail->send();
                      }
                      //mail To tOWN ADMIN
                      //SMS To Townadmin
                      if (isset($townDetails['mar_contact_number']) && !empty($townDetails['mar_contact_number'])) {
                           $msg = 'Order Recieved Please check you mail ' . $toMail . ' for more details';
                           $phone = $townDetails['mar_contact_number'];
                           $townAdminNumbers[] = $townDetails['mar_contact_number'];
                           send_sms($msg, $phone, 2);
                      }
                 }

                 if (!empty($orderMaster)) {
                      $temProductItemsTA = '';

                      $orderSupplier = array_unique($orderSupplier);

                      foreach ($orderSupplier as $key => $supId) {

                           $supplier = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*, ta.usr_id AS ta_usr_id, '
                                                   . 'ta.usr_username AS ta_usr_username, ta.usr_email AS ta_usr_email, ta.usr_phone AS ta_usr_phone', false)
                                           ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                           ->join(tbl_users . ' ta', 'ta.usr_id = ' . tbl_supplier_master . '.supm_added_by', 'LEFT')
                                           ->get_where(tbl_supplier_master, array(tbl_supplier_master . '.supm_id' => $supId))->row_array();

                           $orderDetails = $this->db->select(implode(',', $selectOrderDetails))
                                           ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                           ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                           ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                           ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                           ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                           ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                           ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $masterId,
                                               'ordd_supplier' => $supId))->result_array();

                           $temProductItems = $temProductItemsTA = '<table border="1"><tr><th>Shop</th><th>Item</th><th>Unit</th><th>Qty</th><th>Price</th></tr>';
                           $supplierssms = array();

                           if (!empty($orderDetails)) {
                                foreach ($orderDetails as $key => $prod) {
                                     $supplierssms[] = $prod['prd_name_en'] . '-' . ' ' . $prod['pdsm_qty'] . ' ' . $prod['unt_unit_en'] . ' X(' . $prod['ordd_noOf_items'] . ')' . '-RS: ' . $prod['ordd_unit_total'];
                                     $temProductItems .= "<tr>" .
                                             "<td>" . $prod['supm_name'] . "</td>" .
                                             "<td>" . $prod['prd_name_en'] . "</td>" .
                                             "<td>" . $prod['pdsm_qty'] . ' ' . $prod['unt_unit_en'] . "</td>" .
                                             "<td>" . $prod['ordd_noOf_items'] . "</td>" .
                                             "<td>" . $prod['ordd_unit_total'] . "</td></tr>";

                                     $temProductItemsTA .= "<tr>" .
                                             "<td>" . $prod['supm_name'] . "</td>" .
                                             "<td>" . $prod['prd_name_en'] . "</td>" .
                                             "<td>" . $prod['pdsm_qty'] . ' ' . $prod['unt_unit_en'] . "</td>" .
                                             "<td>" . $prod['ordd_noOf_items'] . "</td>" .
                                             "<td>" . $prod['ordd_unit_total'] . "</td></tr>";
                                }
                           }

                           $temProductItems .= "</table>";
                           $temProductItemsTA .= "</table>";

                           //Mail to suppliers
                           $toMail = '';
                           $orderNumber = isset($orderMaster['ordm_number']) ? $orderMaster['ordm_number'] : '';
                           if (!empty($supplier)) {
                                $mail1 = isset($supplier['supm_email']) ? $supplier['supm_email'] : '';
                                $mail2 = isset($supplier['usr_email']) ? $supplier['usr_email'] : '';
                                $toMail = $mail1 ? $mail1 : $mail2;
                           }
                           if (!empty($toMail)) {
                                $this->mail->isSMTP();
                                $this->mail->Host = MAIL_HOST;
                                $this->mail->SMTPAuth = true;
                                $this->mail->Username = MAIL_USERNAME;
                                $this->mail->Password = MAIL_PASSWORD;
                                $this->mail->SMTPSecure = 'ssl';
                                $this->mail->Port = 465;
                                $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                                $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                                $this->mail->addAddress($toMail);
                                $this->mail->SMTPOptions = array(
                                    'ssl' => array(
                                        'verify_peer' => false,
                                        'verify_peer_name' => false,
                                        'allow_self_signed' => true,
                                    ),
                                );
                                $this->mail->Subject = 'New order, order number: ' . $orderNumber;
                                $this->mail->isHTML(true);
                                $this->mail->Body = $temProductItems;
                                //$this->mail->send();
                           }
                           //Mail to suppliers
                           //SMS To supplier 
                           $suppliersContact = $this->db->select('GROUP_CONCAT(spc_number) AS spc_number')->where(array('spc_master_id' => $supId, 'spc_status' => 1))
                                           ->get(tbl_supplier_contacts)->row()->spc_number;
                           if (!empty($suppliersContact)) {
                                $prds = implode(',', $supplierssms);
                                $msg = 'Order recieved ' . $prds . ' For order number: ' . $orderNumber . ', Please mention order id on purchase cover';
                                send_sms($msg, $suppliersContact, 2);
                           }
                      }
                 }

                 // SMS to customer
                 $buyer = $this->common_model->getUser($data['master']['ordm_buyer']);
                 if (isset($buyer['usr_phone']) && !empty($buyer['usr_phone'])) {
                      $msg = 'Dear ' . $buyer['usr_first_name'] . ' Cogratulation, You are succesfully purchased. We will deliver shortly Any further enquiry,'
                              . 'pls contact ' . implode(',', $townAdminNumbers);
                      $phone = $buyer['usr_phone'];
                      send_sms($msg, $phone, 2);
                 }

                 generate_log(array(
                     'log_title' => 'New order wia api',
                     'log_desc' => serialize($data),
                     'log_controller' => 'API-new-order',
                     'log_action' => 'C',
                     'log_ref_id' => $masterId,
                     'log_added_by' => 0,
                 ));
                 return $masterId;
            }
            generate_log(array(
                'log_title' => 'New order wia api error',
                'log_desc' => serialize($data),
                'log_controller' => 'API-new-order-error',
                'log_action' => 'C',
                'log_ref_id' => $masterId,
                'log_added_by' => 0,
            ));
            return false;
       }

       function getProductOrder($userId) {
            $selectOrderMaster = array(
                tbl_products_order_master . '.*',
                tbl_supplier_master . '.supm_id',
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_shop_charges',
                tbl_users . '.usr_username AS buy_usr_username',
                tbl_users . '.usr_id AS buy_usr_id',
                tbl_products_order_statuses . '.*'
            );
            $order['master'] = $this->db->select(implode(',', $selectOrderMaster))
                            ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_master . '.ordm_supplier', 'LEFT')
                            ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                            ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                            ->get_where(tbl_products_order_master, array('ordm_buyer' => $userId))->result_array();

            $selectOrderDetails = array(
                tbl_products_order_details . '.*',
                tbl_products_stock_details . '.*',
                tbl_products_stock_master . '.*',
                tbl_products_master . '.*',
                tbl_products_units . '.*'
            );
            if (!empty($order['master'])) {
                 foreach ($order['master'] as $key => $value) {
                      $details = $this->db->select(implode(',', $selectOrderDetails))
                                      ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                      ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product', 'LEFT')
                                      ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                      ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $value['ordm_id']))->result_array();
                      $order['master'][$key]['productDetails'] = $details;
                 }
            }
            return $order;
       }

       function updateOrderStatus($orderDetails) {
            if (isset($orderDetails['order_id']) && !empty($orderDetails['order_id'])) {
                 $this->db->where('ordm_id', $orderDetails['order_id']);
                 $this->db->update(tbl_products_order_master, array('ordm_current_status' => $orderDetails['order_current_status']));
                 $this->db->insert(tbl_products_order_status_assoc, array(
                     'pssa_order_master_id' => $orderDetails['order_id'],
                     'pssa_status_id' => $orderDetails['order_current_status'],
                     'pssa_comments' => $orderDetails['order_comments']
                 ));

                 generate_log(array(
                     'log_title' => 'Order status change',
                     'log_desc' => serialize($orderDetails),
                     'log_controller' => 'order-change-status',
                     'log_action' => 'U',
                     'log_ref_id' => $orderDetails['order_id'],
                     'log_added_by' => 0,
                 ));
                 return true;
            }
            return false;
       }

       function myOrderByDeliveryBoy($dboy, $status = 0) {
            $status = explode(',', $status);
            $selectOrderMaster = array(
                tbl_products_order_master . '.ordm_id',
                tbl_products_order_master . '.ordm_number',
                tbl_products_order_master . '.ordm_total_amount',
                tbl_products_order_master . '.ordm_total_discount',
                tbl_products_order_master . '.ordm_delivery_chrg',
                tbl_products_order_master . '.ordm_total_shop_charge',
                tbl_products_order_master . '.ordm_grand_total',
                tbl_products_order_master . '.ordm_current_status',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_products_order_master . '.ordm_added_on',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_users . '.usr_username AS buy_usr_username',
                tbl_users . '.usr_first_name AS buy_usr_first_name',
                tbl_users . '.usr_last_name AS buy_usr_last_name',
                tbl_users . '.usr_phone AS buy_usr_phone',
                tbl_users . '.usr_email AS buy_usr_email',
                tbl_users . '.usr_address AS buy_usr_address',
                tbl_users . '.usr_city AS buy_usr_city',
                tbl_users . '.usr_state AS buy_usr_state',
                tbl_users . '.usr_district AS buy_usr_district',
                tbl_users . '.usr_id AS buy_usr_id',
                'dboy.usr_first_name AS dboy_usr_first_name',
                'dboy.usr_last_name AS dboy_usr_last_name',
                tbl_address_book . '.adbk_email',
                tbl_address_book . '.adbk_first_name',
                tbl_address_book . '.adbk_last_name',
                tbl_address_book . '.adbk_phone',
                tbl_address_book . '.adbk_address',
                tbl_address_book . '.adbk_bulding_name',
                tbl_address_book . '.adbk_block_name',
                tbl_address_book . '.adbk_street',
                tbl_address_book . '.adbk_pin_code',
                tbl_address_book . '.adbk_locality',
                tbl_address_book . '.adbk_lat',
                tbl_address_book . '.adbk_lon',
                tbl_address_book . '.adbk_city',
                tbl_address_book . '.adbk_state',
                tbl_address_book . '.adbk_ladmark',
                tbl_address_book . '.adbk_country',
                tbl_products_order_statuses . '.*',
                'itemCount.*'
            );

            $subQuery = '(SELECT ordd_order_master_id, SUM(ordd_noOf_items) AS ordd_no_of_items '
                    . 'FROM ' . tbl_products_order_details . ' GROUP BY ordd_order_master_id)';
            if (!empty($dboy)) {
                 if (!empty($status)) {
                      $this->db->where_in(tbl_products_order_master . '.ordm_current_status', $status);
                 }
                 $orderMaster = $this->db->select($selectOrderMaster)
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                 ->join($subQuery . ' itemCount', 'itemCount.ordd_order_master_id = ' . tbl_products_order_master . '.ordm_id', 'LEFT')
                                 ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                 ->join(tbl_address_book, tbl_address_book . '.adbk_id = ' . tbl_products_order_master . '.ordm_delivery_address', 'LEFT')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->order_by(tbl_products_order_master . '.ordm_id', 'DESC')->get_where(tbl_products_order_master, array('ordm_delivery_boy' => $dboy))->result_array();
                 return $orderMaster;
            }
       }

       function trackDBoy($dbId, $lat, $lon) {

            $orderIdArray = $this->db->select(tbl_products_purchase_invoice . '.ppi_order_id,' . tbl_products_order_master . '.ordm_number')
                            ->join(tbl_products_order_master, tbl_products_order_master . '.ordm_id = ' . tbl_products_purchase_invoice . '.ppi_order_id', 'LEFT')
                            ->where('(' . tbl_products_order_master . '.ordm_current_status = ' . ORDER_PICKED . ' OR ' . tbl_products_order_master . '.ordm_current_status = ' . ORDER_DISPATCHED . ')')
                            ->where(tbl_products_order_master . '.ordm_delivery_boy', $dbId)->get(tbl_products_purchase_invoice)->result_array();

            if (!empty($orderIdArray)) {
                 foreach ($orderIdArray as $value) {

                      //Change order status 
                      $this->db->where('ordm_id', $value['ppi_order_id'])->update(tbl_products_order_master, array('ordm_current_status' => ORDER_DISPATCHED));
                      $history = $this->db->get_where(tbl_products_order_status_assoc, array('pssa_order_master_id' => $value['ppi_order_id'],
                                  'pssa_status_id' => ORDER_DISPATCHED))->row_array();
                      if (empty($history)) {
                           $this->db->insert(tbl_products_order_status_assoc, array(
                               'pssa_order_master_id' => $value['ppi_order_id'], 'pssa_status_id' => ORDER_DISPATCHED,
                               'pssa_added_on' => date('Y-m-d h:i:s'), 'pssa_added_by' => $dbId)
                           );
                      }
                      //Change order status 

                      $ins = array(
                          'podt_order_id' => $value['ppi_order_id'],
                          'podt_dboy_id' => $dbId,
                          'podt_lat' => $lat,
                          'podt_lon' => $lon,
                      );

                      $this->db->insert(tbl_products_order_dboy_tracking, $ins);
                      $id = $this->db->insert_id();
                      generate_log(array(
                          'log_title' => 'Delivery boy tracking',
                          'log_desc' => serialize($ins),
                          'log_controller' => 'dboy-tracking',
                          'log_action' => 'U',
                          'log_ref_id' => $id,
                          'log_added_by' => 0,
                      ));
                 }
            }
       }

       function trackDBoyByOrder($orderId, $limit) {
            $this->db->order_by('podt_id', 'DESC')->where(array('podt_order_id' => $orderId));
            if ($limit) {
                 $this->db->limit($limit);
            }
            return $this->db->get(tbl_products_order_dboy_tracking)->result_array();
       }

       function changeOrderStatus($orderId, $statusId, $userId) {

            $orderIdArray = explode(',', $orderId);
            $statusDetails = $this->db->get_where(tbl_products_order_statuses, array('pos_id' => $statusId))->row_array();
            $coments = isset($statusDetails['pos_desc']) ? $statusDetails['pos_desc'] : '';
            if (is_array($orderIdArray)) {
                 foreach ($orderIdArray as $ordId) {
                      // Get old status
                      $oldStatus = $this->db->get_where(tbl_products_order_master, array('ordm_id' => $ordId))->row_array();

                      $this->db->where('ordm_id', $ordId);
                      $update['ordm_current_status'] = $statusId; //New status id

                      if ($this->db->update(tbl_products_order_master, $update)) {
                           for ($i = 0; $i <= $statusId; $i++) {
                                $isalready = $this->db->get_where(tbl_products_order_status_assoc, array(
                                            'pssa_order_master_id' => $ordId,
                                            'pssa_status_id' => $statusId))->row_array();
                                if (empty($isalready)) {
                                     $this->db->insert(tbl_products_order_status_assoc, array(
                                         'pssa_order_master_id' => $ordId, 'pssa_status_id' => $statusId,
                                         'pssa_added_on' => date('Y-m-d h:i:s'),
                                         'pssa_added_by' => $userId, 'pssa_comments' => $coments)
                                     );
                                }
                           }

                           $oldStst = isset($oldStatus['ordm_current_status']) ? $oldStatus['ordm_current_status'] : 0; // old status
                           generate_log(array(
                               'log_title' => 'Order status changed',
                               'log_desc' => 'Order status changed to *' . $oldStst . '-' . $statusId,
                               'log_controller' => 'order-status-changed',
                               'log_action' => 'U',
                               'log_ref_id' => $ordId,
                               'log_added_by' => 0
                           ));
                      }
                 }
            } else {
                 // Get old status
                 $oldStatus = $this->db->get_where(tbl_products_order_master, array('ordm_id' => $orderId))->row_array();

                 $update['ordm_current_status'] = $statusId; //New status id
                 $this->db->where('ordm_id', $orderId);
                 if ($this->db->update(tbl_products_order_master, $update)) {

                      for ($i = 0; $i <= $statusId; $i++) {
                           $isalready = $this->db->get_where(tbl_products_order_status_assoc, array(
                                       'pssa_order_master_id' => $orderId,
                                       'pssa_status_id' => $statusId))->row_array();
                           if (empty($isalready)) {
                                $this->db->insert(tbl_products_order_status_assoc, array(
                                    'pssa_order_master_id' => $orderId, 'pssa_status_id' => $statusId,
                                    'pssa_added_on' => date('Y-m-d h:i:s'),
                                    'pssa_added_by' => $userId, 'pssa_comments' => $coments)
                                );
                           }
                      }

                      $oldStst = isset($oldStatus['ordm_current_status']) ? $oldStatus['ordm_current_status'] : 0; // old status
                      generate_log(array(
                          'log_title' => 'Order status changed',
                          'log_desc' => 'Order status changed to *' . $oldStst . '-' . $statusId,
                          'log_controller' => 'order-status-changed',
                          'log_action' => 'U',
                          'log_ref_id' => $orderId,
                          'log_added_by' => 0
                      ));

                      return true;
                 }
                 return false;
            }
       }

       function newAddress($data) {
            if (!empty($data)) {
                 $isDefaultAddress = $this->db->get_where(tbl_address_book, array('adbk_user_id' => $data['adbk_user_id'], 'adbk_is_default_add' => 1))->row_array();
                 $userDetails = $this->db->get_where(tbl_users, array('usr_id' => $data['adbk_user_id']))->row_array();
                 $data['adbk_email'] = isset($userDetails['usr_email']) ? $userDetails['usr_email'] : '';
                 if (empty($isDefaultAddress)) {
                      $data['adbk_is_default_add'] = 1;
                 }
                 $data['adbk_added_on'] = date('Y-m-d h:i:s');
                 if ($this->db->insert(tbl_address_book, $data)) {
                      $addressId = $this->db->insert_id();

                      generate_log(array(
                          'log_title' => 'New address added',
                          'log_desc' => serialize($data),
                          'log_controller' => 'new-address',
                          'log_action' => 'C',
                          'log_ref_id' => $addressId,
                          'log_added_by' => 0
                      ));
                      return $addressId;
                 } else {
                      generate_log(array(
                          'log_title' => 'New address added error',
                          'log_desc' => serialize($data),
                          'log_controller' => 'new-address-error',
                          'log_action' => 'C',
                          'log_ref_id' => 0,
                          'log_added_by' => 0
                      ));
                 }
            }
            return false;
       }

       function updateAddress($data) {
            if (!empty($data) && isset($data['adbk_id'])) {
                 $addressBokkId = $data['adbk_id'];

                 $this->db->where('adbk_id', $addressBokkId);
                 if ($this->db->update(tbl_address_book, $data)) {

                      generate_log(array(
                          'log_title' => 'Edit address',
                          'log_desc' => serialize($data),
                          'log_controller' => 'edit-address',
                          'log_action' => 'U',
                          'log_ref_id' => $addressBokkId,
                          'log_added_by' => 0
                      ));
                      return true;
                 }
            }
            return false;
       }

       function deleteAddress($addressId) {
            $this->db->where('adbk_id', $addressId);
            if ($this->db->delete(tbl_address_book)) {

                 generate_log(array(
                     'log_title' => 'Delete address',
                     'log_desc' => 'Delete address',
                     'log_controller' => 'delete-address',
                     'log_action' => 'D',
                     'log_ref_id' => $addressId,
                     'log_added_by' => 0
                 ));
                 return true;
            }
            return false;
       }

       function setDefaultAddress($addressId) {
            $addressDetails = $this->db->get_where(tbl_address_book, array('adbk_id' => $addressId))->row_array();
            if (!empty($addressDetails)) {

                 $this->db->where('adbk_user_id', $addressDetails['adbk_user_id']);
                 $this->db->update(tbl_address_book, array('adbk_is_default_add' => 0));

                 $this->db->where('adbk_id', $addressId);
                 if ($this->db->update(tbl_address_book, array('adbk_is_default_add' => 1))) {

                      generate_log(array(
                          'log_title' => 'Set default address',
                          'log_desc' => 'Set default address',
                          'log_controller' => 'set-default-address',
                          'log_action' => 'U',
                          'log_ref_id' => $addressId,
                          'log_added_by' => 0
                      ));
                      return true;
                 }
            }
            return false;
       }

       function getAddress($address, $userId) {
            if ($address > 0) {
                 return $this->db->get_where(tbl_address_book, array('adbk_id' => $address))->result_array();
            }
            if ($userId > 0) {
                 return $this->db->get_where(tbl_address_book, array('adbk_user_id' => $userId))->result_array();
            }
       }

       public function searchByShopName($keyword, $limit, $page) {
            $imageUrl = site_url('assets/uploads/product') . '/thumb_';
            $this->db->distinct()->select(tbl_supplier_keywords . '.supk_supplier');
            if (!empty($keyword)) {
                 $this->db->like('supk_keyword_en', $keyword, 'both');
            }

            if ($limit) {
                 $this->db->limit($limit, $page);
            }

            $suppliers = $this->db->get(tbl_supplier_keywords)->result_array();
            echo $this->db->last_query();
            debug($suppliers);
       }

       public function searchByCategoryName($keyword, $limit, $page) {
            $imageUrl = site_url('assets/uploads/product') . '/thumb_';
            $this->db->distinct(tbl_products_master . '.*,' . tbl_products_stock_master . '.psm_id');
            $this->db->join(tbl_products_stock_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product', 'LEFT');
            if (!empty($keyword)) {
                 $this->db->like(tbl_products_master . '.prd_keywords', $keyword, 'both');
            }
            if ($limit) {
                 $this->db->limit($limit, $page);
            }
            $this->db->where(tbl_products_stock_master . '.psm_id > 0');
            $this->db->group_by(tbl_products_master . '.prd_id');
            $products = $this->db->get(tbl_products_master)->result_array();
       }

       function myFirms($lat, $lng, $miles, $limit, $page) {
            if ($limit) {
                 $this->db->limit($limit, $page);
            }
            if (!empty($lat) && !empty($lng)) {
                 $selArray = array(
                     tbl_supplier_master . '.supm_latitude',
                     tbl_supplier_master . '.supm_longitude',
                     tbl_supplier_master . '.supm_id',
                     tbl_supplier_master . '.supm_shop_charges',
                     tbl_supplier_master . '.supm_cat_id',
                     "(3956 * 2 * ASIN(SQRT( POWER(SIN(($lat - " . tbl_supplier_master . ".supm_latitude)*pi()/180/2),2)+COS($lat*pi()/180 )*COS(" . tbl_supplier_master . ".supm_latitude*pi()/180) * POWER(SIN(($lng- " . tbl_supplier_master . ".supm_longitude)*pi()/180/2),2)))) as distances"
                 );
                 $this->db->where(tbl_supplier_master . ".supm_longitude between ($lng-$miles/cos(radians($lat))*69) and ($lng+$miles/cos(radians($lat))*69) and " . tbl_supplier_master . ".supm_latitude between ($lat-($miles/69)) and ($lat+($miles/69))");
                 $this->db->having("distances < $miles");
            } else {
                 $selArray = array(
                     tbl_supplier_master . '.supm_latitude',
                     tbl_supplier_master . '.supm_longitude',
                     tbl_supplier_master . '.supm_id',
                     tbl_supplier_master . '.supm_shop_charges',
                     tbl_supplier_master . '.supm_cat_id'
                 );
            }
            $suppliers = $this->db->select($selArray, false)
                            ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                            ->group_by(tbl_supplier_master . '.supm_cat_id')
                            ->get(tbl_supplier_master)->result_array();
            $cats = array_column($suppliers, 'supm_cat_id');

            $imageUrl = site_url('assets/uploads/category') . '/';
            $selArray = array(
                tbl_category . '.*',
                "CONCAT('$imageUrl',cat_image) AS cat_image"
            );

            $categories = $this->db->select($selArray)->where('cat_status', 1)
                            ->where_in('cat_id', $cats)->get(tbl_category)->result_array();
            return $categories;
       }

       function singleFirms($id) {
            if (!empty($id)) {
                 $imUrlPanoramaShop = "'" . site_url('assets/uploads/panorama-shop') . "/'";
                 $suppliers = $this->db->select(tbl_supplier_master . '.*,' . tbl_users . '.*,' . tbl_countries . '.*,' . tbl_states . '.*,' .
                                         tbl_market_places . '.*, CONCAT(' . $imUrlPanoramaShop . ',' . tbl_supplier_master . '.supm_panoramic_image) AS supm_panoramic_image', false)
                                 ->join(tbl_users, tbl_users . '.usr_supplier = ' . tbl_supplier_master . '.supm_id', 'LEFT')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_countries, tbl_countries . '.ctr_id = ' . tbl_supplier_master . '.supm_country', 'LEFT')
                                 ->join(tbl_states, tbl_states . '.stt_id = ' . tbl_supplier_master . '.supm_state', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                 ->where(tbl_supplier_master . '.supm_id', $id)->where(tbl_users_groups . '.group_id', 2)
                                 ->get(tbl_supplier_master)->row_array();
                 if ($suppliers) {
                      $imUrlShop = "'" . site_url('assets/uploads/shops') . "/'";
                      $suppliers['keywords'] = $this->db->get_where(tbl_supplier_keywords, array('supk_supplier' => $suppliers['supm_id']))->result_array();
                      $suppliers['shop_images'] = $this->db->select('CONCAT(' . $imUrlShop . ',ssi_image) AS ssi_image', false)
                                      ->get_where(tbl_supplier_shop_images, array('ssi_supplier' => $suppliers['supm_id']))->result_array();

                      $imUrlBaner = "'" . site_url('assets/uploads/home_banner') . "/'";
                      $suppliers['banner_images_app'] = $this->db->select('CONCAT(' . $imUrlBaner . ',sbi_image) AS sbi_image', false)->order_by('sbi_is_default', 'DESC')
                                      ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 2))->result_array();

                      $suppliers['banner_images_web'] = $this->db->select('CONCAT(' . $imUrlBaner . ',sbi_image) AS sbi_image', false)->order_by('sbi_is_default', 'DESC')
                                      ->get_where(tbl_supplier_banner_images, array('sbi_supplier' => $suppliers['supm_id'], 'sbi_type' => 1))->result_array();

                      $suppliers['markets'] = $this->db->select(tbl_market_cate_assoc . '.*,' . tbl_category . '.*')
                                      ->join(tbl_category, tbl_market_cate_assoc . '.mca_category_id = ' . tbl_category . '.cat_id')
                                      ->where(tbl_market_cate_assoc . '.mca_market_id', $suppliers['supm_market'])
                                      ->get(tbl_market_cate_assoc)->result_array();

                      $suppliers['contacts'] = $this->db->get_where(tbl_supplier_contacts, array('spc_master_id' => $id))->result_array();

                      $suppliers['categories'] = explode(',', $this->db->select('GROUP_CONCAT(scat_category) AS categories')
                                      ->where('scat_master_id', $id)->get(tbl_supplier_categories)->row()->categories);

                      $suppliers['buildingCates'] = explode(',', $this->db->select('GROUP_CONCAT(sbca_category) AS sbca_category')
                                      ->where('sbca_supplier', $id)->get(tbl_supplier_buildings_cate_assoc)->row()->sbca_category);
                      //supplier user id -> to use as channel name in pusher chat added by irfan
                      $suppliers['supplier_user_id'] = $this->common_model->getUserIdFromSupId($suppliers['supm_id']);
                 }
                 return $suppliers;
            }
            return false;
       }

       function getUserByIdentityToken($token = '', $id = '') {
            if ($token) {
                 return $this->db->get_where(tbl_users, array('usr_token' => $token))->row_array();
            } else if ($id) {
                 return $this->db->get_where(tbl_users, array('usr_id' => $id))->row_array();
            }
       }

       function updateUser($id, $data) {
            if (!empty($id) && !empty($data)) {
                 $this->db->where('usr_id', $id);
                 $this->db->update(tbl_users, $data);

                 generate_log(array(
                     'log_title' => 'User update wia api',
                     'log_desc' => serialize($data),
                     'log_controller' => 'API-update-user',
                     'log_action' => 'U',
                     'log_ref_id' => $id,
                     'log_added_by' => 0,
                 ));

                 return true;
            }
            return false;
       }

       function getShopCharge($supplier) {
            return $this->db->get_where(tbl_supplier_master, array('supm_id' => $supplier))->row()->supm_shop_charges;
       }

       function calculateDeliveryCharge($shipAddress, $suppliers) {
            $suppliers = explode(',', $suppliers);
            $data = array();
            $data['shipAddDetails'] = $shipAddDetails = $this->db->select('adbk_id, adbk_lat,adbk_lon')->get_where(tbl_address_book, array('adbk_id' => $shipAddress))->row_array();
            $data['supplierDetails'] = $supplierDetails = $this->db->select('supm_shop_charges, supm_id, supm_latitude,supm_longitude')->where_in('supm_id', $suppliers)->get(tbl_supplier_master)->result_array();

            $distances = array();
            $data['distances'] = 0;
            $data['distancesMax'] = 0;
            $data['defaultDistance'] = 0;
            $data['defaultCharge'] = 0;
            $data['excessDelCharge'] = 0;
            $data['deliveryCharge'] = get_settings_by_key('min_del_chrg');

            if (((isset($shipAddDetails['adbk_lat']) && isset($shipAddDetails['adbk_lon'])) &&
                    (!empty($shipAddDetails['adbk_lat']) && !empty($shipAddDetails['adbk_lon']))) && !empty($supplierDetails)) {
                 foreach ($supplierDetails as $key => $value) {
                      $distances[$value['supm_id']] = (int) distancer($shipAddDetails['adbk_lat'], $shipAddDetails['adbk_lon'], $value['supm_latitude'], $value['supm_longitude'], 'K');
                 }

                 $data['distances'] = $distances;
                 $data['distancesMax'] = $maxDistance = max($distances);
                 $data['defaultDistance'] = $defaultDistance = get_settings_by_key('min_del_kl');
                 $data['defaultCharge'] = $defaultCharge = get_settings_by_key('min_del_chrg');
                 $data['excessDelCharge'] = $excessDelCharge = get_settings_by_key('excess_del_charge');

                 if ($maxDistance <= $defaultDistance) {
                      $data['deliveryCharge'] = get_settings_by_key('min_del_chrg');
                 } else {
                      $data['deliveryCharge'] = $defaultCharge + ($maxDistance - $defaultDistance) * $excessDelCharge;
                 }
            }
            return $data;
       }

       function getLoggedUser($id) {
            if ($id) {
                 $imUrl = "'" . site_url('assets/uploads/avatar') . "/'";
                 return $this->db->select("usr_email, usr_username, usr_phone_code, usr_phone, CONCAT(" . $imUrl . ",usr_avatar) AS usr_avatar", false)
                                 ->get_where(tbl_users, array('usr_id' => $id))->row_array();
            }
       }

       function getOrders($orderId = '', $userId = '') {
            $selectOrderMaster = array(
                tbl_products_order_master . '.ordm_id',
                tbl_products_order_master . '.ordm_number',
                tbl_products_order_master . '.ordm_total_amount',
                tbl_products_order_master . '.ordm_total_discount',
                tbl_products_order_master . '.ordm_delivery_chrg',
                tbl_products_order_master . '.ordm_total_shop_charge',
                tbl_products_order_master . '.ordm_grand_total',
                tbl_products_order_master . '.ordm_current_status',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_products_order_master . '.ordm_added_on',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_users . '.usr_username AS buy_usr_username',
                tbl_users . '.usr_first_name AS buy_usr_first_name',
                tbl_users . '.usr_last_name AS buy_usr_last_name',
                tbl_users . '.usr_phone AS buy_usr_phone',
                tbl_users . '.usr_email AS buy_usr_email',
                tbl_users . '.usr_address AS buy_usr_address',
                tbl_users . '.usr_city AS buy_usr_city',
                tbl_users . '.usr_state AS buy_usr_state',
                tbl_users . '.usr_district AS buy_usr_district',
                tbl_users . '.usr_id AS buy_usr_id',
                'dboy.usr_first_name AS dboy_usr_first_name',
                'dboy.usr_last_name AS dboy_usr_last_name',
                tbl_address_book . '.adbk_email',
                tbl_address_book . '.adbk_first_name',
                tbl_address_book . '.adbk_last_name',
                tbl_address_book . '.adbk_phone',
                tbl_address_book . '.adbk_address',
                tbl_address_book . '.adbk_bulding_name',
                tbl_address_book . '.adbk_block_name',
                tbl_address_book . '.adbk_street',
                tbl_address_book . '.adbk_pin_code',
                tbl_address_book . '.adbk_locality',
                tbl_address_book . '.adbk_lat',
                tbl_address_book . '.adbk_lon',
                tbl_address_book . '.adbk_city',
                tbl_address_book . '.adbk_state',
                tbl_address_book . '.adbk_ladmark',
                tbl_address_book . '.adbk_country',
                tbl_products_order_statuses . '.*',
                'itemCount.*'
            );
            $selectOrderDetails = array(
                tbl_products_order_details . '.ordd_id',
                tbl_products_order_details . '.ordd_product_name',
                tbl_products_order_details . '.ordd_noOf_items',
                tbl_products_order_details . '.ordd_unit_amount',
                tbl_products_order_details . '.ordd_unit_discount',
                tbl_products_order_details . '.ordd_unit_total',
                tbl_products_order_details . '.ordd_product_image',
                tbl_products_order_details . '.ordd_town',
                tbl_products_order_details . '.ordd_actual_weight',
                tbl_products_order_details . '.ordd_actual_rate',
                tbl_products_units . '.unt_unit_name_en',
                tbl_products_units . '.unt_unit_en',
                tbl_products_master . '.prd_name_en',
                tbl_products_master . '.prd_keywords',
                tbl_products_master . '.prd_desc',
                tbl_supplier_master . '.supm_id',
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_number',
                tbl_supplier_master . '.supm_email',
                tbl_supplier_master . '.supm_address',
                tbl_supplier_master . '.supm_market',
                tbl_supplier_master . '.supm_shop_charges',
                tbl_market_places . '.mar_name',
                tbl_market_places . '.mar_lat',
                tbl_market_places . '.mar_long',
                tbl_products_stock_details . '.*'
            );
            $subQuery = '(SELECT ordd_order_master_id, SUM(ordd_noOf_items) AS ordd_no_of_items '
                    . 'FROM ' . tbl_products_order_details . ' GROUP BY ordd_order_master_id)';
            if (!empty($orderId)) {
                 $orderMaster = $this->db->select($selectOrderMaster)
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                 ->join($subQuery . ' itemCount', 'itemCount.ordd_order_master_id = ' . tbl_products_order_master . '.ordm_id', 'LEFT')
                                 ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                 ->join(tbl_address_book, tbl_address_book . '.adbk_id = ' . tbl_products_order_master . '.ordm_delivery_address', 'LEFT')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->get_where(tbl_products_order_master, array('ordm_id' => $orderId))->row_array();

                 if (!empty($orderMaster)) {
                      $orderMaster['productDetails'] = $this->db->select(implode(',', $selectOrderDetails))
                                      ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                      ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $orderId))->result_array();
                 }
                 return $orderMaster;
            } else if (!empty($userId)) {

                 $orderMaster = $this->db->select($selectOrderMaster)
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                                 ->join($subQuery . ' itemCount', 'itemCount.ordd_order_master_id = ' . tbl_products_order_master . '.ordm_id', 'LEFT')
                                 ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                                 ->join(tbl_address_book, tbl_address_book . '.adbk_id = ' . tbl_products_order_master . '.ordm_delivery_address', 'LEFT')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->order_by(tbl_products_order_master . '.ordm_id', 'DESC')->get_where(tbl_products_order_master, array('ordm_buyer' => $userId))->result_array();
                 return $orderMaster;
            }
       }

       function addToFavourite($data) {
            if (!empty($data)) {
                 $this->db->insert(tbl_favorite_list, $data);
                 $favId = $this->db->insert_id();
                 generate_log(array(
                     'log_title' => 'Product or supplier added to favourite list',
                     'log_desc' => serialize($data),
                     'log_controller' => 'API-add-tofavlist',
                     'log_action' => 'C',
                     'log_ref_id' => $favId,
                     'log_added_by' => 0,
                 ));
                 return $favId;
            }
            return false;
       }

       function removeFavourite($id) {
            if (!empty($id)) {
                 $this->db->delete(tbl_favorite_list, array('fav_id' => $id));
                 generate_log(array(
                     'log_title' => 'Product or supplier added to favourite list',
                     'log_desc' => 'Product or supplier added to favourite list',
                     'log_controller' => 'API-remove-tofavlist',
                     'log_action' => 'D',
                     'log_ref_id' => $id,
                     'log_added_by' => 0,
                 ));
                 return true;
            }
            return false;
       }

       function myFavouriteList($userId) {
            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
            $prdImageUrl = site_url('assets/uploads/product') . '/thumb_';
            if (!empty($userId)) {
                 $select = array(
                     tbl_favorite_list . '.*',
                     tbl_users . '.usr_username AS buy_usr_username',
                     tbl_users . '.usr_first_name AS buy_usr_first_name',
                     tbl_users . '.usr_last_name AS buy_usr_last_name',
                     tbl_users . '.usr_phone AS buy_usr_phone',
                     tbl_users . '.usr_email AS buy_usr_email',
                     tbl_users . '.usr_address AS buy_usr_address',
                     tbl_users . '.usr_city AS buy_usr_city',
                     tbl_users . '.usr_state AS buy_usr_state',
                     tbl_users . '.usr_district AS buy_usr_district',
                     tbl_users . '.usr_id AS buy_usr_id',
                     tbl_products_master . '.prd_name_en',
                     tbl_products_master . '.prd_default_image',
                     tbl_products_master . '.prd_id',
                     tbl_supplier_master . '.supm_name',
                     tbl_products_category . '.pcat_title',
                     tbl_products_stock_master . '.*'
                 );

                 $return = $this->db->select($select)->join(tbl_users, tbl_users . '.usr_id = ' . tbl_favorite_list . '.fav_added_by', 'LEFT')
                                 ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_favorite_list . '.fav_consign_id', 'LEFT')
                                 ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_stock_master . '.psm_product', 'LEFT')
                                 ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_favorite_list . '.fav_consign_id', 'LEFT')
                                 ->join(tbl_products_category, tbl_products_category . '.pcat_id = ' . tbl_favorite_list . '.fav_consign_id', 'LEFT')
                                 ->get_where(tbl_favorite_list, array(tbl_favorite_list . '.fav_added_by' => $userId))->result_array();

                 if (!empty($return)) {
                      foreach ($return as $key => $value) {
                           $return[$key]['fav_id'] = (int) $value['fav_id'];
                           if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                                $images = $this->db->select(tbl_products_images . ".*, CONCAT('$prdImageUrl',pimg_image) AS pimg_image", false)
                                                ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id'], 'pimg_id' => $value['prd_default_image']))->row_array();
                           } else {
                                $images = $this->db->select(tbl_products_images . ".*, CONCAT('$prdImageUrl',pimg_image) AS pimg_image", false)
                                                ->limit(1)->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                           }

                           $return[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                           $return[$key]['images'] = $this->db->select(tbl_products_images . ".*, CONCAT('$prdImageUrl',pimg_image) AS pimg_image", false)
                                           ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();

                           $return[$key]['stock_qty'] = $this->db->select(tbl_products_stock_details . '.*,' . tbl_products_units . '.*')
                                           ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                           ->get_where(tbl_products_stock_details, array('pdsm_stock_master' => $value['psm_id']))->result_array();

                           $return[$key]['stock_qty'] = array_filter($return[$key]['stock_qty']);
                           $return[$key]['stock_images'] = $this->db->select(tbl_products_stock_images . ".*, CONCAT('$imageUrl',psi_image) AS psi_image", false)
                                           ->get_where(tbl_products_stock_images, array('psi_product_master' => $value['psm_id']))->result_array();

                           $return[$key]['stock_images'] = array_filter($return[$key]['stock_images']);
                      }
                 }
                 return $return;
            }
            return false;
       }

       function cancelOrder($orderId, $userId) {

            //Get product order details
            $orderDetails = $this->getOrders($orderId);

            if (!empty($orderDetails) && (isset($orderDetails['productDetails']) && !empty($orderDetails['productDetails']) )) {

                 $marketId = array_unique(array_column($orderDetails['productDetails'], 'ordd_town'));
                 $isConf = $this->db->select(tbl_products_order_master . '.*, ' . tbl_products_order_statuses . '.*')
                                 ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                                 ->get_where(tbl_products_order_master, array('ordm_id' => $orderId))->row_array();

                 if (empty($isConf)) {
                      return array('error' => true, 'msg' => 'Order not found!');
                 } else if ($isConf['ordm_current_status'] == 0) {
                      $this->db->where('ordm_id', $orderId)->update(tbl_products_order_master, array('ordm_current_status' => 4));
                      $this->db->insert(tbl_products_order_status_assoc, array(
                          'pssa_order_master_id' => $orderId,
                          'pssa_status_id' => 4,
                          'pssa_comments' => 'Order successfully canceled',
                          'pssa_added_on' => date('Y-m-d h:i:s'),
                          'pssa_added_by' => $userId
                      ));
                      generate_log(array(
                          'log_title' => 'Order successfully canceled',
                          'log_desc' => serialize($isConf),
                          'log_controller' => 'API-cancel-order',
                          'log_action' => 'U',
                          'log_ref_id' => $orderId,
                          'log_added_by' => $userId,
                      ));

                      //SMS for town admin
                      $townDetails = $this->db->where_in('mar_id', $marketId)->get(tbl_market_places)->result_array();
                      if (!empty($townDetails)) {
                           foreach ($townDetails as $key => $value) {
                                $msg = 'Order cancelled, order number :' . $orderDetails['ordm_number'] . ' customer name : ' . $orderDetails['adbk_first_name'];
                                send_sms($msg, $value['mar_contact_number'], 2);
                           }
                      }

                      //SMS to customer
                      $msg = "Order cancelled order number :" . $orderDetails['ordm_number'] . " customer name : " .
                              $orderDetails['adbk_first_name'];
                      if ($orderDetails['ordm_payment_type'] == 2) {
                           $msg = $msg . "  online payment wil be refunded in 3 to 5 working days";
                      }
                      send_sms($msg, $orderDetails['buy_usr_phone'], 2);

                      //SMS  fo seller
                      $suppliers = array_unique(array_column($orderDetails['productDetails'], 'supm_number'));
                      if (!empty($suppliers)) {
                           $suppliers = implode($suppliers, ',');
                           $msg = "order cancelled order number :" . $orderDetails['ordm_number'] . " Thank you for co-operating with kleemz";
                           send_sms($msg, $suppliers, 2);
                      }
                      return array('error' => false, 'msg' => 'Order successfully canceled');
                 } else {
                      return array('error' => true, 'msg' => "Can't cancel this order because, order current status is " . strtolower($isConf['pos_desc']));
                 }
            }
       }

       function getMasterProductsList($cateId = 0, $limit, $page, $town = 0, $credential = 0) {

            $townProducts = $this->db->distinct()->select('psm_product')->get_where(tbl_products_stock_master, array("psm_market_place" => $town, 'psm_status' => 1))->result_array();
            $townProducts = array_column($townProducts, 'psm_product');
            if (!empty($townProducts)) {
                 $imageUrl = site_url('assets/uploads/product') . '/thumb_';
                 if ($cateId > 0) {
                      $products = $this->db->select('pcata_product')->where('pcata_category', $cateId)
                                      ->where_in('pcata_product', $townProducts)->get(tbl_products_category_assoc)->result_array();
                      $products = !empty($products) ? array_column($products, 'pcata_product') : '';

                      $this->db->where_in(tbl_products_master . '.prd_id', $products);
                      $this->db->where(tbl_products_master . '.prd_status', 1);
                 }
                 if ($limit) {
                      $this->db->limit($limit, $page);
                 }
                 $selectFields = array(
                     `distinct ` . tbl_products_master . '.prd_id',
                     tbl_products_master . '.prd_name_en',
                     tbl_products_master . '.prd_desc',
                     tbl_products_stock_master . '.psm_product_priority'
                 );
                 $this->db->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_product = ' . tbl_products_master . '.prd_id', 'LEFT');
                 $this->db->order_by(tbl_products_stock_master . '.psm_product_priority', 'ASC');
                 $this->db->group_by(tbl_products_master . '.prd_id');
                 $products = $this->db->select($selectFields)->get(tbl_products_master)->result_array();
                 if (!empty($products)) {
                      foreach ($products as $key => $value) {

                           if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                                $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                                ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id'],
                                                    'pimg_id' => $value['prd_default_image']))->row_array();
                           } else {

                                $images = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                                ->limit(1)->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                           }
                           $products[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                           $products[$key]['images'] = $this->db->select(tbl_products_images . '.*,' . "CONCAT('$imageUrl',pimg_image) AS pimg_image", false)
                                           ->get_where(tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                           $products[$key]['categories'] = $this->db->get_where(tbl_products_category_assoc, array('pcata_product' => $value['prd_id']))->result_array();
                           $products[$key]['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcata_category) AS pcata_category')
                                           ->where(array('pcata_product' => $value['prd_id']))->get(tbl_products_category_assoc)->row()->pcata_category);
                      }
                      return $products;
                 }
            }
            return array();
       }

       public function getSuppliersProductsByProduct($id = '', $filter = '', $lat, $lng, $miles, $credential = 0, $supplier = 0, $town = 0) {
            $imageUrl = site_url('assets/uploads/category') . '/thumb_';
            $arraySelect = array(
                tbl_products_master . '.prd_id',
                tbl_products_master . '.prd_number',
                tbl_products_master . '.prd_name_en',
                tbl_products_master . '.prd_desc',
                tbl_products_master . '.prd_unit',
                tbl_products_master . '.prd_supplier',
                tbl_supplier_master . '.*'
            );
            $this->db->select($arraySelect)
                    ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_master . '.prd_supplier', 'LEFT');
            $products = $this->db->where(tbl_products_master . '.prd_id', $id)->get(tbl_products_master)->row_array();
            $products = array_filter($products);
            if (!empty($products)) {
                 //$products['specification'] = $this->db->order_by("psp_id", "asc")->get_where(tbl_products_specification, array('psp_product' => $products['prd_id']))->result_array();
                 //$products['keyword'] = $this->db->get_where(tbl_products_keyword, array('pkwd_product' => $products['prd_id']))->result_array();
                 if (isset($products['prd_default_image']) && !empty($products['prd_default_image'])) {
                      $images = $this->db->get_where(tbl_products_images, array('pimg_product' => $products['prd_id'], 'pimg_id' => $products['prd_default_image']))->row_array();
                 } else {
                      $images = $this->db->limit(1)->get_where(tbl_products_images, array('pimg_product' => $products['prd_id']))->row_array();
                 }
                 $products['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                 $products['images'] = $this->db->get_where(tbl_products_images, array('pimg_product' => $products['prd_id']))->result_array();

                 $products['categories'] = $this->db->select(tbl_products_category_assoc . '.*,' . tbl_products_category . '.pcat_title')
                                 ->join(tbl_products_category, tbl_products_category . '.pcat_id = ' . tbl_products_category_assoc . '.pcata_category', 'LEFT')
                                 ->get_where(tbl_products_category_assoc, array('pcata_product' => $products['prd_id']))->result_array();

                 $products['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcata_category) AS pcata_category')
                                 ->where(array('pcata_product' => $products['prd_id']))->get(tbl_products_category_assoc)->row()->pcata_category);

                 if (!empty($town)) {
                      if ($supplier > 0) {
                           $this->db->where(tbl_supplier_master . '.supm_id', $supplier);
                      }
                      $selectArray = array(
                          tbl_products_stock_master . '.psm_id',
                          tbl_products_stock_master . '.psm_supplier',
                          tbl_products_stock_master . '.psm_supplier_user_id',
                          tbl_products_stock_master . '.psm_product_local_name',
                          tbl_supplier_master . '.supm_name',
                          tbl_supplier_master . '.supm_address',
                          tbl_supplier_shop_images . '.*'
                      );
                      $products['suppliers'] = $this->db->select($selectArray, false)
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_stock_master . '.psm_supplier', 'LEFT')
                                      ->join(tbl_supplier_shop_images, tbl_supplier_shop_images . '.ssi_id = ' . tbl_supplier_master . '.supm_default_image', 'LEFT')
                                      ->where(tbl_products_stock_master . '.psm_product', $id)->where(tbl_products_stock_master . '.psm_status', 1)
                                      ->where(tbl_products_stock_master . '.psm_market_place', $town)->get(tbl_products_stock_master)->result_array();
                 } else {
                      if ($supplier > 0) {
                           $this->db->where(tbl_supplier_master . '.supm_id', $supplier);
                      }
                      $selectArray = array(
                          tbl_products_stock_master . '.psm_id',
                          tbl_products_stock_master . '.psm_supplier',
                          tbl_products_stock_master . '.psm_supplier_user_id',
                          tbl_products_stock_master . '.psm_product_local_name',
                          tbl_supplier_master . '.supm_name',
                          tbl_supplier_master . '.supm_address',
                          tbl_supplier_shop_images . '.ssi_image'
                      );
                      $products['suppliers'] = $this->db->select($selectArray)
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_stock_master . '.psm_supplier', 'LEFT')
                                      ->join(tbl_supplier_shop_images, tbl_supplier_shop_images . '.ssi_id = ' . tbl_supplier_master . '.supm_default_image', 'LEFT')
                                      ->where(tbl_products_stock_master . '.psm_product', $id)->where(tbl_products_stock_master . '.psm_status', 1)->get(tbl_products_stock_master)->result_array();
                      //->where(tbl_products_stock_master . '.psm_market_place', $town)
                 }

                 $products['suppliers'] = array_filter($products['suppliers']);

                 if (!empty($products['suppliers'])) {
                      foreach ($products['suppliers'] as $key => $value) {

                           $products['suppliers'][$key]['fav_id'] = 0;
                           if (!empty($credential)) {
                                $isFav = $this->db->where(array('fav_consign_id' => $value['psm_id'], 'fav_added_by' => $credential))
                                                ->like('fav_consign', 'STK', 'BOTH')->get(tbl_favorite_list)->row_array();
                                $products['suppliers'][$key]['fav_id'] = isset($isFav['fav_id']) ? (int) $isFav['fav_id'] : 0;
                           }
                           $selectArray = array(
                               tbl_products_stock_details . '.pdsm_id',
                               tbl_products_stock_details . '.pdsm_qty',
                               tbl_products_stock_details . '.pdsm_price',
                               tbl_products_stock_details . '.pdsm_offer_price',
                               tbl_products_stock_details . '.pdsm_ttl_stock',
                               tbl_products_stock_details . '.pdsm_unit',
                               tbl_products_units . '.unt_id',
                               tbl_products_units . '.unt_unit_en',
                               tbl_products_units . '.unt_desc_en'
                           );
                           $products['suppliers'][$key]['stock_qty'] = $this->db->select($selectArray)
                                           ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                           ->get_where(tbl_products_stock_details, array('pdsm_stock_master' => $value['psm_id']))->result_array();

                           $products['suppliers'][$key]['stock_qty'] = array_filter($products['suppliers'][$key]['stock_qty']);
                           $products['suppliers'][$key]['stock_images'] = $this->db->select(tbl_products_stock_images . ".*, CONCAT('$imageUrl',psi_image) AS psi_image", false)
                                           ->get_where(tbl_products_stock_images, array('psi_product_master' => $value['psm_id']))->result_array();

                           $products['suppliers'][$key]['stock_images'] = array_filter($products['suppliers'][$key]['stock_images']);
                      }
                 }
            }
            return $products;
       }

       function uploadInvoiceByDB($data) {

            $data['ppi_added_on'] = date('Y-m-d h:i:s');
            $orderMaster = $this->db->get_where(tbl_products_order_master, array('ordm_id' => $data['ppi_order_id']))->row_array();
            if (!empty($orderMaster)) {
                 $data['ppi_order_date'] = $orderMaster['ordm_added_on'];
                 $data['ppi_order_num'] = $orderMaster['ordm_number'];
                 $this->db->insert(tbl_products_purchase_invoice, $data);
                 $invoiceId = $this->db->insert_id();

                 //Change status
                 $isalready = $this->db->get_where(tbl_products_order_status_assoc, array(
                             'pssa_order_master_id' => $data['ppi_order_id'],
                             'pssa_status_id' => ORDER_PICKED))->row_array();
                 if (empty($isalready)) {
                      $this->db->insert(tbl_products_order_status_assoc, array(
                          'pssa_order_master_id' => $data['ppi_order_id'],
                          'pssa_status_id' => ORDER_PICKED,
                          'pssa_added_by' => $data['ppi_added_by']));
                      $this->db->where('ordm_id', $data['ppi_order_id'])->update(tbl_products_order_master, array('ordm_current_status' => ORDER_PICKED));
                 }
                 //Change status

                 return $invoiceId;
            }
            return false;
       }

       function uploadInvoiceByDBImages($data) {
            $this->db->insert(tbl_products_purchase_invoice_images, $data);
            return true;
       }

       function removeInvoice($invoiceId) {
            $invoiceDetails = $this->db->get_where(tbl_products_purchase_invoice, array('ppi_id' => $invoiceId))->row_array();
            $this->db->delete(tbl_products_purchase_invoice, array('ppi_id' => $invoiceId));
            $images = $this->db->get_where(tbl_products_purchase_invoice_images, array('ppii_invoice_id' => $invoiceId))->result_array();
            if (!empty($images)) {
                 foreach ($images as $key => $value) {
                      if (file_exists(FILE_UPLOAD_PATH . 'product_order_invoice/' . $value['ppii_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'product_order_invoice/' . $value['ppii_image']);
                      }
                 }
                 $this->db->delete(tbl_products_purchase_invoice_images, array('ppii_invoice_id' => $invoiceId));
                 $this->db->delete(tbl_products_order_pickup, array('pop_invoice_master' => $invoiceId));

                 if (!empty($invoiceDetails) && isset($invoiceDetails['ppi_order_id'])) {
                      $this->db->delete(tbl_products_order_status_assoc, array(
                          'pssa_order_master_id' => $invoiceDetails['ppi_order_id'],
                          'pssa_status_id' => ORDER_PICKED
                      ));
                 }
            }
       }

       function addProductsOrderPickup($orddId, $invoiceId, $orderId) {
            if (!empty($orddId) && !empty($invoiceId) && !empty($orderId)) {
                 foreach ($orddId as $key => $odid) {
                      $this->db->insert(tbl_products_order_pickup, array('pop_invoice_master' => $invoiceId,
                          'pop_ordd_id' => $odid, 'pop_order_id' => $orderId
                      ));
                 }
                 return true;
            }
            return false;
       }

       function removeInvoiceImage($imageId) {
            $images = $this->db->get_where(tbl_products_purchase_invoice_images, array('ppii_id' => $imageId))->row_array();
            if (!empty($images)) {
                 if (file_exists(FILE_UPLOAD_PATH . 'product_order_invoice/' . $images['ppii_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'product_order_invoice/' . $images['ppii_image']);
                 }
                 $this->db->delete(tbl_products_purchase_invoice_images, array('ppii_id' => $imageId));
            }
       }

       function getInvoiceByOrder($orderId, $supplier = 0) {
            if (!empty($orderId)) {
                 $imageUrl = site_url('assets/uploads/product_order_invoice') . '/';
                 if ($supplier > 0) {
                      $this->db->where(tbl_products_purchase_invoice . '.ppi_supplier_id', $supplier);
                 }
                 $invoiceMaster = $this->db->select(tbl_products_purchase_invoice . '.*,' . tbl_users . '.usr_code,' . tbl_users . '.usr_username,' . tbl_supplier_master . '.supm_name')
                                 ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_purchase_invoice . '.ppi_added_by', 'LEFT')
                                 ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_purchase_invoice . '.ppi_supplier_id', 'LEFT')
                                 ->where(tbl_products_purchase_invoice . '.ppi_order_id', $orderId)->get(tbl_products_purchase_invoice)->result_array();

                 if (!empty($invoiceMaster)) {
                      foreach ($invoiceMaster as $key => $value) {
                           $invoiceMaster[$key]['images'] = $this->db->select("ppii_id, ppii_invoice_id, CONCAT('$imageUrl',ppii_image) AS ppii_image", false)
                                           ->get_where(tbl_products_purchase_invoice_images, array('ppii_invoice_id' => $value['ppi_id']))->result_array();

                           $invoiceMaster[$key]['prodPicked'] = $this->db->get_where(tbl_products_order_pickup, array('pop_invoice_master' => $value['ppi_id']))->result_array();
                      }
                 }
                 return $invoiceMaster;
            }
            return false;
       }

       function removeProductPicked($popId) {
            $this->db->delete(tbl_products_order_pickup, array('pop_id' => $popId));
            return true;
       }

       function addProductPicked($data) {
            $this->db->insert(tbl_products_order_pickup, $data);
            return true;
       }

       function isAlreadyExistsInvoice($orderId, $supplierId) {
            return $this->db->get_where(tbl_products_purchase_invoice, array('ppi_order_id' => $orderId, 'ppi_supplier_id' => $supplierId))->row_array();
       }

       function getOrderDetailsByDB($db) {
            $selectOrderMaster = array(
                tbl_products_order_master . '.ordm_id',
                tbl_products_order_master . '.ordm_number',
                tbl_products_order_master . '.ordm_total_amount',
                tbl_products_order_master . '.ordm_total_discount',
                tbl_products_order_master . '.ordm_delivery_chrg',
                tbl_products_order_master . '.ordm_total_shop_charge',
                tbl_products_order_master . '.ordm_grand_total',
                tbl_products_order_master . '.ordm_current_status',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_products_order_master . '.ordm_added_on',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_users . '.usr_username AS buy_usr_username',
                tbl_users . '.usr_first_name AS buy_usr_first_name',
                tbl_users . '.usr_last_name AS buy_usr_last_name',
                tbl_users . '.usr_phone AS buy_usr_phone',
                tbl_users . '.usr_email AS buy_usr_email',
                tbl_users . '.usr_address AS buy_usr_address',
                tbl_users . '.usr_city AS buy_usr_city',
                tbl_users . '.usr_state AS buy_usr_state',
                tbl_users . '.usr_district AS buy_usr_district',
                tbl_users . '.usr_id AS buy_usr_id',
                'dboy.usr_first_name AS dboy_usr_first_name',
                'dboy.usr_last_name AS dboy_usr_last_name',
                tbl_address_book . '.adbk_email',
                tbl_address_book . '.adbk_first_name',
                tbl_address_book . '.adbk_last_name',
                tbl_address_book . '.adbk_phone',
                tbl_address_book . '.adbk_address',
                tbl_address_book . '.adbk_bulding_name',
                tbl_address_book . '.adbk_block_name',
                tbl_address_book . '.adbk_street',
                tbl_address_book . '.adbk_pin_code',
                tbl_address_book . '.adbk_locality',
                tbl_address_book . '.adbk_lat',
                tbl_address_book . '.adbk_lon',
                tbl_address_book . '.adbk_city',
                tbl_address_book . '.adbk_state',
                tbl_address_book . '.adbk_ladmark',
                tbl_address_book . '.adbk_country',
                tbl_products_order_statuses . '.*',
                'itemCount.*'
            );
            $selectOrderDetails = array(
                tbl_products_order_details . '.ordd_id',
                tbl_products_order_details . '.ordd_product_name',
                tbl_products_order_details . '.ordd_noOf_items',
                tbl_products_order_details . '.ordd_unit_amount',
                tbl_products_order_details . '.ordd_unit_discount',
                tbl_products_order_details . '.ordd_unit_total',
                tbl_products_order_details . '.ordd_product_image',
                tbl_products_order_details . '.ordd_town',
                tbl_products_units . '.unt_unit_name_en',
                tbl_products_units . '.unt_unit_en',
                tbl_products_master . '.prd_name_en',
                tbl_products_master . '.prd_keywords',
                tbl_products_master . '.prd_desc',
                tbl_supplier_master . '.supm_id',
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_number',
                tbl_supplier_master . '.supm_email',
                tbl_supplier_master . '.supm_address',
                tbl_supplier_master . '.supm_market',
                tbl_supplier_master . '.supm_shop_charges',
                tbl_market_places . '.mar_name',
                tbl_market_places . '.mar_lat',
                tbl_market_places . '.mar_long',
                tbl_products_stock_details . '.*'
            );
            $subQuery = '(SELECT ordd_order_master_id, SUM(ordd_noOf_items) AS ordd_no_of_items ' . 'FROM ' . tbl_products_order_details . ' GROUP BY ordd_order_master_id)';
            $orderMaster = $this->db->select($selectOrderMaster)
                            ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                            ->join($subQuery . ' itemCount', 'itemCount.ordd_order_master_id = ' . tbl_products_order_master . '.ordm_id', 'LEFT')
                            ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                            ->join(tbl_address_book, tbl_address_book . '.adbk_id = ' . tbl_products_order_master . '.ordm_delivery_address', 'LEFT')
                            ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                            ->get_where(tbl_products_order_master, array('ordm_delivery_boy' => $db, 'ordm_current_status' => ORDER_PICKED))->result_array();

            if (!empty($orderMaster)) {
                 foreach ($orderMaster as $key => $value) {
                      $orderMaster[$key]['productDetails'] = $this->db->select(implode(',', $selectOrderDetails))
                                      ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                      ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->join(tbl_products_order_pickup, tbl_products_order_pickup . '.pop_ordd_id = ' . tbl_products_order_details . '.ordd_id', 'RIGHT')
                                      ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $value['ordm_id']))->result_array();
                 }
            }
            return $orderMaster;
       }

       function activeDeaciveUser($userId, $status) {
            $this->db->where('usr_id', $userId);
            $this->db->update(tbl_users, array('usr_active' => $status));
            return true;
       }

       function actualWeightAdjustment($orddId, $actualWeight, $db, $actualRate) {
            $this->db->where('ordd_id', $orddId)->update(tbl_products_order_details, array('ordd_actual_weight' => $actualWeight,
                'ordd_actual_rate' => $actualRate));

            $logArray = array(
                'actual_weight' => $actualWeight,
                'actual_rate' => $actualRate,
                'db' => $db,
                'ordd_id' => $orddId
            );

            generate_log(array(
                'log_title' => 'Actual weight adjustment',
                'log_desc' => serialize($logArray),
                'log_controller' => 'actualWeightAdjustment',
                'log_action' => 'U',
                'log_ref_id' => $orddId,
                'log_added_by' => $db,
            ));

            return true;
       }

       function getOrderDetailsByDBAndStatus($db, $status) {
            $selectOrderMaster = array(
                tbl_products_order_master . '.ordm_id',
                tbl_products_order_master . '.ordm_number',
                tbl_products_order_master . '.ordm_total_amount',
                tbl_products_order_master . '.ordm_total_discount',
                tbl_products_order_master . '.ordm_delivery_chrg',
                tbl_products_order_master . '.ordm_total_shop_charge',
                tbl_products_order_master . '.ordm_grand_total',
                tbl_products_order_master . '.ordm_current_status',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_products_order_master . '.ordm_added_on',
                tbl_products_order_master . '.ordm_payment_type',
                tbl_users . '.usr_username AS buy_usr_username',
                tbl_users . '.usr_first_name AS buy_usr_first_name',
                tbl_users . '.usr_last_name AS buy_usr_last_name',
                tbl_users . '.usr_phone AS buy_usr_phone',
                tbl_users . '.usr_email AS buy_usr_email',
                tbl_users . '.usr_address AS buy_usr_address',
                tbl_users . '.usr_city AS buy_usr_city',
                tbl_users . '.usr_state AS buy_usr_state',
                tbl_users . '.usr_district AS buy_usr_district',
                tbl_users . '.usr_id AS buy_usr_id',
                'dboy.usr_first_name AS dboy_usr_first_name',
                'dboy.usr_last_name AS dboy_usr_last_name',
                tbl_address_book . '.adbk_email',
                tbl_address_book . '.adbk_first_name',
                tbl_address_book . '.adbk_last_name',
                tbl_address_book . '.adbk_phone',
                tbl_address_book . '.adbk_address',
                tbl_address_book . '.adbk_bulding_name',
                tbl_address_book . '.adbk_block_name',
                tbl_address_book . '.adbk_street',
                tbl_address_book . '.adbk_pin_code',
                tbl_address_book . '.adbk_locality',
                tbl_address_book . '.adbk_lat',
                tbl_address_book . '.adbk_lon',
                tbl_address_book . '.adbk_city',
                tbl_address_book . '.adbk_state',
                tbl_address_book . '.adbk_ladmark',
                tbl_address_book . '.adbk_country',
                tbl_products_order_statuses . '.*',
                'itemCount.*'
            );
            $selectOrderDetails = array(
                tbl_products_order_details . '.ordd_id',
                tbl_products_order_details . '.ordd_product_name',
                tbl_products_order_details . '.ordd_noOf_items',
                tbl_products_order_details . '.ordd_unit_amount',
                tbl_products_order_details . '.ordd_unit_discount',
                tbl_products_order_details . '.ordd_unit_total',
                tbl_products_order_details . '.ordd_product_image',
                tbl_products_order_details . '.ordd_town',
                tbl_products_units . '.unt_unit_name_en',
                tbl_products_units . '.unt_unit_en',
                tbl_products_master . '.prd_name_en',
                tbl_products_master . '.prd_keywords',
                tbl_products_master . '.prd_desc',
                tbl_supplier_master . '.supm_id',
                tbl_supplier_master . '.supm_name',
                tbl_supplier_master . '.supm_number',
                tbl_supplier_master . '.supm_email',
                tbl_supplier_master . '.supm_address',
                tbl_supplier_master . '.supm_market',
                tbl_supplier_master . '.supm_shop_charges',
                tbl_market_places . '.mar_name',
                tbl_market_places . '.mar_lat',
                tbl_market_places . '.mar_long',
                tbl_products_stock_details . '.*'
            );
            $status = explode(',', $status);
            $subQuery = '(SELECT ordd_order_master_id, SUM(ordd_noOf_items) AS ordd_no_of_items ' . 'FROM ' . tbl_products_order_details . ' GROUP BY ordd_order_master_id)';
            $orderMaster = $this->db->select($selectOrderMaster)
                            ->join(tbl_users, tbl_users . '.usr_id = ' . tbl_products_order_master . '.ordm_buyer', 'LEFT')
                            ->join($subQuery . ' itemCount', 'itemCount.ordd_order_master_id = ' . tbl_products_order_master . '.ordm_id', 'LEFT')
                            ->join(tbl_users . ' dboy', 'dboy.usr_id = ' . tbl_products_order_master . '.ordm_delivery_boy', 'LEFT')
                            ->join(tbl_address_book, tbl_address_book . '.adbk_id = ' . tbl_products_order_master . '.ordm_delivery_address', 'LEFT')
                            ->join(tbl_products_order_statuses, tbl_products_order_statuses . '.pos_id = ' . tbl_products_order_master . '.ordm_current_status', 'LEFT')
                            ->where_in(tbl_products_order_master . '.ordm_current_status', $status)->get_where(tbl_products_order_master, array('ordm_delivery_boy' => $db))->result_array();

            if (!empty($orderMaster)) {
                 foreach ($orderMaster as $key => $value) {
                      $orderMaster[$key]['productDetails'] = $this->db->select(implode(',', $selectOrderDetails))
                                      ->join(tbl_products_stock_details, tbl_products_stock_details . '.pdsm_id = ' . tbl_products_order_details . '.ordd_prd_stock_id', 'LEFT')
                                      ->join(tbl_products_stock_master, tbl_products_stock_master . '.psm_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_master, tbl_products_master . '.prd_id = ' . tbl_products_order_details . '.ordd_prd_master_id', 'LEFT')
                                      ->join(tbl_products_units, tbl_products_units . '.unt_id = ' . tbl_products_stock_details . '.pdsm_unit', 'LEFT')
                                      ->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT')
                                      ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_supplier_master . '.supm_market', 'LEFT')
                                      ->get_where(tbl_products_order_details, array('ordd_order_master_id' => $value['ordm_id']))->result_array();
                 }
            }
            return $orderMaster;
       }

       public function checkCategoryExist($category_id) {
            $results = $this->db->get_where(tbl_category, array('cat_status' => 1, 'cat_id' => $category_id))->result_array();
            if (!empty($results)) {
                 return true;
            }
            return false;
       }

       function getSuppliersMarket($town = '') {
            if (!empty($town)) {
                 $this->db->like(tbl_market_places . '.mar_name', $town, 'both');
            }
            $townids = $this->db->select('mar_id')->join(tbl_supplier_master, tbl_supplier_master . '.supm_market = ' . tbl_market_places . '.mar_id', 'RIGHT')
                            ->order_by(tbl_market_places . '.mar_name', 'ASC')->where('mar_status', 1)->get(tbl_market_places)->result_array();
            $townids = array_unique(array_column($townids, 'mar_id'));

            if (!empty($townids)) {
                 return $this->db->select('mar_id, mar_name, mar_lat, mar_long, mar_status')
                                 ->order_by('mar_name', 'ASC')->where_in('mar_id', $townids)->get(tbl_market_places)->result_array();
            }
       }

       function userExistsInGroup($key, $val, $group) {
            if (!empty($key) && !empty($val) && !empty($group)) {
                 $select = array(
                     tbl_users . '.usr_id',
                     tbl_users . '.usr_code',
                     tbl_users . '.usr_first_name',
                     tbl_users . '.usr_phone',
                     tbl_users . '.usr_email',
                     tbl_users_groups . '.group_id as group_id',
                     tbl_groups . '.name as group_name',
                     tbl_groups . '.description as group_desc'
                 );
                 return $this->db->select($select)
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->where_in(tbl_groups . '.id', explode(',', $group))
                                 ->like($key, $val, 'both')->get(tbl_users)->row_array();
            }
       }

       public function getCompanyNews() {
            return get_settings_by_key('company_news');
       }

       /**
        * TODO: get virtual shop categories
        * @param type $limit
        * @return type
        */
       function getVirtualShopCategories($limit, $town) {
            //print_r('erasa');die;
            $this->db->select();
            $imageUrl = site_url('assets/uploads/category') . '/';
            if ($limit > 0) {
                 $this->db->limit($limit);
            }
            return $this->db->select(tbl_category . ".*, CONCAT('$imageUrl',cat_image) AS cat_image, CONCAT('$imageUrl',cat_page_banner) AS cat_page_banner", false)
                            ->order_by('cat_title', 'ASC')->join(tbl_virtshop_catog_town_assoc, 'vsc_cat_id = cat_id', 'INNER')->get_where(tbl_category, array('cat_status' => 1, 'vsc_town_id' => $town))->result_array();
       }

       function addToRating($data) {

            $alreadyDone = $this->db->get_where(tbl_rating, array('rat_consign_id' => $data['rat_consign_id'],
                        'rat_added_by' => $data['rat_added_by']))->row_array();

            if (!empty($alreadyDone)) {
                 return false;
            }
            if ($this->db->insert(tbl_rating, $data)) {
                 $ttlRating = $this->db->select_sum('rat_rating')->get_where(tbl_rating, array('rat_consign_id' => $data['rat_consign_id']))->row()->rat_rating;
                 $ttlCount = $this->db->get_where(tbl_rating, array('rat_consign_id' => $data['rat_consign_id']))->num_rows();
                 $rate = @ round($ttlRating / $ttlCount);
                 $this->db->where('supm_id', $data['rat_consign_id'])->update(tbl_rating, array('supm_rating' => $rate));
                 return true;
            }
            return false;
       }
  } 