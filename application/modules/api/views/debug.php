<?php echo site_url('api'); ?><br>
Register
<form action="<?php echo site_url('api/signup');?>" method="post">
     <input type="text" name="name" placeholder="name"/><br />
     <input type="text" name="mobile" placeholder="mobile"/><br />
     <input type="text" name="phone_code" placeholder="phone_code"/><br />
     <input type="text" name="email" placeholder="email"/><br />
     <input type="text" name="password" placeholder="password"/><br />
     <input type="text" name="pushy_token" placeholder="pushy_token"/><br />
     <input type="submit" value="Submit"/>
</form>

Register verify otp
<form action="<?php echo site_url('api/verifyOTP');?>" method="post">
     <input type="text" name="token" placeholder="token"/><br />
     <input type="text" name="otp" placeholder="otp"/><br />
     <input type="text" name="type" placeholder="type"/><br />
     <input type="submit" value="Submit"/>
</form>

New product order
<form action="<?php echo site_url('api/newProductOrder');?>" method="post">
     <textarea name="order">{"master":{"ordm_number":"15933@1587708009539","ordm_buyer":"15933","ordm_total_amount":1100.0,"ordm_total_discount":0.0,"ordm_delivery_chrg":0.0,"ordm_grand_total":1100.0},"details":[{"ordd_product_image":"https://kleemz.com/k_cpanel/assets/uploads/product/thumb_1587543205.3635chicken_biriyani_.jpg","ordd_product_name":"Chicken Biriyani (Catering)","ordd_prd_master_id":"5363","ordd_prd_stock_id":"18448","ordd_unit_id":"1","ordd_unit_amount":"1100.0","ordd_unit_discount":0.0,"ordd_unit_total":1100.0,"ordd_noOf_items":"2","ordd_supplier":"16949"}]}</textarea><br />
     <input type="text" name="ordm_payment_type" placeholder="ordm_payment_type" value="1"/><br />
     <input type="text" name="ordm_delivery_address" placeholder="ordm_delivery_address" value="47"/><br />
     <input type="text" name="ordm_billing_address" placeholder="ordm_billing_address" value="47"/><br />
     <input type="text" name="ordm_buyer_selected_town" placeholder="ordm_buyer_selected_town" value="8"/><br />
     <input type="submit" value="Submit"/>
</form>

Forgot passwrod
<form action="<?php echo site_url('api/forgot_password');?>" method="post">
     <input type="text" name="emailorphone" placeholder="emailorphone"/><br />
     <input type="submit" value="Submit"/>
</form>

Shop charge
<form action="<?php echo site_url('api/getShopCharge');?>" method="post">
     <input type="text" name="supplier" placeholder="supplier"/><br />
     <input type="submit" value="Submit"/>
</form>

Calculate Delivery Charge
<form action="<?php echo site_url('api/calculateDeliveryCharge');?>" method="post">
     <input type="text" name="ship_address" placeholder="ship_address" value="10"/><br />
     <input type="text" name="suppliers" placeholder="suppliers" value="14896,14898"/><br />
     <input type="submit" value="Submit"/>
</form>

getProductSubCategories
<form action="<?php echo site_url('api/getProductSubCategories');?>" method="post">
     <input type="text" name="parent_cat" placeholder="parent_cat" value=""/><br />
     <input type="submit" value="Submit"/>
</form>

getProducts
<form action="<?php echo site_url('api/getProducts');?>" method="post">
     category : <input type="text" name="category" placeholder="category" value="166"/>veg<br />
     page : <input type="text" name="page" placeholder="page" value="0"/><br />
     limit : <input type="text" name="limit" placeholder="limit" value="100"/><br />
     town : <input type="text" name="town" placeholder="town" value="364"/>Valanchery<br /> 
     credential : <input type="text" name="credential" placeholder="credential" value="16293"/><br /> 
     <input type="submit" value="Submit"/>
</form>

getProductDetails
<form action="<?php echo site_url('api/getProductDetails');?>" method="post">
     prdid : <input type="text" name="prdid" placeholder="prdid" value="94"/><br />
     lat : <input type="text" name="lat" placeholder="lat" value="10.888774403109297"/><br />
     lng : <input type="text" name="lng" placeholder="lng" value="76.07347985066473"/><br />
     town : <input type="text" name="town" placeholder="town" value="364"/><br />
     credential : <input type="text" name="credential" placeholder="credential" value="16293"/><br />
     <input type="submit" value="Submit"/>
</form>

getHomeBanners
<form action="<?php echo site_url('api/getHomeBanners');?>" method="post">
     <input type="text" name="lat" placeholder="lat" value="1564"/><br />
     <input type="text" name="lan" placeholder="lan" value="1564"/><br />
     <input type="text" name="town" placeholder="town" value="10"/><br />
     <input type="submit" value="Submit"/>
</form>

getLoggedUser
<form action="<?php echo site_url('api/getLoggedUser');?>" method="post">
     <input type="text" name="credential" placeholder="credential" value=""/>
     <input type="submit" value="Submit"/>
</form>

updateUser
<form action="<?php echo site_url('api/updateUser');?>" method="post">
     <input type="text" name="credential" placeholder="credential" value="12"/><br />
     <textarea name="details">{"usr_email":"hfgjgfhkgh@gmail.com","usr_username":"Icici","usr_phone_code":"+91","usr_phone":"12345678"}</textarea><br />
     <input type="submit" value="Submit"/>
</form>

myOrderHistory
<form action="<?php echo site_url('api/myOrderHistory');?>" method="post">
     <input type="text" name="userId" placeholder="userId" value="16571"/><br />
     <input type="submit" value="Submit"/>
</form>

myOrderDetailsHistory
<form action="<?php echo site_url('api/myOrderDetailsHistory');?>" method="post">
     <input type="text" name="orderId" placeholder="orderId" value="1"/><br />
     <input type="submit" value="Submit"/>
</form>

addToFavourite
<form action="<?php echo site_url('api/addToFavourite');?>" method="post">
     <input type="text" name="type" placeholder="type" value="PRD"/><br />
     <input type="text" name="id" placeholder="id" value="1"/><br />
     <input type="text" name="userId" placeholder="userId" value="16571"/><br />
     <input type="submit" value="Submit"/>
</form>

removeFavourite
<form action="<?php echo site_url('api/removeFavourite');?>" method="post">
     <input type="text" name="id" placeholder="id" value="1"/><br />
     <input type="submit" value="Submit"/>
</form>

myFavouriteList
<form action="<?php echo site_url('api/myFavouriteList');?>" method="post">
     <input type="text" name="userId" placeholder="userId" value="16571"/><br />
     <input type="submit" value="Submit"/>
</form>

updateProfile
<form action="<?php echo site_url('api/updateProfile');?>" method="post" enctype="multipart/form-data">
     <input type="file" name="avatar" placeholder="avatar"/><br />
     <input type="submit" value="Submit"/>
</form>



cancelOrder
<form action="<?php echo site_url('api/cancelOrder');?>" method="post">
     <input type="text" name="orderId" placeholder="orderId" value="1880"/><br />
     <input type="text" name="userId" placeholder="userId" value="19717"/><br />
     <input type="submit" value="Submit"/>
</form>

changeOrderStatus
<form action="<?php echo site_url('api/changeOrderStatus');?>" method="post">
     <input type="text" name="order_id" placeholder="order_id" value="1307"/><br />
     <input type="text" name="status" placeholder="status" value="3"/><br />
     <input type="text" name="user_id" placeholder="user_id" value="18525"/><br />
     <input type="submit" value="Submit"/>
</form>


getMasterProductsList
<form action="<?php echo site_url('api/getMasterProductsList');?>" method="post">
     category : <input type="text" name="category" placeholder="category" value="166"/>veg<br />
     page : <input type="text" name="page" placeholder="page" value="0"/><br />
     limit : <input type="text" name="limit" placeholder="limit" value="100"/><br />
     town : <input type="text" name="town" placeholder="town" value="364"/>Valanchery<br /> 
     credential : <input type="text" name="credential" placeholder="credential" value="16293"/><br /> 
     <input type="submit" value="Submit"/>
</form>

getSuppliersProductsByProduct
<form action="<?php echo site_url('api/getSuppliersProductsByProduct');?>" method="post">
     prdid : <input type="text" name="prdid" placeholder="prdid" value="1"/><br />
     lat : <input type="text" name="lat" placeholder="lat" value="10.888774403109297"/><br />
     lng : <input type="text" name="lng" placeholder="lng" value="76.07347985066473"/><br />
     town : <input type="text" name="town" placeholder="town" value="364"/><br />
     credential : <input type="text" name="credential" placeholder="credential" value="16293"/><br />
     <input type="submit" value="Submit"/>
</form>

deliveryboyRequest
<form action="<?php echo site_url('api/deliveryboyRequest');?>" method="post" enctype="multipart/form-data">
     first name : <input type="text" name="usr_first_name" placeholder="first name"/><br />
     phone : <input type="text" name="usr_phone" placeholder="phone"/><br />
     address : <input type="text" name="usr_address" placeholder="address"/><br />
     avatar : <input type="file" name="usr_avatar" placeholder="avatar"/><br />
     <input type="submit" value="Submit"/>
</form>

townSearch
<form action="<?php echo site_url('api/townSearch');?>" method="post" enctype="multipart/form-data">
     town : <input type="text" name="town" placeholder="town"/><br />
     limit : <input type="text" name="limit" placeholder="limit"/><br />
     status : <input type="text" name="status" placeholder="status"/><br />
     <input type="submit" value="Submit"/>
</form>

uploadInvoiceByDB
<form action="<?php echo site_url('api/uploadInvoiceByDB');?>" method="post" enctype="multipart/form-data">
     ppi_order_id : <input type="text" name="ppi_order_id" placeholder="ppi_order_id" value="2034"/><br />
     ppi_added_by : <input type="text" name="ppi_added_by" placeholder="ppi_added_by" value="9088"/><br />
     ppi_supplier_id : <input type="text" name="ppi_supplier_id" placeholder="ppi_supplier_id" value="5859"/><br />
     ppi_invoice_num : <input type="text" name="ppi_invoice_num" placeholder="ppi_invoice_num" value=""/><br />
     ppi_invoice_img : <input type="file" name="ppi_invoice_img[]" placeholder="avatar" multiple/><br />
     ordd_id : <input type="text" name="ordd_id[]" placeholder="ordd_id" value="8632"/><br />
     ordd_id : <input type="text" name="ordd_id[]" placeholder="ordd_id" value="8633"/><br />
     ordd_id : <input type="text" name="ordd_id[]" placeholder="ordd_id" value="8634"/><br />
     ordd_id : <input type="text" name="ordd_id[]" placeholder="ordd_id" value="8635"/><br />
     ordd_id : <input type="text" name="ordd_id[]" placeholder="ordd_id" value="8636"/><br />
     <input type="submit" value="Submit"/>
</form>

removeInvoice
<form action="<?php echo site_url('api/removeInvoice');?>" method="post" enctype="multipart/form-data">
     ppi_id : <input type="text" name="ppi_id" placeholder="ppi_id" value="1"/><br />
     <input type="submit" value="Submit"/>
</form>

getInvoiceByOrder
<form action="<?php echo site_url('api/getInvoiceByOrder');?>" method="post" enctype="multipart/form-data">
     ppi_id : <input type="text" name="ppi_order_id" placeholder="ppi_order_id" value="2034"/><br />
     <input type="submit" value="Submit"/>
</form>

getProductsBySupplier
<form action="<?php echo site_url('api/getProductsBySupplier');?>" method="post" enctype="multipart/form-data">
     limit : <input type="text" name="limit" placeholder="limit" value="100"/><br />
     page : <input type="text" name="page" placeholder="page" value="0"/><br />
     supplier : <input type="text" name="supplier" placeholder="supplier" value="5322"/>
     <input type="submit" value="Submit"/>
</form