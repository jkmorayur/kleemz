<?php

  defined('BASEPATH') or exit('No direct script access allowed');
  require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
  require './vendor/autoload.php';

  class api extends CI_Controller {

       public $func_without_token = array(
           'townSearch',
       );
       public $mail = '';

       public function __construct() {
            parent::__construct();
            $this->load->library('upload');
            $this->load->model('api_auth_model', 'api_auth');
            $this->load->model('api_model', 'api_model');
            $this->load->helper(array('form'));
            $this->load->model('business/business_model', 'supplier');
            /* Load form validation library */
            $this->load->library('form_validation');
//            $this->checkToken();
            $this->mail = new PHPMailer;
            if ($this->uri->segment(2) != 'debug') {
                 header('Content-Type: application/json');
            }
       }

       function nearestTown() {
            $lat = isset($_POST['lat']) ? trim($_POST['lat']) : '';
            $lng = isset($_POST['lng']) ? trim($_POST['lng']) : '';
            $miles = DEF_MILE; //isset($_POST['miles']) ? trim($_POST['miles']) : 10;
            $limit = isset($_POST['limit']) ? trim($_POST['limit']) : 10;
            if (!empty($lat) && !empty($lng)) {
                 $return = $this->api_model->nearestTown($lat, $lng, $miles, $limit);
                 die(json_encode($return));
            }
       }

       function townSearch() {
            $town = isset($_POST['town']) ? trim($_POST['town']) : '';
            $limit = isset($_POST['limit']) ? trim($_POST['limit']) : '';
            $status = isset($_POST['status']) ? trim($_POST['status']) : 1;
            if (!empty($limit)) {
                 $return = $this->api_model->townSearch($town, $limit, $status);
                 die(json_encode($return));
            } else {
                 $return = array('error' => 'Passing empty parameters');
                 die(json_encode($return));
            }
       }

       /**
        * TODO: delete this function on next live
        * @param type $limit
        * @return type
        */
       function getAllCategories() {
            $limit = $this->input->post('limit', true);
            $return = $this->api_model->getAllCategories($limit);
            die(json_encode($return));
       }

       function getAllFirmCategories() {
            $limit = $this->input->post('limit', true);
            $return = $this->api_model->getAllFirmCategories($limit);
            die(json_encode($return));
       }

       function getProductParentCategories() {
            $return = $this->api_model->getProductParentCategories();
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       function getProducts() {
            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $category = (isset($_POST['category']) && !empty($_POST['category'])) ? trim($_POST['category']) : 0;
            $town = (isset($_POST['town']) && !empty($_POST['town'])) ? trim($_POST['town']) : 0;
            $credential = (isset($_POST['credential']) && !empty($_POST['credential'])) ? trim($_POST['credential']) : 0;
            generate_log(array(
                'log_title' => 'getProducts',
                'log_desc' => serialize($this->input->post()),
                'log_controller' => 'getProducts',
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => $credential
            ));
            $return = $this->api_model->getProducts($category, $limit, $pageNum, $town, $credential);
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       public function searchProducts() {
            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $keyword = (isset($_POST['keyword']) && !empty($_POST['keyword'])) ? trim($_POST['keyword']) : '';
            if (empty($keyword)) {
                 die(json_encode(array('error' => true, 'msg' => 'Empty keyword')));
            } else {
                 $data = $this->api_model->searchProducts($keyword, $limit, $pageNum);
                 die(json_encode($data, JSON_UNESCAPED_SLASHES));
            }
       }

       function getProductSubCategories() {
            $parent_cat = isset($_POST['parent_cat']) ? trim($_POST['parent_cat']) : '';
            $return = $this->api_model->getProductSubCategories($parent_cat);
            die(json_encode($return));
       }

       function getNearestFoodPlaces() {
            $lat = isset($_POST['lat']) ? trim($_POST['lat']) : '';
            $lng = isset($_POST['lng']) ? trim($_POST['lng']) : '';
            $miles = DEF_MILE; //isset($_POST['miles']) ? trim($_POST['miles']) : 10;
            $limit = isset($_POST['limit']) ? trim($_POST['limit']) : 10;
            if (empty($lat) || empty($lng)) {
                 die(json_encode(array('error' => true, 'msg' => 'Empty latitude or longitude')));
            } else {
                 $return = $this->api_model->getNearestFoodPlaces($lat, $lng, $miles, $limit);
                 die(json_encode($return, JSON_UNESCAPED_SLASHES));
            }
       }

       public function checkToken() {
            $method = $this->uri->segment(2);

            // $log_id = $this->api_model->saveApiRequest('buyer_api',$method,$_SERVER,$_REQUEST,$_FILES);
            if ((!$this->input->get_request_header('Token', true) || !$this->input->get_request_header('Token')) && !in_array($method, $this->func_without_token)) {
                 $return = array('error' => 'Invalid Access Token');
                 echo json_encode($return);
                 die();
            }if (!in_array($method, $this->func_without_token)) {
                 if (!$this->api_auth->checkTokenExist($this->input->get_request_header('Token'), $method)) {
                      $return = array('error' => 'Invalid Access Token');
                      echo json_encode($return);
                      die();
                 }
            }
            // $user = $this->api_model->getUserDetailsFromToken($this->input->get_request_header('Token'));
            // $this->api_model->updateApiRequest($log_id,$user['usr_id']);
       }

       public function login() {
            // $input_data = $this->input->post(); //json_decode(trim(file_get_contents('php://input')), true);
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $pushy_token = $this->input->post('pushy_token', true);
            $user_group_id = $this->input->post('user_group_id', true);
            if ($email != null && $email != '' && $password != '' && $password != null && $user_group != '') {
                 $det = $this->api_auth->login($email, $password, false, $user_group_id);
                 if ($det == "timeout") {
                      $return = array('msg' => 'Max Login Attempts Exceeded', 'error' => true);
                 } else if ($det == "inactive") {
                      $return = array('msg' => 'Account Inactive', 'error' => true);
                 } else if ($det == "invalid") {
                      $return = array('msg' => 'Invalid Credentials', 'error' => true);
                 } else {
                      $this->api_model->updatePushyToken($pushy_token, 'usr_token', $det->usr_token);
                      $return = array(
                          'error' => false,
                          'msg' => 'You are successfully logged in',
                          'token' => $det->usr_token,
                          'full_name' => $det->usr_first_name . ' ' . $det->usr_last_name,
                          'username' => $email,
                          'company' => $det->usr_company,
                          'image' => site_url() . 'assets/uploads/avatar/' . $det->usr_avatar,
                          'identity' => $det->usr_id,
                          'email' => $det->usr_email
                      );
                 }
                 die(json_encode($return));
            } else {
                 $return = array('msg' => 'Partial Content', 'error' => true);
                 die(json_encode($return));
            }
       }

       function getProductDetails() {
            $prdid = isset($_POST['prdid']) ? trim($_POST['prdid']) : 0;
            $lat = isset($_POST['lat']) ? trim($_POST['lat']) : '';
            $lng = isset($_POST['lng']) ? trim($_POST['lng']) : '';
            $credential = (isset($_POST['credential']) && !empty($_POST['credential'])) ? trim($_POST['credential']) : 0;
            $supplier = (isset($_POST['supplier']) && !empty($_POST['supplier'])) ? trim($_POST['supplier']) : 0;
            $miles = DEF_MILE; //isset($_POST['miles']) ? trim($_POST['miles']) : 10;

            generate_log(array(
                'log_title' => 'getProducts',
                'log_desc' => serialize($this->input->post()),
                'log_controller' => 'getProductDetails',
                'log_action' => 'C',
                'log_ref_id' => $prdid,
                'log_added_by' => $credential
            ));

            if (empty($prdid)) {
                 die(json_encode(array('msg' => 'Empty product id', 'error' => true)));
            } else {
                 $productDetails = $this->api_model->getProductDetails($prdid, '', $lat, $lng, $miles, $credential, $supplier);
                 die(json_encode(array('msg' => '', 'error' => false, 'data' => $productDetails)));
            }
       }

       public function sendMail($tomail, $type, $mdata = '') {
            /* Sent activation email */
            $this->mail->isSMTP();
            $this->mail->Host = MAIL_HOST;
            $this->mail->SMTPAuth = true;
            $this->mail->Username = MAIL_USERNAME;
            $this->mail->Password = MAIL_PASSWORD;
            $this->mail->SMTPSecure = 'ssl';
            $this->mail->Port = 465;
            $this->mail->setFrom(FROM_MAIL, FROM_NAME);
            $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
            $this->mail->addAddress($tomail);
            //remove this from server
            $this->mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ),
            );
            //remove this from server
            if ($type == "reg") {
                 $this->mail->Subject = 'Hi ' . $tomail . ', please verify your account';
                 $mailData['mail'] = $tomail;
                 $mailData['otp'] = $mdata['otp'];
                 $mailContent = $this->load->view('user/register-mail-otp-template', $mailData, true);
            }
            if ($type == "reset_pwd") {
                 $this->mail->Subject = 'Reset your password';
                 $mailData['name'] = $mdata['name'];
                 $mailData['otp'] = $mdata['otp'];
                 $mailContent = $this->load->view('user/forget-mail-template', $mailData, true);
            }
            $this->mail->isHTML(true);

            $this->mail->Body = $mailContent;
            return $this->mail->send();
            /* Sent activation email */
       }

       public function forgot_password() {
            $email = $this->input->post('emailorphone');
            if ($email != null && $email != '') {
                 $otp = generate_otp();
                 $forgotten = $this->api_auth->forgotten_password($email, $otp);
                 if ($forgotten) {
                      $user = $this->api_model->getUserByIdentity($email);
                      $maildata['otp'] = $otp;
                      $maildata['name'] = $user['usr_first_name'] . ' ' . $user['usr_last_name'];
                      //$this->sendMail($email, 'reset_pwd', $maildata);
                      $msg = '<#> ' . $otp . ', is your OTP to access kleemz.com. Requested at : ' . date('h:i:s') . ', ' . date('d/m/Y') . ' ' . get_settings_by_key('sms_otp_signature');
                      $buf = send_sms($msg, $email, 4);
                      $return = array('msg' => 'An OTP has been sent to your registerd mobile number', 'error' => false,
                          'token' => $user['usr_token'], 'identity' => $user['usr_id'], 'otp' => $otp, 'buf' => $buf);
                 } else {
                      $return = array('error' => true, 'msg' => 'No matching user found');
                 }
                 echo json_encode($return);
                 die();
            } else {
                 $return = array('error' => true, 'msg' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
       }

       public function verifyForgotPasswordOTP() {
            $token = trim($this->input->post('token', true));
            $otp = trim($this->input->post('otp', true));

            if ($token == null || $token == '' || $otp == null || $otp == '') {
                 $return = array('msg' => 'Partial Content', 'error' => true);
                 die(json_encode($return));
            }
            $user = $this->api_model->getUserByIdentityOTPForgetPassword($token, $otp);

            if ($user) {
                 if (isset($user['otp_expry']) && ($user['otp_expry'] > OTP_VALIDITY)) {
                      $userDetails = array(
                          'token' => $user['usr_token'],
                          'identity' => $user['usr_id']
                      );
                      die(json_encode(array('msg' => 'OTP Expired', 'error' => true, 'user_details' => $userDetails)));
                 } else {
                      if ($otp == $user['usr_forgotten_password_otp']) {
                           $return = array('msg' => 'OTP Verified', 'error' => false, 'otp_code' => $user['usr_forgotten_password_code']);
                      } else {
                           $return = array('msg' => 'Invalid OTP', 'error' => true);
                      }
                      die(json_encode($return));
                 }
            } else {
                 die(json_encode(array('msg' => 'Invalid OTP', 'error' => true)));
            }
       }

       /**
        * Resend OTP with respect to forgot password
        */
       public function resendForgotPasswordOTP() {
            $token = trim($this->input->post('token', true));
            if (!empty($token)) {
                 $user = $this->api_model->getUserByIdentityToken($token, '');
                 if (!empty($user)) {
                      $otp = generate_otp();
                      $mob = $user['usr_phone'];
                      $id = $user['usr_id'];
                      $msg = '<#> ' . $otp . ', is your OTP to access kleemz.com. Requested at : ' . date('h:i:s') . ', ' . date('d/m/Y') . ' ' . get_settings_by_key('sms_otp_signature');
                      send_sms($msg, $mob, 4);
                      $updateArray = array(
                          'usr_forgotten_password_otp' => $otp,
                          'usr_otp' => $otp,
                          'usr_forgotten_password_time' => date('Y-m-d H:i:s')
                      );
                      $this->api_model->updateUser($id, $updateArray);
                      die(json_encode(array('msg' => 'OTP sent to your phone', 'error' => false,
                          'phoneNum' => $mob, 'full_name' => $user['usr_username'],
                          'token' => $token, 'identity' => $id, 'otp' => $otp,
                          'email' => $user['usr_email'])));
                 }
                 die(json_encode(array('msg' => 'Unknown user', 'error' => true)));
            } else {
                 die(json_encode(array('msg' => 'Empty token', 'error' => true)));
            }
       }

       public function signup() {
            $fname = trim($this->input->post('name', true));
            $mob = trim($this->input->post('mobile', true));
            $phoneCode = trim($this->input->post('phone_code', true));
            $email = trim($this->input->post('email', true));
            $password = trim($this->input->post('password', true));
            $pushy_token = trim($this->input->post('pushy_token', true));

            $usr_log = trim($this->input->post('usr_log', true));
            $usr_lat = trim($this->input->post('usr_lat', true));

            if ($mob != null && $mob != '' && $password != '' && $password != null && $fname != '' && $fname != null) {
                 $otp = generate_otp();
                 $group = array(4);
                 $data['usr_first_name'] = $fname;
                 $data['usr_username'] = $fname;
                 $data['usr_phone'] = $mob;
                 $data['usr_email'] = $email;
                 $data['usr_added_by'] = 0;
                 $data['usr_otp'] = $otp;
                 $data['usr_phone_code'] = $phoneCode;
                 $data['usr_log'] = $usr_log;
                 $data['usr_lat'] = $usr_lat;
                 $usr_tkn = $this->api_auth->register($fname, $password, $email, $data, $group);
                 if (isset($usr_tkn['tok']) && isset($usr_tkn['identity'])) {
                      //$maildata['otp'] = $otp;
                      //$this->sendMail($email, 'reg', $maildata);
                      $msg = '<#> ' . $otp . ', is your OTP to access kleemz.com. Requested at : ' . date('h:i:s') . ', ' . date('d/m/Y') . ' ' . get_settings_by_key('sms_otp_signature');
                      send_sms($msg, $mob, 4);
                      $this->api_model->updatePushyToken($pushy_token, 'usr_token', $usr_tkn['tok']);
                      $return = array('error' => false, 'msg' => 'Registration successfully completed.', 'phone' => $mob,
                          'token' => $usr_tkn['tok'], 'identity' => (string) $usr_tkn['identity'], 'full_name' => $fname, 'email' => $email, 'otp' => $otp);
                 } else {
                      $return = array('error' => true, 'msg' => 'Failed to register');
                 }
                 die(json_encode($return));
            } else {
                 $return = array('error' => true, 'msg' => 'Partial Content');
                 die(json_encode($return));
            }
       }

       public function verifyOTP() {
            $token = trim($this->input->post('token', true));
            $otp = trim($this->input->post('otp', true));
            $type = trim($this->input->post('type', true));

            if ($token == null || $token == '' || $otp == null || $otp == '') {
                 $return = array('msg' => 'Partial Content', 'error' => true);
                 die(json_encode($return));
            }
            $user = $this->api_model->getUserByIdentityOTP($token, $otp);

            if ($user) {

                 if (isset($user['otp_expry']) && ($user['otp_expry'] > OTP_VALIDITY)) {
                      $userDetails = array(
                          'token' => $user['usr_token'],
                          'identity' => $user['usr_id']
                      );
                      die(json_encode(array('msg' => 'OTP Expired', 'error' => true, 'user_details' => $userDetails)));
                 } else {

                      if ($type == "reset_pwd") {
                           if ($otp == $user['usr_forgotten_password_otp']) {
                                $return = array('msg' => 'OTP Verified', 'error' => false, 'otp_code' => $user['usr_forgotten_password_code']);
                           } else {
                                $return = array('msg' => 'Invalid OTP', 'error' => true);
                           }
                      } else {
                           if ($otp == $user['usr_otp']) {
                                $this->api_model->verifyUser($user['usr_id'], $user);
                                $return = array('msg' => 'OTP Verified', 'error' => false);
                           } else {
                                $return = array('msg' => 'Invalid OTP', 'error' => true);
                           }
                      }
                      die(json_encode($return));
                 }
            } else {
                 die(json_encode(array('msg' => 'Invalid OTP', 'error' => true)));
            }
       }

       /**
        * Resend OTP with respect to register
        */
       public function resendOTP() {
            $token = trim($this->input->post('token', true));
            if (!empty($token)) {
                 $user = $this->api_model->getUserByIdentityToken($token, '');
                 if (!empty($user)) {
                      $otp = generate_otp();
                      $mob = $user['usr_phone'];
                      $id = $user['usr_id'];
                      $msg = '<#> ' . $otp . ', is your OTP to access kleemz.com. Requested at : ' . date('h:i:s') . ' ' . date('d/m/Y') . ' ' . get_settings_by_key('sms_otp_signature');
                      send_sms($msg, $mob, 4);
                      $updateArray = array(
                          'usr_otp' => $otp,
                          'usr_created_date' => date('Y-m-d h:i:s')
                      );
                      $this->api_model->updateUser($id, $updateArray);
                      die(json_encode(array('msg' => 'OTP sent to your phone', 'error' => false,
                          'token' => $token, 'identity' => $id, 'full_name' => $user['usr_username'],
                          'email' => $user['usr_email'], 'otp' => $otp)));
                 }
                 die(json_encode(array('msg' => 'Unknown user', 'error' => true)));
            } else {
                 die(json_encode(array('msg' => 'Empty token', 'error' => true)));
            }
       }

       public function update_password() {
            $email = $this->input->post('emailorphone', true);
            $password = $this->input->post('password', true);
            $usr_forgotten_password_code = $this->input->post('otp_code', true);
            if (empty($email) || empty($password) || empty($usr_forgotten_password_code)) {
                 $return = array('error' => true, 'msg' => 'Partial Content');
                 die(json_encode($return));
            }
            if ($this->api_auth->reset_password($email, $password, $usr_forgotten_password_code)) {
                 $return = array('error' => false, 'msg' => 'Password Updated');
            } else {
                 $return = array('error' => true, 'msg' => 'Failed to update');
            }
            die(json_encode($return));
       }

       public function home($from = '', $log_token = '') {

            $data['banners'] = $this->api_model->getHomeBanners();
            $data['latest_product'] = $this->api_model->getLatestProducts();
            $data['hotselling_product'] = $this->api_model->getHotSellingProducts();
            $data['offer_product'] = $this->api_model->getOfferProducts();
            $data['stock_product'] = $this->api_model->getStockProducts();
            $data['flash_sale_stock'] = $this->api_model->getFlashSaleStock();
            $data['suppliers'] = $this->api_model->getHomeSuppliers();
            $pushy_token = $this->input->post('pushy_token', true);
            if ($this->input->get_request_header('Token', true) || $from == 'login') {
                 $token = $this->input->get_request_header('Token');
                 if ($log_token) {
                      $token = $log_token;
                 }
                 $det = $this->api_model->getUserDetailsFromToken($token);
                 if ($det) {
                      $this->api_model->updatePushyToken($pushy_token, 'usr_token', $token);
                      $favourite = $this->api_model->getFavIds($det['usr_id']);
                      $data['favourite_suppliers'] = $favourite['supplier_id'];
                      $data['favourite_products'] = $favourite['product_id'];

                      $user_details['full_name'] = $det['usr_first_name'] . ' ' . $det['usr_last_name'];
                      $user_details['username'] = $det['usr_email'];
                      $user_details['company'] = $det['usr_company'];
                      $user_details['image'] = site_url() . 'assets/uploads/avatar/' . $det['usr_avatar'];
                      $user_details['is_mail_verified'] = $det['usr_is_mail_verified'];
                      $data['user_details'] = $user_details;
                      $data['recommended_products'] = $this->api_model->getRecommendations($det['usr_id']);
                      $count = $this->api_model->favouriteCount($det['usr_id']);
                      $count['browsing_history'] = 10;
                      $data['count'] = $count;
                 }
            }
            if ($from) {
                 return $data;
            }
            echo json_encode($data);
            die();
       }

       public function view_all() {
            $data = array();
            $type = $this->input->post('type', true);
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            $cat_id = $this->input->post('cat_id', true);
            $sup_id = $this->input->post('sup_id', true);
            $filter = $this->input->post('filter', true);
            $sort_by = $this->input->post('sort_by', true);
            $flash_filter = $this->input->post('flash_sale_filter', true);
            if (!$offset) {
                 $offset = 0;
            }

            if (!$limit) {
                 $limit = 30;
            }
            // if ($type == null || $type == '') {
            //     $return = array('error' => 'Partial Content');
            //     echo json_encode($return);
            //     die();
            // }
            if ($type == "latest" || $type == "our_product") {
                 $data['products'] = $this->api_model->getLatestProducts('view_all', $offset, $limit, $filter, $sort_by, $sup_id, $type);
            }
            if ($type == "hotselling") {
                 $data['products'] = $this->api_model->getHotSellingProducts('view_all', $offset, $limit, $filter, $sort_by);
            }
            if ($type == "offer") {
                 $data['products'] = $this->api_model->getOfferProducts('view_all', $offset, $limit, $filter, $sort_by);
            }
            if ($type == "stock") {
                 $data['products'] = $this->api_model->getStockProducts('view_all', $offset, $limit, $filter, $sort_by, $sup_id);
            }
            if ($type == "flash_sale") {
                 $data['products'] = $this->api_model->getFlashSaleStock('view_all', $offset, $limit, $filter, $sort_by, $flash_filter);
            }
            if ($type == "recommended") {
                 if ($this->input->get_request_header('Token', true)) {
                      $token = $this->input->get_request_header('Token');
                      $det = $this->api_model->getUserDetailsFromToken($token);
                      $data['products'] = $this->api_model->getRecommendations($det['usr_id'], 'view_all', $offset, $limit, $filter, $sort_by);
                 } else {
                      $return = array('error' => 'Access Denied');
                      echo json_encode($return);
                      die();
                 }
            }
            if (!$type) {
                 $data['products'] = $this->api_model->getAllProducts($offset, $limit, $cat_id, $sup_id, $filter, $sort_by);
            }
            echo json_encode($data);
            die();
       }

       public function product_details() {
            $prd_id = $this->input->post('prd_id', true);
            if ($prd_id == null || $prd_id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $return = array('product_details' => $this->api_model->getProductDetails(encryptor($prd_id, 'D')));
            echo json_encode($return);
            die();
       }

       public function contact_supplier() {
            $prd_id = $this->input->post('prd_id', true);
            $sup_id = $this->input->post('sup_id', true);
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $sub = $this->input->post('subject', true);
            $msg = $this->input->post('message', true);
            $unit = $this->input->post('unit', true);
            $quantity = $this->input->post('quantity', true);
            if ($prd_id == null || $prd_id == '' || $sup_id == null || $sup_id == '' || $sub == null || $sub == '' || $msg == null || $msg == '' || $quantity == null || $quantity == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $data = array();
            if (isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])) {
                 $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'product_enquiry/', 'attachment', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
                 $data['ord_attachment'] = $file;
            }
            $dec_sup = encryptor($sup_id, 'D');
            $data['ord_added_by'] = $usr_id;
            $data['ord_number'] = gen_random();
            $data['ord_buyer'] = $usr_id;
            $data['ord_message'] = $msg;
            $data['ord_subject'] = $sub;
            $data['ord_qty'] = $quantity;
            $data['ord_unit'] = $unit;
            $data['ord_prod_id'] = encryptor($prd_id, 'D');
            $sup_det = $this->api_model->getUserDetails($dec_sup);
            $data['ord_supplier'] = $sup_det['usr_supplier'];
            $data['ord_supplier_user_id'] = $dec_sup;
            if ($this->api_model->placeOrder($data)) {
                 $return = array('msg' => 'Your inquiry submitted successfully.We will touch you soon!');
            } else {
                 $return = array('error' => 'Something went wrong!!');
            }
            echo json_encode($return);
            die();
       }

       public function get_config() {
            $conf = $this->api_model->getConfig();
            echo json_encode($conf);
            die();
       }

       public function checkCaptcha($cap_id, $captcha) {
            if (file_exists(FILE_UPLOAD_PATH . 'captcha/' . $cap_id . $captcha . '.png')) {
                 // unlink(FILE_UPLOAD_PATH . 'captcha/' . $cap_id . $captcha . '.png');
                 return true;
            } else {
                 $return = array('captcha_status' => false);
                 echo json_encode($return);
                 die();
            }
       }

       public function get_states() {
            $stt_country_id = $this->input->post('country_id', true);
            $return = array('states' => $this->api_model->getStates($stt_country_id));
            echo json_encode($return);
            die();
       }

       public function uploadFiles($files, $path, $name, $types) {
            $this->load->library('upload');
            $newFileName = microtime(true) . $files[$name]['name'];

            $config['upload_path'] = $path;
            $config['allowed_types'] = $types;
            $config['file_name'] = $newFileName;
            $config['max_size'] = 0; //in KB

            $this->upload->initialize($config);
            if (!$this->upload->do_upload($name)) {
                 $ext = pathinfo($files[$name]['name'], PATHINFO_EXTENSION);
                 $up = $this->upload->display_errors();
                 generate_log(array(
                     'log_title' => 'Upload files error',
                     'log_desc' => serialize($up),
                     'log_controller' => 'upload-files-error',
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => 0
                 ));
                 die(json_encode(array('error' => true, 'msg' => $up . ' ,type=' . $ext . ' ,name =' . $files[$name]['name'])));
            } else {
                 $uploadData = $this->upload->data();
                 generate_log(array(
                     'log_title' => 'Upload files success',
                     'log_desc' => serialize($uploadData),
                     'log_controller' => 'upload-files-success',
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => 0
                 ));
                 return $uploadData['file_name'];
            }
       }

       public function suppliers() {
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            if (!$offset) {
                 $offset = 0;
            }

            if (!$limit) {
                 $limit = 30;
            }
            $data['suppliers'] = $this->api_model->getSuppliers('', $offset, $limit);
            echo json_encode($data);
            die();
       }

       public function supplier_details() {
            $supm_id = $this->input->post('sup_id', true);
            if ($supm_id == null || $supm_id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $uid = '';
            if ($det) {
                 $uid = $det['usr_id'];
            }
            $data = $this->api_model->getSuppliers($supm_id, '', '', $uid);

            if (!$data) {
                 $data['supplier_home'] = array();
                 $data['supplier_profile'] = array();
            }
            $return = array('supplier_home' => $data['supplier_home'], 'supplier_profile' => $data['supplier_profile']);
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       function getProductsBySupplier() {
            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $category = (isset($_POST['supplier']) && !empty($_POST['supplier'])) ? trim($_POST['supplier']) : 0;
            $return = $this->api_model->getProductsBySupplier($category, $limit, $pageNum);
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       public function categories() {
            $data['categories'] = $this->api_model->getCategories();
            echo json_encode($data);
            die();
       }

       public function markets() {
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            $cat_id = $this->input->post('category', true);
            $market = $this->input->post('market', true);
            $building = $this->input->post('building', true);
            if (!$offset) {
                 $offset = 0;
            }

            if (!$limit) {
                 $limit = 30;
            }

            $data['markets'] = $this->api_model->getMarketSuppliers($limit, $offset, $cat_id, $market, $building);
            echo json_encode($data);
            die();
       }

       public function market_config() {
            $data = $this->api_model->marketConfig();
            echo json_encode($data);
            die();
       }

       public function rfq_config() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $captcha = array();
            $i = 1;
            foreach (getCiCaptcha(6) as $key => $cap) {
                 $captcha[$i]['id'] = 'c' . $cap['time'];
                 $captcha[$i]['image'] = site_url() . 'assets/uploads/captcha/' . $cap['filename'];
                 $i++;
            }
            $data['captcha'] = array_values($captcha);
            $data['categories'] = $this->api_model->getCategories();
            $data['markets'] = $this->api_model->getMarkets();
            $data['units'] = $this->api_model->getUnits();
            $data['user_mail_verify_status'] = $det['usr_is_mail_verified'];
            $data['captcha'] = $captcha;
            echo json_encode($data);
            die();
       }

       public function rfq() {
            $cat_id = $this->input->post('cat_id', true);
            $mar_id = $this->input->post('mar_id', true);
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $product = $this->input->post('product', true);
            $requirment = $this->input->post('requirment', true);
            $quantity = $this->input->post('quantity', true);
            $valid = $this->input->post('validity', true);
            $mail = $this->input->post('mail', true);
            $unit = $this->input->post('unit', true);

            if ($cat_id == null || $cat_id == '' || $product == null || $product == '' || $requirment == null || $requirment == '' || $quantity == null || $quantity == '' || $valid == null || $valid == '' || $mail == null || $mail == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $data = array();
            if (isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])) {
                 $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'rfq/', 'attachment', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
                 $data['rfq_attachment'] = $file;
            }
            $data['rfq_category'] = $cat_id;
            $data['rfq_market'] = $mar_id != '' ? $mar_id : 0;
            $data['rfq_product_name'] = $product;
            $data['rfq_qty'] = $quantity;
            $data['rfq_unit'] = $unit;
            $data['rfq_requirment'] = $requirment;
            $data['rfq_validity'] = date('Y-m-d', strtotime($valid));
            $data['rfq_mail'] = $mail;
            $data['rfq_user'] = $usr_id;

            if ($this->api_model->insertRfq($data)) {
                 $return = array('msg' => 'Request submitted, we will touch you soon!');
            } else {
                 $return = array('error' => 'Something went wrong!!');
            }
            echo json_encode($return);
            die();
       }

       public function feeds() {
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            if (!$offset) {
                 $offset = 0;
            }
            if (!$limit) {
                 $limit = 30;
            }
            $type = $this->input->post('type', true);
            if ($type == null || $type == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            if (!$det) {
                 $det['usr_id'] = '';
            }
            $usr_id = $det['usr_id'];
            $feed = $this->api_model->getFeeds($usr_id, $type, $offset, $limit);
            echo json_encode($feed);
            die();
       }

       public function add_to_favourite() {
            $type = $this->input->post('type', true);
            $id = $this->input->post('id', true);
            if ($type == null || $type == '' || $id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $data['fav_consign'] = strtoupper($type);
            $data['fav_consign_id'] = encryptor($id, 'D');
            $data['fav_added_by'] = $usr_id;
            $return = array('msg' => 'Added to favourite list');
            if (!$this->api_model->addToFavourite($data)) {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function favourites() {
            $type = $this->input->post('type', true);
            if ($type == null || $type == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            if (!$offset) {
                 $offset = 0;
            }
            if (!$limit) {
                 $limit = 30;
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $type = strtoupper($type);
            $data = $this->api_model->getFavourites($usr_id, $type, $offset, $limit);
            echo json_encode($data);
            die();
       }

       public function activate() {
            $otp = $this->input->post('otp', true);
            $usr_id = $this->input->post('usr_id', true);

            if ($otp == null || $otp == '' || $usr_id == null || $usr_id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $usr_id = encryptor($usr_id, 'D');
            $return = array('error' => 'Invalid OTP');
            if ($this->api_model->activateUserAccount($usr_id, $otp)) {
                 $return = array('msg' => 'Verified Successfully');
            }
            echo json_encode($return);
            die();
       }

       public function resend_otp() {
            $email = $this->input->post('email', true);
            $type = $this->input->post('type', true);
            if (!$email || $this->api_model->checkIfValueExists(array('usr_email' => $email))) {
                 $return = array('error' => 'Invalid email');
                 echo json_encode($return);
                 die();
            }
            $user = $this->api_model->getUserByIdentity($email);
            if ($user) {
                 if ($type == "reset_pwd") {
                      $maildata['otp'] = $this->api_model->updateOtpByMail($email, 'usr_forgotten_password_otp');
                      $maildata['name'] = $user['usr_first_name'] . ' ' . $user['usr_last_name'];
                      $this->sendMail($email, 'reset_pwd', $maildata);
                 }
            } else {
                 $return = array('error' => 'No Matching Email Found');
                 echo json_encode($return);
                 die();
            }

            if ($type == "reg") {
                 $maildata['otp'] = $this->api_model->updateOtpByMail($email, 'usr_otp');
                 $this->sendMail($email, 'reg', $maildata);
            }
            $return = array('msg' => 'OTP sent');
            echo json_encode($return);
            die();
       }

       public function inbox() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $data['inquiry_count'] = $this->api_model->inquiryCount($usr_id);
            $data['inbox'] = $this->api_model->getInbox($usr_id);
            echo json_encode($data);
            die();
       }

       public function chats() {
            $to = $this->input->post('to_id', true);
            $type = $this->input->post('type', true);

            if ($to == null || $to == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            if ($to == 'admin') {
                 $to = encryptor(1);
            }
            $to = encryptor($to, 'D');
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            if (!$offset) {
                 $offset = 0;
            }
            if (!$limit) {
                 $limit = 30;
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);

            if ($type == "rfq") {
                 $sup_id = $this->common_model->getUserIdFromSupId($to);
                 $_to_user = $this->api_model->getUserDetails($sup_id['usr_id']);
                 $user_det = array(
                     'name' => $_to_user['usr_first_name'] . ' ' . $_to_user['usr_last_name'],
                     'image' => site_url() . 'assets/uploads/avatar/' . $_to_user['usr_avatar'],
                 );
                 $usr_id = $det['usr_id'];
                 $data['to_user_details'] = $user_det;
                 $data['chats'] = $this->api_model->loadRfqChats($sup_id['usr_id'], $usr_id, $offset, $limit);
                 echo json_encode($data);
                 die();
            } else {
                 $_to_user = $this->api_model->getUserDetails($to);
                 $user_det = array(
                     'name' => $_to_user['usr_first_name'] . ' ' . $_to_user['usr_last_name'],
                     'image' => site_url() . 'assets/uploads/avatar/' . $_to_user['usr_avatar'],
                 );
                 $usr_id = $det['usr_id'];
                 $data['to_user_details'] = $user_det;
                 $data['chats'] = $this->api_model->loadChats($to, $usr_id, $offset, $limit);
                 echo json_encode($data);
                 die();
            }
       }

       public function send_message() {
            $type = $this->input->get('type', true);
            $to = $this->input->get('to_id', true);
            if ($to == null || $to == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            if ($to == 'admin') {
                 $to = encryptor(1);
            }
            $chnl = $to;
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $to = encryptor($to, 'D');

            $to_usr_grp = $this->api_model->getUserGroup($to);
            $to_usr_det = $this->api_model->getUserDetailsFromId($to);

            if ($type == 'rfq') {
                 $sup_id = $this->common_model->getUserIdFromSupId($to);
                 $rfq_id = $this->input->get('rfq_id', true);
                 $content = $this->input->get('content', true);

                 $chat['message'] = $content;
                 $chat['time'] = date('h:i A') . ' | ' . date('M d');
                 $chat['from'] = $det['usr_first_name'];
                 $chat['rfq_id'] = $rfq_id;
                 $chat['rfq_link'] = site_url('product/rfq-list/' . encryptor($rfq_id));
                 $chat['from_img'] = 'assets/uploads/avatar/' . $det['usr_avatar'];
                 $this->sendPusherMessage(encryptor($sup_id['usr_id']), 'rfq-event', $chat);

                 $res = $this->api_model->insertRfqChat($rfq_id, $usr_id, $sup_id['usr_id'], $content);
                 if ($res) {
                      $return = array('msg' => 'Message sent', 'id' => encryptor($res));
                 } else {
                      $return = array('error' => 'Something went wrong');
                 }
                 echo json_encode($return);
                 die();
            } else {
                 $ext = '';
                 $is_prd = $this->input->get('is_prd_details', true);
                 $file = $this->input->get('file', true);
                 $content = $this->input->get('content', true);
                 if (!$content) {
                      $content = '';
                 }
                 if (!$file) {
                      $file = '';
                 }
                 if ($is_prd) {
                      $file = 'uploads/product/' . $file;
                 } else {
                      if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                           $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                           $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'chats/', 'file', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
                      }
                 }

                 $enc_id = encryptor($usr_id);
                 $insert = array(
                     'c_from_id' => $usr_id,
                     'c_to_id' => $to,
                     'c_content' => $content,
                     'c_attachment' => $file,
                     'c_is_prd_details' => $is_prd,
                 );

                 $cht_id = $this->common_model->saveChat($insert);

                 if ($to_usr_grp == 2) {
                      if ($to_usr_det['usr_pushy_token']) {
                           $push['type'] = 'recieved';
                           $push['msg_type'] = 'chat';
                           $push['from_id'] = $usr_id;
                           $push['content'] = $content;

                           $attach = 0;
                           if ($file) {
                                $attach = 1;
                           }
                           $push['attachment_link'] = '';
                           $push['attachment_preview'] = '';
                           $push['is_prd_details'] = $is_prd;
                           if ($is_prd) {
                                $push['attachment_link'] = site_url() . 'assets/' . $file;
                                $push['attachment_preview'] = site_url() . 'assets/' . $file;
                           } else if ($attach) {
                                $push['is_prd_details'] = 3;
                                $push['attachment_link'] = site_url() . 'assets/uploads/chats/' . $file;
                                $push['attachment_preview'] = site_url() . 'assets/images/attachment.jpg';
                                if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                                     $push['attachment_preview'] = site_url() . 'assets/uploads/chats/' . $file;
                                     $push['is_prd_details'] = 2;
                                }
                           }
                           $push['from_name'] = $to_usr_det['usr_first_name'] . " " . $to_usr_det['usr_last_name'];
                           $push['time'] = date("Y-m-d H:i:s");
                           $push['is_attachment'] = $attach;



                           if ($this->common_model->isFirstChat($usr_id, $to)) {
                                $push['profile_image'] = isset($to_usr_det['usr_avatar']) ? site_url() . 'assets/uploads/avatar/' . $to_usr_det['usr_avatar'] : '';
                           }



                           $this->common_model->sendPushNotificationForSupplier($push, $to_usr_det['usr_pushy_token'], '');
                      }
                 }

                 // if ($file) {
                 //     $file = 'uploads/chats/' . $file;
                 // }
                 $chat['attachment'] = $file;
                 $chat['message'] = $content;
                 $chat['from'] = $det['usr_first_name'];
                 $chat['ext'] = $ext;
                 $chat['from_ch'] = $enc_id;
                 $chat['is_prd'] = $is_prd;
                 if ($this->sendPusherMessage($chnl, 'chat-event', $chat)) {
                      $return = array('msg' => 'Message sent', 'id' => encryptor($cht_id));
                 } else {
                      $return = array('error' => 'Something went wrong');
                 }
                 echo json_encode($return);
                 die();
            }
       }

       public function sendPusherMessage($channel, $event, $chat) {
            $options = array(
                'cluster' => PUSHER_CLUSTER,
                'useTLS' => true,
            );
            $pusher = new Pusher\Pusher(
                    PUSHER_APP_KEY, PUSHER_APP_SECRET, PUSHER_APP_ID, $options
            );
            return $pusher->trigger($channel, $event, $chat);
       }

       public function rfq_list() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $return = array('rfq_list' => $this->api_model->getRfqList($usr_id));
            echo json_encode($return);
            die();
       }

       public function change_mail() {
            $email = $this->input->post('email', true);
            if (!$this->api_model->checkIfValueExists(array('usr_email' => $email))) {
                 $return = array('error' => 'Email already taken');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $otp = rand(1000, 9999);
            if ($this->api_model->changeEmail($email, $usr_id, $otp)) {
                 $maildata['otp'] = $otp;
                 $this->sendMail($email, 'reg', $maildata);
                 $return = array('msg' => 'An OTP has been sent to your new email');
                 echo json_encode($return);
                 die();
            } else {
                 $return = array('error' => 'Failed to update');
                 echo json_encode($return);
                 die();
            }
       }

       public function terms() {
            $return = array('terms' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
            echo json_encode($return);
            die();
       }

       public function rfq_details() {
            $rfq_id = $this->input->post('id', true);
            if ($rfq_id == null || $rfq_id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $details = $this->api_model->getRfqDetails($rfq_id);
            echo json_encode($details);
            die();
       }

       public function remove_favourite() {
            $type = $this->input->post('type', true);
            $count = $this->input->post('count', true);
            if ($type == null || $type == '' || $count == null || $count == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $id = array();
            for ($i = 0; $i < $count; $i++) {
                 $id[$i] = encryptor($this->input->post("id$i"), 'D');
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $data['fav_consign'] = strtoupper($type);
            $data['fav_consign_id'] = $id;
            $data['fav_added_by'] = $usr_id;
            $dlt = $this->api_model->removeFavorite($data);
            // if ($dlt) {
            $return = array('msg' => 'Favourite removed');
            // } else {
            //     $return = array('error' => 'Something went wrong');
            // }
            echo json_encode($return);
            die();
       }

       public function follow_supplier() {
            $id = $this->input->post('sup_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $add = $this->api_model->addToFollow($usr_id, encryptor($id, 'D'));
            if ($add) {
                 $return = array('msg' => 'Followed');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function unfollow() {
            $id = $this->input->post('sup_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $add = $this->api_model->removeFollow($usr_id, encryptor($id, 'D'));
            if ($add) {
                 $return = array('msg' => 'Removed');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function following_list() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $data = $this->api_model->getFollowings($usr_id);
            $return = array('following' => $data);
            echo json_encode($return);
            die();
       }

       public function delete_rfq() {
            $id = $this->input->post('rfq_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $del = $this->api_model->removeRfq($usr_id, $id);
            if ($del) {
                 $return = array('msg' => 'RFQ Deleted');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function inquiry_list() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $data['inquiry_list'] = $this->api_model->getInquiryList($usr_id);
            echo json_encode($data);
            die();
       }

       public function inquiry_details() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $id = $this->input->post('inq_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $data['inquiry_details'] = $this->api_model->getInquiryDetails(encryptor($id, 'D'), $usr_id);
            echo json_encode($data);
            die();
       }

       public function inquiry_reply() {
            $inq_id = $this->input->post('inq_id', true);
            $sup_id = $this->input->post('sup_id', true);
            $subject = $this->input->post('subject', true);
            $description = $this->input->post('description', true);
            if ($inq_id == null || $inq_id == '' || $sup_id == null || $sup_id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $dec_inq_id = encryptor($inq_id, 'D');
            if ($this->api_model->addInquiryReply($dec_inq_id, encryptor($sup_id, 'D'), $subject, $description, $usr_id)) {

                 $chat['title'] = $subject;
                 $chat['desc'] = $description;
                 $chat['type'] = 'inq';
                 $chat['time'] = date('h:i A') . ' | ' . date('M d');
                 $chat['from'] = $det['usr_first_name'];
                 $chat['order_id'] = $dec_inq_id;
                 $chat['from_img'] = 'assets/uploads/avatar/' . $det['usr_avatar'];
                 $chat['inq_link'] = site_url('order/order_summery/' . $inq_id);
                 $this->sendPusherMessage($sup_id, 'inquiry-event', $chat);

                 $return = array('msg' => 'Reply Sent');
            } else {
                 $return = array('error' => 'Partial Content');
            }
            echo json_encode($return);
            die();
       }

       public function notification_list() {
            $offset = $this->input->post('offset', true);
            $limit = $this->input->post('limit', true);
            if (!$offset) {
                 $offset = 0;
            }
            if (!$limit) {
                 $limit = 30;
            }
            $data['notification_list'] = $this->api_model->getNotificationList($offset, $limit);
            // if ($data['notification_list']) {
            //     foreach ($data['notification_list'] as $key => $val) {
            //         $data['notification_list'][$key]['from'] = 'Fujeeka';
            //     }
            // }
            echo json_encode($data);
            die();
       }

       public function notification_details() {
            $id = $this->input->post('id', true);
            $data['notification_details'] = $this->api_model->getNotificationDetails($id);
            echo json_encode($data);
            die();
       }

       public function general_enquiry() {
            $inq['enq_sup_id'] = $this->input->post('sup_id', true);
            $inq['enq_name'] = $this->input->post('enq_name', true);
            $inq['enq_email'] = $this->input->post('enq_email', true);
            $inq['enq_phone'] = $this->input->post('enq_phone', true);
            $inq['enq_subject'] = $this->input->post('enq_subject', true);
            $inq['enq_query'] = $this->input->post('enq_query', true);
            if ($inq['enq_sup_id'] == null || $inq['enq_sup_id'] == '' || $inq['enq_email'] == null || $inq['enq_email'] == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $inq['enq_sup_id'] = encryptor($inq['enq_sup_id'], 'D');
            $add = $this->api_model->addGeneralInquiry($inq);
            if ($add) {
                 $return = array('msg' => 'Enquiry addedd.We will contact you soon.');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function logistics() {
            $data['logistics'] = $this->api_model->getLogistics();
            echo json_encode($data);
            die();
       }

       public function logistic_services() {
            $id = $this->input->post('par_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $data = $this->api_model->getLogisticServices(encryptor($id, 'D'));
            echo json_encode($data);
            die();
       }

       public function quality_control() {
            $data['quality_control'] = $this->api_model->getQuality();
            echo json_encode($data);
            die();
       }

       public function quality_services() {
            $id = $this->input->post('par_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $data = $this->api_model->getQualityServices(encryptor($id, 'D'));
            echo json_encode($data);
            die();
       }

       public function get_quote() {
            $count = $this->input->post('count', true);
            $data['logq_service'] = $this->input->post('service_id', true);
            $data['logq_logistics'] = $this->input->post('partner_id', true);
            $data['logq_cbm'] = $this->input->post('cbm', true);
            $data['logq_weight'] = $this->input->post('weight', true);
            $data['logq_prod_type'] = $this->input->post('prd_type', true);
            $data['logq_email'] = $this->input->post('email', true);
            $data['logq_mobile'] = $this->input->post('mobile', true);
            $data['logq_loc_from'] = $this->input->post('from', true);
            $data['logq_loc_to'] = $this->input->post('to', true);
            $data['logq_desc'] = $this->input->post('description', true);
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            if ($det) {
                 $data['logq_added_by'] = $det['usr_id'];
            }

            if ($data['logq_logistics'] == null || $data['logq_logistics'] == '' || $data['logq_service'] == null || $data['logq_service'] == '' || $count == null || $count == '' || $data['logq_cbm'] == null || $data['logq_cbm'] == '' || $data['logq_weight'] == null || $data['logq_weight'] == '' || $data['logq_mobile'] == null || $data['logq_mobile'] == '' || $data['logq_email'] == null || $data['logq_email'] == '' || $data['logq_loc_from'] == null || $data['logq_loc_from'] == '' || $data['logq_loc_to'] == null || $data['logq_loc_to'] == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $id = array();
            for ($i = 0; $i < $count; $i++) {
                 $id[$i] = $this->input->post("shipping_id$i");
            }
            $data['logq_service'] = encryptor($data['logq_service'], 'D');
            $data['logq_logistics'] = encryptor($data['logq_logistics'], 'D');
            if ($this->api_model->insertQuote($data, $id)) {
                 $return = array('msg' => 'Quote submitted.We will contact you soon.');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function update_image() {
            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            $usr_id = $det['usr_id'];
            $avatar = '';
            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                 $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'avatar/', 'file', 'gif|jpg|png|jpeg');
                 $avatar = $file;
            }
            if (!$avatar) {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            $res = $this->api_model->updateUserImage($usr_id, $avatar);
            if ($res) {
                 $return = array('msg' => 'Image updated', 'image' => site_url() . 'assets/uploads/avatar/' . $avatar);
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function get_quality_quote() {
            $count = $this->input->post('count', true);
            $data['qcq_service'] = $this->input->post('service_id', true);
            $data['qcq_qc_id'] = $this->input->post('partner_id', true);
            $data['qcq_name'] = $this->input->post('name', true);
            $data['qcq_email'] = $this->input->post('email', true);
            $data['qcq_mobile'] = $this->input->post('mobile', true);
            $data['qcq_desc'] = $this->input->post('description', true);

            $token = $this->input->get_request_header('Token');
            $det = $this->api_model->getUserDetailsFromToken($token);
            if ($det) {
                 $data['qcq_added_by'] = $det['usr_id'];
            }

            if ($data['qcq_service'] == null || $data['qcq_service'] == '' || $data['qcq_qc_id'] == null || $data['qcq_qc_id'] == '' || $data['qcq_email'] == null || $data['qcq_email'] == '' || $data['qcq_mobile'] == null || $data['qcq_mobile'] == '' || $data['qcq_name'] == null || $data['qcq_name'] == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }

            $data['qcq_service'] = encryptor($data['qcq_service'], 'D');
            $data['qcq_qc_id'] = encryptor($data['qcq_qc_id'], 'D');
            if ($this->api_model->insertQcQuote($data)) {
                 $return = array('msg' => 'Quote submitted.We will contact you soon.');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function delete_chat() {
            $id = $this->input->post('id', true);
            $type = $this->input->post('type', true);
            if ($id == null || $id == '' || $type == null || $type == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            if ($this->api_model->deleteChat($type, $id)) {
                 $return = array('msg' => 'Deleted');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       public function delete_inquiry() {
            $id = $this->input->post('inq_id', true);
            if ($id == null || $id == '') {
                 $return = array('error' => 'Partial Content');
                 echo json_encode($return);
                 die();
            }
            if ($this->api_model->deleteInquiry($id)) {
                 $return = array('msg' => 'Deleted');
            } else {
                 $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
       }

       function getHomeBanners() {
            $town = $this->input->post('town', true);
            $cate_id = $this->input->post('banner_category', true);
            $cate_id = empty($cate_id) ? 2 : $cate_id;
            $return = $this->api_model->getHomeBanners($town, $cate_id);
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       function autocompleteProduct() {
            $keyword = $this->input->post('keyword', true);
            $limit = $this->input->post('limit', true);
            $limit = empty($limit) ? $limit : 10;
            $return = $this->api_model->autocompleteProduct(trim($keyword), $limit);
            die(json_encode($return));
       }

       function paytmChecksumGenerator() {
            header("Pragma: no-cache");
            header("Cache-Control: no-cache");
            header("Expires: 0");
// following files need to be included
            require_once APPPATH . 'libraries/paytm/config_paytm.php';
            require_once APPPATH . 'libraries/paytm/encdec_paytm.php';
            $checkSum = "";

// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
            $findme = 'REFUND';
            $findmepipe = '|';

            $paramList = array();

            $paramList["MID"] = '';
            $paramList["ORDER_ID"] = '';
            $paramList["CUST_ID"] = '';
            $paramList["INDUSTRY_TYPE_ID"] = '';
            $paramList["CHANNEL_ID"] = '';
            $paramList["TXN_AMOUNT"] = '';
            $paramList["WEBSITE"] = '';

            foreach ($_POST as $key => $value) {
                 $pos = strpos($value, $findme);
                 $pospipe = strpos($value, $findmepipe);
                 if ($pos === false || $pospipe === false) {
                      $paramList[$key] = $value;
                 }
            }

//Here checksum string will return by getChecksumFromArray() function.
            $checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
//print_r($_POST);
            echo json_encode(array("CHECKSUMHASH" => $checkSum, "ORDER_ID" => $_POST["ORDER_ID"], "payt_STATUS" => "1"), JSON_UNESCAPED_SLASHES);
            //Sample response return to SDK
//  {"CHECKSUMHASH":"GhAJV057opOCD3KJuVWesQ9pUxMtyUGLPAiIRtkEQXBeSws2hYvxaj7jRn33rTYGRLx2TosFkgReyCslu4OUj\/A85AvNC6E4wUP+CZnrBGM=","ORDER_ID":"asgasfgasfsdfhl7","payt_STATUS":"1"} 
       }

       function newProductOrder() {
            if (!empty($this->input->post())) {
//                 $paymentType = $this->input->post('ordm_payment_type' , true);
//                 if($paymentType == 2) {
//                      die(json_encode(array('error' => true, 'msg' => 'Sorry, online payment temporarily unavailable')));
//                 }
                 $orderCode = json_decode($this->input->post('order'), true);
                 $addedby = isset($orderCode['master']['ordm_buyer']) ? $orderCode['master']['ordm_buyer'] : 0;
                 $_POST['ordm_buyer_selected_town'] = isset($_POST['ordm_buyer_selected_town']) ? $_POST['ordm_buyer_selected_town'] : 0;

                 generate_log(array(
                     'log_title' => 'newProductOrder',
                     'log_desc' => serialize($this->input->post()),
                     'log_controller' => 'newProductOrder',
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $addedby
                 ));
                 $orderId = $this->api_model->newProductOrder($orderCode, $this->input->post('ordm_payment_type'), $this->input->post('ordm_delivery_address'), $this->input->post('ordm_billing_address'), $this->input->post('ordm_buyer_selected_town'));
                 die(json_encode(array('error' => false, 'msg' => 'New product order', 'orderId' => $orderId)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Insufficient data', 'orderId' => 0)));
            }
       }

       function getProductOrder() {
            $userId = $this->input->post('userId', true);
            if (!empty($userId)) {
                 $orderDetails = $this->api_model->getProductOrder($userId);
                 die(json_encode(array('error' => false, 'msg' => 'New product order', 'orderDetails' => $orderDetails)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Insufficient data')));
            }
       }

       function updateOrderStatus() {
            if ($this->api_model->updateOrderStatus(json_decode($this->input->post('order'), true))) {
                 die(json_encode(array('error' => false, 'msg' => 'Status updated')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Insufficient data')));
            }
       }

       function getAppHomeCategory() {
            $return = $this->api_model->getAppHomeCategory();
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       function getCategoryDetails() {
            $categories = $this->api_model->getCategoryDetails($this->input->post('cateId'));
            die(json_encode($categories, JSON_UNESCAPED_SLASHES));
       }

       function deliveryboyRequest() {
            $data = $this->input->post();
            $fname = trim($this->input->post('usr_first_name', true));
            $email = trim($this->input->post('usr_email', true));
            $phone = trim($this->input->post('usr_phone', true));
            $password = '';
            $pushy_token = trim($this->input->post('pushy_token', true));

            $emailAlreadyExists = $this->api_model->userExistsInGroup('usr_email', $email, '10');
            $phoneAlreadyExists = $this->api_model->userExistsInGroup('usr_phone', $phone, '10');

            if ($emailAlreadyExists) {
                 die(json_encode(array('error' => true, 'msg' => 'Email already exists')));
            }

            if ($phoneAlreadyExists) {
                 die(json_encode(array('error' => true, 'msg' => 'Contact number already used')));
            }

            if (isset($_FILES['usr_avatar']['name']) && !empty($_FILES['usr_avatar']['name'])) {
                 $data['usr_document'] = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'avatar/', 'usr_avatar', 'gif|jpg|png|jpeg');
            }

            $group = array(10);
            $usr_tkn = $this->api_auth->registerDeliveryBoy($fname, $password, $email, $data, $group);
            if (isset($usr_tkn['tok']) && isset($usr_tkn['identity'])) {
                 $this->api_model->updatePushyToken($pushy_token, 'usr_token', $usr_tkn);
                 $return = array('error' => false, 'msg' => 'Registration successfully completed.',
                     'token' => $usr_tkn['tok'], 'identity' => $usr_tkn['identity'], 'full_name' => $fname, 'email' => $email);
            } else {
                 $return = array('error' => true, 'msg' => 'Failed to register');
            }
            die(json_encode($return));
       }

       function myOrderByDeliveryBoy() {
            $dboy = trim($this->input->post('dboy', true));
            $status = trim($this->input->post('status', true));

            if (!empty($dboy)) {
                 $orderDetails = $this->api_model->myOrderByDeliveryBoy($dboy, $status);
                 die(json_encode(array('error' => false, 'orderDetails' => $orderDetails)));
            } else {
                 die(json_encode(array('error' => true, 'orderDetails' => '', 'msg' => '')));
            }
       }

       function trackDBoy() {

            $dbId = $this->input->post('dbId', true);
            $lat = $this->input->post('lat', true);
            $log = $this->input->post('log', true);

            if (!empty($dbId) && !empty($lat) && !empty($log)) {
                 $this->api_model->trackDBoy($dbId, $lat, $log);
                 die(json_encode(array('error' => false, 'msg' => 'Success')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function trackDBoyByOrder() {
            $order_id = $this->input->post('order_id', true);
            $limit = $this->input->post('limit', true);
            $limit = empty($limit) ? $limit : 10;

            if (!empty($order_id)) {
                 $trackingDetails = $this->api_model->trackDBoyByOrder($order_id, $limit);
                 die(json_encode(array('error' => false, 'msg' => 'Success', 'trackingDetails' => $trackingDetails)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function changeOrderStatus() {
            $order_id = $this->input->post('order_id', true);
            $status = $this->input->post('status', true);
            $userId = isset($_POST['user_id']) ? $_POST['user_id'] : 0;

            if (!empty($order_id)) {
                 $this->api_model->changeOrderStatus($order_id, $status, $userId);
                 die(json_encode(array('error' => false, 'msg' => 'Success')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function newAddress() {
            $address = $this->input->post();
            generate_log(array(
                'log_title' => 'New address added',
                'log_desc' => serialize($address),
                'log_controller' => 'new-address',
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => 0
            ));
            if ($this->api_model->newAddress($address)) {
                 die(json_encode(array('error' => false, 'msg' => 'Address successfully added')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function updateAddress() {
            $address = $this->input->post();
            if ($this->api_model->updateAddress($address)) {
                 die(json_encode(array('error' => false, 'msg' => 'Address successfully edited')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function deleteAddress() {
            $address = $this->input->post('Adbook_id', true);
            if ($this->api_model->deleteAddress($address)) {
                 die(json_encode(array('error' => false, 'msg' => 'Address successfully deleted')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function setDefaultAddress() {
            $address = $this->input->post('Adbook_id', true);
            if ($this->api_model->setDefaultAddress($address)) {
                 die(json_encode(array('error' => false, 'msg' => 'Address successfully deleted')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error')));
            }
       }

       function getAddress() {
            $address = $this->input->post('Adbook_id', true);
            $userId = $this->input->post('userId', true);
            $address = $this->api_model->getAddress($address, $userId);
            die(json_encode(array('error' => false, 'address' => $address)));
       }

       function searchByShopName() {
            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $keyword = (isset($_POST['keyword']) && !empty($_POST['keyword'])) ? trim($_POST['keyword']) : '';
            if (empty($keyword)) {
                 die(json_encode(array('error' => true, 'msg' => 'Empty keyword')));
            } else {
                 $data = $this->api_model->searchByShopName($keyword, $limit, $pageNum);
                 die(json_encode($data, JSON_UNESCAPED_SLASHES));
            }
       }

       function searchByCategoryName() {
            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $keyword = (isset($_POST['keyword']) && !empty($_POST['keyword'])) ? trim($_POST['keyword']) : '';
            if (empty($keyword)) {
                 die(json_encode(array('error' => true, 'msg' => 'Empty keyword')));
            } else {
                 $data = $this->api_model->searchByCategoryName($keyword, $limit, $pageNum);
                 die(json_encode($data, JSON_UNESCAPED_SLASHES));
            }
       }

       function searchByTownName() {
            
       }

       function myFirms() {
//            $_POST = array(
//                'limit' => 10,
//                'page' => 0,
//                'lat' => 11.072035,
//                'lng' => 76.074005,
//                'miles' => 10
//            );

            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;

            $lat = isset($_POST['lat']) ? trim($_POST['lat']) : '';
            $lng = isset($_POST['lng']) ? trim($_POST['lng']) : '';
            $miles = DEF_MILE; //isset($_POST['miles']) ? trim($_POST['miles']) : 10;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;

            $data = $this->api_model->myFirms($lat, $lng, $miles, $limit, $pageNum);
            die(json_encode(array('error' => false, 'msg' => '', 'data' => $data), JSON_UNESCAPED_SLASHES));
       }

       function singleFirms() {
            $id = (isset($_POST['id']) && !empty($_POST['id'])) ? trim($_POST['id']) : 0;
            $data = $this->api_model->singleFirms($id);
            die(json_encode(array('error' => false, 'msg' => '', 'data' => $data), JSON_UNESCAPED_SLASHES));
       }

       function getShopCharge() {
            $supplier = $this->input->post('supplier', true);
            if (!empty($supplier)) {
                 $shopCharge = $this->api_model->getShopCharge($supplier);
                 die(json_encode(array('error' => false, 'shopCharge' => $shopCharge, 'msg' => '')));
            } else {
                 die(json_encode(array('error' => true, 'shopCharge' => 0.00, 'msg' => 'Empty supplier passed')));
            }
       }

       function calculateDeliveryCharge() {
            $shipAddress = $this->input->post('ship_address', true);
            $suppliers = $this->input->post('suppliers', true);
            $shopCharge = $this->api_model->calculateDeliveryCharge($shipAddress, $suppliers);
            die(json_encode(array('error' => false, 'data' => $shopCharge)));
       }

       function debug() {
            $this->load->view('debug');
       }

       function getLoggedUser() {
            $credential = $this->input->post('credential', true);
            $user = $this->api_model->getLoggedUser($credential);
            if (!empty($user)) {
                 die(json_encode(array('error' => false, 'data' => $user, 'msg' => ''), JSON_UNESCAPED_SLASHES));
            } else {
                 die(json_encode(array('error' => true, 'data' => '', 'msg' => 'User not found')));
            }
       }

       function updateProfile() {
            $avatarFullPath = '';
            $credential = $this->input->post('credential', true);
            $details = json_decode($this->input->post('details'), true);

            if (isset($_FILES['avatar']['name']) && !empty($_FILES['avatar']['name'])) {
                 $this->load->library('upload');
                 $newFileName = rand(9999999, 0) . clean_image_name($_FILES['avatar']['name']);
                 $config['upload_path'] = './assets/uploads/avatar/';
                 $config['allowed_types'] = 'jpg|jpeg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);
                 if ($this->upload->do_upload('avatar')) {
                      $uploadData = $this->upload->data();
                      $details['usr_avatar'] = $uploadData['file_name'];
                      $avatarFullPath = site_url('assets/uploads/avatar') . '/' . $details['usr_avatar'];
                 } else {
                      generate_log(array(
                          'log_title' => 'New address added',
                          'log_desc' => serialize($this->upload->display_errors()),
                          'log_controller' => 'upload-error',
                          'log_action' => 'C',
                          'log_ref_id' => 0,
                          'log_added_by' => 0
                      ));
                 }
            }

            if ($this->api_model->updateUser($credential, $details)) {
                 die(json_encode(array('error' => false, 'msg' => 'Profile updated', 'avatar' => $avatarFullPath)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error occured', 'avatar' => $avatarFullPath)));
            }
       }

       function myOrderHistory() {
            $credential = $this->input->post('userId', true);
            $data = $this->api_model->getOrders(0, $credential);
            if ($data) {
                 die(json_encode(array('error' => false, 'msg' => 'Order Details', 'data' => $data)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'You have no orders yet.')));
            }
       }

       function myOrderDetailsHistory() {
            $credential = $this->input->post('orderId', true);
            $data = $this->api_model->getOrders($credential);
            if ($data) {
                 die(json_encode(array('error' => false, 'msg' => 'Order Details', 'data' => $data)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'You have no orders yet.')));
            }
       }

       public function addToFavourite() {
            $type = $this->input->post('type', true);
            $id = $this->input->post('conId', true);
            $userId = $this->input->post('userId', true);
            if ($type == null || $type == '' || $id == null || $id == '' || strtolower($id) == 'null' || $userId == null || $userId == '') {
                 die(json_encode(array('error' => true, 'msg' => 'Partial Content')));
            }
            $data['fav_consign'] = strtoupper($type);
            $data['fav_consign_id'] = $id;
            $data['fav_added_by'] = $userId;
            if ($insertId = $this->api_model->addToFavourite($data)) {
                 die(json_encode(array('error' => false, 'msg' => 'Added to favourite list', 'id' => $insertId, 'type' => $data['fav_consign'])));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error occured')));
            }
       }

       public function removeFavourite() {
            $id = $this->input->post('id', true);
            if ($id == null || $id == '') {
                 die(json_encode(array('error' => true, 'msg' => 'Partial Content')));
            }
            if ($this->api_model->removeFavourite($id)) {
                 die(json_encode(array('error' => false, 'msg' => 'Item removed from your favourite list')));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Error occured')));
            }
       }

       public function myFavouriteList() {
            $id = $this->input->post('userId', true);
            if ($id == null || $id == '') {
                 die(json_encode(array('error' => true, 'msg' => 'Partial Content')));
            }
            if ($data = $this->api_model->myFavouriteList($id)) {
                 die(json_encode(array('error' => false, 'msg' => '', 'data' => $data), JSON_UNESCAPED_SLASHES));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Empty list')));
            }
       }

       function cancelOrder() {
            $orderId = $this->input->post('orderId', true);
            $userId = $this->input->post('userId', true);
            $resp = $this->api_model->cancelOrder($orderId, $userId);
            die(json_encode($resp));
       }

       function productOrderFail() {
            generate_log(array(
                'log_title' => 'product Order Fail',
                'log_desc' => serialize($this->input->post()),
                'log_controller' => 'product-order-error',
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => 0
            ));
       }

       function getMasterProductsList() {
            $limit = (isset($_POST['limit']) && !empty($_POST['limit'])) ? trim($_POST['limit']) : 0;
            $page = (isset($_POST['page']) && !empty($_POST['page'])) ? trim($_POST['page']) : 0;
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $category = (isset($_POST['category']) && !empty($_POST['category'])) ? trim($_POST['category']) : 0;
            $town = (isset($_POST['town']) && !empty($_POST['town'])) ? trim($_POST['town']) : 0;
            $credential = (isset($_POST['credential']) && !empty($_POST['credential'])) ? trim($_POST['credential']) : 0;
            generate_log(array(
                'log_title' => 'getMasterProductsList-jk',
                'log_desc' => serialize($this->input->post()),
                'log_controller' => 'getMasterProductsList-jk',
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => $credential
            ));
            $return = $this->api_model->getMasterProductsList($category, $limit, $pageNum, $town, $credential);
            die(json_encode($return, JSON_UNESCAPED_SLASHES));
       }

       function getSuppliersProductsByProduct() {

            $prdid = isset($_POST['prdid']) ? trim($_POST['prdid']) : 0;
            $lat = isset($_POST['lat']) ? trim($_POST['lat']) : '';
            $lng = isset($_POST['lng']) ? trim($_POST['lng']) : '';
            $credential = (isset($_POST['credential']) && !empty($_POST['credential'])) ? trim($_POST['credential']) : 0;
            $supplier = (isset($_POST['supplier']) && !empty($_POST['supplier'])) ? trim($_POST['supplier']) : 0;
            $miles = DEF_MILE; //isset($_POST['miles']) ? trim($_POST['miles']) : 10;
            $town = (isset($_POST['town']) && !empty($_POST['town'])) ? trim($_POST['town']) : 0;

//            generate_log(array(
//                'log_title' => 'product Order Fail',
//                'log_desc' => serialize($this->input->post()),
//                'log_controller' => 'getSuppliersProductsByProduct',
//                'log_action' => 'C',
//                'log_ref_id' => 0,
//                'log_added_by' => $credential
//            ));

            if (empty($prdid)) {
                 die(json_encode(array('msg' => 'Empty product id', 'error' => true)));
            } else {
                 $productDetails = $this->api_model->getSuppliersProductsByProduct($prdid, '', $lat, $lng, $miles, $credential, $supplier, $town);
                 die(json_encode(array('msg' => '', 'error' => false, 'data' => $productDetails)));
            }
       }

       function uploadInvoiceByDB() {
            generate_log(array(
                'log_title' => 'uploadInvoiceByDB',
                'log_desc' => serialize($this->input->post()),
                'log_controller' => 'uploadInvoiceByDB-post',
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => 0
            ));
            generate_log(array(
                'log_title' => 'uploadInvoiceByDB',
                'log_desc' => serialize($_FILES),
                'log_controller' => 'uploadInvoiceByDB-files',
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => 0
            ));
            if (!isset($_POST['ordd_id']) || empty($_POST['ordd_id'])) {
                 die(json_encode(array('msg' => 'No product selected', 'error' => true)));
            }
            if (!isset($_POST['ppi_order_id']) || empty($_POST['ppi_order_id'])) {
                 die(json_encode(array('msg' => 'Order id empty', 'error' => true)));
            }
            if (!isset($_POST['ppi_added_by']) || empty($_POST['ppi_added_by'])) {
                 die(json_encode(array('msg' => 'Delivery boy id empty', 'error' => true)));
            }
            if (!isset($_POST['ppi_supplier_id']) || empty($_POST['ppi_supplier_id'])) {
                 die(json_encode(array('msg' => 'Supplier id empty', 'error' => true)));
            }

            $isAlreadyExists = $this->api_model->isAlreadyExistsInvoice($_POST['ppi_order_id'], $_POST['ppi_supplier_id']);
            if (!empty($isAlreadyExists)) {
                 die(json_encode(array('msg' => 'Order already taken, you can only upload new invoice or pickup new product', 'error' => true)));
            }

            if (isset($_FILES['ppi_invoice_img']['name'][0]) && !empty($_FILES['ppi_invoice_img']['name'][0])) {
                 $orddId = $_POST['ordd_id'];
                 unset($_POST['ordd_id']);
                 $invoiceId = $this->api_model->uploadInvoiceByDB($this->input->post());
                 $this->api_model->addProductsOrderPickup($orddId, $invoiceId, $_POST['ppi_order_id']);
                 $count = count($_FILES['ppi_invoice_img']['name']);
                 if ($count > 0) {
                      for ($i = 0; $i < $count; $i++) {
                           $_FILES['tmpUploade']['name'] = $_FILES['ppi_invoice_img']['name'][$i];
                           $_FILES['tmpUploade']['type'] = $_FILES['ppi_invoice_img']['type'][$i];
                           $_FILES['tmpUploade']['tmp_name'] = $_FILES['ppi_invoice_img']['tmp_name'][$i];
                           $_FILES['tmpUploade']['error'] = $_FILES['ppi_invoice_img']['error'][$i];
                           $_FILES['tmpUploade']['size'] = $_FILES['ppi_invoice_img']['size'][$i];
                           $ppii_image = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'product_order_invoice/', 'tmpUploade', 'gif|jpg|png|jpeg');

                           $this->api_model->uploadInvoiceByDBImages(array(
                               'ppii_invoice_id' => $invoiceId,
                               'ppii_image' => $ppii_image
                           ));
                      }
                 }
                 $data = $this->api_model->getInvoiceByOrder($_POST['ppi_order_id'], $_POST['ppi_supplier_id']);
                 die(json_encode(array('msg' => 'Invoice uploaded successfully completed', 'error' => false, 'data' => $data), JSON_UNESCAPED_SLASHES));
            } else {
                 die(json_encode(array('msg' => 'Invoice not found', 'error' => true)));
            }
       }

       function removeInvoice() {
            if (!isset($_POST['ppi_id']) || empty($_POST['ppi_id'])) {
                 die(json_encode(array('msg' => 'Invoice id empty', 'error' => true)));
            } else {
                 $this->api_model->removeInvoice($_POST['ppi_id']);
                 die(json_encode(array('msg' => 'Invoice deleted successfully completed', 'error' => false)));
            }
       }

       function removeInvoiceImage() {
            if (!isset($_POST['ppii_id']) || empty($_POST['ppii_id'])) {
                 die(json_encode(array('msg' => 'Invoice image id empty', 'error' => true)));
            } else {
                 $this->api_model->removeInvoiceImage($_POST['ppii_id']);
                 die(json_encode(array('msg' => 'Invoice image deleted successfully', 'error' => false)));
            }
       }

       function addNewInvoice() {
            if (!isset($_POST['ppii_invoice_id']) || empty($_POST['ppii_invoice_id'])) {
                 die(json_encode(array('msg' => 'Invoice id empty', 'error' => true)));
            }
            if (isset($_FILES['ppi_invoice_img']['name'][0]) && !empty($_FILES['ppi_invoice_img']['name'][0])) {
                 $invoiceId = $_POST['ppii_invoice_id'];
                 $count = count($_FILES['ppi_invoice_img']['name']);
                 if ($count > 0) {
                      for ($i = 0; $i < $count; $i++) {
                           $_FILES['tmpUploade']['name'] = $_FILES['ppi_invoice_img']['name'][$i];
                           $_FILES['tmpUploade']['type'] = $_FILES['ppi_invoice_img']['type'][$i];
                           $_FILES['tmpUploade']['tmp_name'] = $_FILES['ppi_invoice_img']['tmp_name'][$i];
                           $_FILES['tmpUploade']['error'] = $_FILES['ppi_invoice_img']['error'][$i];
                           $_FILES['tmpUploade']['size'] = $_FILES['ppi_invoice_img']['size'][$i];
                           $ppii_image = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'product_order_invoice/', 'tmpUploade', 'gif|jpg|png|jpeg');

                           $this->api_model->uploadInvoiceByDBImages(array(
                               'ppii_invoice_id' => $invoiceId,
                               'ppii_image' => $ppii_image
                           ));
                      }
                 }
                 die(json_encode(array('msg' => 'Invoice uploaded successfully completed', 'error' => false)));
            } else {
                 die(json_encode(array('msg' => 'Invoice not found', 'error' => true)));
            }
       }

       function getInvoiceByOrder() {
            $supplier = isset($_POST['ppi_supplier_id']) ? $_POST['ppi_supplier_id'] : 0;
            if (!isset($_POST['ppi_order_id']) || empty($_POST['ppi_order_id'])) {
                 die(json_encode(array('msg' => 'Invoice id empty', 'error' => true)));
            } else {
                 $data = $this->api_model->getInvoiceByOrder($_POST['ppi_order_id'], $supplier);
                 die(json_encode(array('msg' => '', 'error' => false, 'data' => $data), JSON_UNESCAPED_SLASHES));
            }
       }

       function removeProductPicked() {
            if (!isset($_POST['pop_id']) || empty($_POST['pop_id'])) {
                 die(json_encode(array('msg' => 'Prod pick id is empty', 'error' => true)));
            } else {
                 $this->api_model->removeProductPicked($_POST['pop_id']);
                 die(json_encode(array('msg' => '', 'error' => false)));
            }
       }

       function pickNewProduct() {
            if (!isset($_POST['pop_invoice_master']) || empty($_POST['pop_invoice_master'])) {
                 die(json_encode(array('msg' => 'Invoice id empty', 'error' => true)));
            } if (!isset($_POST['pop_ordd_id']) || empty($_POST['pop_ordd_id'])) {
                 die(json_encode(array('msg' => 'Order detail id empty', 'error' => true)));
            }if (!isset($_POST['pop_order_id']) || empty($_POST['pop_order_id'])) {
                 die(json_encode(array('msg' => 'Order id empty', 'error' => true)));
            } else {
                 $data = $this->api_model->addProductPicked($_POST);
                 die(json_encode(array('msg' => '', 'error' => false, 'data' => $data)));
            }
       }

       function getOrderDetailsByDB() {
            if (!isset($_POST['db']) || empty($_POST['db'])) {
                 die(json_encode(array('msg' => 'Invoice delivery boy', 'error' => true)));
            } else {
                 $data = $this->api_model->getOrderDetailsByDB($_POST['db']);
                 die(json_encode(array('msg' => '', 'error' => false, 'data' => $data)));
            }
       }

       function activeDeactivateDB() {
            $db = $this->input->post('db', true);
            $status = $this->input->post('status', true);
            $msg = 'DB activated';
            if ($status == 0) {
                 $msg = "DB de-activated";
            }
            $this->api_model->activeDeaciveUser($db, $status);
            die(json_encode(array('msg' => $msg, 'error' => false)));
       }

       function isUserIsActive() {
            $userId = $this->input->post('user', true);
            $this->api_model->isUserIsActive($userId);
       }

       function actualWeightAdjustment() {
            $orddId = $this->input->post('orddId', true);
            $actualWeight = $this->input->post('actualWeight', true);
            $actualRate = $this->input->post('actualRate', true);
            $db = $this->input->post('db', true);
            if (empty($orddId) || empty($actualWeight)) {
                 die(json_encode(array('msg' => 'Something wrong', 'error' => true)));
            } else {
                 $this->api_model->actualWeightAdjustment($orddId, $actualWeight, $db, $actualRate);
                 die(json_encode(array('msg' => 'Order weight adjustment', 'error' => false)));
            }
       }

       function getOrderDetailsByDBAndStatus() {
            if (!isset($_POST['db']) || empty($_POST['db'])) {
                 die(json_encode(array('msg' => 'Invoice delivery boy', 'error' => true)));
            } if (!isset($_POST['status']) || empty($_POST['status'])) {
                 die(json_encode(array('msg' => 'Status is empty', 'error' => true)));
            } else {
                 $data = $this->api_model->getOrderDetailsByDBAndStatus($_POST['db'], $_POST['status']);
                 die(json_encode(array('msg' => '', 'error' => false, 'data' => $data)));
            }
       }

       public function signupVirtualPartner() {

            if ($this->validateSignupVirtualPartner()) {

                 $store_image = '';
                 $store_logo = '';
                 if ($supplierID = $this->api_auth->newVirtualShop($_POST)) {

                      if (isset($_FILES['shopImages']['name']) && !empty($_FILES['shopImages']['name'])) {

                           $uploadData = array();
                           $pixel = array();
                           $newFileName = microtime(true) . $_FILES['shopImages']['name'];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'shops/';
                           $config['allowed_types'] = 'gif|jpg|png|jpeg';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);
                           if (!$this->upload->do_upload('userAvatar')) {
                                $error = array('error' => $this->upload->display_errors());
                                //   print_r($error);die;
                                //debug($this->upload->display_errors(), 0);
                           } else {
                                $uploadData = $this->upload->data();
                                $imgId = $this->api_auth->addImages(array('ssi_supplier' => $supplierID,
                                    'ssi_image' => $uploadData['file_name'],
                                    'ssi_alttag' => 'shop_img'));
                                //for home page banner
                                resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 724, 378);
                                //for view all supplier
                                resize(FILE_UPLOAD_PATH . 'shops/' . $uploadData['file_name'], 1800, 800);

                                $this->api_auth->setDefaultImage($imgId, $supplierID);
                           }
                      }
                      if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                           $pixel = array();
                           $uploadData = array();
                           $config1['upload_path'] = FILE_UPLOAD_PATH . 'avatar/';
                           $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                           $config1['file_name'] = $newFileName;
                           $this->upload->initialize($config1);
                           if ($this->upload->do_upload('userAvatar')) {
                                $uploadData = $this->upload->data();
                                $this->supplier->addBannerImages(array('sbi_type' => 11, 'sbi_supplier' => $supplierID, 'sbi_image' =>
                                    $uploadData['file_name'],
                                    'sbi_alttag' => ''
                                ));
                           } else {
                                //debug($this->upload->display_errors(), 0);
                           }
                      }

                      $_POST['user']['usr_group'] = 11;
                      $_POST['user']['usr_supplier'] = $supplierID;
                      $_POST['user']['usr_email'] = $_POST['supplier']['supm_email'];
                      $_POST['user']['usr_username'] = empty($_POST['user']['usr_first_name']) ? $_POST['supplier']['supm_name'] : $_POST['user']['usr_first_name'];
                      $identity = $this->api_auth->VirtualShopUserRegister($_POST['user']);
                      $return = array('error' => false, 'msg' => 'Registeration success', 'token' => '', 'identity' => $identity, 'full_name' => $_POST['user']['usr_username'], 'email' => $_POST['supplier']['supm_email']);
                      die(json_encode($return));
                 }
                 $return = array('error' => true, 'msg' => 'Failed to register');
                 die(json_encode($return));
            }
       }

       public function validateSignupVirtualPartner() {
            /* Set validation rule for name field in the form */
            if (empty($_POST['categories']) || empty($_POST['upn_phone_number']) || empty($_POST['user'])) {
                 $return = array('error' => true, 'msg' => 'Partial Content');
                 die(json_encode($return));
            }

            $this->form_validation->set_rules('supplier[supm_market]', "Category ID", "trim|required|numeric");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid Category ID');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('supplier[supm_name]', "Shop Name", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Shop Name required');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('supplier[supm_email]', "Shop Email", "trim|valid_email|required");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid Shop email');
                 die(json_encode($return));
            }
            if (empty($_POST['upn_phone_number'])) {
                 $return = array('error' => true, 'msg' => 'Shop Contact Number required');
                 die(json_encode($return));
            }

            foreach ($_POST['upn_phone_number'] as $row) {
                 if (!isset($row['number']) || !isset($row['code'])) {
                      continue;
                 }
                 $this->form_validation->set_rules('upn_phone_number[' . $row['number'] . ']', "Shop Contact Number", "trim|regex_match[/^[0-9]{10}$/]");
                 $this->form_validation->set_rules('upn_phone_number[' . $row['code'] . ']', "Shop Contact Number", "trim|numeric");
            }

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid Shop Contact Number');
                 die(json_encode($return));
            }
            if (empty($_POST['categories'])) {
                 $return = array('error' => true, 'msg' => 'Categories required');
                 die(json_encode($return));
            }

            foreach ($_POST['categories'] as $key => $value) {
                 $this->form_validation->set_rules('categories[' . $value . ']', "Shop Category", "trim|numeric");
            }

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid Categories');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('user[usr_first_name]', "First Nmae", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid First name');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('user[usr_last_name]', "Last Nmae", "trim");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid Last name');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('user[usr_password]', "Password", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Password required');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('user[usr_password_confirm]', "Confirm Password", "trim|required|matches[user[usr_password]]");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Password must match Confirm Password');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('user[usr_phone]', "Personal phone", "trim|required|regex_match[/^[0-9]{10}$/]");
            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Invalid Personal Phone');
                 die(json_encode($return));
            }
            $this->form_validation->set_rules('user[usr_address]', "Personal address", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                 $return = array('error' => true, 'msg' => 'Personal address required');
                 die(json_encode($return));
            }

            return true;
       }

       function getSuppliersMarket() {
            $town = $this->input->post('town', true);
            $searchResult = $this->api_model->getSuppliersMarket($town);
            if (!empty($searchResult)) {
                 die(json_encode(array('error' => false, 'msg' => '', 'data' => $searchResult)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'Town not found')));
            }
       }

       /**
        * get company news
        * author : husni
        */
       function getCompanyNews() {
            $newsResult = $this->api_model->getCompanyNews();
            if (!empty($newsResult)) {
                 die(json_encode(array('error' => false, 'msg' => '', 'data' => $newsResult)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'News not found')));
            }
       }

       function getFirmsByCategory() {
            $catid = $this->input->post('catid', true);
            $limit = $this->input->post('limit', true);
            $page = $this->input->post('page', true);
            $pageNum = ($page > 0) ? ($page * $limit) : 0;
            $firms = $this->api_model->getFirmsByCategory($catid, $limit, $pageNum);
            if (!empty($firms)) {
                 die(json_encode(array('error' => false, 'msg' => '', 'data' => $firms)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'No firms found')));
            }
       }

       /**
        * get Virtual shop categories after selecting town
        * author : husni
        */
       function getVirtualShopCategories() {

            $limit = $this->input->post('limit', true);
            $town = $this->input->post('town', true);
            $return = $this->api_model->getVirtualShopCategories($limit, $town);
            die(json_encode(array('error' => false, 'msg' => '', 'data' => $return)));
            // die(json_encode($return));
       }

       function addToRating() {
            $type = $this->input->post('type', true);
            $id = $this->input->post('conId', true);
            $userId = $this->input->post('userId', true);
            $rating = $this->input->post('rating', true);
            $title = $this->input->post('title', true);
            $desc = $this->input->post('desc', true);

            if ($type == null || $type == '' || $id == null || $id == '' || strtolower($id) == 'null' || $userId == null || $userId == '') {
                 die(json_encode(array('error' => true, 'msg' => 'Partial Content')));
            }
            $data['rat_consign'] = strtoupper($type); // Supplier, product
            $data['rat_consign_id'] = $id; //supplier id, product id
            $data['rat_added_by'] = $userId;
            $data['rat_rating'] = $rating;
            $data['rat_title'] = $title;
            $data['rat_desc'] = $desc;
            $data['rat_added_on'] = date('Y-m-d h:i:s');
            if ($insertId = $this->api_model->addToRating($data)) {
                 die(json_encode(array('error' => false, 'msg' => 'Rating successfully', 'id' => $insertId, 'data' => $data)));
            } else {
                 die(json_encode(array('error' => true, 'msg' => 'You already rated')));
            }
       }

  }
  