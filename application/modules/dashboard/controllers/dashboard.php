<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class dashboard extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->page_title = "Dashboard";
            $this->load->model('dashboard_model', 'dashboard');
            $this->lock_in();
       }

       public function index() {
            if ($this->usr_grp == 'BY') { // Only buyer privilege
                 $data = $this->dashboard->dashBoardStatistics();
                 $data['dashboardMeterials'] = $this->dashboard->dashboardMeterials();
                 $this->render_page('dashboard/dashboard-buyer', $data);
            } else {
                 $data = $this->dashboard->dashBoardStatistics();
                 $data['dashboardMeterials'] = $this->dashboard->dashboardMeterials();
                 $this->render_page('dashboard/dashboard', $data);
            }
       }

  }
  