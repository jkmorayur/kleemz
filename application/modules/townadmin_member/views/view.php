<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Town admin</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/update');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="<?php echo $data['usr_id'];?>" type="hidden" name="usr_id" id="usr_id"/>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Town admin<span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select required data-parsley-required-message="Select Town admin" name="user[usr_town_admin]" 
                                                id="bld_type" class="form-control">
                                             <option value="">Town admin</option>
                                             <?php foreach ((array) $townAdmin as $key => $value) {?>
                                                    <option <?php echo ($data['usr_town_admin'] == $value['usr_id']) ? 'selected="selected"' : '';?>
                                                         value="<?php echo $value['usr_id'];?>"><?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?></option>
                                                    <?php }?>
                                        </select>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">First Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $data['usr_first_name'];?>" type="text" id="last-name" name="user[usr_first_name]" 
                                               required="required" data-parsley-required-message="First Name required" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $data['usr_last_name'];?>" type="text" id="last-name" name="user[usr_last_name]" 
                                               data-parsley-required-message="Last Name required" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-3 col-xs-12">
                                        <input type="text" id="mobile" name="user[usr_phone]" maxlength="15" data-past=".usr_whatsapp" 
                                               value="<?php echo $data['usr_phone'];?>"
                                               class="pastContent numOnly form-control col-md-7 col-xs-12">
                                   </div>
                                  
                              </div>

                              <?php if (!empty($data['usr_avatar'])) {?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city"></label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <?php
                                               echo img(array('src' => 'assets/uploads/avatar/' . $data['usr_avatar'], 'width' => '100', 'id' => 'imgBrandImage'));
                                               ?>
                                          </div>
                                     </div>
                                <?php }?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city">ID Proof</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="usr_avatar" data-parsley-fileextension="jpg,png,gif,jpeg" class="autoComCity form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
