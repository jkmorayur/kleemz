<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Town admin members</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <a href="<?php echo site_url($controller . '/add');?>"  name='type' value="excel" class="btn btn-success">New admin member</a>
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Name</th>
                                        <th>Town</th>
                                        <th>Phone</th>
                                        <?php
                                          if ((check_permission($controller, 'delete'))) {
                                               echo '<th>Action</th>';
                                          }
                                        ?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $data as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/view/' . encryptor($value['usr_id']));?>">
                                               <td class="trVOE"><?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?></td>
                                               <td class="trVOE">
                                                    <?php
                                                    echo $this->townadmin_member->getTownByUser($value['usr_id']);
                                                    ?>
                                               </td>
                                               <td class="trVOE"><?php echo $value['usr_phone'];?></td>
                                               <td>
                                                    <?php if (check_permission($controller, 'delete')) {?>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" title="Delete"
                                                            data-url="<?php echo site_url($controller . '/deleteCountry/' . encryptor($value['usr_id']));?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    <?php } ?>
                                               </td>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>