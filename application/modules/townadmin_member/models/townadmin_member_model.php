<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class townadmin_member_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getUsers($id = '') {
            if ($id) {
                 if ($this->usr_grp == 'BY') {
                      $this->db->where(tbl_users . '.usr_id', $this->uid);
                 }
                 $this->db->where(tbl_users . '.usr_id', $id);
                 $return['data'] = $this->db->select(tbl_users . '.*, ' .
                                         tbl_users_groups . '.group_id as group_id, ' .
                                         tbl_groups . '.name as group_name, ' .
                                         tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                         tbl_market_places . '.*')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_market', 'LEFT')
                                 ->where(tbl_groups . '.id', 9)->get(tbl_users)->row_array();
                 $return['phone'] = $this->db->get_where(tbl_users_phone_number, array('upn_user_id' => $id))->result_array();
                 $return['town'] = $this->db->get_where(tbl_townadmin_town_assoc, array('tta_user_id' => $id))->result_array();
                 return $return;
            } else {
                 if ($this->usr_grp == 'BY') {
                      $this->db->where(tbl_users . '.usr_id', $this->uid);
                 }
                 $this->db->where(tbl_users . '.usr_active !=', 0);
                 $this->db->where(tbl_users . '.usr_id !=', 1);
                 $this->db->where('(' . tbl_users . '.usr_supplier IS null')->or_where(tbl_users . '.usr_supplier = 0)');
                 return $this->db->select(tbl_users . '.*, ' .
                                         tbl_users_groups . '.group_id as group_id, ' .
                                         tbl_groups . '.name as group_name, ' .
                                         tbl_groups . '.description as group_desc, branch.usr_username AS branch,' .
                                         tbl_market_places . '.*')
                                 ->join(tbl_users_groups, tbl_users_groups . '.user_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_groups, tbl_users_groups . '.group_id = ' . tbl_groups . '.id', 'LEFT')
                                 ->join(tbl_users . ' branch', 'branch.usr_id = ' . tbl_users . '.usr_id', 'LEFT')
                                 ->join(tbl_market_places, tbl_market_places . '.mar_id = ' . tbl_users . '.usr_market', 'LEFT')
                                 ->where(tbl_groups . '.id', 9)->get(tbl_users)->result_array();
            }
       }

       function getTownByUser($id) {
            return $this->db->select(tbl_users . ".usr_town_admin, CONCAT(admin.usr_first_name, ' ' , admin.usr_last_name) AS admin_first_name", false)
                            ->join(tbl_users . ' admin', 'admin.usr_id = ' . tbl_users . '.usr_town_admin', 'LEFT')
                            ->where(array(tbl_users . '.usr_id' => $id))->get(tbl_users)->row()->admin_first_name;
       }

       function register($data) {
            if ($data) {
                 $username = isset($data['user']['usr_first_name']) ? $data['user']['usr_first_name'] : '';
                 $password = isset($data['user']['usr_password']) ? $data['user']['usr_password'] : '';
                 $email = isset($data['user']['usr_email']) ? $data['user']['usr_email'] : '';

                 $group = array(9); // Sets user to admin.

                 unset($data['user']['usr_password']);
                 unset($data['user']['usr_password_conf']);

                 $data['user']['usr_added_by'] = $this->uid;
                 $lastInsertId = $this->ion_auth->register($username, $password, $email, $data['user'], $group);
                 // Save phone numbers
                 generate_log(array(
                     'log_title' => 'Create new user',
                     'log_desc' => serialize($data),
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $lastInsertId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Create new user',
                     'log_desc' => 'Failed to create new user',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => 0,
                     'log_added_by' => $this->uid
                 ));

                 return false;
            }
       }

       function update($data) {

            $id = isset($data['usr_id']) ? $data['usr_id'] : '';
            unset($data['usr_id']);
            $phones = $data['upn_phone_number'] ? $data['upn_phone_number'] : array();
            $markets = $data['market'] ? $data['market'] : array();
            $data = $data['user'];

            if (isset($data['usr_password']) && empty($data['usr_password'])) {
                 unset($data['usr_password']);
            }
            unset($data['usr_password_conf']);

            $data['usr_active'] = 1;

            $data['usr_updated_by'] = $this->uid;
            $this->ion_auth->update($id, $data);

            generate_log(array(
                'log_title' => 'Update user',
                'log_desc' => 'Updated user details',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'U',
                'log_ref_id' => $id,
                'log_added_by' => $this->uid
            ));

            return true;
       }

  }
  