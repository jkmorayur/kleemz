<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class townadmin_member extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Townadmin';
            $this->load->model('townadmin_member_model', 'townadmin_member');
            $this->load->model('townadmin/townadmin_model', 'townadmin');
            $this->load->model('market/market_model', 'market');
            $this->lock_in();
       }

       public function index() {

            $data['data'] = $this->townadmin_member->getUsers();
//            debug($data['data']);
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function add() {
            if (!empty($_POST)) {
                 if (isset($_FILES['usr_avatar']['name'])) {
                      $this->load->library('upload');
                      $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                      $config['upload_path'] = './assets/uploads/avatar/';
                      $config['allowed_types'] = 'jpg|jpeg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      if ($this->upload->do_upload('usr_avatar')) {
                           $uploadData = $this->upload->data();
                           $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                      }
                 }
                 if ($this->townadmin_member->register($_POST)) {
                      $this->session->set_flashdata('app_success', 'New townadmin created!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't create row!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['townAdmin'] = $this->townadmin->getUsers();
                 $data['marketPlaces'] = $this->market->gerMarketPlaces();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       public function view($id) {
            $id = encryptor($id, 'D');
            $data = $this->townadmin_member->getUsers($id);
            $data['marketPlaces'] = $this->market->gerMarketPlaces();
            $data['townAdmin'] = $this->townadmin->getUsers();
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function update() {
            if (isset($_FILES['usr_avatar']['name'])) {
                 $this->load->library('upload');
                 $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                 $config['upload_path'] = './assets/uploads/avatar/';
                 $config['allowed_types'] = 'jpg|jpeg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);
                 if ($this->upload->do_upload('usr_avatar')) {
                      $uploadData = $this->upload->data();
                      $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                 }
            }
            if ($this->townadmin_member->update($_POST)) {
                 $this->session->set_flashdata('app_success', 'Town admin successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', 'Town admin successfully updated!');
            }
            redirect(strtolower(__CLASS__));
       }

       function deleteCountry($id = '') {
            $id = encryptor($id, 'D');
            if ($this->ion_auth->delete_user($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Town admin successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => 'Something went wrong,try again')));
            }
       }

  }
  