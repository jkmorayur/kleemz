<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>General Settings</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <?php echo form_open_multipart("settings/insert", array('id' => "frmSettings", 'class' => "form-horizontal form-label-left"))?>
                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                   <li role="presentation" class="active">
                                        <a href="javascript:;" goto="#basic_settings" id="home-tab" role="tab" 
                                           data-toggle="tab" aria-expanded="true">Basic Settings</a>
                                   </li>
                                   <li role="presentation" class="pars">
                                        <a href="javascript:;" goto="#image_settings" id="home-tab" role="tab" 
                                           data-toggle="tab" aria-expanded="true">Image Settings</a>
                                   </li>
                                   <li role="presentation" class="pars">
                                        <a href="javascript:;" goto="#sms_settings" role="tab" id="profile-tab" 
                                           data-toggle="tab" aria-expanded="false">SMS Settings</a>
                                   </li>
                              </ul>
                         </div>
                         <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane fade active in" id="basic_settings" aria-labelledby="home-tab">
                                   <div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Admin E-mail</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[admin_email]" id="mod_title" placeholder="Admin E-mail"
                                                         value="<?php echo get_settings_by_key('admin_email');?>"/>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Minimum delivery</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control numOnly" autocomplete="off"
                                                         style="float: left;width: 48%;"
                                                         name="settings[min_del_kl]" id="mod_title" placeholder="Default delivery area (KL)"
                                                         value="<?php echo get_settings_by_key('min_del_kl');?>"/>
                                                  
                                                  <input type="text" class="form-control col-md-1 col-xs-1 decimalOnly" autocomplete="off"
                                                         style="float: left;width: 49%;margin-left: 10px;"
                                                         name="settings[min_del_chrg]" id="mod_title" placeholder="Default delivery charge"
                                                         value="<?php echo get_settings_by_key('min_del_chrg');?>"/>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Excess delivery charge / KM</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control numOnly" autocomplete="off"  
                                                         name="settings[excess_del_charge]" id="mod_title" placeholder="Excess delivery charge  / KM"
                                                         value="<?php echo get_settings_by_key('excess_del_charge');?>"/>
                                             </div>
                                        </div>
                                        
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">News Upload</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <textarea class="form-control" autocomplete="off"  
                                                         name="settings[company_news]" id="" placeholder="Upload News"
                                                         value="<?php echo get_settings_by_key('company_news');?>"><?php echo get_settings_by_key('company_news');?>
                                                  </textarea>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="image_settings" aria-labelledby="home-tab">
                                   <div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <div class="panel panel-default">
                                                       <div class="panel-heading">Suppliers image upload options</div>
                                                       <div class="panel-body">
                                                            <label class="control-label col-md-5 col-sm-3 col-xs-12">Maximum shop image</label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                 <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                                        name="settings[sup_shop_img_limit]" id="mod_title" placeholder="Maximum shop image"
                                                                        value="<?php echo get_settings_by_key('sup_shop_img_limit');?>"/>
                                                                 <small>Maximum shop image</small>
                                                            </div>

                                                            <label class="control-label col-md-5 col-sm-3 col-xs-12">Maximum home banners</label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                 <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                                        name="settings[sup_home_bnr_img_limit]" id="mod_title" placeholder="Maximum home banners"
                                                                        value="<?php echo get_settings_by_key('sup_home_bnr_img_limit');?>"/>
                                                                 <small>Maximum home banner</small>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Product image limit</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[prod_img_limit]" id="mod_title" placeholder="Product image limit"
                                                         value="<?php echo get_settings_by_key('prod_img_limit');?>"/>
                                                  <small>Products image upload limit</small>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Image Thumbnail Width</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[thumbnail_width]" id="mod_title" placeholder="Image Thumbnail Width"
                                                         value="<?php echo get_settings_by_key('thumbnail_width');?>"/>
                                                  <small>Image Thumbnail Width</small>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Image Thumbnail Height</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[thumbnail_height]" id="mod_title" placeholder="Image Thumbnail Height"
                                                         value="<?php echo get_settings_by_key('thumbnail_height');?>"/>
                                                  <small>Image Thumbnail Heigh</small>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Image Thumbnail Quality</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[thumbnail_quality]" id="mod_title" placeholder="Image Thumbnail Quality"
                                                         value="<?php echo get_settings_by_key('thumbnail_quality');?>"/>
                                                  <small>Image Thumbnail Quality Eg:60%</small>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="sms_settings" aria-labelledby="home-tab">
                                   <div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Sender Id</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sms_sender_id]" id="mod_title" placeholder="Sender Id"
                                                         value="<?php echo get_settings_by_key('sms_sender_id');?>"/>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sms_username]" id="mod_title" placeholder="Username"
                                                         value="<?php echo get_settings_by_key('sms_username');?>"/>
                                             </div>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sms_password]" id="mod_title" placeholder="Password"
                                                         value="<?php echo get_settings_by_key('sms_password');?>"/>
                                             </div>
                                        </div>
                                        
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Signup OTP length</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sms_signup_otp_length]" id="mod_title" placeholder="Signup OTP length"
                                                         value="<?php echo get_settings_by_key('sms_signup_otp_length');?>"/>
                                             </div>
                                        </div>

                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">OTP Signature</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sms_otp_signature]" id="mod_title" placeholder="OTP Signature"
                                                         value="<?php echo get_settings_by_key('sms_otp_signature');?>"/>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>