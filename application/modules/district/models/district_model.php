<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class district_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
       }

       function getDistrictByState($stateId = '') {
            return $this->db->select('dit_id AS col_id, dit_district AS col_title')->order_by('dit_district')
                    ->get_where(tbl_district, array('dit_state' => $stateId))->result_array();
       }

  }
  