<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class district extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'District';
            $this->load->model('district_model', 'district');
            $this->load->model('country/country_model', 'country');
       }
       
       function getDistrictByState() {
            $cntryId = isset($_POST['id']) ? $_POST['id'] : '';
            die(json_encode($this->district->getDistrictByState($cntryId)));
       }
    }
  