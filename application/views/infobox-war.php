<div class="alert-info-box ui-pnotify-container alert-info ui-pnotify-shadow" role="alert" style="background: red; height: auto;float: left;width: 100%;">
     <div class="ui-pnotify-closer" aria-role="button" tabindex="0" title="Close" 
          style="cursor: pointer; visibility: hidden; display: none;">
          <span class="glyphicon glyphicon-remove"></span>
     </div>
     <div class="ui-pnotify-sticker" aria-role="button" aria-pressed="true" tabindex="0" title="Unstick" 
          style="cursor: pointer; visibility: hidden; display: none;"><span class="glyphicon glyphicon-play" 
                                                                       aria-pressed="true"></span></div>
     <div class="ui-pnotify-icon">
          <span class="glyphicon glyphicon-info-sign" style="float: left;font-size: 25px;"></span>
          <h4 class="ui-pnotify-title" style="margin: 2px 0px 0px 10px; float: left;">Notify</h4>
     </div>
     <div class="ui-pnotify-text" aria-role="alert" style="float: left;width: 100%;margin-left: 35px;">
          <?php echo $message; ?>
     </div>
</div>