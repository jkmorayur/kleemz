<div class="follow-alert alert alert-success alert-dismissable success_msgBox" style="display: none;">
     <!-- <button type="button" class="close" onclick="$(this).parent('div').hide();">×</button> -->
         <i class="fas fa-check"></i>
     <span class="success_msg"></span>
</div>
<div class="alert alert-danger alert-dismissible  msgBox_fail" role="alert" style="display: none;">
         <i class="fas fa-times"></i>
     <strong class="fail_msg"></strong>
</div>
<div class="follow-alert alert alert-success alert-dismissable fail_msgBox" style="display: none;">
     <!-- <button type="button" class="close" onclick="$(this).parent('div').hide();">×</button> -->
     <span class="fail_msg"></span>
</div>
<?php if ($success = $this->session->flashdata('app_success')): ?>
       <div class="alert alert-success alert-dismissible msgBox" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fas fa-check"></i>
                <?php echo $success ?>
            <strong></strong>
       </div>
  <?php endif ?>
  <?php if ($error = $this->session->flashdata('app_error')): ?>
       <div class="alert alert-danger alert-dismissible msgBox" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fas fa-times"></i>
                <?php echo $error ?>
            <strong></strong>
       </div>
  <?php endif ?>

<!-- <div class="alert alert-success alert-dismissible fade in msgBox" style="display: none;">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
     <strong class="sus_msg"><?php echo $success ?></strong>
</div> -->