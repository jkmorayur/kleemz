<div class="slide">
     <ul>
          <?php foreach ((array) $banner as $key => $value) {?>
                 <li data-bg="<?php echo 'uploads/banner/' . $value['bnr_image'];?>"></li>
            <?php }?>
     </ul>
</div>

<header id="home" class="welcome-fullseen-slider-area">
     <div class="header-top-area">
          <div class="container">
               <div class="col-12 pr-0 text-right">
                    <div class="logo-container">
                         <!-- Start Logo -->
                         <div class="logo">
                              <a href="<?php echo site_url();?>">
                                   <img src="images/fujeeka-logo-g.png" alt="fujeeka logo">
                              </a>
                         </div>
                         <!-- / End Logo -->
                    </div>
                    <div class="mnu">
                         <!-- Start Menu -->
                         <div class="menu-container">
                              <div class="wsmenucontainer clearfix">
                                   <div id="overlapblackbg"></div>
                                   <div class="wsmobileheader clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> <!-- <a class="smallogo"><img src="assets/images/fujeeka-logo.png"  alt="" /></a> --></div>

                                   <div class="headerfull">
                                        <!--Main Menu HTML Code-->
                                        <div class="wsmain">
                                             <nav class="wsmenu clearfix">
                                                  <ul class="mobile-sub wsmenu-list">
                                                       <li><a href="javascript:;" class="navtext">Service & Membership</a>
                                                          <div class="megamenu clearfix halfmenu">
                                                            <div class="container-fluid">
                                                              <div class="row">
                                                                <div class="col-lg-12 col-md-12">
                                                                  <ul class="wstliststy06 clearfix">
                                                                    <!-- <li class="wstheading clearfix">Option 1</li> -->
                                                                    <li><a href="javascript:;">Register</a></li>
                                                                    <li><a href="javascript:;">Terms &amp; Conditions</a></li>
                                                                  </ul>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </li>

                                                       <li><a href="javascript:;" class="navtext">Help & Community</a>
                                                          <div class="megamenu clearfix halfmenu">
                                                            <div class="container-fluid">
                                                              <div class="row">
                                                                <div class="col-lg-12 col-md-12">
                                                                  <ul class="wstliststy02 clearfix">
                                                                    <li><a href="javascript:;">For Buyer</a></li>
                                                                    <li><a href="javascript:;">For Supplier</a></li>
                                                                    <li><a href="javascript:;">For New User</a></li>
                                                                    <li><a href="javascript:;">Submit Dispute</a> </li>
                                                                    <li><a href="javascript:;">FAQ</a> </li>
                                                                    
                                                                  </ul>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </li>

                                                       <li>
                                                           <a href="<?=site_url('home/get_app') ?>" class="navtext"><i class="fas fa-mobile-alt green-txt"></i> Get App</a>
                                                        </li>

                                                       <li class="signin sh"><a  class="navtext">
                                                                 <?php if (!$this->uid) {?>
                                                                        <div class="reg">
                                                                             Sign In | Register 
                                                                        </div>
                                                                   <?php } else {?>
                                                                        <div class="reg">
                                                                             My Fujeeka
                                                                        </div>
                                                                   <?php }?>
                                                            </a>
                                                            <div class="megamenu clearfix halfmenu">
                                                                 <div class="container-fluid">
                                                                      <div class="row">
                                                                           <div class="signin-container">

                                                                                <?php if (!$this->uid) {?>
                                                                                       <div class="top-section">

                                                                                            <h4 class="heading3">Get started now!</h4>

                                                                                            <a class="btn btn-success btn-green btn-signin" href="<?php echo site_url('user/login');?>">Sign In</a>
                                                                                            <div class="w-100">
                                                                                                 or
                                                                                            </div>
                                                                                            <a class="btn btn-success btn-reg" href="<?php echo site_url('user/signup');?>">Buyer Sign up</a>

                                                                                       </div>
                                                                                  <?php }?>

                                                                                <?php if ($this->uid) {?>
                                                                                       <div class="top2">
                                                                                            <div class="img">
                                                                                                 <?php
                                                                                                 echo img(array('src' => 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar'),
                                                                                                     'class' => 'img-fluid'));
                                                                                                 ?>
                                                                                              <!-- <img src="images/avatar" alt="" class="img-fluid"> -->
                                                                                            </div>
                                                                                            <div class="name">
                                                                                                 <h3>Welcome, <?php echo $this->session->userdata('usr_username');?></h3>
                                                                                                 <p><a href="<?php echo site_url('dashboard');?>" target="_blank" rel="noopener noreferrer">View Dashboard</a></p>
                                                                                            </div>
                                                                                       </div>
                                                                                       <div class="my-section">
                                                                                            <div class="w-100 heading-bx">
                                                                                                 My Fujeeka
                                                                                            </div>
                                                                                            <ul>
                                                                                                 <li><a href="<?= site_url('product/rfq-list')?>" target="_blank" rel="noopener noreferrer">Manage RFQ</a></li>
                                                                                                 <li><a href="<?= site_url('order')?>" target="_blank" rel="noopener noreferrer">My Orders</a></li>
                                                                                                 <li><a href="<?= site_url('dashboard')?>" target="_blank" rel="noopener noreferrer">My Account</a></li>
                                                                                                 <li class="signout"><a href="<?= site_url('user/logout')?>" >Signout</a></li>
                                                                                            </ul>
                                                                                       </div>
                                                                                  <?php }?>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </li>
                                                  </ul>
                                             </nav>
                                        </div>
                                        <!--Menu HTML Code-->
                                   </div>
                              </div>
                              <!-- /menu -->
                              <div class="clearfix"></div>
                         </div>
                         <!-- / End Menu -->
                    </div>
                    <!-- banner content area -->
                    <section class="ban-content">
                         <h2>The right solution for your <span>wholesale</span> purchase needs</h2>
                         <!-- search bar -->
                         <!--                         <div class="searchbar">
                                                       <div class="input-group-prepend">
                                                            <div class="input-group-btn search-panel">
                                                                 <button class="btn btn-outline-secondary dropdown-toggle filter-btn btnSearchMaster" type="button" 
                                                                         data-url="<?php echo site_url('home/search');?>" aria-expanded="false"
                                                                         data-toggle="dropdown" aria-haspopup="true">Product</button>
                                                                 </button>
                                                                 <div class="dropdown-menu" role="menu">
                                                                      <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Supplier</a>
                                                                      <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Product</a>
                                                                 </div>
                                                            </div>
                                                            <input type="hidden" name="search_param" value="all" id="search_param">
                                                            <input type="text" class="form-control txtQuickSearch" name="x" placeholder="What are you looking for?">
                                                            <span class="input-group-btn btnQuickSearch">
                                                                 <button class="btn btn-default srch-btn" type="button">
                                                                      <i class="fas fa-search"></i>
                                                                 </button>
                                                            </span>
                                                       </div>
                                                       <div class="divSearchResult"></div>
                                                  </div>-->
                         <?php echo $this->load->view('searchbar');?>
                         <!--/ searchbar -->
                         <p class="it-txt grey-txt">Eg : Mobile phones, headsets, power banks etc.</p>
                    </section>
                    <!-- / banner content area -->
               </div>
          </div>
     </div>
     <!-- end topbar -->
     <div class="clearfix"></div>
</header>