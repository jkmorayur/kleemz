<div class="search-bx">
     <div class="input-group-prepend">
          <div class="input-group-btn search-panel">
               <button class="btn btn-outline-secondary dropdown-toggle filter-btn btnSearchMaster" type="button" 
                       data-url="<?php echo site_url('home/search');?>" aria-expanded="false"
                       data-toggle="dropdown" aria-haspopup="true"><?php echo (isset($segments) && !empty($segments)) ? $segments : 'Product'; ?></button>
               </button>
               <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Supplier</a>
                    <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Product</a>
                    <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Stock</a>
                    <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Logistics</a>
                    <a class="dropdown-item" href="javascript:;" onclick="$('.btnSearchMaster').html($(this).html());">Quality Control</a>
               </div>
          </div>
          <input type="hidden" name="search_param" value="all" id="search_param">
          <input type="text" id="ser_text" class="form-control commonAutoComplete txtQuickSearch" name="x" placeholder="The right solution for your wholesale purchase needs"
                 value="<?php echo isset($searchString) ? clean_text($searchString, 0) : '';?>">
          <span class="input-group-btn btnQuickSearch" data-url="<?php echo site_url('stuffs/search');?>">
               <button class="btn btn-success srch-btn" type="button">
                    <i class="fas fa-search"></i>
               </button>
          </span>
     </div>
     <!-- <div class="divSearchResult"></div> -->
</div>