<!doctype html>
<html lang="en">
     <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="keywords" content="">
          <meta name="author" content="Fujishka">

          <base href="<?php echo base_url('assets/');?>/"/>
          <title><?php echo $template['title']?></title>
          <?php echo $template['metadata']?>
          <script>
               var site_url = "<?php echo site_url();?>";
               var none_image = "<?php echo $this->config->item('none_image');?>";
          </script>

          <!-- favicon-->
          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
          <link rel="icon" href="images/favicon.ico" type="image/x-icon">
          <!-- Bootstrap CSS -->
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
          <!-- fontawesome CSS -->
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
          <!--Main Menu File-->
          <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
          <!-- CUSTOM CSS -->
          <link rel="stylesheet" href="css/slider.css">
          <link rel="stylesheet" href="css/main.css">
          <?php if($this->router->fetch_method()=='get_360_list'){ ?>
               <link rel="stylesheet" href="css/fontello.css">
          <?php } ?>
          <?php if($this->router->fetch_method()!='sitemap' && $this->router->fetch_method()!='get_360_list' && $this->router->fetch_method()!=='complaint_form' && $this->router->fetch_method()!=='contact_us' && $this->router->fetch_method()!=='complaints'){ ?>
               <link rel="stylesheet" href="css/logistic.css">
          <?php } ?> 
          <?php if($this->router->fetch_method()=='getQuote'){ ?>
               <link rel="stylesheet" href="css/logisticquote.css">
          <?php } ?> 
          <?php if($this->router->fetch_method()=='contact_us'){ ?>
            <link rel="stylesheet" href="css/sitemap.css">
          <?php } ?>
          <?php if($this->router->fetch_method()=='complaints'){ ?>
            <link rel="stylesheet" href="css/submit.css">
          <?php } ?> 
          <?php if($this->router->fetch_method()=='complaint_form'){ ?>
            <link rel="stylesheet" href="css/compliant_form.css">
          <?php } ?>  
          <?php if($this->router->fetch_method()=='about_us'){ ?>
            <link rel="stylesheet" href="css/about.css">
          <?php } ?> 
           <?php if($this->router->fetch_method()=='sitemap'){ ?>
            <link rel="stylesheet" href="css/sitemap.css">
          <?php } ?> 
          
     </head>
     <body class="">

          <?php echo $template['partials']['site_default_header'];?>
          <?php echo $template['partials']['flash_messages']?>
          <?php echo $template['body']?>

         <!-- /content area -->
<div class="clearfix"></div>
<footer>
        <div class="container">
            <div class="links-container">
                <div class="row">
                <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Registration</h5>
                                <ul>
                                   <li><a href="<?php echo site_url('user/signup');?>">Buyer</a></li>
                                   <li><a href="<?php echo site_url('supplier/supplier-register');?>">Supplier</a></li>
                                   <li><a href="<?php echo site_url('market/market-register');?>">Building</a></li>
                                   <li><a href="<?php echo site_url('logistics/register');?>">Logistics</a></li>
                                   <li><a href="<?= site_url('quality-control/register')?>">Quality Control</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>Info</h5>
                                <ul>
                                   <li><a href="<?=site_url('about-us')?>">About Fujeeka</a></li>
                                    <li><a href="<?php echo site_url('logistics');?>">Logistics</a></li>
                                    <li><a href="<?php echo site_url('quality-control');?>">Quality Control</a></li>
                                    <li><a href="<?=site_url('contact-us')?>">Contact Us</a></li>
                                    <li><a href="javascript:;">Sitemap</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>LEGAL</h5>
                                <ul>
                                   <li><a href="<?=site_url('terms-of-use');?>">Terms of Use</a></li>
                                   <li><a href="<?= site_url('privacy-policies'); ?>">Privacy Policy</a></li>
                                   <li><a href="<?=site_url('product-listing-policies');?>">Product Listing Policy</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>Help</h5>
                                <ul>
                                    <li><a href="javascript:;">Help Center</a></li>
                                    <li><a href="<?=site_url('complaint_register')?>">Submit Complaint</a> </li>
                                    <li><a href="<?=site_url('faq')?>">FAQ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="f-rfq">
                            <h5>Post your buying request</h5>
                            <?php echo form_open_multipart(site_url('product/rfq'), array('class' => "form-signin", 'data-parsley-validate' => "true"))?>
                                <div class="form-group">
                                    <select class="form-control" id="sell1" name="rfq_category" required data-parsley-required-message="Select a category">
                                         <option value="">Select Category</option>
                                         <?php foreach(get_buying_config('cat') as $cat){ ?>
                                             <option value="<?=$cat['cat_id']?>"><?=$cat['cat_title']?></option>
                                         <?php } ?>    
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select class="form-control" id="markets" name="markets">
                                        <option value="">Select Market</option>
                                        <?php foreach(get_buying_config('mar') as $mar){ ?>
                                             <option value="<?=$mar['mar_id']?>"><?=$mar['mar_name']?></option>
                                        <?php } ?>      
                                   </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete" id="product" placeholder="Keywords of products you are looking for" name="pname" data-url="<?php echo site_url('product/productAutoName');?>">
                                    <input type="hidden" id="prd_id" value="0" name="rfq_product">
                                    <input type="hidden"  class="unit_sel"  name="unit">

                                </div>

                                <button class="btn btn-success btn-green" value="footer" name="from_footer" type="submit">Get Quote</button>
                            <?php echo form_close()?>
                        </div>
                    </div>

                    <div class="col-md-4">
                    
                        <!-- Subscribe -->
                        <div class="subscribe-section">
                              <h5>Subscribe Newsletter</h5>
                              <form class="form-inline frmNewsletterSub" data-url="<?php echo site_url('general/newsletterSub'); ?>">
                                   <div class="form-group mr-2 mb-2">
                                        <label for="inputPassword2" class="sr-only">Password</label>
                                        <input type="email" class="form-control txtNewsSubEmail" id="subs-email" placeholder="Enter your email ">
                                   </div>
                                   <button type="submit" class="btn btn-success btn-green mb-2">Subscribe</button>
                                   <p>Subscribe newsletter to get notifications</p>
                              </form>
                              <p class="newsLtrMsg" style="color: red;font-size: 10px;"></p>
                         </div>
                        <!-- /Subscribe -->

                        <div class="gapp">
                            <h5>Get App</h5>
                            <div class="app-qr">
                                <a href="<?=site_url('home/get_app')?>"><img src="<?=site_url('assets/images/app-store.png')?>" alt="scan code to install ios app">
                                <img src="<?=site_url('assets/images/playstore.png')?>" alt="scan code to install android app"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-base">
                <div class="row">
                    <!-- <div class="col-md-4 download-app">
              <img src="assets/images/app-store.png" alt="Download from Appstore" class="img-fluid">
              <img src="assets/images/playstore.png" alt="Download from Playstore" class="img-fluid">
            </div> -->
                    <div class="col-md-6 text-left">
                        &copy; Copyright <?=date('Y')?> Fujeeka | All Rights Reserved
                    </div>
                    <div class="col-md-6">
                        <div class="sicon">
                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="ifacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>

                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="itwittter" title="Twitter"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>

                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="iInsta" title="Instagram"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- modernizr JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!--Main Menu File-->
<script type="text/javascript" src="js/webslidemenu.js"></script>
<!-- CUSTOM JS -->

<?php if($this->router->fetch_method()!='get_360_list'){ ?>
     <script src="js/scripts-inner.js"></script>
<?php } ?>
<script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script src="js/parsley.min.js"></script>
</body>
</html>
<script>
    site_url = "<?=site_url();?>" ;
</script>
<?php if($this->router->fetch_method()=='get_360_list'){ ?>
     <script src="js/360_list.js"></script>
<?php } ?>
<?php if(($this->router->fetch_class()=='logistics' || $this->router->fetch_class()=='quality_control') && $this->router->fetch_method()=='index'){ ?>
<script>
$(document).ready(function(){
  $(".lg").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    } 
  });
});
</script>
<?php } ?>
