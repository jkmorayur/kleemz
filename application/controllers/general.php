<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  require './vendor/autoload.php';

  class general extends App_Controller {

       public function __construct() {
            parent::__construct();

            require_once(APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php');
            $this->mail = new PHPMailer;
       }

       function cmnChangeStatus($pkId, $table, $statusFieldName, $whrFieldName) {
            $pkId = encryptor($pkId, 'D');
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            if ($this->common_model->changeStatus($pkId, $ischecked, $table, $statusFieldName, $whrFieldName)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Status successfully changed')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't change status")));
            }
       }

       function doUploadFromEditor() {
            $newFileName = rand(9999999, 0);
            $config['upload_path'] = FILE_UPLOAD_PATH . 'redactor/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = $newFileName;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                 debug($this->upload->display_errors());
            } else {
                 $data = $this->upload->data();
                 $array = array(
                     'filelink' => '../assets/uploads/redactor/' . $data['file_name']
                 );

                 echo json_encode($array);
                 exit;
            }
       }

       function clearSupplier() {
            if ($this->common_model->clearSupplier()) {
                 $this->session->set_flashdata('app_success', 'Database cleaned successfully!');
            } else {
                 $this->session->set_flashdata('app_error', 'Error occured!');
            }
            redirect(__CLASS__ . '/dbclean');
       }

       function cleanProduct() {
            if ($this->common_model->cleanProduct()) {
                 $this->session->set_flashdata('app_success', 'Database cleaned successfully!');
            } else {
                 $this->session->set_flashdata('app_error', 'Error occured!');
            }
            redirect(__CLASS__ . '/dbclean');
       }

       function dbclean() {
            $this->load->view('dbclean');
       }

       function showme() {
            debug($this->session->userdata);
       }

       function getNotifications() {
            /* New supplier registration */
            echo json_encode($this->common_model->getNotifications());
       }

       function newsletterSub() {
            if ($this->common_model->newsletterSub($this->input->post('email'))) {
                 /* Mail to subscriber */
                 $this->mail->isSMTP();
                 $this->mail->Host = 'bh-44.webhostbox.net';
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = 'support@fujishka.org';
                 $this->mail->Password = 'support#@1';
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom('info@fujishka.org', 'Fujishka');
                 $this->mail->addReplyTo('info@fujishka.org', 'Fujishka');
                 $this->mail->addAddress($this->input->post('email'));
                 $this->mail->Subject = 'Newsletter subscription';
                 $this->mail->isHTML(true);
                 $mailData['email'] = $this->input->post('email');
                 $this->mail->Body = $this->load->view('newsletter-subscription-template', $mailData, true);
                 $this->mail->send();
                 die(json_encode(array('status' => 'success', 'msg' => 'You successfully subscribed!')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => 'You already subscribed!')));
            }
       }

       function toggler($table, $updateField, $pkField, $pkId) {
            if (!empty($table) && !empty($updateField) && !empty($pkField) && !empty($pkId)) {
                 if ($this->common_model->toggler($table, $updateField, $pkField, $pkId)) {
                      die(json_encode(array('status' => 'success', 'msg' => 'Field successfully updated')));
                 }
            }
            die(json_encode(array('status' => 'fail', 'msg' => 'Error occured')));
       }

       function supplier() {
            $this->load->model('emp_details/emp_details_model', 'emp_details');
            $f = $this->db->get_where(tbl_supplier_master)->result_array();
            foreach ($f as $key => $value) {
                 /* $this->db->insert(tbl_supplier_categories, array('scat_master_id' => $value['supm_id'], 'scat_category' => $value['supm_cat_id']));
                   $this->db->insert(tbl_supplier_contacts, array('spc_master_id' => $value['supm_id'], 'spc_number' => $value['supm_number']));
                   $this->db->insert(tbl_supplier_banner_images, array(
                   'sbi_supplier' => $value['supm_id'],
                   'sbi_image' => str_replace('uploads/', '', $value['supm_home_banner']),
                   'sbi_is_default' => 1,
                   'sbi_status' => 1,
                   'sbi_type' => 1
                   )); */

                 $udata['usr_group'] = 2;
                 $udata['usr_supplier'] = $value['supm_id'];
                 $udata['usr_username'] = $value['supm_name'];
                 $udata['usr_first_name'] = $value['supm_name'];

                 $udata['usr_email'] = $value['supm_email'];
                 $udata['usr_password'] = '123456';
                 $udata['usr_password_confirm'] = '123456';
                 $udata['usr_phone'] = $value['supm_number'];
                 $udata['usr_address'] = $value['supm_address'];

                 $this->emp_details->register($udata);
            }
       }

       function moveUser() {
            $this->load->model('emp_details/emp_details_model', 'emp_details');
            $f = $this->db->get_where('cpnl_users_new', array('usr_user_type' => 1))->result_array();
//            debug($f);

            $arrayCol = array();
            $table = $this->db->select('COLUMN_NAME')
                            ->where(array('TABLE_SCHEMA' => 'kleemz', 'TABLE_NAME' => 'cpnl_users'))
                            ->get('INFORMATION_SCHEMA.COLUMNS')->result_array();
            foreach ($table as $key => $value) {
                 $arrayCol[$value['COLUMN_NAME']] = '';
            }
            unset($arrayCol['usr_id']);
            unset($arrayCol['usr_supplier']);
            unset($arrayCol['usr_forgotten_password_time']);
            unset($arrayCol['usr_is_subscribed_newsletter']);
            unset($arrayCol['usr_since']);
            unset($arrayCol['usr_updated_by']);
            unset($arrayCol['usr_otp']);
            unset($arrayCol['login_otp']);
//            debug($arrayCol);
//            debug($f);

            foreach ($f as $key => $value) {

                 $udata['usr_group'] = 8;
                 foreach ($arrayCol as $key1 => $value1) {
                      $udata[$key1] = isset($value[$key1]) ? $value[$key1] : '';
                 }

                 debug($udata, 0);
                 $this->emp_details->register($udata);
            }
       }

       function bulkCrop() {
            $imagePath = './assets/uploads/category';
            $per = 80;
            $quality = 80;
            $images = scandir($imagePath);
            unset($images['0']);
            unset($images['1']);
            $images = array_values($images);
            foreach ($images as $key => $value) {
                 echo resize_thumb_proportion($imagePath . '/' . $value, $per, $quality) . '<br>';
            }
       }

       function mergePrdKeyword() {
            $key = $this->db->query('SELECT `pkwd_product`, GROUP_CONCAT(`pkwd_val_en`) AS keyword FROM `cpnl_products_keyword` group by(`pkwd_product`)')->result_array();
            foreach ($key as $key => $value) {
                 $this->db->where('prd_id', $value['pkwd_product']);
                 $this->db->update(tbl_products_master, array('prd_keywords' => $value['keyword']));
            }
       }

       function mmm() {
            $f = $this->db->get_where(tbl_products_stock_master)->result_array();
            foreach ($f as $key => $value) {
                 $supplir = $this->db->get_where(tbl_supplier_master, array('supm_id' => $value['psm_supplier']))->row_array();
                 $this->db->where('psm_id', $value['psm_id']);
                 $this->db->update(tbl_products_stock_master, array('psm_market_place' => $supplir['supm_market']));
            }
       }

       function blanktownadmin() {
            $this->lock_in();

            $this->load->library("pagination");
            $limit = 20;
            $page = !isset($_GET['page']) ? 0 : $_GET['page'];
            $linkParts = explode('&page=', current_url() . '?' . $_SERVER['QUERY_STRING']);
            $link = $linkParts[0];
            $config = getPaginationDesign();

            $data = $_GET;
            $business = $this->common_model->suppliers('', $limit, $page, $_GET);
            $data['suppliers'] = $business['data'];

            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config["base_url"] = $link;
            $config["total_rows"] = $business['count'];
            $config["per_page"] = $limit;
            $config["uri_segment"] = 3;

            /* Table info */
            $data['pageIndex'] = $page + 1;
            $data['limit'] = $page + $limit;
            $data['totalRow'] = number_format($business['count']);
            /* Table info */

            $this->pagination->initialize($config);
            $data["links"] = $this->pagination->create_links();
            $this->render_page('list', $data);
       }

       function migrate() {
            echo $this->db->get_where(tbl_supplier_master, array('supm_added_by' => 0))->num_rows() . '<br>';

            $r = $this->db->select(tbl_supplier_master . '.supm_id,' . tbl_supplier_master . '.supm_b_userid,' . tbl_users . '.usr_id')
                            ->join(tbl_users, tbl_users . '.user_id_tmp = ' . tbl_supplier_master . '.supm_b_userid', 'RIGHT')
                            ->where(array(tbl_supplier_master . '.supm_added_by' => 0))->get(tbl_supplier_master)->result_array();

            foreach ($r as $key => $value) {
                 $ff = $this->db->get_where(tbl_users_groups, array('user_id' => $value['usr_id']))->row_array();
                 if (!empty($ff)) {
                      $this->db->where('supm_id', $value['supm_id']);
                      $this->db->update(tbl_supplier_master, array('supm_added_by' => $ff['user_id']));
                 }
            }
       }

       function setsupplier() {
            $f = $this->db->get_where(tbl_products_order_details, array('ordd_town_admin' => 0))->result_array();
            foreach ($f as $key => $value) {
                 if ($value['ordd_supplier'] > 0) {
                      $addedBy = $this->db->select('supm_added_by')->where(array('supm_id' => $value['ordd_supplier']))->get(tbl_supplier_master)->row()->supm_added_by;
                      $this->db->where('ordd_id', $value['ordd_id']);
                      $this->db->update(tbl_products_order_details, array('ordd_town_admin' => $addedBy));
                 }
            }
       }

       function shopcharge() {

            $f = $this->db->get(tbl_products_order_master)->result_array();
            foreach ($f as $key => $value) {
                 $this->db->join(tbl_supplier_master, tbl_supplier_master . '.supm_id = ' . tbl_products_order_details . '.ordd_supplier', 'LEFT');
                 $de = $this->db->get_where(tbl_products_order_details, array('ordd_order_master_id' => $value['ordm_id']))->result_array();

                 $this->db->where('ordm_id', $value['ordm_id']);
                 $this->db->update(tbl_products_order_master, array('ordm_total_shop_charge' => array_sum(array_column($de, 'supm_shop_charges'))));
            }
       }

       function delcharg() {
            $f = $this->db->get(tbl_products_order_master)->result_array();
            foreach ($f as $key => $value) {
                 $this->db->where('ordm_id', $value['ordm_id']);
                 $newDel = $value['ordm_delivery_chrg'] - $value['ordm_total_shop_charge'];
                 $this->db->update(tbl_products_order_master, array('ordm_delivery_chrg' => $newDel));
            }
       }

       function settown() {
            $f = $this->db->select('ordd_order_master_id, ordd_town')->get(tbl_products_order_details)->result_array();
            foreach ($f as $key => $value) {
                 $this->db->where('ordm_id', $value['ordd_order_master_id']);
                 $this->db->update(tbl_products_order_master, array('ordm_buyer_selected_town' => $value['ordd_town']));
            }
       }

       function prodTownAssoc() {
            $stockMaster = $this->db->select('psm_id, psm_product, psm_market_place, psm_supplier')->get(tbl_products_stock_master)->result_array();
            foreach ($stockMaster as $key => $value) {
                 if (!empty($value)) {
                      $market = $this->db->select('mar_name, mar_lat, mar_long')->get_where(tbl_market_places, array('mar_id' => $value['psm_market_place']))->row_array();
                      
                      $marketName = isset($market['mar_name']) ? $market['mar_name'] : '';
                      $lat = isset($market['mar_lat']) ? $market['mar_lat'] : '';
                      $lng = isset($market['mar_long']) ? $market['mar_long'] : '';
                      
                      $this->db->insert(tbl_products_town_assoc, array(
                          'pta_stock_master' => $value['psm_id'],
                          'pta_supplier' => $value['psm_supplier'],
                          'pta_prod_master' => $value['psm_product'],
                          'pta_town' => $value['psm_market_place'],
                          'pta_town_name' => $marketName,
                          'pta_lat' => $lat,
                          'pta_lng' => $lng
                      ));
                 }
            }
       }
  } 