
<?php
class CsrfCheck
{

    public function initialize()
    {
        $req_from = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : '';
        if(stripos($_SERVER['REQUEST_URI'], '/newsletterSub') !== false || stripos($_SERVER['REQUEST_URI'], '/api') !== false || stripos($_SERVER['REQUEST_URI'], '/supplier_api') || $req_from == 'XMLHttpRequest') {
            $config = &load_class('Config', 'core');
            $config->set_item('csrf_protection', FALSE);
        }
    }
}
