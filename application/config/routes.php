<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');
  /*
    | -------------------------------------------------------------------------
    | URI ROUTING
    | -------------------------------------------------------------------------
    | This file lets you re-map URI requests to specific controller functions.
    |
    | Typically there is a one-to-one relationship between a URL string
    | and its corresponding controller class/method. The segments in a
    | URL normally follow this pattern:
    |
    |	example.com/class/method/id/
    |
    | In some instances, however, you may want to remap this relationship
    | so that a different class/function is called than the one
    | corresponding to the URL.
    |
    | Please see the user guide for complete details:
    |
    |	http://codeigniter.com/user_guide/general/routing.html
    |
    | -------------------------------------------------------------------------
    | RESERVED ROUTES
    | -------------------------------------------------------------------------
    |
    | There area two reserved routes:
    |
    |	$route['default_controller'] = 'welcome';
    |
    | This route indicates which controller class should be loaded if the
    | URI contains no data. In the above example, the "welcome" class
    | would be loaded.
    |
    |	$route['404_override'] = 'errors/page_missing';
    |
    | This route will tell the Router what URI segments to use if those provided
    | in the URL cannot be matched to a valid route.
    |
   */

  $route['default_controller'] = "f_cpanel";
  $route['404_override'] = 'error404';

  $route['user/forgot-password'] = 'user/forgot_password';
  $route['user/forgot-password/(:any)'] = 'user/forgot_password/$1';
  $route['user/reset-password'] = 'user/reset_password';
  $route['user/reset-password/(:any)'] = 'user/reset_password/$1';
  $route['user/change-password'] = 'user/change_password';
  $route['user/personal-information/(:any)'] = 'user/personal_information/$1';

  $route['product/contact-supplier'] = 'product/contact_supplier';
  $route['product/contact-supplier/(:any)'] = 'product/contact_supplier/$1';
  $route['product/product-details'] = 'product/product_details';
  $route['product/product-details/(:any)'] = 'product/product_details/$1';
  $route['product/product-listing'] = 'product/product_listing';
  $route['product/product-listing/(:any)'] = 'product/product_listing/$1';
  $route['product/rfq-list'] = 'product/rfq_list';
  $route['product/rfq-list/(:any)'] = 'product/rfq_list/$1';
  $route['product/new-rfq'] = 'product/new_rfq';

  $route['stuffs/search/(:any)'] = 'product/search/$1';

  $route['supplier/supplier-list'] = 'supplier/supplier_list';
  $route['business/activate-supplier'] = 'business/activate_supplier';
  $route['business/activate-supplier/(:any)'] = 'business/activate_supplier/$1';
  $route['supplier/supplier-product/(:any)'] = 'supplier/supplier_product/$1';
  $route['supplier/supplier-register'] = 'supplier/supplier_register';
  $route['supplier/360-market'] = 'supplier/get_360_list';

  $route['user/become-seller/(:any)'] = 'user/become_seller/$1';

  $route['favorites/supplier-list'] = 'favorites/supplier_list';
  $route['favorites/product-list'] = 'favorites/product_list';

  $route['privacy-policies'] = 'terms_policies/index';
  $route['product-listing-policies'] = 'terms_policies/listing_policy';
  $route['terms-of-use'] = 'terms_policies/terms_of_use';
  $route['intellectual-property-rights'] = 'terms_policies/intellectual_property';
  $route['complaint-terms'] = 'terms_policies/complaint_terms';
  $route['service-agreement'] = 'terms_policies/service_agreement';
  $route['rfq-policy'] = 'terms_policies/rfq_policy';
  $route['advertising-policy'] = 'terms_policies/advertising_policy';
  $route['faq'] = 'terms_policies/faq';
  $route['contact-us'] = 'terms_policies/contact_us';
  $route['complaints'] = 'terms_policies/complaints';
  $route['submit-complaint'] = 'terms_policies/complaint_form';
  $route['about-us'] = 'terms_policies/about_us';
  $route['terms-conditions'] = 'terms_policies/terms_conditions';
  $route['registration-process'] = 'terms_policies/registration_process';
  $route['quality-control-terms'] = 'terms_policies/quality_control';
  $route['sitemap'] = 'terms_policies/sitemap';

  $route['market/market-register'] = 'market/market_register';

  $route['emp-details/update-profile/(:any)'] = 'emp_details/updateProfile/$1';

  $route['feed/add-feed'] = 'feed/add_feed';

  $route['logistics/request-quotes'] = 'logistics/request_quotes';
  $route['logistics/get-quote/(:any)'] = 'logistics/getQuote/$1';

  $route['quality-control/register'] = 'quality_control/register';
  $route['quality-control'] = 'quality_control/index';
  $route['quality-control/get-quote/(:any)'] = 'quality_control/getQuote/$1';
  $route['complaint/complaint_details/(:any)'] = 'complaint/complaint_details/$1';
  /* End of file routes.php */
/* Location: ./application/config/routes.php */