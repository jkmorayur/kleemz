<?php

  $config['modules_exclude'] = array(
      'f_cpanel' => array(
          'index' => 'index',
          'login' => 'login'
      ),
      'user_permission' => array(
          'access_denied' => 'Access denied'
      ),
      'emp_details' => array(
          'updateprofile' => 'updateProfile',
          'migrate' => 'migrate'
      ),
      'dashboard' => array(
          'index' => 'Home'
      ),
      'general' => array(
          'showme' => 'showme',
          'getnotifications' => 'Get Notifications',
          'newslettersub' => 'newsletterSub',
          'douploadfromeditor' => 'doUploadFromEditor',
          'cmnchangestatus' => 'cmnChangeStatus'
      ),
      'clean' => array(
          'index' => 'index',
          'cleanenquires' => 'cleanEnquires'
      ),
      'building' => array(
          'getbuildingcategories' => 'getBuildingCategories',
          'setdefaultimage' => 'setDefaultImage',
          'removeimage' => 'removeImage'
      ),
      'category' => array(
          'removeimage' => 'removeImage',
          'removebanner' => 'removeBanner',
          'checkcategoryexists' => 'checkCategoryExists',
          'getsubcategories' => 'getSubCategories'
      ),
      'market' => array(
          'removeimage' => 'removeImage',
          'getcaregorybymarket' => 'getCaregoryByMarket',
          'markets' => 'markets',
          'market_register' => 'market_register'
      ),
      'business' => array(
          'removesupplierbanner' => 'Delete business banner',
          'setdefaultbannerimage' => 'Set Default Banner',
          'removepanorama' => 'Remove Panorama Image',
          'report' => 'Export Supplier Report',
          'setdefaultimage' => 'setDefaultImage',
          'removeavatar' => 'removeAvatar',
          'removeimage' => 'removeImage',
          'checkifvalueexists' => 'checkIfValueExists',
          'supplier_list' => 'supplier_list',
          'activate_supplier' => 'Activate supplier',
          'home' => 'home',
          'contact' => 'contact',
          'profile' => 'profile',
          'supplier_product' => 'products',
          'addtofavorate' => 'addToFavorate',
          'get_360' => 'get_360',
          'enquiry' => 'enquiry',
          'supplier_register' => 'supplier register',
          'feeds' => 'feeds',
          'getsuppliercategories' => 'getsuppliercategories',
          'get_360_list' => 'get_360_list',
          'myprofile' => 'myprofile',
          'businessautocomplete' => 'businessAutocomplete',
          'bidcategories' => 'bidCategories',
          'verifysupplierrejection' => 'verifySupplierRejection',
          'myproducts' => 'myproducts',
          'update_shop_charge' => 'Update shop charge'
      ),
      'supplier_privilege' => array(
          'getpermission' => 'Get permission'
      ),
      'user_permission' => array(
          'setpermission' => 'Set permission',
          'getpermission' => 'Get permission',
      ),
      'home' => array(
          'index' => 'index',
          'search' => 'search',
          'get_app' => 'get_app',
          'productsearch' => 'productsearch',
      ),
      'commodity' => array(
          'index' => 'index'
      ),
      'states' => array(
          'getstatesbycountry' => 'getStatesByCountry'
      ),
      'user' => array(
          'index' => 'index',
          'insert' => 'insert',
          'login' => 'Login',
          'signup' => 'Signup',
          'logout' => 'Logout',
          'checkemailexists' => 'checkEmailExists',
          'signupfirststep' => 'signupFirstStep',
          'activate' => 'activate',
          'update' => 'insert',
          'logout' => 'Logout',
          'inserttestvalues' => 'insertTestValues',
          'forgot_password' => 'Forgot password',
          'reset_password' => 'reset password',
          'refreshcaptcha' => 'Refresh Captcha',
          'changeuserpermissions' => 'changeUserPermissions',
          'chat' => 'chat',
          'loadchat' => 'loadChat',
          'pusher' => 'pusher',
          'become_seller' => 'become_seller',
          'chat_head' => 'chat_head',
          'seller_requests' => 'seller_requests',
          'change_password' => 'change password',
          'verify_mail' => 'verify_mail',
          'change_mail' => 'change_mail',
          'personal_information' => 'Personal info',
          'quick_login' => 'quick_login',
          'reset_password_modal' => 'reset_password_modal',
          'sup_home' => 'sup_home'
      ),
      'product' => array(
          'product_listing' => 'product_listing',
          'product_details' => 'product_details',
          'productautospecification' => 'productAutoSpecification',
          'contact_supplier' => 'contact_supplier',
          'supplier_products' => 'supplier_products',
          'docontactsupplier' => 'doContactSupplier',
          'productautoname' => 'productAutoName',
          'getsuppliernotification' => 'get all notification',
          'setdefaultimage' => 'setDefaultImage',
          'removeimage' => 'removeImage',
          'sendrfqquote' => 'sendRfqQuote',
          'search' => 'search',
          'editorimageupload' => 'editorImageUpload',
          'category' => 'category',
          'sendrfqcomments' => 'sendRFQComments',
          'loadrfqcomments' => 'loadRFQComments',
          'loadseperaterfqcomment' => 'loadseperaterfqcomment',
          'stock' => 'stock',
          'rfq' => 'rfq',
          'activeproducts' => 'activeProducts',
          'active' => 'active',
          'getsuppliers' => 'getSuppliers',
          'getbasicproductimage' => 'getBasicProductImage',
          'deletesinglestock' => 'deleteSingleStock'
      ),
      'mailbox' => array(
          'index' => 'index',
          'read' => 'read',
          'outbox' => 'outbox',
          'inbox' => 'inbox',
          'deletemail' => 'deleteMail',
          'unread' => 'unread',
          'trash' => 'trash',
          'sent' => 'sent',
          'bindrecipient' => 'Bind recipient',
          'filterrecipient' => 'filterRecipient',
          'reply' => 'reply'
      ),
      'add' => array(
          'removeimage' => 'removeImage'
      ),
      'favorites' => array(
          'addproducttofavoritelist' => 'addproducttofavoritelist',
          'addsuppliertofavoritelist' => 'addSupplierToFavoriteList',
          'supplier_list' => 'supplier_list',
          'product_list' => 'product_list',
          'removefaveprod' => 'removefaveprod',
          'removefavesupp' => 'removefavesupp',
          'addtofollow' => 'addToFollow',
          'removefollow' => 'addToFollow',
          'following' => 'following',
          'followers' => 'followers'
      ),
      'terms_policies' => array(
          'index' => 'index',
          'listing_policy' => 'listing_policy',
          'terms_of_use' => 'terms_of_use',
          'intellectual_property' => 'intellectual_property',
          'complaint_terms' => 'complaint_terms',
          'service_agreement' => 'service_agreement',
          'rfq_policy' => 'rfq_policy',
          'advertising_policy' => 'advertising_policy',
          'faq' => 'faq',
          'contact_us' => 'contact_us',
          'complaints' => 'complaints',
          'complaint_form' => 'complaint_form',
          'about_us' => 'about_us',
          'terms_conditions' => 'terms_conditions',
          'quality_control' => 'quality-control',
          'registration_process' => 'registration-process',
          'sitemap' => 'sitemap'
      ),
      'order' => array(
          'sendcomments' => 'sendComments',
          'loadinquirycomments' => 'loadInquiryComments',
          'setdeliveryboy' => 'setDeliveryBoy',
          'getlivetrack' => 'getLiveTrack'
      ),
      'logistics' => array(
          'index' => 'default',
          'register' => 'register',
          'removeimage' => 'removeImage',
          'setdefaultimage' => 'setDefaultImage',
          'getquote' => 'getQuote',
          'newlogisticsquote' => 'newLogisticsQuote',
          'request_quotes_details' => 'request_quotes_details'
      ),
      'quality_control' => array(
          'index' => 'default',
          'register' => 'register',
          'removeimage' => 'removeImage',
          'setdefaultimage' => 'setDefaultImage',
          'getquote' => 'getQuote',
          'newqaquote' => 'newLogisticsQuote',
          'request_quotes_details' => 'request_quotes_details'
      ),
      'complaint' => array(
          'index' => 'index',
          'update' => 'update',
          'updatestatus' => 'updatestatus',
          'complaint_details' => 'complaint_details',
          'infringement_complaints' => 'infringement_complaints',
      ),
      'property1_complaints' => array(
          'index' => 'index',
          'update' => 'update',
          'updatestatus' => 'updatestatus',
          'complaint_details' => 'complaint_details'
      ),
      'complaint_register' => array(
          'index' => 'index',
          'register' => 'register',
          'registration' => 'registration',
          'register_complaint' => 'register_complaint',
          'complaints_property_review' => 'complaints_property_review',
          'register_property_infringement' => 'register_property_infringement',
          'productsearch' => 'productsearch',
      ),
      'contact' => array(
          'enquiry' => 'enquiry'
      ),
      'district' => array(
          'getdistrictbystate' => 'getDistrictByState'
      ),
      'product_category' => array(
          'checkifvalueexists' => 'checkIfValueExists',
          'changestatus' => 'changestatus'
      ),
      'deliveryboy' => array(
          'changeuserstatus' => 'Changeuser status',
      )
  );

  $config['modules'] = array(
      'deliveryboy' => array(
          'index' => 'Delivery boy list',
          'add' => 'Add new delivery boy',
          'view' => 'View a delivery boy',
          'update' => 'Update delivery boy',
          'delete' => 'Delete delivery boy'
      ),
      'category' => array(
          'index' => 'Business category list',
          'add' => 'Add new business category',
          'view' => 'View a business category',
          'update' => 'Update business category',
          'delete' => 'Delete business category'
      ),
      'states' => array(
          'index' => 'State List',
          'add' => 'Add new state',
          'view' => 'View a state',
          'update' => 'Update state',
          'delete' => 'Delete state'
      ),
      'country' => array(
          'index' => 'List country',
          'add' => 'Add new country',
          'view' => 'View a country',
          'update' => 'Update country',
          'delete' => 'Delete country'
      ),
      'market' => array(
          'index' => 'List market places/town',
          'add' => 'Add new market places/town',
          'view' => 'View a market places/town',
          'update' => 'Update market places/town',
          'delete' => 'Delete market places/town'
      ),
      'business' => array(
          'index' => 'List all business',
          'add' => 'Add new business',
          'view' => 'View a business',
          'update' => 'Update business',
          'delete' => 'Delete business',
//          'changestatus' => 'Change business status',
          'pendingapproval' => 'Pending approval',
          'verifysupplier' => 'Verify business',
          'rejectsupplier' => 'Reject business'
      ),
      'townadmin' => array(
          'index' => 'List town admins',
          'add' => 'Add new town admin',
          'view' => 'View a town admin',
          'update' => 'Update town admin',
          'delete' => 'Delete town admin'
      ),
      'product' => array(
          'index' => 'List products',
          'add' => 'Create product',
          'view' => 'View product details',
          'update' => 'Update product details',
          'delete' => 'Delete product details',
          'changestatus' => 'Change product status active/de-active',
          'stock_list' => 'List your stocks',
          'add_stock' => 'Create new stock',
          'view_stock' => 'View stock details',
          'changestockstatus' => 'Change stock status',
          'deletestock' => 'Delete stock',
          'updatestock' => 'Update stock details'
      ),
      'units' => array(
          'index' => 'List all units',
          'add' => 'Add new unit',
          'view' => 'View a unit',
          'update' => 'Update unit',
          'delete' => 'Delete unit'
      ),
      'order' => array(
          'index' => 'List my order',
          'order_summery' => 'View order summery',
          'changeorderstatuse' => 'Change order status',
          'report' => 'Export order report',
          'canassigndeliveryboy' => 'Assign order to delivery boy',
          'track_dboy' => 'Track delivery boy',
          'canviewbuyerdetails' => 'Can view buyer details',
      ),
      'reports' => array(
          'srr' => 'List SRR reports',
          'generatesrr' => 'Generate SRR',
          'submitsrr' => 'Submit SRR',
          'srr_summery' => 'View SRR summary',
          'exportsrr' => 'Export SRR'
      ),
      'supplier_privilege' => array(
          'setpermission' => 'Set permission'
      )
  ); 