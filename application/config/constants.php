<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  /*
    |--------------------------------------------------------------------------
    | File and Directory Modes
    |--------------------------------------------------------------------------
    |
    | These prefs are used when checking and setting modes when working
    | with the file system.  The defaults are fine on servers with proper
    | security, but you may wish (or even need) to change the values in
    | certain environments (Apache running a separate process for each
    | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
    | always be used to set the mode correctly.
    |
   */
  define('FILE_READ_MODE', 0644);
  define('FILE_WRITE_MODE', 0666);
  define('DIR_READ_MODE', 0755);
  define('DIR_WRITE_MODE', 0777);

  /*
    |--------------------------------------------------------------------------
    | File Stream Modes
    |--------------------------------------------------------------------------
    |
    | These modes are used when working with fopen()/popen()
    |
   */

  define('FOPEN_READ', 'rb');
  define('FOPEN_READ_WRITE', 'r+b');
  define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
  define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
  define('FOPEN_WRITE_CREATE', 'ab');
  define('FOPEN_READ_WRITE_CREATE', 'a+b');
  define('FOPEN_WRITE_CREATE_STRICT', 'xb');
  define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

  define('TABLE_PREFIX', 'cpnl_');
  define('TABLE_PREFIX_RANA', 'rana_');
  define('UPLOAD_PATH', './assets/uploads/');

  define('DEFAULT_THUMB_H', 44);
  define('DEFAULT_THUMB_W', 50);

  define('FILE_UPLOAD_PATH', './assets/uploads/');

  define('PUSHER_APP_KEY', '853be53b86c43083fe5c');
  define('PUSHER_APP_SECRET', '9e8eb557eb1c089a631e');
  define('PUSHER_APP_ID', '609938');
  define('PUSHER_CLUSTER', 'ap2');
  define('PUSHY_SECRET_API_KEY', '4550461087bd86217c72d3efc9fae2ef2bccfb05e998a4d6d7ef67382e882c29');
  define('PUSHY_SUP_SECRET_API_KEY', 'fc855639ac3887fa414bc0b64a33365583f083c240579d9d88cc3f650bdb90d4');

  define('FOLLOW_UP_STATUS', serialize(array('1' => 'Hot', '2' => 'Warm', '3' => 'Cold')));
  define('ENQUIRY_UP_STATUS', serialize(array('1' => 'Hot', '2' => 'Warm', '3' => 'Cold')));
  define('VEHICLE_DETAILS_STATUS', serialize(array('1' => 'Sale', '2' => 'Buy', '3' => 'Exchange')));
  define('FUAL', serialize(array('2' => 'Diesel', '1' => 'Petrol', '3' => 'Gas', '4' => 'Hybrid', '5' => 'Electric', '6' => 'CNG')));
  define('CUST_AGE_GROUP', serialize(array('20-30' => '20-30', '30-40' => '30-40', '40-50' => '40-50')));
  define('MODE_OF_CONTACT', serialize(array(
      '1' => 'Phone',
      '2' => 'Whatsup',
      '3' => 'Mail',
      '4' => 'Facebook',
      '5' => 'Events',
      '6' => 'Referal',
      '7' => 'OLX',
      '8' => 'Fasttrack',
      '9' => 'Showroom',
      '10' => 'C/O MD',
      '11' => 'C/O VP',
      '12' => 'C/O CEO',
      '13' => 'C/O Others',
      '14' => 'Car Trade',
      '15' => 'Just Dial',
      '16' => 'Car Wale',
      '17' => 'Field')));
  define('MODE_OF_CONTACT_FOLLOW_UP', serialize(array('1' => 'Telephone', '2' => 'Direct meet', '3' => 'Showroom visit')));
  define('INSURANCE_TYPES', serialize(array('1' => 'RTI', '2' => 'Platinum/Gold/Silver', '3' => 'B2B', '4' => 'First Class', '5' => 'Second Class')));
  define('EVALUATION_TYPES', serialize(array('1' => 'Our own', '2' => 'Park and sale', '3' => 'Park and sale with customer')));

  /* Definition for today */
  define('TODAY', 'DATE(CURDATE())');
  define('YESTERDAY', 'DATE(DATE_ADD(CURDATE(), INTERVAL -1 DAY))');

  define('STATIC_TITLE', 'kleemz');


  define('MAIL_HOST', 'mail.vestletech.com');
  define('MAIL_USERNAME', 'kleemz@vestletech.com');
  define('MAIL_PASSWORD', 'kleemz#@1');
  define('FROM_MAIL', 'kleemz@vestletech.com');
  define('FROM_NAME', 'Kleemz');
  define('REPLY_TO_MAIL', 'kleemz@vestletech.com');
  define('REPLY_TO_NAME', 'Kleemz');
  
  define('OTP_VALIDITY', 5);//in minuts
  define('DEF_MILE', 5);//Default miles
  
  
  //Order statuses
  define('NEW_ORDER', 0);
  define('ORDER_CONFIRMED', 1);
  define('ORDER_PROCESSING', 2);
  define('ORDER_DELIVERED', 3);
  define('ORDER_CANCELLED', 4);
  define('ORDER_DISPATCHED', 5);
  define('ORDER_PICKED', 6);
  define('ASSIGN_TO_DB', 7);
  
  //Shop charge slots
  define('SHOP_CHRG_SLAB_1', 5);
  define('SHOP_CHRG_SLAB_2', 7);
  define('SHOP_CHRG_SLAB_3', 2);//Old value 10
  
  /* TABLES */
  define('tbl_advertisement', TABLE_PREFIX . 'advertisement');
  define('tbl_app_sessions', TABLE_PREFIX . 'app_sessions');
  define('tbl_banner', TABLE_PREFIX . 'banner');
  define('tbl_buildings_images', TABLE_PREFIX . 'buildings_images');
  define('tbl_buildings_market_categoey_assoc', TABLE_PREFIX . 'buildings_market_categoey_assoc');
  define('tbl_buildins', TABLE_PREFIX . 'buildins');
  define('tbl_category', TABLE_PREFIX . 'category');
  define('tbl_chats', TABLE_PREFIX . 'chats');
  define('tbl_complaint_types', TABLE_PREFIX . 'complaint_types');
  define('tbl_complaints', TABLE_PREFIX . 'complaints');
  define('tbl_contacts', TABLE_PREFIX . 'contacts');
  define('tbl_countries', TABLE_PREFIX . 'countries');
  define('tbl_enquiry', TABLE_PREFIX . 'enquiry');
  define('tbl_favorite_list', TABLE_PREFIX . 'favorite_list');
  define('tbl_feeds', TABLE_PREFIX . 'feeds');
  define('tbl_general_log', TABLE_PREFIX . 'general_log');
  define('tbl_groups', TABLE_PREFIX . 'groups');
  define('tbl_language', TABLE_PREFIX . 'language');
  define('tbl_login_attempts', TABLE_PREFIX . 'login_attempts');
  define('tbl_logistics_comp_images', TABLE_PREFIX . 'logistics_comp_images');
  define('tbl_logistics_comp_specification', TABLE_PREFIX . 'logistics_comp_specification');
  define('tbl_logistics_quotes', TABLE_PREFIX . 'logistics_quotes');
  define('tbl_mailboox_attachments', TABLE_PREFIX . 'mailboox_attachments');
  define('tbl_mailboox_master', TABLE_PREFIX . 'mailboox_master');
  define('tbl_market_cate_assoc', TABLE_PREFIX . 'market_cate_assoc');
  define('tbl_market_places', TABLE_PREFIX . 'market_places');
  define('tbl_newsletter_subscribe', TABLE_PREFIX . 'newsletter_subscribe');
  define('tbl_notification_settings', TABLE_PREFIX . 'notification_settings');
  define('tbl_products_master', TABLE_PREFIX . 'products_master');
  define('tbl_products_category', TABLE_PREFIX . 'products_category');
  define('tbl_products_category_assoc', TABLE_PREFIX . 'products_category_assoc');
  define('tbl_products_images', TABLE_PREFIX . 'products_images');
  define('tbl_products_keyword', TABLE_PREFIX . 'products_keyword');
  define('tbl_products_order_comments', TABLE_PREFIX . 'products_order_comments');
  define('tbl_products_order_master', TABLE_PREFIX . 'products_order_master');
  define('tbl_products_order_details', TABLE_PREFIX . 'products_order_details');
  define('tbl_products_order_status_assoc', TABLE_PREFIX . 'products_order_status_assoc');
  define('tbl_products_order_statuses', TABLE_PREFIX . 'products_order_statuses');
  define('tbl_products_specification', TABLE_PREFIX . 'products_specification');
  define('tbl_products_units', TABLE_PREFIX . 'products_units');
  define('tbl_property_infringement', TABLE_PREFIX . 'property_infringement');
  define('tbl_pushnotification_master', TABLE_PREFIX . 'pushnotification_master');
  define('tbl_recommendation', TABLE_PREFIX . 'recommendation');
  define('tbl_rfq', TABLE_PREFIX . 'rfq');
  define('tbl_rfq_notification', TABLE_PREFIX . 'rfq_notification');
  define('tbl_rfq_products_send', TABLE_PREFIX . 'rfq_products_send');
  define('tbl_seller_requests', TABLE_PREFIX . 'seller_requests');
  define('tbl_settings', TABLE_PREFIX . 'settings');
  define('tbl_states', TABLE_PREFIX . 'states');
  define('tbl_supplier_banner_images', TABLE_PREFIX . 'supplier_banner_images');
  define('tbl_supplier_buildings_cate_assoc', TABLE_PREFIX . 'supplier_buildings_cate_assoc');
  define('tbl_supplier_followers', TABLE_PREFIX . 'supplier_followers');
  define('tbl_supplier_grade', TABLE_PREFIX . 'supplier_grade');
  define('tbl_supplier_keywords', TABLE_PREFIX . 'supplier_keywords');
  define('tbl_supplier_master', TABLE_PREFIX . 'supplier_master');
  define('tbl_supplier_shop_images', TABLE_PREFIX . 'supplier_shop_images');
  define('tbl_user_access', TABLE_PREFIX . 'user_access');
  define('tbl_users', TABLE_PREFIX . 'users');
  define('tbl_users_groups', TABLE_PREFIX . 'users_groups');
  define('tbl_users_phone_number', TABLE_PREFIX . 'users_phone_number');
  define('tbl_district', TABLE_PREFIX . 'district');
  define('tbl_business_types', TABLE_PREFIX . 'business_types');
  define('tbl_supplier_contacts', TABLE_PREFIX . 'supplier_contacts');
  define('tbl_supplier_categories', TABLE_PREFIX . 'supplier_categories');
  define('tbl_townadmin_town_assoc', TABLE_PREFIX . 'townadmin_town_assoc');
  define('tbl_supplier_status_log', TABLE_PREFIX . 'supplier_status_log');
  define('tbl_products_schedule', TABLE_PREFIX . 'products_schedule');
  define('tbl_products_stock_master', TABLE_PREFIX . 'products_stock_master');
  define('tbl_products_stock_images', TABLE_PREFIX . 'products_stock_images');
  define('tbl_products_stock_details', TABLE_PREFIX . 'products_stock_details');
  define('tbl_cart_master', TABLE_PREFIX . 'cart_master');
  define('tbl_app_home_category', TABLE_PREFIX . 'app_home_category');
  define('tbl_products_order_dboy_tracking', TABLE_PREFIX . 'products_order_dboy_tracking');
  define('tbl_address_book', TABLE_PREFIX . 'address_book');
  define('tbl_report_srr_details', TABLE_PREFIX . 'report_srr_details');
  define('tbl_report_srr_master', TABLE_PREFIX . 'report_srr_master');
  define('tbl_products_town_assoc', TABLE_PREFIX . 'products_town_assoc');
  define('tbl_products_purchase_invoice', TABLE_PREFIX . 'products_purchase_invoice');
  define('tbl_products_purchase_invoice_images', TABLE_PREFIX . 'products_purchase_invoice_images');
  define('tbl_products_order_pickup', TABLE_PREFIX . 'products_order_pickup');
  define('tbl_virtshop_catog_town_assoc', TABLE_PREFIX . 'virtshop_catog_town_assoc');
  